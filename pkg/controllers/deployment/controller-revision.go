/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package deployment

import (
	"encoding/json"
	"fmt"
	history "kuku-controller/pkg/controllers/history"
	"kuku-controller/pkg/utils"
	"time"

	v3dutils "kuku-controller/pkg/utils/kumori/v3deployments"

	context "kuku-controller/pkg/controllers/deployment/context"

	log "github.com/sirupsen/logrus"
	kumoriv1 "gitlab.com/kumori-systems/community/libraries/client-go/pkg/apis/kumori/v1"
	appsv1 "k8s.io/api/apps/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/client-go/kubernetes/scheme"
)

// updateConflictError is the error used to indicate that the maximum number of retries against the API
// server have been attempted and we need to back off
var patchCodec = scheme.Codecs.LegacyCodec(kumoriv1.SchemeGroupVersion)

// HistoryItem represents the instant a given revision was set as the current revision
type HistoryItem struct {
	Timestamp int64  `json:"timestamp"`
	Comment   string `json:"comment"`
	Revision  int64  `json:"revision"`
}

// UpdateRevisions updates a V3Deployment revision history by cheking if this V3Deployment
// manifests represents a new version of this V3Deployment or restores a previous version
func (c *Controller) UpdateRevisions(kcontext *context.Context) error {
	meth := c.Name + ".UpdateRevisions()"
	log.Debugf("%s. Deployment: '%s'", meth, kcontext.Deployment.GetName())

	// if err := c.adoptOrphanRevisions(kukuContext.Deployment); err != nil {
	// 	return err
	// }

	// list all revisions and sort them
	revisions, err := c.ListRevisions(kcontext.Deployment)
	if err != nil {
		return err
	}
	history.SortControllerRevisions(revisions)

	// get the current, and update revisions
	currentRevision, collisionCount, err := c.getRevisions(kcontext.Deployment, revisions)
	if err != nil {
		return err
	}

	// Creates the V3Deployment status
	status := kumoriv1.V3DeploymentStatus{
		ObservedGeneration: kcontext.Deployment.Generation,
		CurrentRevision:    currentRevision.Name,
		CollisionCount:     new(int32),
	}
	*status.CollisionCount = collisionCount
	kcontext.Deployment.Status = status
	deployment, err := v3dutils.UpdateStatus(c.kumoriClientset, kcontext.Deployment)
	if err != nil {
		log.Errorf("%s. Error updating deployment '%s' status. Error: %s", meth, kcontext.Deployment.GetName(), err.Error())
		return err
	}

	if err = c.TruncateHistory(deployment, revisions, currentRevision); err != nil {
		log.Errorf("%s. Error truncating controller revision history for deployment '%s' status. Error: %s", meth, kcontext.Deployment.GetName(), err.Error())
		return err
	}

	return nil
}

// ListRevisions returns a Deployment's revisions list.
func (c *Controller) ListRevisions(deployment *kumoriv1.V3Deployment) ([]*appsv1.ControllerRevision, error) {
	meth := c.Name + ".ListRevisions()"
	log.Debugf("%s. Deployment: '%s'", meth, deployment.GetName())
	// selector, err := metav1.LabelSelectorAsSelector(&metav1.LabelSelector{
	// 	MatchLabels: deployment.GetObjectMeta().GetLabels(),
	// })
	deploymentLabels := deployment.GetObjectMeta().GetLabels()
	selector, err := metav1.LabelSelectorAsSelector(&metav1.LabelSelector{
		MatchLabels: map[string]string{
			utils.KumoriDomainLabel: deploymentLabels[utils.KumoriDomainLabel],
			utils.KumoriOwnerLabel:  deploymentLabels[utils.KumoriOwnerLabel],
			utils.KumoriNameLabel:   deploymentLabels[utils.KumoriNameLabel],
		},
	})
	if err != nil {
		return nil, err
	}
	return c.controllerHistory.ListControllerRevisions(deployment, selector)
}

// adoptOrphanRevisions adopts any orphaned ControllerRevisions matched by deployment's Selector.
func (c *Controller) adoptOrphanRevisions(deployment *kumoriv1.V3Deployment) error {
	meth := c.Name + ".adpotOrphanRevisions()"
	log.Debugf("%s. Deployment: '%s'", meth, deployment.GetName())

	return fmt.Errorf("Function %s not implemented", meth)
}

// getRevisions returns the updated ControllerRevisions for a V3Deployment. It also
// returns a collision count that records the number of name collisions saw when creating
// new ControllerRevisions. This count is incremented on every name collision and is used in
// building the ControllerRevision names for name collision avoidance. This method may create
// a new revision, or modify the Revision of an existing revision if an update to set is detected.
// This method expects that revisions is sorted when supplied.
func (c *Controller) getRevisions(
	deployment *kumoriv1.V3Deployment,
	revisions []*appsv1.ControllerRevision,
) (*appsv1.ControllerRevision, int32, error) {
	meth := c.Name + ".getRevisions()"
	log.Debugf("%s. Deployment: '%s'", meth, deployment.GetName())

	var updateRevision *appsv1.ControllerRevision

	revisionCount := len(revisions)
	history.SortControllerRevisions(revisions)

	// Use a local copy of set.Status.CollisionCount to avoid modifying set.Status directly.
	// This copy is returned so the value gets carried over to set.Status in updateStatefulSet.
	var collisionCount int32
	if deployment.Status.CollisionCount != nil {
		collisionCount = *deployment.Status.CollisionCount
	}

	// create a new revision from the current set
	updateRevision, err := newRevision(deployment, nextRevision(revisions), &collisionCount)
	if err != nil {
		return nil, collisionCount, err
	}

	// find any equivalent revisions
	equalRevisions := history.FindEqualRevisions(revisions, updateRevision)
	equalCount := len(equalRevisions)

	if equalCount > 0 && history.EqualRevision(revisions[revisionCount-1], equalRevisions[equalCount-1]) && c.sameLabelsAndAnnotations(updateRevision, revisions[revisionCount-1]) {
		// if the equivalent revision is immediately prior the update revision has not changed
		log.Debugf("%s. Deployment: '%s'. Unchanged. Ignoring", meth, deployment.GetName())
		updateRevision = revisions[revisionCount-1]
	} else if equalCount > 0 {
		// if the equivalent revision is not immediately prior we will roll back by incrementing the
		// Revision of the equivalent revision. We use the original revision instead of the newly
		// created `updateRevision` because the former includes all labels, annotations and other elements
		// included when the ControllerRevision is stored in ETCD. This version is cloned and the labels
		// and annotations updated using the ones in `updatedRevision` since they may change. We also add
		// the current timestamp, comment and revision to the p kumori.systems/history annotation, which
		// contains historic information about the times this revision has been applied.
		// NOTE: the cloned revision will be unnecessarily cloned again later. However, we do it that way to
		// keep the original revision unchanged and keep the history library also unchanged.
		log.Debugf("%s. Deployment: '%s'. Rolling back to '%s'. New revision: '%d'", meth, deployment.GetName(), equalRevisions[equalCount-1].GetName(), updateRevision.Revision)
		updateRevision, err = c.updateRevision(equalRevisions[equalCount-1], updateRevision)
		if err != nil {
			return nil, collisionCount, err
		}
	} else {
		//if there is no equivalent revision we create a new one
		log.Debugf("%s. Deployment: '%s'. Setting new ControllerRevision", meth, deployment.GetName())
		updateRevision, err = c.controllerHistory.CreateControllerRevision(deployment, updateRevision, &collisionCount)
		if err != nil {
			return nil, collisionCount, err
		}
		log.Debugf("%s. Deployment: '%s'. New ControllerRevision '%s'. New revision: '%d'", meth, deployment.GetName(), updateRevision.GetName(), updateRevision.Revision)
	}

	return updateRevision, collisionCount, nil
}

// sameLabelsAndAnnotations checks if both revisions have the same labels and annotations
func (c *Controller) sameLabelsAndAnnotations(revision1, revision2 *appsv1.ControllerRevision) bool {

	// Boths revisions have the same labels
	for key, value := range revision1.ObjectMeta.Labels {
		value2, ok := revision2.ObjectMeta.Labels[key]
		if !ok {
			return false
		}
		if value != value2 {
			return false
		}
	}

	// Boths revisions have the same annotations
	for key, value := range revision1.ObjectMeta.Annotations {
		if key == utils.KumoriRevisionHistoryAnnotation {
			continue
		}
		value2, ok := revision2.ObjectMeta.Annotations[key]
		if !ok {
			return false
		}
		if value != value2 {
			return false
		}
	}

	// Returns true since they are equal
	return true

}

// updateRevision updates a revision labels, annotations and history.
func (c *Controller) updateRevision(
	originalRevision *appsv1.ControllerRevision,
	updateRevision *appsv1.ControllerRevision,
) (updatedRevision *appsv1.ControllerRevision, err error) {

	// Clone the originalRevision, which is supposed to be an object recovered from the cluster ETCD and
	// have all required attributes
	clone := originalRevision.DeepCopy()

	// Update the cloned object labels
	clone.ObjectMeta.Labels = updateRevision.ObjectMeta.Labels
	clone.ObjectMeta.Annotations = updateRevision.ObjectMeta.Annotations

	// Update history. Each time a revisión is set as a current revision, a new entry is added to
	// the history to keep track when each revisión has been set.
	if _, ok := originalRevision.ObjectMeta.Annotations[utils.KumoriRevisionHistoryAnnotation]; ok {
		var originalHistory []HistoryItem
		err = json.Unmarshal([]byte(originalRevision.ObjectMeta.Annotations[utils.KumoriRevisionHistoryAnnotation]), &originalHistory)
		if err != nil {
			return
		}
		var updateHistory []HistoryItem
		err = json.Unmarshal([]byte(updateRevision.ObjectMeta.Annotations[utils.KumoriRevisionHistoryAnnotation]), &updateHistory)
		if err != nil {
			return
		}
		newHistory := append(originalHistory, updateHistory...)
		content, err := json.Marshal(newHistory)
		if err != nil {
			return nil, err
		}
		clone.ObjectMeta.Annotations[utils.KumoriRevisionHistoryAnnotation] = string(content)
	}

	// Update the revision
	updatedRevision, err = c.controllerHistory.UpdateControllerRevision(
		clone,
		updateRevision.Revision)
	if err != nil {
		return nil, err
	}
	return
}

// nextRevision finds the next valid revision number based on revisions. If the length of revisions
// is 0 this is 1. Otherwise, it is 1 greater than the largest revision's Revision. This method
// assumes that revisions has been sorted by Revision.
func nextRevision(revisions []*appsv1.ControllerRevision) int64 {
	count := len(revisions)
	if count <= 0 {
		return 1
	}
	return revisions[count-1].Revision + 1
}

// newRevision creates a new ControllerRevision containing a patch that reapplies the target state of
// set. The Revision of the returned ControllerRevision is set to revision. If the returned error is
// nil, the returned ControllerRevision is valid. V3Deployment revisions are stored as patches that
// re-apply the current state of set to a new Deployment using a strategic merge patch to replace the
// saved state of the new Deployment.
func newRevision(
	deployment *kumoriv1.V3Deployment,
	revision int64,
	collisionCount *int32,
) (*appsv1.ControllerRevision, error) {
	patch, err := getPatch(deployment)
	if err != nil {
		return nil, err
	}
	cr, err := history.NewControllerRevision(deployment,
		controllerKind,
		deployment.GetLabels(),
		runtime.RawExtension{Raw: patch},
		revision,
		collisionCount)
	if err != nil {
		return nil, err
	}
	if cr.ObjectMeta.Annotations == nil {
		cr.ObjectMeta.Annotations = make(map[string]string)
	}
	for key, value := range deployment.Annotations {
		cr.ObjectMeta.Annotations[key] = value
	}
	history := []HistoryItem{{
		Timestamp: time.Now().Unix(),
		Comment:   deployment.ObjectMeta.Annotations[utils.KumoriCommentAnnotation],
		Revision:  revision,
	}}
	content, _ := json.Marshal(history)
	cr.ObjectMeta.Annotations[utils.KumoriRevisionHistoryAnnotation] = string(content)
	return cr, nil
}

// getPatch returns a strategic merge patch that can be applied to restore a V3Deployment to a
// previous version. If the returned error is nil the patch is valid. The current state that we save is
// the entire spec. We can modify this later to encompass more state (or less) and remain compatible
// with previously recorded patches.
func getPatch(deployment *kumoriv1.V3Deployment) ([]byte, error) {
	str, err := runtime.Encode(patchCodec, deployment)
	if err != nil {
		return nil, err
	}
	var raw map[string]interface{}
	json.Unmarshal([]byte(str), &raw)
	objCopy := make(map[string]interface{})
	spec := raw["spec"].(map[string]interface{})
	objCopy["spec"] = spec
	patch, err := json.Marshal(objCopy)
	return patch, err
}

// truncateHistory truncates any non-live ControllerRevisions in revisions from V3Deployme's history. The
// CurrentRevision in V3Deployment's Status is considered to be live. Non-live revisions are deleted, starting with
// the revision with the lowest Revision, until only RevisionHistoryLimit revisions remain. If the returned error is
// nil the operation was successful. This method expects that revisions is sorted when supplied.
func (c *Controller) TruncateHistory(
	deployment *kumoriv1.V3Deployment,
	revisions []*appsv1.ControllerRevision,
	current *appsv1.ControllerRevision) error {

	meth := fmt.Sprintf("%s.TruncateHistory. Deployment: %s", c.Name, deployment.GetName())
	log.Debugf(meth)

	revisionHistory := make([]*appsv1.ControllerRevision, 0, len(revisions))
	// mark all live revisions
	live := map[string]bool{}
	if current != nil {
		live[current.Name] = true
	}
	// collect live revisions and historic revisions
	for i := range revisions {
		if !live[revisions[i].Name] {
			revisionHistory = append(revisionHistory, revisions[i])
		}
	}
	historyLen := len(revisionHistory)
	// Note: THis is how the controller revisions history limit is set for StatefulSets. I keep this in case we decide
	// to allow setting this value per V3Deployment instead of using the same value for all.
	// historyLimit := int(*deployment.Spec.RevisionHistoryLimit)
	historyLimit := int(c.config.RevisionHistoryLimit)
	if historyLen <= historyLimit {
		log.Debugf("%s. History length (%d) below limit (%d). Skipping...", meth, historyLen, historyLimit)
		return nil
	}
	// delete any non-live history to maintain the revision limit.
	log.Debugf("%s. History length (%d) above limit (%d). Removing older %d revisions", meth, historyLen, historyLimit, (historyLen - historyLimit))
	revisionHistory = revisionHistory[:(historyLen - historyLimit)]
	for i := 0; i < len(revisionHistory); i++ {
		log.Debugf("%s. Removing revision '%d' (%s)", meth, revisionHistory[i].Revision, revisionHistory[i].GetName())
		if err := c.controllerHistory.DeleteControllerRevision(revisionHistory[i]); err != nil {
			log.Errorf("%s. Error removing revision '%d' (%s): %s", meth, revisionHistory[i].Revision, revisionHistory[i].GetName(), err.Error())
			return err
		}
	}
	return nil
}
