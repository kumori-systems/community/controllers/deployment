/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package deployment

import (
	"encoding/json"
	"fmt"
	"sort"
	"time"

	"github.com/fsnotify/fsnotify"
	kumoriclientset "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated/clientset/versioned"

	kumoriv1 "gitlab.com/kumori-systems/community/libraries/client-go/pkg/apis/kumori/v1"

	kumorischeme "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated/clientset/versioned/scheme"
	kumoriinformers "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated/informers/externalversions/kumori/v1"
	kumorilisters "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated/listers/kumori/v1"

	base "gitlab.com/kumori-systems/community/libraries/base-controller/pkg/base-controller"

	log "github.com/sirupsen/logrus"
	appsv1 "k8s.io/api/apps/v1"
	v1 "k8s.io/api/core/v1"
	netv1 "k8s.io/api/networking/v1"
	policyv1 "k8s.io/api/policy/v1"

	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/labels"

	kubeclientset "k8s.io/client-go/kubernetes"

	kubeinformers "k8s.io/client-go/informers/apps/v1"
	coreinformers "k8s.io/client-go/informers/core/v1"
	netinformers "k8s.io/client-go/informers/networking/v1"
	policyinformers "k8s.io/client-go/informers/policy/v1"
	kubelisters "k8s.io/client-go/listers/apps/v1"
	corelisters "k8s.io/client-go/listers/core/v1"
	netlisters "k8s.io/client-go/listers/networking/v1"
	policylisters "k8s.io/client-go/listers/policy/v1"
	cache "k8s.io/client-go/tools/cache"
	workqueue "k8s.io/client-go/util/workqueue"

	history "kuku-controller/pkg/controllers/history"
	utils "kuku-controller/pkg/utils"
	kerrors "kuku-controller/pkg/utils/errors"
	configMapsManager "kuku-controller/pkg/utils/kube/configmaps"
	deploymentsManager "kuku-controller/pkg/utils/kube/deployments"
	networkPoliciesManager "kuku-controller/pkg/utils/kube/networkpolicies"
	podDisruptionBudgetsManager "kuku-controller/pkg/utils/kube/poddisruptionbudgets"
	secretsManager "kuku-controller/pkg/utils/kube/secrets"
	servicesManager "kuku-controller/pkg/utils/kube/services"
	statefulSetsManager "kuku-controller/pkg/utils/kube/statefulsets"
	kusort "kuku-controller/pkg/utils/sort"

	hashutils "kuku-controller/pkg/utils/hash"
	linkutils "kuku-controller/pkg/utils/kumori/kukulinks"
	v3dutils "kuku-controller/pkg/utils/kumori/v3deployments"

	viper "github.com/spf13/viper"

	config "kuku-controller/pkg/controllers/deployment/config"
	context "kuku-controller/pkg/controllers/deployment/context"
	generators "kuku-controller/pkg/controllers/deployment/generators"
)

// Just the description-name of our controller
const controllerName = "deploymentcontroller"

// controllerKind contains the schema.GroupVersionKind for this controller type.
var controllerKind = kumoriv1.SchemeGroupVersion.WithKind("V3Deployment")

// Controller is the controller implementation for V3Deployments.
type Controller struct {
	base.BaseController

	kubeClientset   kubeclientset.Interface
	kumoriClientset kumoriclientset.Interface

	deploymentInformer          kubeinformers.DeploymentInformer
	statefulSetInformer         kubeinformers.StatefulSetInformer
	configMapInformer           coreinformers.ConfigMapInformer
	serviceInformer             coreinformers.ServiceInformer
	networkPolicyInformer       netinformers.NetworkPolicyInformer
	revisionInformer            kubeinformers.ControllerRevisionInformer
	secretInformer              coreinformers.SecretInformer
	podDisruptionBudgetInformer policyinformers.PodDisruptionBudgetInformer
	v3DeploymentInformer        kumoriinformers.V3DeploymentInformer
	kukuLinkInformer            kumoriinformers.KukuLinkInformer
	kukuSecretInformer          kumoriinformers.KukuSecretInformer
	kukuVolumeInformer          kumoriinformers.KukuVolumeInformer
	kukuCaInformer              kumoriinformers.KukuCaInformer
	kukuCertInformer            kumoriinformers.KukuCertInformer
	kukuDomainInformer          kumoriinformers.KukuDomainInformer
	kukuPortInformer            kumoriinformers.KukuPortInformer

	deploymentsLister          kubelisters.DeploymentLister
	statefulSetsLister         kubelisters.StatefulSetLister
	configMapsLister           corelisters.ConfigMapLister
	servicesLister             corelisters.ServiceLister
	networkPoliciesLister      netlisters.NetworkPolicyLister
	revisionsLister            kubelisters.ControllerRevisionLister
	secretsLister              corelisters.SecretLister
	podDisruptionBudgetsLister policylisters.PodDisruptionBudgetLister
	v3DeploymentsLister        kumorilisters.V3DeploymentLister
	kukuLinksLister            kumorilisters.KukuLinkLister
	kukuSecretsLister          kumorilisters.KukuSecretLister
	kukuVolumesLister          kumorilisters.KukuVolumeLister
	kukuCasLister              kumorilisters.KukuCaLister
	kukuCertsLister            kumorilisters.KukuCertLister
	kukuDomainsLister          kumorilisters.KukuDomainLister
	kukuPortsLister            kumorilisters.KukuPortLister

	controllerHistory history.Interface

	config config.Config
}

// NewController returns a new kuku controller
func NewController(
	kubeClientset kubeclientset.Interface,
	kumoriClientset kumoriclientset.Interface,
	deploymentInformer kubeinformers.DeploymentInformer,
	statefulSetInformer kubeinformers.StatefulSetInformer,
	configMapInformer coreinformers.ConfigMapInformer,
	serviceInformer coreinformers.ServiceInformer,
	networkPolicyInformer netinformers.NetworkPolicyInformer,
	revisionInformer kubeinformers.ControllerRevisionInformer,
	secretInformer coreinformers.SecretInformer,
	podDisruptionBudgetInformer policyinformers.PodDisruptionBudgetInformer,
	v3DeploymentInformer kumoriinformers.V3DeploymentInformer,
	kukuLinkInformer kumoriinformers.KukuLinkInformer,
	kukuSecretInformer kumoriinformers.KukuSecretInformer,
	kukuVolumeInformer kumoriinformers.KukuVolumeInformer,
	kukuCaInformer kumoriinformers.KukuCaInformer,
	kukuCertInformer kumoriinformers.KukuCertInformer,
	kukuDomainInformer kumoriinformers.KukuDomainInformer,
	kukuPortInformer kumoriinformers.KukuPortInformer,
) (c *Controller) {
	meth := fmt.Sprintf("%s.NewController", controllerName)
	log.Infof("%s. Creating controller", meth)

	syncedFunctions := make([]cache.InformerSynced, 16)
	syncedFunctions[0] = deploymentInformer.Informer().HasSynced
	syncedFunctions[1] = statefulSetInformer.Informer().HasSynced
	syncedFunctions[2] = configMapInformer.Informer().HasSynced
	syncedFunctions[3] = serviceInformer.Informer().HasSynced
	syncedFunctions[4] = networkPolicyInformer.Informer().HasSynced
	syncedFunctions[5] = revisionInformer.Informer().HasSynced
	syncedFunctions[6] = secretInformer.Informer().HasSynced
	syncedFunctions[7] = podDisruptionBudgetInformer.Informer().HasSynced
	syncedFunctions[8] = v3DeploymentInformer.Informer().HasSynced
	syncedFunctions[9] = kukuLinkInformer.Informer().HasSynced
	syncedFunctions[10] = kukuSecretInformer.Informer().HasSynced
	syncedFunctions[11] = kukuVolumeInformer.Informer().HasSynced
	syncedFunctions[12] = kukuCaInformer.Informer().HasSynced
	syncedFunctions[13] = kukuCertInformer.Informer().HasSynced
	syncedFunctions[14] = kukuDomainInformer.Informer().HasSynced
	syncedFunctions[15] = kukuPortInformer.Informer().HasSynced

	primarySharedInformers := make([]cache.SharedIndexInformer, 1)
	primarySharedInformers[0] = v3DeploymentInformer.Informer()

	secondarySharedInformers := make([]cache.SharedIndexInformer, 14)
	secondarySharedInformers[0] = deploymentInformer.Informer()
	secondarySharedInformers[1] = statefulSetInformer.Informer()
	secondarySharedInformers[2] = configMapInformer.Informer()
	secondarySharedInformers[3] = serviceInformer.Informer()
	secondarySharedInformers[4] = networkPolicyInformer.Informer()
	secondarySharedInformers[5] = secretInformer.Informer()
	secondarySharedInformers[6] = podDisruptionBudgetInformer.Informer()
	secondarySharedInformers[7] = kukuLinkInformer.Informer()
	secondarySharedInformers[8] = kukuSecretInformer.Informer()
	secondarySharedInformers[9] = kukuVolumeInformer.Informer()
	secondarySharedInformers[10] = kukuCaInformer.Informer()
	secondarySharedInformers[11] = kukuCertInformer.Informer()
	secondarySharedInformers[12] = kukuDomainInformer.Informer()
	secondarySharedInformers[13] = kukuPortInformer.Informer()

	baseController := base.BaseController{
		Name:                     controllerName,
		Kind:                     controllerKind.Kind,
		Workqueue:                workqueue.NewNamedRateLimitingQueue(workqueue.DefaultControllerRateLimiter(), controllerName),
		Recorder:                 base.CreateRecorder(kubeClientset, kumorischeme.AddToScheme, controllerName),
		SyncedFunctions:          syncedFunctions,
		PrimarySharedInformers:   primarySharedInformers,
		SecondarySharedInformers: secondarySharedInformers,
	}

	config := getConfig()

	c = &Controller{
		BaseController:              baseController,
		kubeClientset:               kubeClientset,
		kumoriClientset:             kumoriClientset,
		deploymentInformer:          deploymentInformer,
		statefulSetInformer:         statefulSetInformer,
		configMapInformer:           configMapInformer,
		serviceInformer:             serviceInformer,
		networkPolicyInformer:       networkPolicyInformer,
		revisionInformer:            revisionInformer,
		secretInformer:              secretInformer,
		podDisruptionBudgetInformer: podDisruptionBudgetInformer,
		v3DeploymentInformer:        v3DeploymentInformer,
		kukuLinkInformer:            kukuLinkInformer,
		kukuSecretInformer:          kukuSecretInformer,
		kukuVolumeInformer:          kukuVolumeInformer,
		kukuCaInformer:              kukuCaInformer,
		kukuCertInformer:            kukuCertInformer,
		kukuDomainInformer:          kukuDomainInformer,
		kukuPortInformer:            kukuPortInformer,
		deploymentsLister:           deploymentInformer.Lister(),
		statefulSetsLister:          statefulSetInformer.Lister(),
		configMapsLister:            configMapInformer.Lister(),
		servicesLister:              serviceInformer.Lister(),
		networkPoliciesLister:       networkPolicyInformer.Lister(),
		revisionsLister:             revisionInformer.Lister(),
		secretsLister:               secretInformer.Lister(),
		podDisruptionBudgetsLister:  podDisruptionBudgetInformer.Lister(),
		v3DeploymentsLister:         v3DeploymentInformer.Lister(),
		kukuLinksLister:             kukuLinkInformer.Lister(),
		kukuSecretsLister:           kukuSecretInformer.Lister(),
		kukuVolumesLister:           kukuVolumeInformer.Lister(),
		kukuCasLister:               kukuCaInformer.Lister(),
		kukuCertsLister:             kukuCertInformer.Lister(),
		kukuDomainsLister:           kukuDomainInformer.Lister(),
		kukuPortsLister:             kukuPortInformer.Lister(),
		controllerHistory:           history.NewHistory(kubeClientset, revisionInformer.Lister()),
		config:                      config,
	}

	c.BaseController.IBaseController = c

	// Watches for changes in configuration. If a change is detected, all V3Deployment objects
	// are queued.
	viper.WatchConfig()
	viper.OnConfigChange(func(e fsnotify.Event) {
		log.Infof("%s. Config file changed: %s", meth, e.Name)
		config := getConfig()
		c.config = config
		UpdateLogLevel()

		c.enqueueAllV3Deployments()
	})

	return
}

func (c *Controller) enqueueAllV3Deployments() {
	meth := fmt.Sprintf("%s.enqueueAllV3Deployments", controllerName)
	log.Infof("%s. Creating controller", meth)

	namespace := viper.GetString("namespace")
	matchLabels := map[string]string{}
	options := labels.SelectorFromSet(matchLabels)
	v3Deployments, err := c.v3DeploymentsLister.V3Deployments(namespace).List(options)
	if err != nil {
		log.Infof("%s. Cannot get V3Deployments to apply a config file change. Error: %s", meth, err.Error())
	}
	for _, v3Deployment := range v3Deployments {
		c.enqueueV3Deployment(v3Deployment)
	}
}

func getConfig() config.Config {
	meth := fmt.Sprintf("%s.getConfig", controllerName)
	startupPeriod, err := time.ParseDuration(viper.GetString("defaultStartupPeriod"))
	if err != nil {
		log.Warnf("%s. Cannot parse default startup period duration. Setting period value to 10s. Error: %s", meth, err.Error())
		startupPeriod, _ = time.ParseDuration("10s")
	}
	startupDelay, err := time.ParseDuration(viper.GetString("defaultStartupInitialDelay"))
	if err != nil {
		log.Warnf("%s. Cannot parse default startup initial delay duration. Setting initial delay value to 0s. Error: %s", meth, err.Error())
		startupDelay, _ = time.ParseDuration("0s")
	}
	startupTimeout, err := time.ParseDuration(viper.GetString("defaultStartupTimeout"))
	if err != nil {
		log.Warnf("%s. Cannot parse default startup timeout duration. Setting period value to 1s. Error: %s", meth, err.Error())
		startupTimeout, _ = time.ParseDuration("1s")
	}
	livenessPeriod, err := time.ParseDuration(viper.GetString("defaultLivenessPeriod"))
	if err != nil {
		log.Warnf("%s. Cannot parse default liveness period duration. Setting period value to 10s. Error: %s", meth, err.Error())
		livenessPeriod, _ = time.ParseDuration("10s")
	}
	livenessDelay, err := time.ParseDuration(viper.GetString("defaultLivenessInitialDelay"))
	if err != nil {
		log.Warnf("%s. Cannot parse default liveness initial delay duration. Setting liveness delay value to 0s. Error: %s", meth, err.Error())
		livenessDelay, _ = time.ParseDuration("0s")
	}
	livenessTimeout, err := time.ParseDuration(viper.GetString("defaultLivenessTimeout"))
	if err != nil {
		log.Warnf("%s. Cannot parse default liveness timeout duration. Setting period value to 1s. Error: %s", meth, err.Error())
		livenessTimeout, _ = time.ParseDuration("1s")
	}
	livenessHighFrequencyPeriod, err := time.ParseDuration(viper.GetString("livenessHighFrequencyPeriod"))
	if err != nil {
		log.Warnf("%s. Cannot parse default liveness high frequency duration. Setting value to 1s. Error: %s", meth, err.Error())
		livenessTimeout, _ = time.ParseDuration("1s")
	}
	livenessMediumFrequencyPeriod, err := time.ParseDuration(viper.GetString("livenessMediumFrequencyPeriod"))
	if err != nil {
		log.Warnf("%s. Cannot parse default liveness medium frequency duration. Setting value to 10s. Error: %s", meth, err.Error())
		livenessTimeout, _ = time.ParseDuration("10s")
	}
	livenessLowFrequencyPeriod, err := time.ParseDuration(viper.GetString("livenessLowFrequencyPeriod"))
	if err != nil {
		log.Warnf("%s. Cannot parse default liveness low frequency duration. Setting value to 60s. Error: %s", meth, err.Error())
		livenessTimeout, _ = time.ParseDuration("60s")
	}
	readinessPeriod, err := time.ParseDuration(viper.GetString("defaultReadinessPeriod"))
	if err != nil {
		log.Warnf("%s. Cannot parse default readiness period duration. Setting period value to 10s. Error: %s", meth, err.Error())
		readinessPeriod, _ = time.ParseDuration("10s")
	}
	readinessDelay, err := time.ParseDuration(viper.GetString("defaultReadinessInitialDelay"))
	if err != nil {
		log.Warnf("%s. Cannot parse default readiness initial delay duration. Setting readiness delay value to 0s. Error: %s", meth, err.Error())
		readinessDelay, _ = time.ParseDuration("0s")
	}
	readinessTimeout, err := time.ParseDuration(viper.GetString("defaultReadinessTimeout"))
	if err != nil {
		log.Warnf("%s. Cannot parse default readiness timeout duration. Setting period value to 1s. Error: %s", meth, err.Error())
		readinessTimeout, _ = time.ParseDuration("1s")
	}
	readinessHighFrequencyPeriod, err := time.ParseDuration(viper.GetString("readinessHighFrequencyPeriod"))
	if err != nil {
		log.Warnf("%s. Cannot parse default readiness high frequency duration. Setting value to 1s. Error: %s", meth, err.Error())
		livenessTimeout, _ = time.ParseDuration("1s")
	}
	readinessMediumFrequencyPeriod, err := time.ParseDuration(viper.GetString("readinessMediumFrequencyPeriod"))
	if err != nil {
		log.Warnf("%s. Cannot parse default readiness medium frequency duration. Setting value to 10s. Error: %s", meth, err.Error())
		livenessTimeout, _ = time.ParseDuration("10s")
	}
	readinessLowFrequencyPeriod, err := time.ParseDuration(viper.GetString("readinessLowFrequencyPeriod"))
	if err != nil {
		log.Warnf("%s. Cannot parse default readiness low frequency duration. Setting value to 60s. Error: %s", meth, err.Error())
		livenessTimeout, _ = time.ParseDuration("60s")
	}

	datamap := viper.GetStringMap("externalServicesAccess")
	var externalServicesAccess *map[string]config.ExternalServiceAccess
	if len(datamap) >= 0 {
		themap := make(map[string]config.ExternalServiceAccess, len(datamap))
		externalServicesAccess = &themap
		for dataKey, _ := range datamap {
			label := viper.GetStringMapString(fmt.Sprintf("externalServicesAccess.%s.source.label", dataKey))
			namespace := viper.GetString(fmt.Sprintf("externalServicesAccess.%s.source.namespace", dataKey))
			owners := viper.GetStringSlice(fmt.Sprintf("externalServicesAccess.%s.target.owners", dataKey))
			deployments := viper.GetStringSlice(fmt.Sprintf("externalServicesAccess.%s.target.deployments", dataKey))
			externalService := config.ExternalServiceAccess{
				Source: config.ExternalServiceAccessSource{},
				Target: config.ExternalServiceAccessTarget{
					Owners:      owners,
					Deployments: deployments,
				},
			}
			if (label != nil) && (len(label) > 0) {
				key, ok := label["key"]
				if !ok {
					log.Warnf("%s. Label 'key' not found in externalServiceAccess.source.label", meth)
					continue
				}
				value, ok := label["value"]
				if !ok {
					log.Warnf("%s. Label 'value' not found in externalServiceAccess.source.label", meth)
					continue
				}
				externalService.Source.Label = &config.ExternalServiceAccessSourceLabel{
					Key:   key,
					Value: value,
				}
			}
			if len(namespace) > 0 {
				externalService.Source.Namespace = &namespace
			}
			(*externalServicesAccess)[dataKey] = externalService
		}
	}

	config := config.Config{
		DefaultStartupInitialDelay:             startupDelay,
		DefaultStartupPeriod:                   startupPeriod,
		DefaultStartupFailureThreshold:         int32(viper.GetInt("defaultStartupFailureThreshold")),
		DefaultStartupTimeout:                  startupTimeout,
		DefaultLivenessInitialDelay:            livenessDelay,
		DefaultLivenessPeriod:                  livenessPeriod,
		DefaultLivenessFailureThreshold:        int32(viper.GetInt("defaultLivenessFailureThreshold")),
		DefaultLivenessTimeout:                 livenessTimeout,
		LivenessHighFrequencyPeriod:            livenessHighFrequencyPeriod,
		LivenessMediumFrequencyPeriod:          livenessMediumFrequencyPeriod,
		LivenessLowFrequencyPeriod:             livenessLowFrequencyPeriod,
		DefaultReadinessInitialDelay:           readinessDelay,
		DefaultReadinessPeriod:                 readinessPeriod,
		DefaultReadinessFailureThreshold:       int32(viper.GetInt("defaultReadinessFailureThreshold")),
		DefaultReadinessTimeout:                readinessTimeout,
		ReadinessHighFrequencyPeriod:           readinessHighFrequencyPeriod,
		ReadinessMediumFrequencyPeriod:         readinessMediumFrequencyPeriod,
		ReadinessLowFrequencyPeriod:            readinessLowFrequencyPeriod,
		EnableStartupProbe:                     viper.GetBool("enableStartupProbe"),
		DisablePersistentVolumes:               viper.GetBool("disablePersistentVolumes"),
		ForceRebootOnUpdate:                    viper.GetBool("forceRebootOnUpdate"),
		UsePvcForVolatileVolumes:               viper.GetBool("usePvcForVolatileVolumes"),
		DefaultVolatileVolumesType:             viper.GetString("defaultVolatileVolumesType"),
		TopologySpreadConstraintsSoftLabelKeys: viper.GetString("topologySpreadConstraintsSoftLabelKeys"),
		TopologySpreadConstraintsHardLabelKeys: viper.GetString("topologySpreadConstraintsHardLabelKeys"),
		TopologySpreadConstraintsMaxSkew:       viper.GetInt32("topologySpreadConstraintsMaxSkew"),
		UserPodsPriorityClass:                  viper.GetString("userPodsPriorityClass"),
		RevisionHistoryLimit:                   viper.GetInt32("revisionHistoryLimit"),
		DefaultDiskRequestSize:                 viper.GetInt32("defaultDiskRequestSize"),
		DefaultDiskRequestUnit:                 viper.GetString("defaultDiskRequestUnit"),
		FsGroup:                                viper.GetString("fsGroup"),
		DefaultFsGroup:                         viper.GetInt64("defaultFsGroup"),
		DefaultMappingEnableTemplate:           viper.GetBool("defaultMappingEnableTemplate"),
		EnableNodeAffinities:                   viper.GetBool("enableNodeAffinities"),
		HpaEnabledLabelKey:                     viper.GetString("hpaEnabledLabelKey"),
		ClusterConfigMapName:                   viper.GetString("clusterConfigMapName"),
		ClusterConfigMapNamespace:              viper.GetString("clusterConfigMapNamespace"),
		ClusterConfigMapKey:                    viper.GetString("clusterConfigMapKey"),
		ExternalServicesAccess:                 externalServicesAccess,
	}

	content, _ := json.MarshalIndent(config, "", "  ")
	log.Infof("%s. Configuration loaded:\n", string(content))

	return config
}

// HandleSecondary is in charge of processing changes in the statefulsets,
// configmaps, services, networkpolicies, v3deployments, kukulinks, and
// kukupersistentvolumes) and force to re-evaluate the primary object,
// v3deployment in this case
func (c *Controller) HandleSecondary(secondary interface{}) {
	meth := c.Name + ".HandleSecondary()"
	log.Debugf(meth)
	obj, v3DeploymentName, err := c.GetObject(secondary)
	if err != nil {
		log.Errorf("%s Error %v", meth, err)
		return
	}
	kukuLinkTools := linkutils.NewKukuLinkTools(c.v3DeploymentsLister, c.kukuLinksLister, c.servicesLister, c.networkPoliciesLister)
	switch obj := obj.(type) {
	// If it is a link, checks if references V3Deployments and enqueues them if so.
	case *kumoriv1.KukuLink:
		log.Infof("%s. Link handler '%s'-'%s' => '%s'-'%s'", meth, obj.Spec.Endpoints[0].Name, obj.Spec.Endpoints[0].Channel, obj.Spec.Endpoints[1].Name, obj.Spec.Endpoints[1].Channel)

		queued := make([]string, 0, 2)

		for _, endpoint := range obj.Spec.Endpoints {
			if (endpoint.Kind == linkutils.DeploymentKukuLinkEndpointType) ||
				(endpoint.Kind == linkutils.SolutionKukuLinkEndpointType) {
				if v3Deployment, err := v3dutils.GetByRegistrationNameAndDomain(c.v3DeploymentsLister, obj.GetNamespace(), endpoint.Name, endpoint.Domain); err == nil {
					queued = append(queued, v3Deployment.GetName())
					c.enqueueV3Deployment(v3Deployment)
				} else {
					log.Errorf("%s. Error looking for V3Deployment %s/%s: %v", meth, endpoint.Domain, endpoint.Name, err)
				}
			} else {
				log.Debugf("%s. Endpoint %s/%s is not a V3Deployment. Skipping", meth, endpoint.Domain, endpoint.Name)
			}
		}

		// In case the Link has been modified, enqueue also any other V3Deployment if
		// any its _Service_ or _NetworkPolicy_ objects are affected by this link
		c.enqueueUnlinked(&kukuLinkTools, queued, obj)

	// If it is a KukuSecret, it finds the V3Deployments using this KukuSecret and
	// enquques them
	case *kumoriv1.KukuSecret:
		log.Debugf("%s. KukuSecret handler. KukuSecret name: '%s'.", meth, obj.GetName())
		ksname := obj.GetName()
		matchLabels := map[string]string{
			ksname: "resource-in-use",
		}
		options := labels.SelectorFromSet(matchLabels)
		v3Deployments, err := c.v3DeploymentsLister.V3Deployments(obj.GetNamespace()).List(options)
		if err != nil {
			log.Errorf("%s. Error getting V3Deployments using KukuSecret '%s': %s", meth, ksname, err.Error())
			return
		}
		if len(v3Deployments) == 0 {
			return
		}
		for _, v3Deployment := range v3Deployments {
			c.enqueueV3Deployment(v3Deployment)
		}

	// If it is a KukuCa, it finds the V3Deployments using this KukuCa and
	// enquques them
	case *kumoriv1.KukuCa:
		log.Debugf("%s. KukuCa handler. KukuCa name: '%s'.", meth, obj.GetName())
		ksname := obj.GetName()
		matchLabels := map[string]string{
			ksname: "resource-in-use",
		}
		options := labels.SelectorFromSet(matchLabels)
		v3Deployments, err := c.v3DeploymentsLister.V3Deployments(obj.GetNamespace()).List(options)
		if err != nil {
			log.Errorf("%s. Error getting V3Deployments using KukuCa '%s': %s", meth, ksname, err.Error())
			return
		}
		if len(v3Deployments) == 0 {
			return
		}
		for _, v3Deployment := range v3Deployments {
			c.enqueueV3Deployment(v3Deployment)
		}

	// If it is a KukuCert, it finds the V3Deployments using this KukuCert and
	// enquques them
	case *kumoriv1.KukuCert:
		log.Debugf("%s. KukuCert handler. KukuCert name: '%s'.", meth, obj.GetName())
		ksname := obj.GetName()
		matchLabels := map[string]string{
			ksname: "resource-in-use",
		}
		options := labels.SelectorFromSet(matchLabels)
		v3Deployments, err := c.v3DeploymentsLister.V3Deployments(obj.GetNamespace()).List(options)
		if err != nil {
			log.Errorf("%s. Error getting V3Deployments using KukuCert '%s': %s", meth, ksname, err.Error())
			return
		}
		if len(v3Deployments) == 0 {
			return
		}
		for _, v3Deployment := range v3Deployments {
			c.enqueueV3Deployment(v3Deployment)
		}

	// If it is a KukuDomain, it finds the V3Deployments using this KukuDomain and
	// enquques them
	case *kumoriv1.KukuDomain:
		log.Debugf("%s. KukuDomain handler. KukuDomain name: '%s'.", meth, obj.GetName())
		ksname := obj.GetName()
		matchLabels := map[string]string{
			ksname: "resource-in-use",
		}
		options := labels.SelectorFromSet(matchLabels)
		v3Deployments, err := c.v3DeploymentsLister.V3Deployments(obj.GetNamespace()).List(options)
		if err != nil {
			log.Errorf("%s. Error getting V3Deployments using KukuDomain '%s': %s", meth, ksname, err.Error())
			return
		}
		if len(v3Deployments) == 0 {
			return
		}
		for _, v3Deployment := range v3Deployments {
			c.enqueueV3Deployment(v3Deployment)
		}

	// If it is a KukuPort, it finds the V3Deployments using this KukuPort and
	// enquques them
	case *kumoriv1.KukuPort:
		log.Debugf("%s. KukuPort handler. KukuPort name: '%s'.", meth, obj.GetName())
		ksname := obj.GetName()
		matchLabels := map[string]string{
			ksname: "resource-in-use",
		}
		options := labels.SelectorFromSet(matchLabels)
		v3Deployments, err := c.v3DeploymentsLister.V3Deployments(obj.GetNamespace()).List(options)
		if err != nil {
			log.Errorf("%s. Error getting V3Deployments using KukuPort '%s': %s", meth, ksname, err.Error())
			return
		}
		if len(v3Deployments) == 0 {
			return
		}
		for _, v3Deployment := range v3Deployments {
			c.enqueueV3Deployment(v3Deployment)
		}

	case *v1.ConfigMap:
		log.Debugf("%s. ConfigMap handler. ConfigMap name: '%s'.", meth, obj.GetName())
		if v3DeploymentName != "" {
			if err := c.secondaryObjectDefaultProcessor(v3DeploymentName, obj, &kukuLinkTools); err != nil {
				log.Errorf("%s. Error processing secondary object '%s': %+v", meth, obj.GetName(), err)
			}
		} else {
			clusterConfigName := c.config.ClusterConfigMapName
			clusterConfigNamespace := c.config.ClusterConfigMapNamespace
			name := obj.GetName()
			namespace := obj.GetNamespace()
			if (name == clusterConfigName) && (namespace == clusterConfigNamespace) {
				c.enqueueAllV3Deployments()
			}
		}

	// Otherwise, checks if the owner reference of this object is a V3Deployment and equeues it if so.
	default:
		if v3DeploymentName != "" {
			if err := c.secondaryObjectDefaultProcessor(v3DeploymentName, obj, &kukuLinkTools); err != nil {
				log.Errorf("%s. Error processing secondary object '%s': %+v", meth, obj.GetName(), err)
			}
		} else {
			// log.Debugf("%s. Default handler. Deployment name is empty. Skipping", meth)
		}
	}
}

func (c *Controller) secondaryObjectDefaultProcessor(
	v3DeploymentName string,
	obj metav1.Object,
	kukuLinkTools *linkutils.KukuLinkTools,
) error {
	meth := fmt.Sprintf("%s.secondaryObjectDefaultProcessor(). V3Deployment: '%s' Obj: '%s'", c.Name, v3DeploymentName, obj.GetName())
	if v3DeploymentName != "" {
		log.Debugf("%s. Default handler. Deployment name: '%s'. Processing object: %s", meth, v3DeploymentName, obj.GetName())
		v3Deployment, err := v3dutils.GetByName(c.v3DeploymentsLister, obj.GetNamespace(), v3DeploymentName)
		if errors.IsNotFound(err) {
			// log.Infof("%s ignoring orphaned object of v3deployment '%s'", meth, kukuDeploymentName)
			// NOTE: kukuDeployment.GetSelfLink() was causing a segmentation fault
			log.Infof("%s ignoring orphaned object '%s' of v3deployment '%s'", meth, obj.GetSelfLink(), v3DeploymentName)
			return nil
		} else if err != nil {
			log.Warnf("%s error retrieving object %s: %v", meth, v3DeploymentName, err)
		}
		c.enqueueV3Deployment(v3Deployment)

		// If the V3Deployment is linked to other V3Deployments, enqueue them too.
		linkedV3Deployments, err := kukuLinkTools.GetReachableDeployments(v3Deployment, "")
		sort.SliceStable(linkedV3Deployments, func(i, j int) bool { return linkedV3Deployments[i].Name < linkedV3Deployments[j].Name })
		if err != nil {
			return fmt.Errorf("error getting deployments linked to V3Deployment '%s': %+v", v3Deployment.Name, err)
		}
		for _, linkedV3Deployment := range linkedV3Deployments {
			c.enqueueV3Deployment(linkedV3Deployment)
		}

	} else {
		return fmt.Errorf("v3DeploymentName is empty")
	}

	return nil
}

// enqueueV3Deployment enqueues a V3Deployment only if it is not going to be deleted (if not has a
// deletion timestamp set)
func (c *Controller) enqueueV3Deployment(v3d *kumoriv1.V3Deployment) {
	meth := fmt.Sprintf("%s.enqueueV3Deployment(). V3Deployment: '%s'", c.Name, v3d.GetName())
	log.Debugf(meth)

	// This check is now performed when the context is loaded
	// // If the V3Deployment is not going to be removed, queue it
	// if v3d.GetDeletionTimestamp().IsZero() {
	// 	log.Debugf("%s enqueued", meth)
	// 	c.EnqueueObject(v3d)
	// 	return
	// }

	// // If the V3Deployment is going to be removed, skip it
	// log.Debugf("%s. V3Deployment has a deletion timestamp set. Skipping.", meth)

	log.Debugf("%s enqueued", meth)
	c.EnqueueObject(v3d)
}

// In case the Link has been modified, enqueue also any other V3Deployment if
// any its _Service_ or _NetworkPolicy_ objects are affected by this link
func (c *Controller) enqueueUnlinked(
	kukuLinkTools *linkutils.KukuLinkTools,
	queued []string,
	kukuLink *kumoriv1.KukuLink,
) {

	meth := c.Name + ".enqueueUnlinked()"

	labeledV3Deployments, err := kukuLinkTools.GetLabeledV3Deployments(kukuLink)
	if err != nil {
		log.Errorf("%s. Error getting V3Deployments related with link %s: %+v", meth, kukuLink.GetName(), err)
	}

	if labeledV3Deployments != nil && len(labeledV3Deployments) > 0 {
		for _, labeledV3Deployment := range labeledV3Deployments {
			alreadyQueued := false
			for _, processedV3d := range queued {
				if processedV3d == labeledV3Deployment.GetName() {
					alreadyQueued = true
					break
				}
			}
			if !alreadyQueued {
				queued = append(queued, labeledV3Deployment.GetName())
				c.enqueueV3Deployment(labeledV3Deployment)
			}
		}
	}
}

// SyncHandler compares the actual state with the desired for a given V3Deployment, and attempts to
// converge both of them.
func (c *Controller) SyncHandler(key string) error {

	meth := fmt.Sprintf("%s.SyncHandler. Key: %s", c.Name, key)
	log.Info(meth)

	// Convert the namespace/name string into a distinct namespace and name
	namespace, name, err := cache.SplitMetaNamespaceKey(key)
	if err != nil {
		log.Errorf("%s. Invalid resource", meth)
		return nil
	}

	// Loads all kuku elements in a Context
	log.Debugf("%s. Calculating context", meth)
	kcontext, kerr := context.CreateContext(
		namespace,
		name,
		&c.config,
		c.v3DeploymentsLister,
		c.kukuLinksLister,
		c.kukuSecretsLister,
		c.kukuPortsLister,
		c.kukuDomainsLister,
		c.kukuCertsLister,
		c.kukuCasLister,
		c.kukuVolumesLister,
		c.servicesLister,
		c.networkPoliciesLister,
		c.configMapsLister,
	)
	if kerrors.ShouldSkip(kerr) {
		log.Debugf("%s Skipping V3Deployment due to: %s", meth, kerr.Error())
		return nil
	}
	if kerrors.ShouldAbort(kerr) {
		if kerrors.ShouldRetry(kerr) {
			if (kcontext != nil) && (kcontext.Deployment != nil) {
				c.Recorder.Eventf(kcontext.Deployment, v1.EventTypeWarning, utils.RequeuedReason, "Requeued due to: %s", kerr.Error())
			}
			log.Errorf("%s. Recoverable error found. Requeuing. Error: %v", meth, kerr)
			return kerr
		}
		if (kcontext != nil) && (kcontext.Deployment != nil) {
			c.Recorder.Eventf(kcontext.Deployment, v1.EventTypeWarning, utils.AbortedReason, "Aborted due to: %s", kerr.Error())
		}
		log.Errorf("%s. Unrecoverable error found. The process will be aborted and not retried. Error: %v", meth, kerr)
		return nil
	}
	if kcontext == nil {
		log.Debugf("%s Skipping V3Deployment due to: KukuContext is nil", meth)
		return nil
	}
	log.Debugf("%s. Context loaded", meth)

	// Updates the V3Deployment revisions. A new revision is created if this manifest represetns
	// a new version of the V3Deployment or restores a previous version (i.e., it is a rollback).
	log.Debugf("%s. Updating revisions.", meth)
	err = c.UpdateRevisions(kcontext)
	if err != nil {
		log.Errorf("%s. Recoverable error found. Requeuing. Error: %v", meth, err)
		return err
	}
	log.Debugf("%s. Revisions updated", meth)

	log.Debugf("%s. Updating state", meth)

	// We create connector services first. Connector services are needed to update the
	// role's statefulsets host aliases to include an alias for each channel to the
	// clusterIp of the load balancer service representing their connector.
	log.Debugf("%s. Calculating expected connector services", meth)
	connectorServices := c.newConnectorServices(kcontext)
	log.Debugf("%s. Updating connector services", meth)
	errors := c.updateServices(kcontext, connectorServices, false)

	// content, _ := json.MarshalIndent(ips, "", "  ")
	// fmt.Printf("\n\n------->IPS:\n%s\n\n", string(content))

	// Create all kube objects needed for this deployments. Those methods DON'T register or
	// update the objects in the cluster. They just create the Go structures to check later
	// if there is something to change in the cluster state.
	log.Debugf("%s. Calculating expected roles stateful sets, services and configmaps", meth)
	statefulSets, services, sConfigMaps := c.newStatefulSets(kcontext)
	log.Debugf("%s. Calculating expected roles stateless deployments and configmaps", meth)
	deployments, dConfigMaps := c.newDeployments(kcontext)
	log.Debugf("%s. Calculating expected secrets.", meth)
	secrets := c.newSecrets(kcontext)
	// Moved temporarly up
	// connectorServices := c.newConnectorServices(kcontext)
	log.Debugf("%s. Calculating network policies", meth)
	policies := c.newNetworkPolicies(kcontext)
	podDisruptionBudgets := c.newPodDisruptionBudgets(kcontext)

	// Creates or updates the network policies in the cluster state. If this process fails,
	// nothing else is notified to the cluster to avoid creating unprotected objects in the state.
	log.Debugf("%s. Updating network policies", meth)
	nperrors := c.updateNetworkPolicies(kcontext, policies, true)
	if (nperrors != nil) && (len(nperrors) > 0) {
		err := fmt.Errorf("%s. Errors creating network policies: %v", meth, nperrors)
		c.Recorder.Eventf(kcontext.Deployment, v1.EventTypeWarning, utils.RequeuedReason, "Requeued due to: %s", err.Error())
		return err
	}

	// Creates or updates the config maps in the cluster state. If this process fails,
	// the controller will still try to create the next elements.
	totalLength := len(sConfigMaps) + len(dConfigMaps)
	allConfigMaps := make([]v1.ConfigMap, 0, totalLength)
	allConfigMaps = append(allConfigMaps, sConfigMaps...)
	allConfigMaps = append(allConfigMaps, dConfigMaps...)
	log.Debugf("%s. Updating config maps", meth)
	errors = c.updateConfigMaps(kcontext, allConfigMaps, true)

	// Creates or updates the secrets used in the deployment. If this process fails, the
	// controller will still try to create the next elements
	log.Debugf("%s. Updating secrets", meth)
	moreErrors := c.updateSecrets(kcontext, secrets, true)
	if (moreErrors != nil) && (len(moreErrors) > 0) {
		errors = append(errors, moreErrors...)
	}

	// Creates or updates the pod disruption budgets to control the pods eviction
	log.Debugf("%s. Updating pod disruption budgets", meth)
	moreErrors = c.updatePodDisruptionBudgets(kcontext, podDisruptionBudgets, true)
	if (moreErrors != nil) && (len(moreErrors) > 0) {
		errors = append(errors, moreErrors...)
	}

	// Creates or updates the stateful sets in the cluster state. If this process fails,
	// the controller will still try to create the next elements.
	log.Debugf("%s. Updating stateful sets", meth)
	moreErrors = c.updateStatefulSets(kcontext, statefulSets, true)
	if (moreErrors != nil) && (len(moreErrors) > 0) {
		errors = append(errors, moreErrors...)
	}

	// Creates or updates the deployments in the cluster state. If this process fails,
	// the controller will still try to create the next elements.
	log.Debugf("%s. Updating deployments", meth)
	moreErrors = c.updateDeployments(kcontext, deployments, true)
	if (moreErrors != nil) && (len(moreErrors) > 0) {
		errors = append(errors, moreErrors...)
	}

	// Creates or updates the services in the cluster state. If this process fails,
	// the controller will still try to create the next elements. The connector services
	// are included again to avoid removing them.
	totalLength = len(services) + len(connectorServices)
	allServices := make([]v1.Service, 0, totalLength)
	allServices = append(allServices, services...)
	allServices = append(allServices, connectorServices...)
	log.Debugf("%s. Updating pending services", meth)
	moreErrors = c.updateServices(kcontext, allServices, true)
	if (moreErrors != nil) && (len(moreErrors) > 0) {
		errors = append(errors, moreErrors...)
	}

	// Checks if there is a recoverable error. If that's the case, the error is returned and
	// the V3Deployment will be requeued to be processed again later.
	if (errors != nil) && (len(errors) > 0) {
		err := fmt.Errorf("Kube objects creation triggered some errors: %v", errors)
		c.Recorder.Eventf(kcontext.Deployment, v1.EventTypeWarning, utils.RequeuedReason, "Requeued due to: %s", err.Error())
		log.Errorf("%s. Recoverable errors found while creating kube objects. Requeuing. Errors: %v", meth, err)
		return err
	}

	// The V3Deployment is not requeued, either because everything went fine or because
	// a non-recoverable error showed up.
	return nil
}

// updateConfigMaps checks if there is any significative difference between the expected and current
// ConfigMaps and performs the necessary actions.
func (c *Controller) updateConfigMaps(context *context.Context, configMaps []v1.ConfigMap, removeMissing bool) []error {
	meth := fmt.Sprintf("%s.updateConfigMaps. Release: '%s'", c.Name, context.ReleaseName)
	log.Debugf(meth)

	var created, updated, deleted []v1.ConfigMap
	var errs []error
	if removeMissing {
		matchLabels := map[string]string{
			utils.KumoriDeploymentIdLabel: context.Name,
			utils.KumoriControllerLabel:   controllerName,
		}
		options := labels.SelectorFromSet(matchLabels)
		created, updated, deleted, errs = configMapsManager.CreateUpdateOrDeleteList(c.kubeClientset, c.configMapsLister, configMaps, options, context.Namespace)
	} else {
		created, updated, errs = configMapsManager.CreateOrUpdateList(c.kubeClientset, c.configMapsLister, configMaps)
	}
	changed := false
	if (created != nil) && (len(created) > 0) {
		changed = true
		for _, elem := range created {
			c.Recorder.Eventf(context.Deployment, v1.EventTypeNormal, utils.CreatedReason, "Config map %s created", elem.GetName())
			log.Infof("%s. Config map %s created", meth, elem.GetName())
		}
	}
	if (updated != nil) && (len(updated) > 0) {
		changed = true
		for _, elem := range updated {
			c.Recorder.Eventf(context.Deployment, v1.EventTypeNormal, utils.UpdatedReason, "Config map %s updated", elem.GetName())
			log.Infof("%s. Config map %s updated", meth, elem.GetName())
		}
	}
	if (deleted != nil) && (len(deleted) > 0) {
		changed = true
		for _, elem := range deleted {
			c.Recorder.Eventf(context.Deployment, v1.EventTypeNormal, utils.DeletedReason, "Config map %s deleted", elem.GetName())
			log.Infof("%s. Config map %s deleted", meth, elem.GetName())
		}
	}
	if !changed {
		log.Infof("%s. Config maps not changed", meth)
	}
	return errs
}

// updateSecrets checks if there is any significative difference between the expected and current
// Secrets and performs the necessary actions.
func (c *Controller) updateSecrets(context *context.Context, secrets []v1.Secret, removeMissing bool) []error {
	meth := fmt.Sprintf("%s.updateSecrets. Release: '%s'", c.Name, context.ReleaseName)
	log.Debugf(meth)

	var created, updated, deleted []v1.Secret
	var errs []error
	if removeMissing {
		matchLabels := map[string]string{
			utils.KumoriDeploymentIdLabel: context.Name,
			utils.KumoriControllerLabel:   controllerName,
		}
		options := labels.SelectorFromSet(matchLabels)
		created, updated, deleted, errs = secretsManager.CreateUpdateOrDeleteList(c.kubeClientset, c.secretsLister, secrets, options, context.Namespace)
	} else {
		created, updated, errs = secretsManager.CreateOrUpdateList(c.kubeClientset, c.secretsLister, secrets)
	}
	changed := false
	if (created != nil) && (len(created) > 0) {
		changed = true
		for _, elem := range created {
			c.Recorder.Eventf(context.Deployment, v1.EventTypeNormal, utils.CreatedReason, "Secret %s created", elem.GetName())
			log.Infof("%s.updateSecrets. Secret %s created", meth, elem.GetName())
		}
	}
	if (updated != nil) && (len(updated) > 0) {
		changed = true
		for _, elem := range updated {
			c.Recorder.Eventf(context.Deployment, v1.EventTypeNormal, utils.UpdatedReason, "Secret %s updated", elem.GetName())
			log.Infof("%s. Secret %s updated", meth, elem.GetName())
		}
	}
	if (deleted != nil) && (len(deleted) > 0) {
		changed = true
		for _, elem := range deleted {
			c.Recorder.Eventf(context.Deployment, v1.EventTypeNormal, utils.DeletedReason, "Secret %s deleted", elem.GetName())
			log.Infof("%s. Secret %s deleted", meth, elem.GetName())
		}
	}
	if !changed {
		log.Infof("%s. Secrets not changed", meth)
	}
	return errs
}

// updateNetworkPolicies checks if there is any significative difference between the expected and current
// NetworkPolicies and performs the necessary actions.
func (c *Controller) updateNetworkPolicies(context *context.Context, networkPolicies []netv1.NetworkPolicy, removeMissing bool) []error {
	meth := fmt.Sprintf("%s.updateNetworkPolicies. Release: '%s'", c.Name, context.ReleaseName)
	log.Debugf(meth)

	var created, updated, deleted []netv1.NetworkPolicy
	var errs []error
	if removeMissing {
		matchLabels := map[string]string{
			utils.KumoriDeploymentIdLabel: context.Name,
			utils.KumoriControllerLabel:   controllerName,
		}
		options := labels.SelectorFromSet(matchLabels)
		created, updated, deleted, errs = networkPoliciesManager.CreateUpdateOrDeleteList(c.kubeClientset, c.networkPoliciesLister, networkPolicies, options, context.Namespace)
	} else {
		created, updated, errs = networkPoliciesManager.CreateOrUpdateList(c.kubeClientset, c.networkPoliciesLister, networkPolicies)
	}
	changed := false
	if (created != nil) && (len(created) > 0) {
		changed = true
		for _, elem := range created {
			c.Recorder.Eventf(context.Deployment, v1.EventTypeNormal, utils.CreatedReason, "Network policy %s created", elem.GetName())
			log.Infof("%s. Network policy %s created", meth, elem.GetName())
		}
	}
	if (updated != nil) && (len(updated) > 0) {
		changed = true
		for _, elem := range updated {
			c.Recorder.Eventf(context.Deployment, v1.EventTypeNormal, utils.UpdatedReason, "Network policy %s updated", elem.GetName())
			log.Infof("%s. Network policy %s updated", meth, elem.GetName())
		}
	}
	if (deleted != nil) && (len(deleted) > 0) {
		changed = true
		for _, elem := range deleted {
			c.Recorder.Eventf(context.Deployment, v1.EventTypeNormal, utils.DeletedReason, "Network policy %s deleted", elem.GetName())
			log.Infof("%s. Network policy %s deleted", meth, elem.GetName())
		}
	}
	if !changed {
		log.Infof("%s. Network policies not changed", meth)
	}
	return errs
}

// updateStatefulSets checks if there is any significative difference between the expected and current
// StatefulSets and performs the necessary actions.
func (c *Controller) updateStatefulSets(context *context.Context, statefulSets []appsv1.StatefulSet, removeMissing bool) []error {
	meth := fmt.Sprintf("%s.updateStatefulSets. Release: '%s'", c.Name, context.ReleaseName)
	log.Debugf(meth)

	var created, updated, deleted []appsv1.StatefulSet
	var errs []error
	if removeMissing {
		matchLabels := map[string]string{
			utils.KumoriDeploymentIdLabel: context.Name,
			utils.KumoriControllerLabel:   controllerName,
		}
		options := labels.SelectorFromSet(matchLabels)
		created, updated, deleted, errs = statefulSetsManager.CreateUpdateOrDeleteList(c.kubeClientset, c.statefulSetsLister, statefulSets, options, context.Namespace)
	} else {
		created, updated, errs = statefulSetsManager.CreateOrUpdateList(c.kubeClientset, c.statefulSetsLister, statefulSets)
	}
	changed := false
	if (created != nil) && (len(created) > 0) {
		changed = true
		for _, elem := range created {
			c.Recorder.Eventf(context.Deployment, v1.EventTypeNormal, utils.CreatedReason, "Stateful set %s created", elem.GetName())
			log.Infof("%s. Stateful set %s created", meth, elem.GetName())
		}
	}
	if (updated != nil) && (len(updated) > 0) {
		changed = true
		for _, elem := range updated {
			c.Recorder.Eventf(context.Deployment, v1.EventTypeNormal, utils.UpdatedReason, "Stateful set %s updated", elem.GetName())
			log.Infof("%s. Stateful set %s updated", meth, elem.GetName())
		}
	}
	if (deleted != nil) && (len(deleted) > 0) {
		changed = true
		for _, elem := range deleted {
			c.Recorder.Eventf(context.Deployment, v1.EventTypeNormal, utils.DeletedReason, "Stateful set %s deleted", elem.GetName())
			log.Infof("%s. Stateful set %s deleted", meth, elem.GetName())
		}
	}
	if !changed {
		log.Infof("%s. Stateful sets not changed", meth)
	}
	return errs
}

// updateDeployments checks if there is any significative difference between the expected and current
// Deployments and performs the necessary actions.
func (c *Controller) updateDeployments(
	context *context.Context,
	deployments []appsv1.Deployment,
	removeMissing bool,
) []error {
	meth := fmt.Sprintf("%s.updateDeployments. Release: '%s'", c.Name, context.ReleaseName)
	log.Debugf(meth)

	var created, updated, deleted []appsv1.Deployment
	var errs []error
	if removeMissing {
		matchLabels := map[string]string{
			utils.KumoriDeploymentIdLabel: context.Name,
			utils.KumoriControllerLabel:   controllerName,
		}
		options := labels.SelectorFromSet(matchLabels)
		created, updated, deleted, errs = deploymentsManager.CreateUpdateOrDeleteList(c.kubeClientset, c.deploymentsLister, deployments, options, context.Namespace)
	} else {
		created, updated, errs = deploymentsManager.CreateOrUpdateList(c.kubeClientset, c.deploymentsLister, deployments)
	}
	changed := false
	if (created != nil) && (len(created) > 0) {
		changed = true
		for _, elem := range created {
			c.Recorder.Eventf(context.Deployment, v1.EventTypeNormal, utils.CreatedReason, "Deployment %s created", elem.GetName())
			log.Infof("%s. Deployment %s created", meth, elem.GetName())
		}
	}
	if (updated != nil) && (len(updated) > 0) {
		changed = true
		for _, elem := range updated {
			c.Recorder.Eventf(context.Deployment, v1.EventTypeNormal, utils.UpdatedReason, "Deployment %s updated", elem.GetName())
			log.Infof("%s. Deployment %s updated", meth, elem.GetName())
		}
	}
	if (deleted != nil) && (len(deleted) > 0) {
		changed = true
		for _, elem := range deleted {
			c.Recorder.Eventf(context.Deployment, v1.EventTypeNormal, utils.DeletedReason, "Deployment %s deleted", elem.GetName())
			log.Infof("%s. Deployment %s deleted", meth, elem.GetName())
		}
	}
	if !changed {
		log.Infof("%s. Deployments not changed", meth)
	}
	return errs
}

// updatePodDisruptionBudgets checks if there is any significative difference between the expected and current
// PodDisruptionBudgets and performs the necessary actions.
func (c *Controller) updatePodDisruptionBudgets(
	context *context.Context,
	podDisruptionBudgets []policyv1.PodDisruptionBudget,
	removeMissing bool,
) []error {

	meth := fmt.Sprintf("%s.updatePodDisruptionBudgets. Release: '%s'", c.Name, context.ReleaseName)
	log.Debugf(meth)

	var created, updated, deleted []policyv1.PodDisruptionBudget
	var errs []error
	if removeMissing {
		matchLabels := map[string]string{
			utils.KumoriDeploymentIdLabel: context.Name,
			utils.KumoriControllerLabel:   controllerName,
		}
		options := labels.SelectorFromSet(matchLabels)
		created, updated, deleted, errs = podDisruptionBudgetsManager.CreateUpdateOrDeleteList(c.kubeClientset, c.podDisruptionBudgetsLister, podDisruptionBudgets, options, context.Namespace)
	} else {
		created, updated, errs = podDisruptionBudgetsManager.CreateOrUpdateList(c.kubeClientset, c.podDisruptionBudgetsLister, podDisruptionBudgets)
	}
	changed := false
	if (created != nil) && (len(created) > 0) {
		changed = true
		for _, elem := range created {
			c.Recorder.Eventf(context.Deployment, v1.EventTypeNormal, utils.CreatedReason, "Deployment %s created", elem.GetName())
			log.Infof("%s. PodDisruptionBudget %s created", meth, elem.GetName())
		}
	}
	if (updated != nil) && (len(updated) > 0) {
		changed = true
		for _, elem := range updated {
			c.Recorder.Eventf(context.Deployment, v1.EventTypeNormal, utils.UpdatedReason, "Deployment %s updated", elem.GetName())
			log.Infof("%s. PodDisruptionBudget %s updated", meth, elem.GetName())
		}
	}
	if (deleted != nil) && (len(deleted) > 0) {
		changed = true
		for _, elem := range deleted {
			c.Recorder.Eventf(context.Deployment, v1.EventTypeNormal, utils.DeletedReason, "Deployment %s deleted", elem.GetName())
			log.Infof("%s. PodDisruptionBudget %s deleted", meth, elem.GetName())
		}
	}
	if !changed {
		log.Infof("%s. PodDisruptionBudget not changed", meth)
	}
	return errs
}

// updateServices checks if there is any significative difference between the expected and current
// Services and performs the necessary actions.
func (c *Controller) updateServices(context *context.Context, services []v1.Service, removeMissing bool) []error {
	meth := fmt.Sprintf("%s.updateServices. Release: '%s'", c.Name, context.ReleaseName)
	log.Debugf(meth)

	var created, updated, deleted []v1.Service
	var errs []error
	if removeMissing {
		matchLabels := map[string]string{
			utils.KumoriDeploymentIdLabel: context.Name,
			utils.KumoriControllerLabel:   controllerName,
		}
		options := labels.SelectorFromSet(matchLabels)
		created, updated, deleted, errs = servicesManager.CreateUpdateOrDeleteList(c.kubeClientset, c.servicesLister, services, options, context.Namespace)
	} else {
		created, updated, errs = servicesManager.CreateOrUpdateList(c.kubeClientset, c.servicesLister, services)
	}
	changed := false
	if (created != nil) && (len(created) > 0) {
		changed = true
		for _, elem := range created {
			c.Recorder.Eventf(context.Deployment, v1.EventTypeNormal, utils.CreatedReason, "Service %s created", elem.GetName())
			log.Infof("%s. Service %s created", meth, elem.GetName())
		}
	}
	if (updated != nil) && (len(updated) > 0) {
		changed = true
		for _, elem := range updated {
			c.Recorder.Eventf(context.Deployment, v1.EventTypeNormal, utils.UpdatedReason, "Service %s updated", elem.GetName())
			log.Infof("%s. Service %s updated", meth, elem.GetName())
		}
	}
	if (deleted != nil) && (len(deleted) > 0) {
		changed = true
		for _, elem := range deleted {
			c.Recorder.Eventf(context.Deployment, v1.EventTypeNormal, utils.DeletedReason, "Service %s deleted", elem.GetName())
			log.Infof("%s. Service %s deleted", meth, elem.GetName())
		}
	}
	if !changed {
		log.Infof("%s. Services not changed", meth)
	}
	return errs
}

// newDeployment main goal is to create a StatefulSet for each stateful role in a given
// V3Deployment. It also creates the ConfigMap storing the role configuration and
// the headless Service used by each created StatefulSet.
func (c *Controller) newStatefulSets(
	kcontext *context.Context,
) ([]appsv1.StatefulSet, []v1.Service, []v1.ConfigMap) {
	meth := fmt.Sprintf("%s.newStatefulSets. Release: '%s'", c.Name, kcontext.ReleaseName)
	log.Debug(meth)
	if ok := checkContext(kcontext); !ok {
		log.Errorf("%s. Context not loaded", meth)
		return nil, nil, nil
	}

	if kcontext.RolesInfo == nil {
		return []appsv1.StatefulSet{}, []v1.Service{}, []v1.ConfigMap{}
	}

	rolesInfo := kcontext.RolesInfo

	// Return empty lists if the roles map is nil
	if (rolesInfo == nil) || (len(*rolesInfo) == 0) {
		return []appsv1.StatefulSet{}, []v1.Service{}, []v1.ConfigMap{}
	}

	statefulSets := make([]appsv1.StatefulSet, 0, len(*rolesInfo))
	configMaps := make([]v1.ConfigMap, 0, len(*rolesInfo))
	services := make([]v1.Service, 0, len(*rolesInfo))
	for roleName, roleInfo := range *rolesInfo {
		if roleInfo.ComponentReference == nil {
			log.Infof("%s. Component not assigned to role %s. Skipping", meth, roleName)
			continue
		}
		if !roleInfo.Stateful {
			continue
		}
		appName := c.calculateAppName(kcontext, roleName)
		configMap, err := c.createConfigMap(kcontext, appName, &roleInfo)
		if err != nil {
			log.Errorf("%s. Error: %v", meth, err)
		} else {
			generator := generators.NewStatefulSetGenerator(
				appName,
				configMap,
				kcontext,
				controllerName,
				kcontext.Name,
				roleInfo.Name,
			)
			statefulSet, service, err := (*generator).Generate()
			if err == nil {
				if statefulSet != nil {
					statefulSets = append(statefulSets, *statefulSet)
				}

				if configMap != nil {
					configMaps = append(configMaps, *configMap)
				}

				if service != nil {
					services = append(services, *service)
				}
			} else {
				log.Errorf("%s. Error: %v", meth, err)
			}
		}
	}

	return statefulSets, services, configMaps
}

// newDeployments main goal is to create a Deployment for each stateless role in a given
// V3Deployment. It also creates the ConfigMap storing the role configuration and
// the headless Service used by each created StatefulSet.
func (c *Controller) newDeployments(
	kcontext *context.Context,
) ([]appsv1.Deployment, []v1.ConfigMap) {
	meth := fmt.Sprintf("%s.newDeployments. Release: '%s'", c.Name, kcontext.ReleaseName)
	log.Debug(meth)
	if ok := checkContext(kcontext); !ok {
		log.Errorf("%s. Context not loaded", meth)
		return nil, nil
	}

	if kcontext.RolesInfo == nil {
		return []appsv1.Deployment{}, []v1.ConfigMap{}
	}

	rolesInfo := kcontext.RolesInfo

	// Return empty lists if the roles map is nil
	if (rolesInfo == nil) || (len(*rolesInfo) == 0) {
		return []appsv1.Deployment{}, []v1.ConfigMap{}
	}

	deployments := make([]appsv1.Deployment, 0, len(*rolesInfo))
	configMaps := make([]v1.ConfigMap, 0, len(*rolesInfo))
	for roleName, roleInfo := range *rolesInfo {
		if roleInfo.ComponentReference == nil {
			log.Infof("%s. Component not assigned to role %s. Skipping", meth, roleName)
			continue
		}
		if roleInfo.Stateful {
			continue
		}
		appName := c.calculateAppName(kcontext, roleName)
		configMap, err := c.createConfigMap(kcontext, appName, &roleInfo)
		if err != nil {
			log.Errorf("%s. Error: %v", meth, err)
		} else {
			generator := generators.NewDeploymentGenerator(
				appName,
				configMap,
				kcontext,
				controllerName,
				kcontext.Name,
				roleInfo.Name,
			)
			deployment, err := (*generator).Generate()
			if err == nil {
				if deployment != nil {
					deployments = append(deployments, *deployment)
				}
				if configMap != nil {
					configMaps = append(configMaps, *configMap)
				}
			} else {
				log.Errorf("%s. Error: %v", meth, err)
			}
		}
	}

	return deployments, configMaps
}

// newPodDisruptionBudgets main goal is to create PodDisruptionBudget to limit the
// number of PODs evicted at the same time. This has been included to restrict the
// effect of the descheduler.
func (c *Controller) newPodDisruptionBudgets(
	kcontext *context.Context,
) []policyv1.PodDisruptionBudget {
	meth := fmt.Sprintf("%s.newPodDisruptionBudgets. Release: '%s'", c.Name, kcontext.ReleaseName)
	log.Debug(meth)
	if ok := checkContext(kcontext); !ok {
		log.Errorf("%s. Context not loaded", meth)
		return nil
	}

	if kcontext.RolesInfo == nil {
		return []policyv1.PodDisruptionBudget{}
	}

	rolesInfo := kcontext.RolesInfo

	// Return empty lists if the roles map is nil
	if (rolesInfo == nil) || (len(*rolesInfo) == 0) {
		return []policyv1.PodDisruptionBudget{}
	}

	podDisruptionBudgets := make([]policyv1.PodDisruptionBudget, 0, len(*rolesInfo))
	for roleName, roleInfo := range *rolesInfo {
		appName := c.calculateAppName(kcontext, roleName)
		generator := generators.NewPodDisruptionBudgetGenerator(
			appName,
			kcontext,
			controllerName,
			kcontext.Name,
			roleInfo.Name,
		)
		podDisruptionBudget, err := (*generator).Generate()
		if err == nil {
			if podDisruptionBudget != nil {
				podDisruptionBudgets = append(podDisruptionBudgets, *podDisruptionBudget)
			}
		} else {
			log.Errorf("%s. Error: %v", meth, err)
		}
	}

	return podDisruptionBudgets
}

func (c *Controller) newConnectorServices(kcontext *context.Context) []v1.Service {
	meth := fmt.Sprintf("%s.newConnectorServices. Release: '%s'", c.Name, kcontext.ReleaseName)
	log.Debug(meth)

	if ok := checkContext(kcontext); !ok {
		log.Errorf("%s. Context not loaded", meth)
		return nil
	}

	if kcontext.ConnectorsInfo == nil {
		return []v1.Service{}
	}

	// connectorNames := GetOrderedConnectorsName(kcontext.ConnectorsInfo)
	connectorNames := kusort.SortKeys(kcontext.ConnectorsInfo)
	services := make([]v1.Service, 0, len(connectorNames))
	for _, cname := range connectorNames {

		// Creates a headless service to keep track of the pairs instance/channels (IP/PORT) reachable
		// from connector "cname"
		value := (*kcontext.ConnectorsInfo)[cname]
		generator := generators.NewHeadlessServiceGenerator(value.Name, kcontext, controllerName, kcontext.Name, value.Port, nil, true)
		service, err := generator.Generate()
		if err != nil {
			log.Errorf("%s. Error creating headless service for connector %s: %v", meth, value.Name, err)
		} else {
			if service != nil {
				services = append(services, *service)
			}
		}

		// Creates a headless service to keep track of the pairs instance/channels (IP/PORT) reachable
		// from connector "cname" but only if they belong to a ready instance
		value = (*kcontext.ConnectorsInfo)[cname]
		generator = generators.NewHeadlessServiceGenerator(value.Name, kcontext, controllerName, kcontext.Name, value.Port, nil, false)
		service, err = generator.Generate()
		if err != nil {
			log.Errorf("%s. Error creating headless ready service for connector %s: %v", meth, value.Name, err)
		} else {
			if service != nil {
				services = append(services, *service)
			}
		}

		// If the connector is a load balancer, add an extra average service as a balancer between the
		// pairs instance/channel (IP/PORT) reachable from the connector
		if value.Type == context.LoadBalancerConnector {
			generator := generators.NewBalancerServiceGenerator(value.Name, kcontext, controllerName, kcontext.Name, value.Port, nil)
			service, err := generator.Generate()
			if err != nil {
				log.Errorf("%s. Error creating balancer service for connector %s: %v", meth, value.Name, err)
			} else {
				if service != nil {
					services = append(services, *service)
				}
			}
		}
	}
	return services
}

func (c *Controller) newNetworkPolicies(kcontext *context.Context) []netv1.NetworkPolicy {
	meth := fmt.Sprintf("%s.newNetworkPolicies. Release: '%s'", c.Name, kcontext.ReleaseName)
	log.Debug(meth)
	if ok := checkContext(kcontext); !ok {
		log.Errorf("%s. Context not loaded", meth)
		return nil
	}

	if kcontext.ConnectorsInfo == nil {
		return []netv1.NetworkPolicy{}
	}

	connectors := *kcontext.Deployment.Spec.Artifact.Description.Connectors
	// NOTE: the policies array capacity is initalised to three times the number of connectors.
	//       If there more than three ingress and egress policies are generated per connector,
	//       then a new array will be created with the capacity increased. Currently, only two
	//       policies are created per connector (one ingress policy and one inbound policy),
	//       three if egress policies are included again.
	policies := make([]netv1.NetworkPolicy, 0, len(connectors)*3)
	for id := range *kcontext.ConnectorsInfo {
		generator := generators.NewConnectorNetworkPolicyGenerator(id, kcontext, controllerName, kcontext.Name)
		if connectorPolicies, err := generator.Generate(); err != nil {
			log.Errorf("%s. Error creating network policies for connector %s: %v", meth, id, err)
		} else {
			// NOTE: egress policies disabled
			// policies = append(policies, *ingressPolicy, *egressPolicy)
			if connectorPolicies != nil {
				policies = append(policies, connectorPolicies...)
			}
		}
	}

	return policies
}

// newSecrets creats the expected Secrets to run the V3Deployment being processed.
func (c *Controller) newSecrets(kcontext *context.Context) []v1.Secret {
	meth := fmt.Sprintf("%s.newSecrets. Release: '%s'", c.Name, kcontext.ReleaseName)
	log.Debug(meth)
	if ok := checkContext(kcontext); !ok {
		log.Errorf("%s. Context not loaded", meth)
		return nil
	}

	generator := generators.NewSecretGenerator(kcontext, controllerName, *kcontext.KumoriOwner)
	secrets, err := generator.Generate()
	if err != nil {
		log.Errorf("%s. Error creating secrets: %s", meth, err.Error())
	}
	return *secrets
}

// createConfigMap creates the config map related to a given KukuService role.
func (c *Controller) createConfigMap(
	kcontext *context.Context,
	appName string,
	roleInfo *context.RoleInfo,
) (*v1.ConfigMap, error) {
	meth := fmt.Sprintf("%s.createConfigMap. Release: '%s'", c.Name, kcontext.ReleaseName)
	log.Debug(meth)
	generator := generators.NewConfigMapGenerator(appName, kcontext, controllerName, kcontext.Name, roleInfo.Name)
	return generator.Generate()
}

// checkContext checks if a given context is valid.
func checkContext(kcontext *context.Context) bool {
	meth := fmt.Sprintf("%s.checkContext. Release: '%s'", controllerName, kcontext.ReleaseName)
	log.Debug(meth)
	if kcontext == nil {
		log.Errorf("%s. Context empty", meth)
		return false
	}
	if !kcontext.Loaded {
		log.Errorf("%s. Context not loaded", meth)
		return false
	}
	return true
}

func (c *Controller) calculateAppName(
	kcontext *context.Context,
	roleName string,
) (appName string) {
	releaseName := kcontext.ReleaseName
	roleNameHash := hashutils.Hash(roleName)
	appName = fmt.Sprintf("%s-%s", releaseName, roleNameHash)
	return
}
