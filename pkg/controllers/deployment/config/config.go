/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package config

import "time"

// ControllerConfig contains the controller configuration parameters
type Config struct {
	DefaultLivenessInitialDelay            time.Duration
	DefaultLivenessFailureThreshold        int32
	DefaultLivenessPeriod                  time.Duration
	DefaultLivenessTimeout                 time.Duration
	LivenessHighFrequencyPeriod            time.Duration
	LivenessMediumFrequencyPeriod          time.Duration
	LivenessLowFrequencyPeriod             time.Duration
	DefaultReadinessInitialDelay           time.Duration
	DefaultReadinessFailureThreshold       int32
	DefaultReadinessPeriod                 time.Duration
	DefaultReadinessTimeout                time.Duration
	ReadinessHighFrequencyPeriod           time.Duration
	ReadinessMediumFrequencyPeriod         time.Duration
	ReadinessLowFrequencyPeriod            time.Duration
	DefaultStartupInitialDelay             time.Duration
	DefaultStartupFailureThreshold         int32
	DefaultStartupPeriod                   time.Duration
	DefaultStartupTimeout                  time.Duration
	EnableStartupProbe                     bool
	DisablePersistentVolumes               bool
	ForceRebootOnUpdate                    bool
	UsePvcForVolatileVolumes               bool
	DefaultVolatileVolumesType             string
	TopologySpreadConstraintsSoftLabelKeys string
	TopologySpreadConstraintsHardLabelKeys string
	TopologySpreadConstraintsMaxSkew       int32
	UserPodsPriorityClass                  string
	RevisionHistoryLimit                   int32
	DefaultDiskRequestSize                 int32
	DefaultDiskRequestUnit                 string
	FsGroup                                string
	DefaultFsGroup                         int64
	DefaultMappingEnableTemplate           bool
	EnableNodeAffinities                   bool
	HpaEnabledLabelKey                     string
	ClusterConfigMapName                   string
	ClusterConfigMapNamespace              string
	ClusterConfigMapKey                    string
	ExternalServicesAccess                 *map[string]ExternalServiceAccess
}

type ExternalServiceAccess struct {
	Source ExternalServiceAccessSource
	Target ExternalServiceAccessTarget
}

type ExternalServiceAccessSource struct {
	Namespace *string
	Label     *ExternalServiceAccessSourceLabel
}

type ExternalServiceAccessSourceLabel struct {
	Key   string
	Value string
}

type ExternalServiceAccessTarget struct {
	Owners      []string
	Deployments []string
}

type FsGroupValue string

const (
	DisabledFsGroupValues FsGroupValue = "disabled"
	FixedFsGroupValues    FsGroupValue = "fixed"
	UserFsGroupValues     FsGroupValue = "user"
)
