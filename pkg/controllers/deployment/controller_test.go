/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package deployment

import (
	"fmt"
	"testing"
	"time"

	log "github.com/sirupsen/logrus"

	kumoriv1 "gitlab.com/kumori-systems/community/libraries/client-go/pkg/apis/kumori/v1"
	kumoriclientset "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated/clientset/versioned/fake"
	kumoriinformers "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated/informers/externalversions"

	utils "kuku-controller/pkg/utils"

	// "k8s.io/client-go/informers/core/v1"

	appsv1 "k8s.io/api/apps/v1"
	v1 "k8s.io/api/core/v1"
	netv1 "k8s.io/api/networking/v1"
	"k8s.io/apimachinery/pkg/runtime"

	// apiextensionsv1beta1 "k8s.io/apiextensions-apiserver/pkg/apis/apiextensions/v1beta1"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	kubeinformers "k8s.io/client-go/informers"
	kubeclientset "k8s.io/client-go/kubernetes/fake"
	cache "k8s.io/client-go/tools/cache"

	core "k8s.io/client-go/testing"
)

const threadiness int = 1
const ns string = "kumori"

type fixture struct {
	t *testing.T

	kubeClientset   *kubeclientset.Clientset
	kumoriClientset *kumoriclientset.Clientset
	// Objects to put in the store.
	v3Deployments []*kumoriv1.V3Deployment
	kukusecrets   []*kumoriv1.KukuSecret
	kukuvolumes   []*kumoriv1.KukuVolume
	// Objects from here preloaded into NewSimpleFake.
	kubeobjects   []runtime.Object
	kumoriobjects []runtime.Object
	// Keys to be processed
	keys []string
}

func newFixture(t *testing.T) *fixture {
	f := &fixture{}
	f.t = t
	f.kumoriobjects = []runtime.Object{}
	f.kubeobjects = []runtime.Object{}
	f.v3Deployments = []*kumoriv1.V3Deployment{}
	f.kukusecrets = []*kumoriv1.KukuSecret{}
	f.kukuvolumes = []*kumoriv1.KukuVolume{}
	return f
}

func (f *fixture) run() []core.Action {
	return f.runController()
}

func (f *fixture) runController() []core.Action {
	// Set up signals so we handle the shutdown signal gracefully
	stopCh := make(chan struct{})
	defer close(stopCh)

	resyncInterval := time.Duration(0)

	kubeClientset := kubeclientset.NewSimpleClientset(f.kubeobjects...)
	kubeInformerFactory := kubeinformers.NewFilteredSharedInformerFactory(
		kubeClientset,
		resyncInterval,
		ns,
		func(opt *metav1.ListOptions) {},
	)

	kumoriClientset := kumoriclientset.NewSimpleClientset(f.kumoriobjects...)
	kumoriInformerFactory := kumoriinformers.NewFilteredSharedInformerFactory(
		kumoriClientset,
		resyncInterval,
		ns,
		func(opt *metav1.ListOptions) {},
	)

	v3DeploymentController := NewController(
		kubeClientset,
		kumoriClientset,
		kubeInformerFactory.Apps().V1().Deployments(),
		kubeInformerFactory.Apps().V1().StatefulSets(),
		kubeInformerFactory.Core().V1().ConfigMaps(),
		kubeInformerFactory.Core().V1().Services(),
		kubeInformerFactory.Networking().V1().NetworkPolicies(),
		kubeInformerFactory.Apps().V1().ControllerRevisions(),
		kubeInformerFactory.Core().V1().Secrets(),
		kubeInformerFactory.Policy().V1().PodDisruptionBudgets(),
		kumoriInformerFactory.Kumori().V1().V3Deployments(),
		kumoriInformerFactory.Kumori().V1().KukuLinks(),
		kumoriInformerFactory.Kumori().V1().KukuSecrets(),
		kumoriInformerFactory.Kumori().V1().KukuVolumes(),
		kumoriInformerFactory.Kumori().V1().KukuCas(),
		kumoriInformerFactory.Kumori().V1().KukuCerts(),
		kumoriInformerFactory.Kumori().V1().KukuDomains(),
		kumoriInformerFactory.Kumori().V1().KukuPorts(),
	)

	kubeInformerFactory.Start(stopCh)
	kumoriInformerFactory.Start(stopCh)

	time.Sleep(500 * time.Millisecond)

	for _, ksecret := range f.kukusecrets {
		if err := kumoriInformerFactory.Kumori().V1().KukuSecrets().Informer().GetIndexer().Add(ksecret); err != nil {
			f.t.Fatal(err)
		}
	}

	for _, kvolume := range f.kukuvolumes {
		if err := kumoriInformerFactory.Kumori().V1().KukuVolumes().Informer().GetIndexer().Add(kvolume); err != nil {
			f.t.Fatal(err)
		}
	}

	for _, deployment := range f.v3Deployments {
		if err := kumoriInformerFactory.Kumori().V1().V3Deployments().Informer().GetIndexer().Add(deployment); err != nil {
			f.t.Fatal(err)
		}
	}

	errorCh := make(chan error)
	go v3DeploymentController.Run(1, stopCh, errorCh)

	for _, key := range f.keys {
		if err := v3DeploymentController.SyncHandler(key); err != nil {
			f.t.Fatal("main.main() Error running controller", err)
			return nil
		}
	}

	actions := kubeClientset.Actions()
	if actions == nil {
		f.t.Fatal("main.main(). Controller did nothing.")
		return nil
	}
	actions = filterInformerActions(actions)

	return actions

}

// filterInformerActions filters list and watch actions for testing resources.
// Since list and watch don't change resource state we can filter it to lower
// nose level in our tests.
func filterInformerActions(actions []core.Action) []core.Action {
	ret := []core.Action{}
	for _, action := range actions {
		if (action.GetNamespace() == ns) &&
			(action.Matches("list", "configmaps") ||
				action.Matches("watch", "configmaps") ||
				action.Matches("list", "networkpolicies") ||
				action.Matches("watch", "networkpolicies") ||
				action.Matches("list", "deployments") ||
				action.Matches("watch", "deployments") ||
				action.Matches("list", "statefulsets") ||
				action.Matches("watch", "statefulsets") ||
				action.Matches("list", "services") ||
				action.Matches("watch", "services")) {
			continue
		}
		ret = append(ret, action)
	}

	return ret
}

func (f *fixture) findResource(verb, resource, name, namespace string, actions []core.Action) metav1.Object {
	for _, action := range actions {
		if !action.Matches(verb, resource) {
			continue
		}
		obj, err := getActionObject(action)
		if err != nil {
			f.t.Errorf("%v", err)
			continue
		}

		if (obj.GetName() == name) && (obj.GetNamespace() == namespace) {
			return obj
		}

	}

	f.t.Fatalf("Action '%s' not found for resource '%s' in namespace '%s'", verb, name, namespace)
	return nil
}

func getActionObject(action core.Action) (metav1.Object, error) {
	var obj runtime.Object
	switch actionType := action.(type) {
	case core.CreateAction:
		createAction, ok := action.(core.CreateAction)
		if !ok {
			return nil, fmt.Errorf("Error processing action of type '%T'", actionType)
		}
		obj = createAction.GetObject()
	case core.UpdateAction:
		updateAction, ok := action.(core.CreateAction)
		if !ok {
			return nil, fmt.Errorf("Error processing action of type '%T'", actionType)
		}
		obj = updateAction.GetObject()
	case core.DeleteAction:
		deleteAction, ok := action.(core.CreateAction)
		if !ok {
			return nil, fmt.Errorf("Error processing action of type '%T'", actionType)
		}
		obj = deleteAction.GetObject()
	}

	if obj == nil {
		return nil, fmt.Errorf("Object not fount for action '%v'", action)
	}
	metaobj, err := getMetavObject(obj)
	if err != nil {
		return nil, err
	}
	return metaobj, nil

}

func getMetavObject(runObj runtime.Object) (metav1.Object, error) {
	switch objType := runObj.(type) {
	case *v1.Service:
		return runObj.(metav1.Object), nil
	case *v1.ConfigMap:
		return runObj.(metav1.Object), nil
	case *appsv1.Deployment:
		return runObj.(metav1.Object), nil
	case *appsv1.StatefulSet:
		return runObj.(metav1.Object), nil
	case *netv1.NetworkPolicy:
		return runObj.(metav1.Object), nil
	default:
		return nil, fmt.Errorf("Unknown type '%v'", objType)
	}
}

func (f *fixture) loadExample() {

	var deployment kumoriv1.V3Deployment
	err := utils.LoadObjectFromYAML(&deployment, "../../../artifacts/examples/deployment.yaml")
	if err != nil {
		f.t.Errorf("%v", err)
	}

	f.v3Deployments = append(f.v3Deployments, &deployment)

	var ksecret kumoriv1.KukuSecret
	err = utils.LoadObjectFromYAML(&ksecret, "../../../artifacts/examples/kukusecret.yaml")
	if err != nil {
		f.t.Errorf("%v", err)
	}

	f.kukusecrets = append(f.kukusecrets, &ksecret)

	var kvolume kumoriv1.KukuVolume
	err = utils.LoadObjectFromYAML(&kvolume, "../../../artifacts/examples/kukuvolume.yaml")
	if err != nil {
		f.t.Errorf("%v", err)
	}

	f.kukuvolumes = append(f.kukuvolumes, &kvolume)
}

func TestController(t *testing.T) {
	log.SetFormatter(&log.TextFormatter{
		ForceColors: true,
	})

	log.SetLevel(log.DebugLevel)

	// NOTE: It seems that UpdateStatus method is not working when the fake clients-set
	// are used in tests. Actually, the StatefulSet controller seems to patch the UpdateStatus
	// method when the tests are exectuted. Due to this, THE TESTS ARE CURRENTLY DISABLED

	f := newFixture(t)

	f.loadExample()

	keys := make([]string, 0, len(f.v3Deployments))
	for _, deployment := range f.v3Deployments {
		key, err := cache.DeletionHandlingMetaNamespaceKeyFunc(deployment)
		if err != nil {
			f.t.Fatal("main.main() Error running controller", err)
			return
		}
		keys = append(keys, key)
	}
	f.keys = keys

	// actions := f.run()
	f.run()

	// for _, action := range actions {
	// 	obj, err := getActionObject(action)
	// 	if err != nil {
	// 		t.Fatal(err)
	// 	}
	// 	fmt.Printf("--------------------------------> ACTIONS: %s\n", action.GetVerb())
	// 	fmt.Printf("                                  RESOURCE TYPE: %s\n", action.GetResource().Resource)
	// 	fmt.Printf("                                  TYPE: %s\n", reflect.TypeOf(action))
	// 	fmt.Printf("                                  NAME: %s\n", obj.GetName())
	// 	fmt.Printf("                                  NAMESPACE: %s\n", obj.GetNamespace())
	// }

	// // EXPECTED INGRESSS NETWORK POLICIES
	// f.findResource("create", "networkpolicies", "hazelcast_deployment-datum-mancenter-cmanagein-0-management-apiserver-network-policy", ns, actions)
	// f.findResource("create", "networkpolicies", "hazelcast_deployment-datumbis-mancenter-cmanagein-0-management-apiserver-network-policy", ns, actions)
	// f.findResource("create", "networkpolicies", "hazelcast_deployment-datum-group-cmembers-0-datum-group-network-policy", ns, actions)
	// f.findResource("create", "networkpolicies", "hazelcast_deployment-datumbis-group-cmembersbis-0-datumbis-group-network-policy", ns, actions)

	// // EXPECTED INGRESSS INBOUND POLICIES
	// f.findResource("create", "networkpolicies", "hazelcast_deployment-inbound-cdatain-0-datum-rest-network-policy", ns, actions)
	// f.findResource("create", "networkpolicies", "hazelcast_deployment-inbound-cmanagein-0-management-apiserver-network-policy", ns, actions)

	// // EXPECTED CONFIG MAPS
	// f.findResource("create", "configmaps", "hazelcast_deployment-management-configmap", ns, actions)
	// f.findResource("create", "configmaps", "hazelcast_deployment-datum-configmap", ns, actions)
	// f.findResource("create", "configmaps", "hazelcast_deployment-datumbis-configmap", ns, actions)

	// // EXPECTED DEPLOYMENTS
	// f.findResource("create", "deployments", "hazelcast_deployment-management-deployment", ns, actions)

	// // EXPECTED STATEFUL SETS
	// f.findResource("create", "statefulsets", "hazelcast_deployment-datum-deployment", ns, actions)
	// f.findResource("create", "statefulsets", "hazelcast_deployment-datumbis-deployment", ns, actions)

	// // EXPECTED SERVICES
	// f.findResource("create", "services", "cdatain-0-hazelcast_deployment", ns, actions)
	// f.findResource("create", "services", "cdatain-0-hazelcast_deployment-lb", ns, actions)
	// f.findResource("create", "services", "cmanagein-0-hazelcast_deployment", ns, actions)
	// f.findResource("create", "services", "cmanagein-0-hazelcast_deployment-lb", ns, actions)
	// f.findResource("create", "services", "cmembers-0-hazelcast_deployment", ns, actions)
	// f.findResource("create", "services", "cmembersbis-0-hazelcast_deployment", ns, actions)
}
