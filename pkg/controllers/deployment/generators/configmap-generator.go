/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package generators

import (
	"encoding/json"
	"fmt"
	"kuku-controller/pkg/utils"
	hashutil "kuku-controller/pkg/utils/hash"
	"kuku-controller/pkg/utils/sort"

	context "kuku-controller/pkg/controllers/deployment/context"

	log "github.com/sirupsen/logrus"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// ConfigMapGenerator generates ConfigMaps
type ConfigMapGenerator struct {
	AppName        string
	Context        *context.Context
	ControllerName string
	Owner          string
	Translations   map[string]string
	RoleInfo       *context.RoleInfo
}

// CreateConfigMapName calculates the name of the ConfigMap storing a given role exposed files
func CreateConfigMapName(appName string) string {
	return fmt.Sprintf("%s-configmap", appName)
}

// NewConfigMapGenerator creates and returns a new ConfigMap generator
func NewConfigMapGenerator(
	appName string,
	kcontext *context.Context,
	controllerName string,
	owner string,
	role string,
) (generator *ConfigMapGenerator) {
	meth := fmt.Sprintf("generators.configmaps.NewConfigMapGenerator. AppName: %s ControllerName: %s Owner: %s Role: %s", appName, controllerName, owner, role)
	log.Debugf(meth)

	roleInfo := (*kcontext.RolesInfo)[role]
	return &ConfigMapGenerator{
		AppName:        appName,
		Context:        kcontext,
		ControllerName: controllerName,
		Owner:          owner,
		RoleInfo:       &roleInfo,
		Translations:   map[string]string{},
	}
}

// Generate creates a ConfigMap containing the exposed files for a single role. The files
// are stored as byte arrays in the `BinaryData` section.
func (generator *ConfigMapGenerator) Generate() (*v1.ConfigMap, error) {
	meth := fmt.Sprintf("generators.configmaps.Generate. AppName: %s", generator.AppName)
	log.Debugf("%s. appName=%s, namespace=%s, releaseName=%s", meth, generator.AppName, generator.Context.ReleaseName, generator.Context.ReleaseName)
	name := CreateConfigMapName(generator.AppName)
	return generator.calculateConfigMap(name)
}

// calculateConfigMap calculates a role ConfigMap
func (generator *ConfigMapGenerator) calculateConfigMap(
	name string,
) (*v1.ConfigMap, error) {
	meth := "generators.configmaps.calculateConfigMap"
	log.Debugf("%s. role=%s", meth, name)

	if generator.RoleInfo.ComponentReference == nil {
		log.Infof("%s. AppName=%s role=%s. Role has not a component assigned. Skipping", meth, generator.AppName, name)
		return nil, nil
	}

	exposedFiles, err := generator.calculateExposedFiles()
	if err != nil {
		return nil, err
	}

	labels := generator.generateLabels()
	annotations := generator.generateAnnotations()

	configMap := v1.ConfigMap{
		ObjectMeta: metav1.ObjectMeta{
			Name:            name,
			Namespace:       generator.Context.Namespace,
			OwnerReferences: *generator.Context.Owners,
		},
		BinaryData: *exposedFiles,
	}
	if labels != nil {
		configMap.ObjectMeta.Labels = *labels
	}
	if annotations != nil {
		configMap.ObjectMeta.Annotations = *annotations
	}
	// content, _ := json.MarshalIndent(configMap, "", "  ")
	// fmt.Printf("\n\n------->CONFIGMAP: %s", string(content))
	return &configMap, nil
}

// calculateExposedFiles calculates which files are going to be exposed for this role
func (generator *ConfigMapGenerator) calculateExposedFiles() (*map[string][]byte, error) {

	roleInfo := *generator.RoleInfo

	meth := fmt.Sprintf("generators.configmaps.calculateExposedFiles. AppName: %s. Role: %s", generator.AppName, roleInfo.Name)

	containersSet := []*map[string]context.ContainerInfo{}
	if roleInfo.ContainersInfo != nil {
		containersSet = append(containersSet, roleInfo.ContainersInfo)
	}
	if roleInfo.InitContainersInfo != nil {
		containersSet = append(containersSet, roleInfo.InitContainersInfo)
	}

	if len(containersSet) <= 0 {
		log.Infof("%s. Containers list is empty", meth)
		return &map[string][]byte{}, nil
	}

	// Initializes the structures holding the exposed files
	data := map[string][]byte{}

	for _, containersInfo := range containersSet {
		keys := sort.SortKeys(containersInfo)
		for _, key := range keys {
			containerInfo := (*containersInfo)[key]
			if containerInfo.Files == nil {
				continue
			}
			for _, fileInfo := range containerInfo.Files {
				if fileInfo.Data == nil {
					continue
				}
				data[fileInfo.Name] = fileInfo.Data
			}
		}
	}

	// Returns exposed files data
	return &data, nil
}

// generateLabels returns the labels to be added toConfigMaps
func (generator *ConfigMapGenerator) generateLabels() *map[string]string {
	labels := map[string]string{
		utils.KumoriControllerLabel:   generator.ControllerName,
		utils.KumoriRoleLabel:         hashutil.Hash(generator.RoleInfo.Name),
		utils.KumoriDeploymentIdLabel: generator.Context.Name,
	}
	if generator.Context.KumoriOwner != nil {
		labels[utils.KumoriOwnerLabel] = hashutil.Hash(*generator.Context.KumoriOwner)
	}
	return &labels
}

// generateAnnotations returns the annotations to be added to ConfigMaps
func (generator *ConfigMapGenerator) generateAnnotations() *map[string]string {
	meth := "generators.configmaps.generateAnnotations"

	annotations := map[string]string{
		utils.KumoriRoleLabel: generator.RoleInfo.Name,
	}
	if generator.Context.KumoriOwner != nil {
		annotations[utils.KumoriOwnerLabel] = *generator.Context.KumoriOwner
	}

	if len(generator.Translations) > 0 {
		content, err := json.Marshal(generator.Translations)
		if err != nil {
			log.Errorf("%s. Error creating annotation: %s", meth, err.Error())
		} else {
			annotations[utils.KumoriTranslations] = string(content)
		}
	}

	return &annotations
}
