/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package generators

import (
	"encoding/json"
	"fmt"
	"kuku-controller/pkg/utils"
	hashutil "kuku-controller/pkg/utils/hash"
	naming "kuku-controller/pkg/utils/naming"

	log "github.com/sirupsen/logrus"

	"k8s.io/apimachinery/pkg/util/intstr"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	"kuku-controller/pkg/controllers/deployment/config"
	context "kuku-controller/pkg/controllers/deployment/context"

	netv1 "k8s.io/api/networking/v1"
)

// NetworkPolicyGenerator generates a NetworkPolicy to restrict connection paths between
// PODs
type NetworkPolicyGenerator struct {
	ConnectorID    string
	Context        *context.Context
	ControllerName string
	Translations   map[string]string
	Owner          string
}

// CreateIngressNetworkPolicyName calculates the ingress NetworkPolicy name
func CreateIngressNetworkPolicyName(
	connectorID string,
	fromEndpoint *context.ConnectorEndpoint,
	toEndpoint *context.ConnectorEndpoint,
) string {
	var name string
	if fromEndpoint.Deployment == toEndpoint.Deployment {
		hashedPart := hashutil.Hash(fmt.Sprintf("%s-%s-%s-%s-%s", *fromEndpoint.Role, fromEndpoint.ChannelName, connectorID, *toEndpoint.Role, toEndpoint.ChannelName))
		name = fmt.Sprintf("%s-%s", fromEndpoint.Deployment, hashedPart)
	} else {
		hashedPart := hashutil.Hash(fmt.Sprintf("%s-%s-%s-%s-%s", *fromEndpoint.Role, fromEndpoint.ChannelName, connectorID, *toEndpoint.Role, toEndpoint.ChannelName))
		name = fmt.Sprintf("%s-%s-%s", fromEndpoint.Deployment, toEndpoint.Deployment, hashedPart)
	}
	return fmt.Sprintf("%s-network-policy", name)
}

// CreateInboundNetworkPolicyName calculates the ingress NetworkPolicy name
func CreateInboundNetworkPolicyName(
	connectorID string,
	toEndpoint *context.ConnectorEndpoint,
) string {
	hashedPart := hashutil.Hash(fmt.Sprintf("%s-%s-%s", connectorID, *toEndpoint.Role, toEndpoint.ChannelName))
	return fmt.Sprintf("%s-inbound-%s-network-policy", toEndpoint.Deployment, hashedPart)
}

// CreateExternalNameNetworkPolicyName calculates the ingress NetworkPolicy name
func CreateExternalNameNetworkPolicyName(
	connectorID string,
	toEndpoint *context.ConnectorEndpoint,
	externalServiceName string,
) string {
	hashedPart := hashutil.Hash(fmt.Sprintf("%s-%s-%s-%s", externalServiceName, connectorID, *toEndpoint.Role, toEndpoint.ChannelName))
	return fmt.Sprintf("%s-external-%s-network-policy", toEndpoint.Deployment, hashedPart)
}

// NewConnectorNetworkPolicyGenerator creates NetworkPolicies to enable communication between
// the PODs of a given connector.
func NewConnectorNetworkPolicyGenerator(
	connectorID string,
	kcontext *context.Context,
	controllerName string,
	owner string,
) (generator *NetworkPolicyGenerator) {
	meth := fmt.Sprintf("generators.networkpolicies.NewConnectorNetworkPolicyGenerator. ConnectorID: %s ControllerName: %s Owner: %s", connectorID, controllerName, owner)
	log.Debugf(meth)
	return newNetworkPolicyGenerator(connectorID, kcontext, controllerName, owner)
}

// NewNetworkPolicyGenerator creates a new NetworkPolicy generator
func newNetworkPolicyGenerator(
	connectorID string,
	kcontext *context.Context,
	controllerName string,
	owner string,
) (generator *NetworkPolicyGenerator) {
	meth := fmt.Sprintf("generators.networkpolicies.newNetworkPolicyGenerator. ConnectorID: %s ControllerName: %s Owner: %s", connectorID, controllerName, owner)
	log.Debugf(meth)
	if connectorID == "" {
		log.Errorf("%s. Error: provided an empty connector", meth)
		return nil
	}
	if kcontext == nil {
		log.Errorf("%s. Error: provided a null context", meth)
		return nil
	}
	if _, ok := (*kcontext.ConnectorsInfo)[connectorID]; !ok {
		log.Errorf("%s. Error: cannot retrieve connector info", meth)
		return nil
	}
	return &NetworkPolicyGenerator{
		ConnectorID:    connectorID,
		Context:        kcontext,
		ControllerName: controllerName,
		Owner:          owner,
		Translations:   map[string]string{},
	}
}

// Generate generates the network policies for a given connector. For each connector two policies are
// created:
// - Ingress: only depended channels can access to provided channels
// - Egress: only provided channels can be accessed from depended channels --> DISABLED
func (generator *NetworkPolicyGenerator) Generate() (
	[]netv1.NetworkPolicy,
	error,
) {
	meth := fmt.Sprintf("generators.networkpolicies.Generate. ConnectorId: %s", generator.ConnectorID)
	log.Debug(meth)

	policies := make([]netv1.NetworkPolicy, 0, 3)

	connectorInfo := (*generator.Context.ConnectorsInfo)[generator.ConnectorID]

	var kerr error

	for _, serverEndpoint := range connectorInfo.Servers {

		// Adds network policies to allow access from the connector clients
		for _, clientEndpoint := range connectorInfo.Clients {
			pairPolicies, err := generator.createPairPolicies(clientEndpoint, serverEndpoint)
			if err != nil {
				kerr = err
			}
			policies = append(policies, pairPolicies...)
		}

		// Adds network policies to allow access from the linked clients
		for _, linkedClientEndpoint := range connectorInfo.LinkedClients {

			// Cheks that this linked endpoint is not also directly connected
			// to this connector. This is very very unlikely since this means
			// that a role/client can reach a role/server either through a
			// connector and through links.
			found := false
			for _, clientEndpoint := range connectorInfo.Clients {
				if clientEndpoint.ChannelName == linkedClientEndpoint.ChannelName &&
					((clientEndpoint.Role == nil && linkedClientEndpoint.Role == nil) ||
						(clientEndpoint.Role != nil &&
							linkedClientEndpoint.Role != nil &&
							*clientEndpoint.Role == *linkedClientEndpoint.Role)) &&
					clientEndpoint.Deployment == linkedClientEndpoint.Deployment {
					found = true
					break
				}
			}
			if !found {
				pairPolicies, err := generator.createPairPolicies(linkedClientEndpoint, serverEndpoint)
				if err != nil {
					kerr = err
				}
				policies = append(policies, pairPolicies...)
			}
		}
	}

	if kerr != nil {
		return policies, kerr
	}

	// content, _ := json.MarshalIndent(policies, "", "  ")
	// fmt.Printf("\n\n------->NETWORKPOLICIES:\n%s\n", string(content))

	return policies, nil
}

func (generator *NetworkPolicyGenerator) createPairPolicies(
	clientEndpoint *context.ConnectorEndpoint,
	serverEndpoint *context.ConnectorEndpoint,
) ([]netv1.NetworkPolicy, error) {
	if (serverEndpoint.Role != nil) && (clientEndpoint.Role != nil) {
		name := CreateIngressNetworkPolicyName(generator.ConnectorID, clientEndpoint, serverEndpoint)
		ingressPolicy, err := generator.calculateIngressNetworkPolicy(name, clientEndpoint, serverEndpoint)
		if err != nil {
			return nil, err
		}
		return []netv1.NetworkPolicy{*ingressPolicy}, nil
	}

	// If the client channel is a service channel, create an inbound network policy
	if (serverEndpoint.Role != nil) && (clientEndpoint.Role == nil) {
		networkPolicies := []netv1.NetworkPolicy{}
		name := CreateInboundNetworkPolicyName(generator.ConnectorID, serverEndpoint)
		inboundPolicy, err := generator.calculateInboundNetworkPolicy(name, serverEndpoint)
		if err != nil {
			return nil, err
		}
		networkPolicies = append(networkPolicies, *inboundPolicy)

		// Task 944: if configured, allow some external services (services deployed in the kubernetes cluster
		// but not as a kumori services) to access this service.
		externalServices := generator.Context.Config.ExternalServicesAccess
		if (generator.Context.KumoriOwner != nil) &&
			(externalServices != nil) &&
			(len(*externalServices) > 0) {
			serviceOwner := *generator.Context.KumoriOwner
			serviceName := fmt.Sprintf("%s/%s", generator.Context.RegistrationDomain, generator.Context.RegistrationName)
			for externalName, externalData := range *externalServices {
				name := CreateExternalNameNetworkPolicyName(generator.ConnectorID, serverEndpoint, externalName)
				ownerMatch := (len(externalData.Target.Owners) <= 0)
				for _, owner := range externalData.Target.Owners {
					if owner == serviceOwner {
						ownerMatch = true
					}
				}
				if !ownerMatch {
					continue
				}
				deploymentMatch := (len(externalData.Target.Deployments) <= 0)
				for _, deployment := range externalData.Target.Deployments {
					if deployment == serviceName {
						deploymentMatch = true
					}
				}
				if !deploymentMatch {
					continue
				}
				externalPolicy, err := generator.calculateExternalAccessNetworkPolicy(name, externalName, &externalData, serverEndpoint)
				if err != nil {
					return nil, err
				}
				networkPolicies = append(networkPolicies, *externalPolicy)
			}
		}
		return networkPolicies, nil
	}

	return nil, nil
}

// calculateIngressNetworkPolicy creates a NewtorkPolicy to enable connections from
// "from.role" to the channel "to.channel" of role "to.role"
func (generator *NetworkPolicyGenerator) calculateIngressNetworkPolicy(
	name string,
	fromEndpoint *context.ConnectorEndpoint,
	toEndpoint *context.ConnectorEndpoint,
) (policy *netv1.NetworkPolicy, err error) {
	portstr := intstr.FromString(naming.CreateName(toEndpoint.ChannelName))
	generator.Translations[portstr.String()] = toEndpoint.ChannelName
	// From ticket 1140
	// protocol := v1.ProtocolTCP
	protocol := utils.CalculateKumoriProtocol(toEndpoint.Protocol)
	portPeers := []netv1.NetworkPolicyPort{{
		Port:     &portstr,
		Protocol: &protocol,
	}}
	labels := generator.generateIngressLabels(fromEndpoint, toEndpoint)
	annotations := generator.generateIngressAnnotations(fromEndpoint, toEndpoint)
	policy = &netv1.NetworkPolicy{
		ObjectMeta: metav1.ObjectMeta{
			Name:            name,
			Namespace:       generator.Context.Namespace,
			OwnerReferences: *generator.Context.Owners,
			Labels:          *labels,
		},
		Spec: netv1.NetworkPolicySpec{
			PodSelector: metav1.LabelSelector{
				MatchLabels: map[string]string{
					utils.KumoriDeploymentIdLabel: toEndpoint.Deployment,
					utils.KumoriRoleLabel:         hashutil.Hash(*toEndpoint.Role),
				},
			},
			Ingress: []netv1.NetworkPolicyIngressRule{
				{
					From: []netv1.NetworkPolicyPeer{
						{
							PodSelector: &metav1.LabelSelector{
								MatchLabels: map[string]string{
									utils.KumoriDeploymentIdLabel: fromEndpoint.Deployment,
									utils.KumoriRoleLabel:         hashutil.Hash(*fromEndpoint.Role),
								},
							},
						},
					},
					Ports: portPeers,
				},
			},
			PolicyTypes: []netv1.PolicyType{
				netv1.PolicyTypeIngress,
			},
		},
	}
	if annotations != nil {
		policy.ObjectMeta.Annotations = *annotations
	}
	return
}

// calculateInboundNetworkPolicy creates a NewtorkPolicy to enable connections from
// inbounds (ingress) to the channel "to.channel" of role "to.role"
func (generator *NetworkPolicyGenerator) calculateInboundNetworkPolicy(
	name string,
	toEndpoint *context.ConnectorEndpoint,
) (policy *netv1.NetworkPolicy, err error) {
	portstr := intstr.FromString(naming.CreateName(toEndpoint.ChannelName))
	generator.Translations[portstr.String()] = toEndpoint.ChannelName
	// From ticket 1140
	// protocol := v1.ProtocolTCP
	protocol := utils.CalculateKumoriProtocol(toEndpoint.Protocol)
	portPeers := []netv1.NetworkPolicyPort{{
		Port:     &portstr,
		Protocol: &protocol,
	}}

	labels := generator.generateInboundLabels(toEndpoint)
	annotations := generator.generateInboundAnnotations(toEndpoint)
	policy = &netv1.NetworkPolicy{
		ObjectMeta: metav1.ObjectMeta{
			Name:            name,
			Namespace:       generator.Context.Namespace,
			OwnerReferences: *generator.Context.Owners,
			Labels:          *labels,
		},
		Spec: netv1.NetworkPolicySpec{
			PodSelector: metav1.LabelSelector{
				MatchLabels: map[string]string{
					utils.KumoriDeploymentIdLabel: toEndpoint.Deployment,
					utils.KumoriRoleLabel:         hashutil.Hash(*toEndpoint.Role),
				},
			},
			Ingress: []netv1.NetworkPolicyIngressRule{
				{
					From: []netv1.NetworkPolicyPeer{
						{
							PodSelector: &metav1.LabelSelector{
								MatchLabels: map[string]string{
									"type": "ingress",
								},
							},
						},
					},
					Ports: portPeers,
				},
			},
			PolicyTypes: []netv1.PolicyType{
				netv1.PolicyTypeIngress,
			},
		},
	}
	if annotations != nil {
		policy.ObjectMeta.Annotations = *annotations
	}
	return
}

// calculateExternalAccessNetworkPolicy creates a NewtorkPolicy to enable connections from
// inbounds (ingress) to the channel "to.channel" of role "to.role".
// Task 944
func (generator *NetworkPolicyGenerator) calculateExternalAccessNetworkPolicy(
	name string,
	fromName string,
	data *config.ExternalServiceAccess,
	toEndpoint *context.ConnectorEndpoint,
) (policy *netv1.NetworkPolicy, err error) {
	portstr := intstr.FromString(naming.CreateName(toEndpoint.ChannelName))
	generator.Translations[portstr.String()] = toEndpoint.ChannelName
	// From ticket 1140
	// protocol := v1.ProtocolTCP
	protocol := utils.CalculateKumoriProtocol(toEndpoint.Protocol)
	portPeers := []netv1.NetworkPolicyPort{{
		Port:     &portstr,
		Protocol: &protocol,
	}}

	labels := generator.generateExternalAccessLabels(fromName, toEndpoint)
	annotations := generator.generateExternalAccessAnnotations(toEndpoint)
	policy = &netv1.NetworkPolicy{
		ObjectMeta: metav1.ObjectMeta{
			Name:            name,
			Namespace:       generator.Context.Namespace,
			OwnerReferences: *generator.Context.Owners,
			Labels:          *labels,
		},
		Spec: netv1.NetworkPolicySpec{
			PodSelector: metav1.LabelSelector{
				MatchLabels: map[string]string{
					utils.KumoriDeploymentIdLabel: toEndpoint.Deployment,
					utils.KumoriRoleLabel:         hashutil.Hash(*toEndpoint.Role),
				},
			},
			Ingress: []netv1.NetworkPolicyIngressRule{
				{Ports: portPeers},
			},
			PolicyTypes: []netv1.PolicyType{
				netv1.PolicyTypeIngress,
			},
		},
	}
	if data.Source.Label != nil {
		policy.Spec.Ingress[0].From = []netv1.NetworkPolicyPeer{
			{
				PodSelector: &metav1.LabelSelector{
					MatchLabels: map[string]string{
						data.Source.Label.Key: data.Source.Label.Value,
					},
				},
			},
		}
	}
	if data.Source.Namespace != nil {
		if len(policy.Spec.Ingress[0].From) <= 0 {
			policy.Spec.Ingress[0].From = []netv1.NetworkPolicyPeer{{}}
		}
		policy.Spec.Ingress[0].From[0].NamespaceSelector = &metav1.LabelSelector{
			MatchLabels: map[string]string{
				"kubernetes.io/metadata.name": *data.Source.Namespace,
			},
		}
	}
	if annotations != nil {
		policy.ObjectMeta.Annotations = *annotations
	}
	return
}

// generateLabels returns the labels to be included in each Service representing
// a given container tag
func (generator *NetworkPolicyGenerator) generateInboundLabels(toEndpoint *context.ConnectorEndpoint) *map[string]string {
	if generator.Context.ConnectorsInfo == nil {
		return nil
	}
	connectorInfo, ok := (*generator.Context.ConnectorsInfo)[generator.ConnectorID]
	if !ok {
		return nil
	}
	labels := map[string]string{
		utils.KumoriControllerLabel:   generator.ControllerName,
		utils.KumoriConnectorLabel:    hashutil.Hash(generator.ConnectorID),
		utils.KumoriDeploymentIdLabel: generator.Context.Name,
		utils.KumoriToRoleLabel:       hashutil.Hash(*toEndpoint.Role),
		utils.KumoriToChannelLabel:    hashutil.Hash(toEndpoint.ChannelName),
	}

	if generator.Context.KumoriOwner != nil {
		labels[utils.KumoriOwnerLabel] = hashutil.Hash(*generator.Context.KumoriOwner)
	}

	theLinkedEndpoints := [][]*context.ConnectorEndpoint{connectorInfo.LinkedClients, connectorInfo.LinkedServers}

	for _, linkedEndpoints := range theLinkedEndpoints {
		if len(linkedEndpoints) > 0 {
			for _, linkedEndpoint := range linkedEndpoints {
				if linkedEndpoint.Links != nil {
					for _, link := range linkedEndpoint.Links {
						labels[link] = "link-in-use"
					}
				}
			}
		}
	}

	return &labels
}

// generateInboundAnnotations returns the annotations to be included in each Service representing
// a given container tag
func (generator *NetworkPolicyGenerator) generateInboundAnnotations(toEndpoint *context.ConnectorEndpoint) *map[string]string {

	meth := "generators.networkpolicies.generateInboundAnnotations"

	annotations := map[string]string{
		utils.KumoriConnectorLabel: generator.ConnectorID,
		utils.KumoriToRoleLabel:    *toEndpoint.Role,
		utils.KumoriToChannelLabel: toEndpoint.ChannelName,
	}

	if generator.Context.KumoriOwner != nil {
		annotations[utils.KumoriOwnerLabel] = *generator.Context.KumoriOwner
	}

	if len(generator.Translations) > 0 {
		content, err := json.Marshal(generator.Translations)
		if err != nil {
			log.Errorf("%s. Error creating template annotation: %s", meth, err.Error())
		} else {
			annotations[utils.KumoriTranslations] = string(content)
		}
	}

	return &annotations
}

// generateLabels returns the labels to be included in each Service representing
// a given container tag
func (generator *NetworkPolicyGenerator) generateIngressLabels(
	fromEndpoint *context.ConnectorEndpoint,
	toEndpoint *context.ConnectorEndpoint,
) *map[string]string {

	if generator.Context.ConnectorsInfo == nil {
		return nil
	}
	connectorInfo, ok := (*generator.Context.ConnectorsInfo)[generator.ConnectorID]
	if !ok {
		return nil
	}
	labels := map[string]string{
		utils.KumoriControllerLabel:   generator.ControllerName,
		utils.KumoriConnectorLabel:    hashutil.Hash(generator.ConnectorID),
		utils.KumoriDeploymentIdLabel: generator.Context.Name,
		utils.KumoriFromIdLabel:       fromEndpoint.Deployment,
		utils.KumoriFromRoleLabel:     hashutil.Hash(*fromEndpoint.Role),
		utils.KumoriFromChannelLabel:  hashutil.Hash(fromEndpoint.ChannelName),
		utils.KumoriToIdLabel:         toEndpoint.Deployment,
		utils.KumoriToRoleLabel:       hashutil.Hash(*toEndpoint.Role),
		utils.KumoriToChannelLabel:    hashutil.Hash(toEndpoint.ChannelName),
	}

	if generator.Context.KumoriOwner != nil {
		labels[utils.KumoriOwnerLabel] = hashutil.Hash(*generator.Context.KumoriOwner)
	}

	theLinkedEndpoints := [][]*context.ConnectorEndpoint{connectorInfo.LinkedClients, connectorInfo.LinkedServers}

	for _, linkedEndpoints := range theLinkedEndpoints {
		if len(linkedEndpoints) > 0 {
			for _, linkedEndpoint := range linkedEndpoints {
				if linkedEndpoint.Links != nil {
					for _, link := range linkedEndpoint.Links {
						labels[link] = "link-in-use"
					}
				}
			}
		}
	}

	return &labels
}

// generateIngressAnnotations returns the annotations to be included in each Service representing
// a given container tag
func (generator *NetworkPolicyGenerator) generateIngressAnnotations(
	fromEndpoint *context.ConnectorEndpoint,
	toEndpoint *context.ConnectorEndpoint,
) *map[string]string {

	meth := "generators.networkpolicies.generateIngressAnnotations"

	annotations := map[string]string{
		utils.KumoriConnectorLabel:   generator.ConnectorID,
		utils.KumoriFromRoleLabel:    *fromEndpoint.Role,
		utils.KumoriFromChannelLabel: fromEndpoint.ChannelName,
		utils.KumoriToRoleLabel:      *toEndpoint.Role,
		utils.KumoriToChannelLabel:   toEndpoint.ChannelName,
	}

	if generator.Context.KumoriOwner != nil {
		annotations[utils.KumoriOwnerLabel] = *generator.Context.KumoriOwner
	}

	if len(generator.Translations) > 0 {
		content, err := json.Marshal(generator.Translations)
		if err != nil {
			log.Errorf("%s. Error creating annotation: %s", meth, err.Error())
		} else {
			annotations[utils.KumoriTranslations] = string(content)
		}
	}

	return &annotations
}

// generateLabels returns the labels to be included in each Service representing
// a given container tag
func (generator *NetworkPolicyGenerator) generateExternalAccessLabels(
	fromExternal string,
	toEndpoint *context.ConnectorEndpoint,
) *map[string]string {
	if generator.Context.ConnectorsInfo == nil {
		return nil
	}
	connectorInfo, ok := (*generator.Context.ConnectorsInfo)[generator.ConnectorID]
	if !ok {
		return nil
	}
	labels := map[string]string{
		utils.KumoriControllerLabel:          generator.ControllerName,
		utils.KumoriConnectorLabel:           hashutil.Hash(generator.ConnectorID),
		utils.KumoriDeploymentIdLabel:        generator.Context.Name,
		utils.KumoriFromExternalServiceLabel: fromExternal,
		utils.KumoriToRoleLabel:              hashutil.Hash(*toEndpoint.Role),
		utils.KumoriToChannelLabel:           hashutil.Hash(toEndpoint.ChannelName),
	}

	if generator.Context.KumoriOwner != nil {
		labels[utils.KumoriOwnerLabel] = hashutil.Hash(*generator.Context.KumoriOwner)
	}

	theLinkedEndpoints := [][]*context.ConnectorEndpoint{connectorInfo.LinkedClients, connectorInfo.LinkedServers}

	for _, linkedEndpoints := range theLinkedEndpoints {
		if len(linkedEndpoints) > 0 {
			for _, linkedEndpoint := range linkedEndpoints {
				if linkedEndpoint.Links != nil {
					for _, link := range linkedEndpoint.Links {
						labels[link] = "link-in-use"
					}
				}
			}
		}
	}

	return &labels
}

// generateInboundAnnotations returns the annotations to be included in each Service representing
// a given container tag
func (generator *NetworkPolicyGenerator) generateExternalAccessAnnotations(
	toEndpoint *context.ConnectorEndpoint,
) *map[string]string {

	meth := "generators.networkpolicies.generateInboundAnnotations"

	annotations := map[string]string{
		utils.KumoriConnectorLabel: generator.ConnectorID,
		utils.KumoriToRoleLabel:    *toEndpoint.Role,
		utils.KumoriToChannelLabel: toEndpoint.ChannelName,
	}

	if generator.Context.KumoriOwner != nil {
		annotations[utils.KumoriOwnerLabel] = *generator.Context.KumoriOwner
	}

	if len(generator.Translations) > 0 {
		content, err := json.Marshal(generator.Translations)
		if err != nil {
			log.Errorf("%s. Error creating template annotation: %s", meth, err.Error())
		} else {
			annotations[utils.KumoriTranslations] = string(content)
		}
	}

	return &annotations
}
