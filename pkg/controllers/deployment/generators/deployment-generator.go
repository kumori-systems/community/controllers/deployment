/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package generators

import (
	"fmt"
	"kuku-controller/pkg/utils"
	hashutil "kuku-controller/pkg/utils/hash"

	context "kuku-controller/pkg/controllers/deployment/context"

	log "github.com/sirupsen/logrus"
	appsv1 "k8s.io/api/apps/v1"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// DeploymentGenerator creates a Deployment to create the PODs of a deployment role.
type DeploymentGenerator struct {
	AppName        string
	ConfigMap      *v1.ConfigMap
	Context        *context.Context
	ControllerName string
	Owner          string
	Translations   map[string]string
	Role           string
}

// CreateDeploymentName calculates the name of the Deployment related to a role
func CreateDeploymentName(appName string) string {
	return fmt.Sprintf("%s-deployment", appName)
}

// NewDeploymentGenerator generates the Deployment objects required for a given role of
// a given V3Deployment
func NewDeploymentGenerator(
	appName string,
	configMap *v1.ConfigMap,
	kcontext *context.Context,
	controllerName string,
	owner string,
	role string,
) (generator *DeploymentGenerator) {
	meth := fmt.Sprintf("generators.deployments.NewDeploymentGenerator. AppName: %s ControllerName: %s Owner: %s Role: %s", appName, controllerName, owner, role)
	log.Debugf(meth)

	return &DeploymentGenerator{
		AppName:        appName,
		ConfigMap:      configMap,
		Context:        kcontext,
		ControllerName: controllerName,
		Owner:          owner,
		Translations:   map[string]string{},
		Role:           role,
	}
}

// Generate a Deployment representing a deployment role
func (generator *DeploymentGenerator) Generate() (*appsv1.Deployment, error) {
	meth := fmt.Sprintf("generators.deployments.Generate. AppName: %s", generator.AppName)
	log.Debugf(meth)

	if !generator.Context.Loaded {
		return nil, fmt.Errorf("context not loaded")
	}
	name := CreateDeploymentName(generator.AppName)
	labels := generator.generateLabels()
	annotations := generator.generateAnnotations()
	templateLabels := generator.generateTemplateLabels()
	templateAnnotations := generator.generateTemplateAnnotations()
	selectorLabels := generator.generateSelectorLabels()
	podTemplateGenerator := NewPodTemplateGenerator(
		generator.AppName,
		generator.ConfigMap,
		generator.Context,
		false,
		templateLabels,
		templateAnnotations,
		selectorLabels,
		generator.Owner,
		generator.Role,
		generator.Translations,
	)

	deployment, err := generator.calculateDeployment(
		name, generator.Role, labels, annotations, selectorLabels, podTemplateGenerator,
	)
	if err != nil {
		return nil, err
	}

	return deployment, nil
}

// calculateDeployment calculates the Deployment object representing a given role in
// the KukuContext
func (generator *DeploymentGenerator) calculateDeployment(
	name string,
	role string,
	labels *map[string]string,
	annotations *map[string]string,
	selectorLabels *map[string]string,
	podTemplateGenerator *PodTemplateGenerator,
) (deployment *appsv1.Deployment, err error) {
	meth := fmt.Sprintf("generators.deployments.calculateDeployment. Name: %s Role: %s", name, role)
	log.Debugf(meth)

	roleInfo, ok := (*generator.Context.RolesInfo)[generator.Role]
	if !ok {
		return nil, fmt.Errorf("role '%s' not found in context", generator.Role)
	}

	var instances int32
	if (roleInfo.RoleSize.Maxinstances != nil) && (*roleInfo.RoleSize.Maxinstances > int32(0)) && (roleInfo.RoleSize.Instances > *roleInfo.RoleSize.Maxinstances) {
		instances = *roleInfo.RoleSize.Maxinstances
	} else if (roleInfo.RoleSize.Mininstances != nil) && (*roleInfo.RoleSize.Mininstances >= int32(0)) && (roleInfo.RoleSize.Instances < *roleInfo.RoleSize.Mininstances) {
		instances = *roleInfo.RoleSize.Mininstances
	} else if roleInfo.RoleSize.Instances < 0 {
		instances = 1
	} else {
		instances = roleInfo.RoleSize.Instances
	}

	// The PodTemplate has a specific generator, shared by DeploymentGenerator and
	// StatefulSet generator.
	podTemplateSpec, _, err := podTemplateGenerator.Generate()
	if err != nil {
		return nil, err
	}
	if podTemplateSpec == nil {
		return nil, nil
	}

	deployment = &appsv1.Deployment{
		ObjectMeta: metav1.ObjectMeta{
			Name:            name,
			Namespace:       generator.Context.Namespace,
			OwnerReferences: *generator.Context.Owners,
		},
		Spec: appsv1.DeploymentSpec{
			Replicas: &instances,
			Selector: &metav1.LabelSelector{},
			Template: *podTemplateSpec,
		},
	}

	if labels != nil {
		deployment.ObjectMeta.Labels = *labels
		if roleInfo.Autoscaled {
			deployment.ObjectMeta.Labels[utils.KumoriAutoscaledLabel] = "true"
		} else {
			deployment.ObjectMeta.Labels[utils.KumoriAutoscaledLabel] = "false"
		}
	}

	if annotations != nil {

		if podTemplateGenerator.Annotations != nil {
			if value, ok := (*podTemplateGenerator.Annotations)[utils.KumoriTranslations]; ok {
				(*annotations)[utils.KumoriTranslations] = value
			}
		}

		deployment.ObjectMeta.Annotations = *annotations
	}

	if selectorLabels != nil {
		deployment.Spec.Selector.MatchLabels = *selectorLabels
	}

	// content, _ := json.MarshalIndent(deployment, "", "  ")
	// fmt.Printf("\n\n------->DEPLOYMENT: %s", string(content))

	return
}

// generateSelectorLabels returns the labels to be used in the pod selector.
func (generator *DeploymentGenerator) generateSelectorLabels() *map[string]string {
	labels := map[string]string{
		utils.KumoriControllerLabel:   generator.ControllerName,
		utils.KumoriDeploymentIdLabel: generator.Context.Name,
		utils.KumoriRoleLabel:         hashutil.Hash(generator.Role),
	}

	return &labels
}

// generateLabels returns the labels to be added to the Deployment.
func (generator *DeploymentGenerator) generateLabels() *map[string]string {
	labels := map[string]string{
		utils.KumoriControllerLabel:       generator.ControllerName,
		utils.KumoriDeploymentNameLabel:   hashutil.Hash(generator.Context.RegistrationName),
		utils.KumoriDeploymentDomainLabel: hashutil.Hash(generator.Context.RegistrationDomain),
		utils.KumoriDeploymentIdLabel:     generator.Context.Name,
		utils.KumoriRoleLabel:             hashutil.Hash(generator.Role),
	}
	if generator.Context.KumoriOwner != nil {
		labels[utils.KumoriOwnerLabel] = hashutil.Hash(*generator.Context.KumoriOwner)
	}
	if generator.Context.ServiceInfo != nil {
		serviceReference := generator.Context.ServiceInfo.Reference
		serviceVersion := fmt.Sprintf("%d.%d.%d", serviceReference.Version[0], serviceReference.Version[1], serviceReference.Version[2])
		if serviceReference.Prerelease != nil {
			serviceVersion = fmt.Sprintf("%s-%s", serviceVersion, *serviceReference.Prerelease)
		}
		labels[utils.KumoriServiceNameLabel] = hashutil.Hash(serviceReference.Name)
		labels[utils.KumoriServiceDomainLabel] = hashutil.Hash(serviceReference.Domain)
		labels[utils.KumoriServiceVersionLabel] = hashutil.Hash(serviceVersion)
	}
	if generator.Context.RolesInfo != nil {
		if rolesInfo, ok := (*generator.Context.RolesInfo)[generator.Role]; ok {
			if rolesInfo.ComponentReference != nil {
				componentReference := (*generator.Context.RolesInfo)[generator.Role].ComponentReference
				componentVersion := fmt.Sprintf("%d.%d.%d", componentReference.Version[0], componentReference.Version[1], componentReference.Version[2])
				if componentReference.Prerelease != nil {
					componentVersion = fmt.Sprintf("%s-%s", componentVersion, *componentReference.Prerelease)
				}
				labels[utils.KumoriComponentNameLabel] = hashutil.Hash(componentReference.Name)
				labels[utils.KumoriComponentDomainLabel] = hashutil.Hash(componentReference.Domain)
				labels[utils.KumoriComponentVersionLabel] = hashutil.Hash(componentVersion)
			}
		}
	}

	return &labels
}

// generateAnnotations returns the annotations to be added to the Deployment.
func (generator *DeploymentGenerator) generateAnnotations() *map[string]string {
	annotations := map[string]string{
		utils.KumoriDeploymentNameLabel:   generator.Context.RegistrationName,
		utils.KumoriDeploymentDomainLabel: generator.Context.RegistrationDomain,
		utils.KumoriRoleLabel:             generator.Role,
	}
	if generator.Context.KumoriOwner != nil {
		annotations[utils.KumoriOwnerLabel] = *generator.Context.KumoriOwner
	}
	if generator.Context.ServiceInfo != nil {
		serviceReference := generator.Context.ServiceInfo.Reference
		serviceVersion := fmt.Sprintf("%d.%d.%d", serviceReference.Version[0], serviceReference.Version[1], serviceReference.Version[2])
		if serviceReference.Prerelease != nil {
			serviceVersion = fmt.Sprintf("%s-%s", serviceVersion, *serviceReference.Prerelease)
		}
		annotations[utils.KumoriServiceNameLabel] = serviceReference.Name
		annotations[utils.KumoriServiceDomainLabel] = serviceReference.Domain
		annotations[utils.KumoriServiceVersionLabel] = serviceVersion
	}
	if generator.Context.RolesInfo != nil {
		if rolesInfo, ok := (*generator.Context.RolesInfo)[generator.Role]; ok {
			if rolesInfo.ComponentReference != nil {
				componentReference := (*generator.Context.RolesInfo)[generator.Role].ComponentReference
				componentVersion := fmt.Sprintf("%d.%d.%d", componentReference.Version[0], componentReference.Version[1], componentReference.Version[2])
				if componentReference.Prerelease != nil {
					componentVersion = fmt.Sprintf("%s-%s", componentVersion, *componentReference.Prerelease)
				}
				annotations[utils.KumoriComponentNameLabel] = componentReference.Name
				annotations[utils.KumoriComponentDomainLabel] = componentReference.Domain
				annotations[utils.KumoriComponentVersionLabel] = componentVersion
			}
		}
	}

	return &annotations
}

// generateTemplateLabels returns the labels to be added to the Deployment Pod template.
func (generator *DeploymentGenerator) generateTemplateLabels() *map[string]string {
	labels := map[string]string{
		utils.KumoriControllerLabel:       generator.ControllerName,
		utils.KumoriDeploymentNameLabel:   hashutil.Hash(generator.Context.RegistrationName),
		utils.KumoriDeploymentDomainLabel: hashutil.Hash(generator.Context.RegistrationDomain),
		utils.KumoriDeploymentIdLabel:     generator.Context.Name,
		utils.KumoriRoleLabel:             hashutil.Hash(generator.Role),
	}
	if generator.Context.KumoriOwner != nil {
		labels[utils.KumoriOwnerLabel] = hashutil.Hash(*generator.Context.KumoriOwner)
	}

	return &labels
}

// generateTemplateAnnotations returns the annotations to be added to the Deployment Pod template.
func (generator *DeploymentGenerator) generateTemplateAnnotations() *map[string]string {
	annotations := map[string]string{
		utils.KumoriDeploymentNameLabel:   generator.Context.RegistrationName,
		utils.KumoriDeploymentDomainLabel: generator.Context.RegistrationDomain,
		utils.KumoriRoleLabel:             generator.Role,
	}
	if generator.Context.KumoriOwner != nil {
		annotations[utils.KumoriOwnerLabel] = *generator.Context.KumoriOwner
	}

	return &annotations
}
