/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package generators

import (
	"fmt"
	context "kuku-controller/pkg/controllers/deployment/context"
	"kuku-controller/pkg/utils"
	hashutil "kuku-controller/pkg/utils/hash"

	log "github.com/sirupsen/logrus"
	appsv1 "k8s.io/api/apps/v1"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// StatefulSetGenerator creates a StatefulSet to create the PODs of a deployment role.
type StatefulSetGenerator struct {
	AppName        string
	ConfigMap      *v1.ConfigMap
	Context        *context.Context
	ControllerName string
	Owner          string
	Translations   map[string]string
	Role           string
}

// CreateStatefulSetName calculates the name of the StatefulSet related to a role
func CreateStatefulSetName(appName string) string {
	return fmt.Sprintf("%s-deployment", appName)
}

// CreateHeadlessServiceName calculates the name of the headless service related to a stateful set
func CreateHeadlessServiceName(statefulSetName string) string {
	return fmt.Sprintf("%s-service", statefulSetName)
}

// NewStatefulSetGenerator generates a new StatefulSetGenerator
func NewStatefulSetGenerator(
	appName string,
	configMap *v1.ConfigMap,
	kcontext *context.Context,
	controllerName string,
	owner string,
	role string,
) (generator *StatefulSetGenerator) {
	meth := fmt.Sprintf("generators.statefulsets.NewStatefulSetGenerator. AppName: %s ControllerName: %s Owner: %s Role: %s", appName, controllerName, owner, role)
	log.Debugf(meth)
	return &StatefulSetGenerator{
		AppName:        appName,
		ConfigMap:      configMap,
		Context:        kcontext,
		ControllerName: controllerName,
		Owner:          owner,
		Translations:   map[string]string{},
		Role:           role,
	}
}

// Generate a StatefulSet representing a deployment role
func (generator *StatefulSetGenerator) Generate() (*appsv1.StatefulSet, *v1.Service, error) {
	meth := fmt.Sprintf("generators.statefulsets.Generate. AppName: %s", generator.AppName)
	log.Debugf(meth)

	if !generator.Context.Loaded {
		return nil, nil, fmt.Errorf("context not loaded")
	}
	name := CreateStatefulSetName(generator.AppName)
	serviceName := CreateHeadlessServiceName(name)
	labels := generator.generateLabels()
	annotations := generator.generateAnnotations()
	serviceAnnotations := copyStringMap(annotations)
	templateLabels := generator.generateTemplateLabels()
	templateAnnotations := generator.generateTemplateAnnotations()
	selectorLabels := generator.generateSelectorLabels()
	podTemplateGenerator := NewPodTemplateGenerator(
		generator.AppName,
		generator.ConfigMap,
		generator.Context,
		false,
		templateLabels,
		templateAnnotations,
		selectorLabels,
		generator.Owner,
		generator.Role,
		generator.Translations,
	)
	statefulSet, err := generator.calculateStatefulSet(
		name, generator.Role, serviceName, labels, annotations, selectorLabels, podTemplateGenerator,
	)
	if err != nil {
		return nil, nil, err
	}
	service, err := generator.calculateHeadlessService(serviceName, labels, serviceAnnotations, templateLabels)
	if err != nil {
		return nil, nil, err
	}

	// content, _ := json.MarshalIndent(statefulSet, "", "  ")
	// fmt.Printf("\n\n------->STATEFULSET: %s", string(content))

	// content, _ = json.MarshalIndent(service, "", "  ")
	// fmt.Printf("\n\n------->SERVICE: %s", string(content))

	return statefulSet, service, nil
}

// generateSelectorLabels returns the labels to be used in the pod selector.
func (generator *StatefulSetGenerator) generateSelectorLabels() *map[string]string {
	labels := map[string]string{
		utils.KumoriControllerLabel:   generator.ControllerName,
		utils.KumoriDeploymentIdLabel: generator.Context.Name,
		utils.KumoriRoleLabel:         hashutil.Hash(generator.Role),
	}

	return &labels
}

// calculateStatefulSet returns a StatefulSet representing a stateful role in a V3Deployment.
func (generator *StatefulSetGenerator) calculateStatefulSet(
	name string,
	role string,
	serviceName string,
	labels *map[string]string,
	annotations *map[string]string,
	selectorLabels *map[string]string,
	podTemplateGenerator *PodTemplateGenerator,
) (statefulSet *appsv1.StatefulSet, err error) {
	meth := fmt.Sprintf("generators.statefulsets.calculateStatefulSet. Name: %s Role: %s ServiceName: %s", name, role, serviceName)
	log.Debugf(meth)

	roleInfo, ok := (*generator.Context.RolesInfo)[generator.Role]
	if !ok {
		return nil, fmt.Errorf("role '%s' not found in context", generator.Role)
	}

	var instances int32
	if (roleInfo.RoleSize.Maxinstances != nil) && (*roleInfo.RoleSize.Maxinstances > int32(0)) && (roleInfo.RoleSize.Instances > *roleInfo.RoleSize.Maxinstances) {
		instances = *roleInfo.RoleSize.Maxinstances
	} else if (roleInfo.RoleSize.Mininstances != nil) && (*roleInfo.RoleSize.Mininstances >= int32(0)) && (roleInfo.RoleSize.Instances < *roleInfo.RoleSize.Mininstances) {
		instances = *roleInfo.RoleSize.Mininstances
	} else if roleInfo.RoleSize.Instances < 0 {
		instances = 1
	} else {
		instances = roleInfo.RoleSize.Instances
	}

	podTemplateSpec, volumeClaimTemplates, err := podTemplateGenerator.Generate()
	if err != nil {
		return nil, err
	}
	if podTemplateSpec == nil {
		return nil, nil
	}

	statefulSet = &appsv1.StatefulSet{
		ObjectMeta: metav1.ObjectMeta{
			Name:            name,
			Namespace:       generator.Context.Namespace,
			OwnerReferences: *generator.Context.Owners,
		},
		Spec: appsv1.StatefulSetSpec{
			Replicas:    &instances,
			Selector:    &metav1.LabelSelector{},
			ServiceName: serviceName,
			Template:    *podTemplateSpec,
		},
	}

	if volumeClaimTemplates != nil {
		claims := make([]v1.PersistentVolumeClaim, 0, len(volumeClaimTemplates))
		for _, claim := range volumeClaimTemplates {
			claim.OwnerReferences = *generator.Context.Owners
			claims = append(claims, claim)
		}
		statefulSet.Spec.VolumeClaimTemplates = claims
	}

	if labels != nil {
		statefulSet.ObjectMeta.Labels = *labels
		if roleInfo.Autoscaled {
			statefulSet.ObjectMeta.Labels[utils.KumoriAutoscaledLabel] = "true"
		} else {
			statefulSet.ObjectMeta.Labels[utils.KumoriAutoscaledLabel] = "false"
		}

	}

	if annotations != nil {

		if podTemplateGenerator.Annotations != nil {
			if value, ok := (*podTemplateGenerator.Annotations)[utils.KumoriTranslations]; ok {
				(*annotations)[utils.KumoriTranslations] = value
			}
		}

		statefulSet.ObjectMeta.Annotations = *annotations
	}

	if selectorLabels != nil {
		statefulSet.Spec.Selector.MatchLabels = *selectorLabels
	}

	return
}

// calculateHeadlessService returns the headless service required by the StatefulSet.
// SEE: https://kubernetes.io/docs/tutorials/stateful-application/basic-stateful-set/
func (generator *StatefulSetGenerator) calculateHeadlessService(
	name string,
	labels *map[string]string,
	annotations *map[string]string,
	selectorLabels *map[string]string,
) (service *v1.Service, err error) {
	meth := fmt.Sprintf("generators.statefulsets.calculateHeadlessService. Name: %s", name)
	log.Debugf(meth)

	ipFamilyPolicy := v1.IPFamilyPolicySingleStack
	service = &v1.Service{
		ObjectMeta: metav1.ObjectMeta{
			Name:            name,
			Namespace:       generator.Context.Namespace,
			OwnerReferences: *generator.Context.Owners,
		},
		Spec: v1.ServiceSpec{
			Type:            v1.ServiceTypeClusterIP,
			Ports:           nil,
			SessionAffinity: "None",
			ClusterIP:       "None",
			IPFamilies:      []v1.IPFamily{v1.IPv4Protocol},
			IPFamilyPolicy:  &ipFamilyPolicy,
		},
	}
	if labels != nil {
		service.ObjectMeta.Labels = *labels
	}
	if selectorLabels != nil {
		service.Spec.Selector = *selectorLabels
	}
	if annotations != nil {
		service.ObjectMeta.Annotations = *annotations
	}
	return service, nil
}

// generateLabels returns the labels to be added to the StatefulSet.
func (generator *StatefulSetGenerator) generateLabels() *map[string]string {
	labels := map[string]string{
		utils.KumoriControllerLabel:       generator.ControllerName,
		utils.KumoriDeploymentNameLabel:   hashutil.Hash(generator.Context.RegistrationName),
		utils.KumoriDeploymentDomainLabel: hashutil.Hash(generator.Context.RegistrationDomain),
		utils.KumoriDeploymentIdLabel:     generator.Context.Name,
		utils.KumoriRoleLabel:             hashutil.Hash(generator.Role),
	}
	if generator.Context.KumoriOwner != nil {
		labels[utils.KumoriOwnerLabel] = hashutil.Hash(*generator.Context.KumoriOwner)
	}
	if generator.Context.ServiceInfo != nil {
		serviceReference := generator.Context.ServiceInfo.Reference
		serviceVersion := fmt.Sprintf("%d.%d.%d", serviceReference.Version[0], serviceReference.Version[1], serviceReference.Version[2])
		if serviceReference.Prerelease != nil {
			serviceVersion = fmt.Sprintf("%s-%s", serviceVersion, *serviceReference.Prerelease)
		}
		labels[utils.KumoriServiceNameLabel] = hashutil.Hash(serviceReference.Name)
		labels[utils.KumoriServiceDomainLabel] = hashutil.Hash(serviceReference.Domain)
		labels[utils.KumoriServiceVersionLabel] = hashutil.Hash(serviceVersion)
	}
	if generator.Context.RolesInfo != nil {
		if rolesInfo, ok := (*generator.Context.RolesInfo)[generator.Role]; ok {
			if rolesInfo.ComponentReference != nil {
				componentReference := (*generator.Context.RolesInfo)[generator.Role].ComponentReference
				componentVersion := fmt.Sprintf("%d.%d.%d", componentReference.Version[0], componentReference.Version[1], componentReference.Version[2])
				if componentReference.Prerelease != nil {
					componentVersion = fmt.Sprintf("%s-%s", componentVersion, *componentReference.Prerelease)
				}
				labels[utils.KumoriComponentNameLabel] = hashutil.Hash(componentReference.Name)
				labels[utils.KumoriComponentDomainLabel] = hashutil.Hash(componentReference.Domain)
				labels[utils.KumoriComponentVersionLabel] = hashutil.Hash(componentVersion)
			}
		}
	}

	return &labels
}

// generateAnnotations returns the annotations to be added to the StatefulSet.
func (generator *StatefulSetGenerator) generateAnnotations() *map[string]string {
	annotations := map[string]string{
		utils.KumoriDeploymentNameLabel:   generator.Context.RegistrationName,
		utils.KumoriDeploymentDomainLabel: generator.Context.RegistrationDomain,
		utils.KumoriRoleLabel:             generator.Role,
	}
	if generator.Context.KumoriOwner != nil {
		annotations[utils.KumoriOwnerLabel] = *generator.Context.KumoriOwner
	}
	if generator.Context.ServiceInfo != nil {
		serviceReference := generator.Context.ServiceInfo.Reference
		serviceVersion := fmt.Sprintf("%d.%d.%d", serviceReference.Version[0], serviceReference.Version[1], serviceReference.Version[2])
		if serviceReference.Prerelease != nil {
			serviceVersion = fmt.Sprintf("%s-%s", serviceVersion, *serviceReference.Prerelease)
		}
		annotations[utils.KumoriServiceNameLabel] = serviceReference.Name
		annotations[utils.KumoriServiceDomainLabel] = serviceReference.Domain
		annotations[utils.KumoriServiceVersionLabel] = serviceVersion
	}
	if generator.Context.RolesInfo != nil {
		if rolesInfo, ok := (*generator.Context.RolesInfo)[generator.Role]; ok {
			if rolesInfo.ComponentReference != nil {
				componentReference := (*generator.Context.RolesInfo)[generator.Role].ComponentReference
				componentVersion := fmt.Sprintf("%d.%d.%d", componentReference.Version[0], componentReference.Version[1], componentReference.Version[2])
				if componentReference.Prerelease != nil {
					componentVersion = fmt.Sprintf("%s-%s", componentVersion, *componentReference.Prerelease)
				}
				annotations[utils.KumoriComponentNameLabel] = componentReference.Name
				annotations[utils.KumoriComponentDomainLabel] = componentReference.Domain
				annotations[utils.KumoriComponentVersionLabel] = componentVersion
			}
		}
	}

	return &annotations
}

// generateTemplateLabels returns the labels to be added to the StatefulSet Pod template.
func (generator *StatefulSetGenerator) generateTemplateLabels() *map[string]string {
	labels := map[string]string{
		utils.KumoriControllerLabel:       generator.ControllerName,
		utils.KumoriDeploymentNameLabel:   hashutil.Hash(generator.Context.RegistrationName),
		utils.KumoriDeploymentDomainLabel: hashutil.Hash(generator.Context.RegistrationDomain),
		utils.KumoriDeploymentIdLabel:     generator.Context.Name,
		utils.KumoriRoleLabel:             hashutil.Hash(generator.Role),
	}
	if generator.Context.KumoriOwner != nil {
		labels[utils.KumoriOwnerLabel] = hashutil.Hash(*generator.Context.KumoriOwner)
	}

	return &labels
}

// generateTemplateAnnotations returns the annotations to be added to the StatefulSet Pod template.
func (generator *StatefulSetGenerator) generateTemplateAnnotations() *map[string]string {
	annotations := map[string]string{
		utils.KumoriDeploymentNameLabel:   generator.Context.RegistrationName,
		utils.KumoriDeploymentDomainLabel: generator.Context.RegistrationDomain,
		utils.KumoriRoleLabel:             generator.Role,
	}
	if generator.Context.KumoriOwner != nil {
		annotations[utils.KumoriOwnerLabel] = *generator.Context.KumoriOwner
	}

	return &annotations
}
