/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package generators

import (
	"encoding/json"
	"fmt"

	context "kuku-controller/pkg/controllers/deployment/context"
	utils "kuku-controller/pkg/utils"
	hashutil "kuku-controller/pkg/utils/hash"

	log "github.com/sirupsen/logrus"
	v1 "k8s.io/api/policy/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	intstr "k8s.io/apimachinery/pkg/util/intstr"
)

// PodDisruptionBudgetGenerator generates the PodDisruptionBudgets required by a given role of a given V3Deployment.
type PodDisruptionBudgetGenerator struct {
	AppName        string
	Context        *context.Context
	ControllerName string
	Owner          string
	Translations   map[string]string
	Role           string
}

// CreatePodDisruptionBudgetName calculates the name of the ConfigMap of a given role
func CreatePodDisruptionBudgetName(appName string) string {
	return fmt.Sprintf("%s-pdb", appName)
}

// NewPodDisruptionBudgetGenerator returns a new generator to create the kubernetes PodDisruptionBudgets required by
// a given role of a given V3Deployment
func NewPodDisruptionBudgetGenerator(
	appName string,
	kcontext *context.Context,
	controllerName string,
	owner string,
	role string,
) (generator *PodDisruptionBudgetGenerator) {
	meth := fmt.Sprintf("generators.poddisruptionbudgets.NewPodDisruptionBudgetGenerator. AppName: %s ControllerName: %s Owner: %s Role: %s", appName, controllerName, owner, role)
	log.Debugf(meth)

	return &PodDisruptionBudgetGenerator{
		AppName:        appName,
		Context:        kcontext,
		ControllerName: controllerName,
		Owner:          owner,
		Role:           role,
		Translations:   map[string]string{},
	}
}

// Generate creates all the PodDisruptionBudgets required by a given role from a given V3Deployment. An opaque PodDisruptionBudget
// will be created per KukuPodDisruptionBudget referenced from a environment variable or mounted file. A docker
// config file will be created per KukuPodDisruptionBudget used to access a private hub
func (generator *PodDisruptionBudgetGenerator) Generate() (*v1.PodDisruptionBudget, error) {
	meth := fmt.Sprintf("generators.poddisruptionbudgets.Generate. AppName: %s", generator.AppName)
	log.Debugf(meth)
	return generator.calculatePodDisruptionBudgets()
}

// calculatePodDisruptionBudgets creates the PodDisruptionBudget objects used to expose PodDisruptionBudgets in files and environment variables
func (generator *PodDisruptionBudgetGenerator) calculatePodDisruptionBudgets() (*v1.PodDisruptionBudget, error) {

	meth := fmt.Sprintf("generators.poddisruptionbudgets.calculatePodDisruptionBudgets, AppName: '%s'. Role: '%s'.", generator.AppName, generator.Role)
	log.Debug(meth)

	// Calculates the object name
	name := CreatePodDisruptionBudgetName(generator.AppName)

	if generator.Context.RolesInfo == nil {
		return nil, fmt.Errorf("information not found for role '%s'", generator.Role)
	}

	// Calculates max unavailable. It depends on the role type. If it is stateful, the max unavailable must be 1. If it is stateless
	// then the max unavailable must be 25%. This simulates how StatefulSet and Deployment object work.
	roleInfo := (*generator.Context.RolesInfo)[generator.Role]
	var maxUnavailable intstr.IntOrString
	if roleInfo.Stateful {
		maxUnavailable = intstr.FromInt(1)
	} else {
		maxUnavailable = intstr.FromString("25%")
	}

	labels := generator.generateLabels()
	annotations := generator.generateAnnotations()

	// Generates the PodDisruptionBudget object
	podDisruptionBudget := v1.PodDisruptionBudget{
		ObjectMeta: metav1.ObjectMeta{
			Name:            name,
			Namespace:       generator.Context.Namespace,
			OwnerReferences: *generator.Context.Owners,
		},
		Spec: v1.PodDisruptionBudgetSpec{
			MaxUnavailable: &maxUnavailable,
		},
	}

	if labels == nil {
		log.Warnf("%s. Labels map empty. This PodDisruptionBudget is useless like this.", meth)
	} else {
		podDisruptionBudget.ObjectMeta.Labels = *labels
		podDisruptionBudget.Spec.Selector = &metav1.LabelSelector{
			MatchLabels: *labels,
		}
	}

	if annotations != nil {
		podDisruptionBudget.ObjectMeta.Annotations = *annotations
	}

	return &podDisruptionBudget, nil
}

// generateLabels generate the labels shared by all generated PodDisruptionBudgets
func (generator *PodDisruptionBudgetGenerator) generateLabels() *map[string]string {
	labels := map[string]string{
		utils.KumoriControllerLabel:   generator.ControllerName,
		utils.KumoriDeploymentIdLabel: generator.Context.Name,
		utils.KumoriRoleLabel:         hashutil.Hash(generator.Role),
	}
	if generator.Context.KumoriOwner != nil {
		labels[utils.KumoriOwnerLabel] = hashutil.Hash(*generator.Context.KumoriOwner)
	}
	return &labels
}

// generateAnnotations generate the annotations shared by all generated PodDisruptionBudgets
func (generator *PodDisruptionBudgetGenerator) generateAnnotations() *map[string]string {

	meth := "generators.poddisruptionbudgets.generateAnnotations"

	annotations := map[string]string{
		utils.KumoriRoleLabel: generator.Role,
	}
	if generator.Context.KumoriOwner != nil {
		annotations[utils.KumoriOwnerLabel] = *generator.Context.KumoriOwner
	}

	if len(generator.Translations) > 0 {
		content, err := json.Marshal(generator.Translations)
		if err != nil {
			log.Errorf("%s. Error creating annotation: %s", meth, err.Error())
		} else {
			annotations[utils.KumoriTranslations] = string(content)
		}
	}

	return &annotations
}
