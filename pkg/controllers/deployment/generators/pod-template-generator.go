/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package generators

import (
	"crypto/sha1"
	"encoding/json"
	"fmt"
	"hash"
	"io"
	"math"
	"path"
	"strconv"
	"strings"

	resource "k8s.io/apimachinery/pkg/api/resource"
	"k8s.io/apimachinery/pkg/util/intstr"

	log "github.com/sirupsen/logrus"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	"kuku-controller/pkg/controllers/deployment/config"
	context "kuku-controller/pkg/controllers/deployment/context"
	utils "kuku-controller/pkg/utils"
	hashutil "kuku-controller/pkg/utils/hash"
	"kuku-controller/pkg/utils/naming"
	"kuku-controller/pkg/utils/sort"
)

// PodTemplateGenerator calculates a PodTemplate representing a deployment role.
//   - AppName: the name of the application PODs will belong to. Usually is a
//     combination of the POD role and the deployment.
//   - ConfigMap: the config map with the PODs configuration (exposed files).
//   - Context: the context containing information about the deployment the PODs
//     will belong to.
//   - EnableServiceLinks: if true, information about services will ne injected
//     as environment variables.
//   - Labels: the labels included in each POD.
//   - Owner: identifies who owns the PODs. Don't mix up with OwnerReferences.
//     Currently, the name of the deployment is used.
//   - Role: the role of this PODs in this deployment as declared in the
//     deployment service.
type PodTemplateGenerator struct {
	Annotations        *map[string]string
	AppName            string
	ConfigMap          *v1.ConfigMap
	Context            *context.Context
	EnableServiceLinks bool
	Labels             *map[string]string
	Owner              string
	Role               string
	SelectorLabels     *map[string]string
	Translations       map[string]string
}

// NewPodTemplateGenerator returns a generator to calculate the PodTemplate representing a given
// role in the KukuContext. This generator is used by StatefulSet and Deployment generators
func NewPodTemplateGenerator(
	appName string,
	configMap *v1.ConfigMap,
	kcontext *context.Context,
	enableServiceLinks bool,
	labels *map[string]string,
	annotations *map[string]string,
	selectorLabels *map[string]string,
	owner string,
	role string,
	translations map[string]string,
) (generator *PodTemplateGenerator) {
	meth := fmt.Sprintf("generators.podtemplates.NewPodTemplateGenerator. AppName: %s  Owner: %s Role: %s", appName, owner, role)
	log.Debugf(meth)

	return &PodTemplateGenerator{
		AppName:            appName,
		ConfigMap:          configMap,
		Context:            kcontext,
		EnableServiceLinks: enableServiceLinks,
		Labels:             labels,
		Annotations:        annotations,
		SelectorLabels:     selectorLabels,
		Owner:              owner,
		Role:               role,
		Translations:       translations,
	}
}

// Generate a PodTemplate representing a deployment role
func (generator *PodTemplateGenerator) Generate() (*v1.PodTemplateSpec, []v1.PersistentVolumeClaim, error) {
	meth := "generators.podtemplates.Generate"
	log.Debugf(meth)
	if !generator.Context.Loaded {
		return nil, nil, fmt.Errorf("context not loaded")
	}
	podTemplate, claimTemplates, err := generator.calculatePodTemplate()
	if err != nil {
		return nil, nil, err
	}

	return podTemplate, claimTemplates, nil
}

// calculatePodTemplate returns a PodTemplate representing a given role, including the necessary
// volumes and volumeMounts to expose properties and secrets as files.
func (generator *PodTemplateGenerator) calculatePodTemplate() (
	podTemplate *v1.PodTemplateSpec,
	volumeClaimTemplates []v1.PersistentVolumeClaim,
	err error,
) {
	role := generator.Role
	meth := fmt.Sprintf("generators.podtemplates.calculatePodTemplate. Role: %s", role)
	log.Debugf(meth)
	ports, err := generator.getComponentPorts(role)
	if err != nil {
		return nil, nil, err
	}
	roleInfo, ok := (*generator.Context.RolesInfo)[generator.Role]
	if !ok {
		return nil, nil, fmt.Errorf("role '%s' not found in context", generator.Role)
	}
	volumes, volumeClaimTemplates, err := generator.calculateResourceVolumes()
	if err != nil {
		return nil, nil, err
	}
	containers, initContainers, err := generator.getContainers(role, ports, nil)
	if err != nil {
		return
	}

	if (containers == nil) || (len(*containers) <= 0) {
		return nil, nil, nil

	}
	configMapName := generator.ConfigMap.GetName()

	volumes2, err := generator.calculateFileVolumes(&roleInfo, configMapName)
	if err != nil {
		return nil, nil, err
	}

	if volumes2 != nil {
		if volumes == nil {
			volumes = volumes2
		} else {
			volumes = append(volumes, volumes2...)
		}
	}

	imagePullSecrets, err := generator.calculateImagePullSecrets(&roleInfo)
	if err != nil {
		return nil, nil, err
	}

	// Calculates the spreading constraints of the role instances among the existing nodes.
	topologySpreadConstraints, err := generator.calculateTopologySpreadConstraints()
	if err != nil {
		return nil, nil, err
	}

	// Calculates the FSGroup
	fsGroup, err := generator.caculateFsGroup(role)
	if err != nil {
		return nil, nil, err
	}

	// content, _ := json.MarshalIndent(imagePullSecrets, "", "  ")
	// fmt.Printf("\n\n-------> ROLE: %s IMAGE PULL SECRETS: %s", generator.Role, string(content))

	dnsConfig := generator.calculateDNSConfig()

	annotations := generator.generateAnnotations()

	affinity, err := generator.calculateAffinity()
	if err != nil {
		return nil, nil, err
	}

	podTemplate = &v1.PodTemplateSpec{
		ObjectMeta: metav1.ObjectMeta{},
		Spec: v1.PodSpec{
			Affinity:           affinity,
			Containers:         *containers,
			DNSConfig:          dnsConfig,
			EnableServiceLinks: &generator.EnableServiceLinks,
			PriorityClassName:  generator.Context.Config.UserPodsPriorityClass,
			Volumes:            volumes,
		},
	}

	if initContainers != nil {
		podTemplate.Spec.InitContainers = *initContainers
	}

	if imagePullSecrets != nil {
		podTemplate.Spec.ImagePullSecrets = imagePullSecrets
	}

	if generator.Labels != nil {
		podTemplate.ObjectMeta.Labels = *generator.Labels
	}

	if annotations != nil {
		podTemplate.ObjectMeta.Annotations = *annotations
	}

	if len(topologySpreadConstraints) > 0 {
		podTemplate.Spec.TopologySpreadConstraints = topologySpreadConstraints
	}

	if fsGroup != nil {
		podTemplate.Spec.SecurityContext = &v1.PodSecurityContext{
			FSGroup: fsGroup,
		}
	}

	// content, _ := json.MarshalIndent(podTemplate, "", "  ")
	// fmt.Printf("\n\n------->POD TEMPLATE: %s", string(content))

	return
}

func (generator *PodTemplateGenerator) calculateAffinity() (affinity *v1.Affinity, err error) {
	if generator.Context.Config.EnableNodeAffinities {
		affinity = &v1.Affinity{
			NodeAffinity: &v1.NodeAffinity{
				RequiredDuringSchedulingIgnoredDuringExecution: &v1.NodeSelector{
					NodeSelectorTerms: []v1.NodeSelectorTerm{
						{
							MatchExpressions: []v1.NodeSelectorRequirement{
								{
									Key:      utils.KumoriRoleLabel,
									Operator: v1.NodeSelectorOpIn,
									Values:   []string{utils.KumoriWorkerNodeValue},
								},
							},
						},
					},
				},
			},
		}
	} else {
		affinity = nil
	}

	return
}

func (generator *PodTemplateGenerator) caculateFsGroup(role string) (fsGroup *int64, err error) {
	meth := fmt.Sprintf("generators.podtemplates.caculateFsGroup. AppName: %s. Role: %s. FsGroup: %s. DefaultFsGroup: %d", generator.AppName, role, generator.Context.Config.FsGroup, generator.Context.Config.DefaultFsGroup)
	log.Debug(meth)

	// Gets the fsGroup policy. Returns if fsGroup is disabled
	fsGroupPolicy := generator.Context.Config.FsGroup
	if fsGroupPolicy == string(config.DisabledFsGroupValues) {
		return
	}

	// Gets the default fsGroup
	defaultFsGroup := generator.Context.Config.DefaultFsGroup

	// Gets role info
	roleInfo, ok := (*generator.Context.RolesInfo)[generator.Role]
	if !ok {
		err = fmt.Errorf("role '%s' not found in context", generator.Role)
		return
	}

	// Checks if any role container is using a custom UserID or GroupID
	for _, container := range *roleInfo.ContainersInfo {
		if container.User != nil && ((container.User.GroupID != nil) || (container.User.UserID != nil)) {
			// By default, the fsGroup is set to the default value
			if fsGroup == nil {
				fsGroup = &defaultFsGroup
			}

			// If the fsGroup policy is set to "fixed", returns the default group ID
			if fsGroupPolicy == string(config.FixedFsGroupValues) {
				break
			}

			// If the fsGroupPolicy is set to "user", checks if a group ID is defined for this
			// container. If so, sets the fsGroup to this ID and returns
			if (fsGroupPolicy == string(config.UserFsGroupValues)) && (container.User.GroupID != nil) {
				fsGroup = container.User.GroupID
				break
			}
		}
	}

	// Return the result
	return
}

// calculateResourceVolumes declares the volumes needed for a given resource.
func (generator *PodTemplateGenerator) calculateResourceVolumes() (
	[]v1.Volume,
	[]v1.PersistentVolumeClaim,
	error,
) {
	role := generator.Role
	meth := fmt.Sprintf("generators.podtemplates.calculateResourceVolumes. Role: '%s'", role)

	// Gets the information previously gathered about this role
	roleInfo, ok := (*generator.Context.RolesInfo)[role]
	if !ok {
		return nil, nil, fmt.Errorf("role '%s' not found in context", role)
	}

	volumeClaimTemplates := make([]v1.PersistentVolumeClaim, 0, 3)
	volumes := make([]v1.Volume, 0, 3)

	// Checks if this role is using any volumes
	if roleInfo.Volumes == nil || len(*roleInfo.Volumes) == 0 {
		return nil, nil, nil
	}

	var kerr error

	// Defines the propper Kubernetes elements depending on the role type
	volumeNames := sort.SortKeys(roleInfo.Volumes)
	for _, volumeName := range volumeNames {
		volumeInfo := (*roleInfo.Volumes)[volumeName]

		// Skip if any information has been gathered for this volume
		if volumeInfo == nil {
			continue
		}

		// The volume is volatile
		if volumeInfo.IsVolatile() {

			// This volatile volume has not a size defined
			if volumeInfo.Size == nil {
				message := fmt.Sprintf("V3Deployment: '%s'. Role: '%s'. Error calculating volume claim for volatile volume '%s'. Volatile volume size not defined", generator.Context.ReleaseName, role, volumeName)
				log.Errorf("%s. %s", meth, message)
				kerr = fmt.Errorf(message)
				continue
			}

			// Fills the kubernetes volume with the calculated info
			preparedResourceName := generator.calculateVolumeName(volumeInfo.Name)
			volume := v1.Volume{
				Name: preparedResourceName,
				VolumeSource: v1.VolumeSource{
					EmptyDir: &v1.EmptyDirVolumeSource{
						SizeLimit: volumeInfo.Size,
					},
				},
			}

			// If volatile volumes are provided using a specific storage class...
			if generator.Context.Config.UsePvcForVolatileVolumes {

				// ...cakculates the volume claim for this volume
				volumeClaimTemplate, err := generator.calculateVolatileVolumeClaim(volumeInfo.Name, volumeInfo, generator.Labels, generator.Annotations, nil)
				if err != nil {
					log.Errorf("%s. V3Deployment: '%s'. Role: '%s'. Error calculating volume claim for volatile volume '%s'.", meth, generator.Context.ReleaseName, role, volumeName)
					kerr = err
					continue
				}
				volume.VolumeSource = v1.VolumeSource{
					Ephemeral: &v1.EphemeralVolumeSource{
						VolumeClaimTemplate: volumeClaimTemplate,
					},
				}
			} else {
				volume.VolumeSource = v1.VolumeSource{
					EmptyDir: &v1.EmptyDirVolumeSource{
						SizeLimit: volumeInfo.Size,
					},
				}
			}

			volumes = append(volumes, volume)
		} else if volumeInfo.IsPersistent() {
			// The volume is persistent

			// This volatile volume has not a size defined
			if volumeInfo.KukuVolume == nil {
				message := fmt.Sprintf("V3Deployment: '%s'. Role: '%s'. Error calculating volume claim for persistent volume '%s'. KukuVolume not found", generator.Context.ReleaseName, role, volumeName)
				log.Errorf("%s. %s", meth, message)
				kerr = fmt.Errorf(message)
				continue
			}

			if volumeInfo.IsShared() {
				preparedResourceName := generator.calculateVolumeName(volumeInfo.Name)
				volumeClaimSource := v1.PersistentVolumeClaimVolumeSource{
					ClaimName: generator.calculatePVCName(volumeInfo),
					ReadOnly:  false,
				}
				volume := v1.Volume{
					Name: preparedResourceName,
					VolumeSource: v1.VolumeSource{
						PersistentVolumeClaim: &volumeClaimSource,
					},
				}
				volumes = append(volumes, volume)
			} else {
				// Calculates the claim for this volume
				volumeClaimTemplate, err := generator.calculatePersistentVolumeClaimTemplate(volumeInfo.Name, volumeInfo, generator.Labels, generator.Annotations, nil)
				if err != nil {
					log.Errorf("%s. V3Deployment: '%s'. Role: '%s'. Error calculating volume claim for volume '%s'.", meth, generator.Context.ReleaseName, role, volumeName)
					kerr = err
					continue
				}
				volumeClaimTemplates = append(volumeClaimTemplates, *volumeClaimTemplate)
			}

		} else {
			return nil, nil, fmt.Errorf("unkown type '%s' for volume '%s'", volumeInfo.Type, volumeName)
		}
	}

	// To avoid false positives when calculating differences with the objects stored in Kubernetes
	if len(volumeClaimTemplates) == 0 {
		volumeClaimTemplates = nil
	}
	if len(volumes) == 0 {
		volumes = nil
	}
	if kerr != nil {
		return volumes, volumeClaimTemplates, kerr
	}
	return volumes, volumeClaimTemplates, nil
}

// calculatePersistentVolumeClaimTemplate calculates the volumeClaims for the persistent volumes.
func (generator *PodTemplateGenerator) calculatePersistentVolumeClaimTemplate(
	resourceName string,
	persistentVolume *context.Volume,
	labels *map[string]string,
	annotations *map[string]string,
	owners *[]metav1.OwnerReference,
) (*v1.PersistentVolumeClaim, error) {

	intSize, ok := persistentVolume.KukuVolume.Spec.Size.AsInt64()
	if !ok {
		return nil, fmt.Errorf("cannot parse volume size '%v' as an integer", persistentVolume.KukuVolume.Spec.Size)
	}
	if intSize <= 0 {
		return nil, fmt.Errorf("volume '%s' size must be bigger than 0 and is '%v'", resourceName, intSize)
	}

	// var storageClassName string
	// if spec.FileSystem != nil {
	// 	storageClassName = string(generator.getStorageClassName(*spec.FileSystem))
	// } else {
	// 	storageClassName = string(generator.getStorageClassName("undefined"))
	// }
	storageClassName := ""

	// PVC names must fit a DNS-1123 name: must consist of lower case alphanumeric characters or '-',
	// and must start and end with an alphanumeric character
	preparedResourceName := generator.calculateVolumeName(resourceName)

	// Volatile volumes are destroyed once its V3Deployment is destroyed. That can be
	// improved by adding the assigned POD as the owner but that can only be made once the
	//[resourceName] PVC is created, not now.
	volumeMode := v1.PersistentVolumeFilesystem
	template := v1.PersistentVolumeClaim{
		ObjectMeta: metav1.ObjectMeta{
			Name: preparedResourceName,
		},
		Spec: v1.PersistentVolumeClaimSpec{
			VolumeMode:       &volumeMode,
			StorageClassName: &storageClassName,
			AccessModes:      []v1.PersistentVolumeAccessMode{v1.ReadWriteOnce},
			Resources: v1.ResourceRequirements{
				Requests: v1.ResourceList{
					"storage": persistentVolume.KukuVolume.Spec.Size,
				},
			},
		},
	}

	if owners != nil && *owners != nil && len(*owners) > 0 {
		template.ObjectMeta.OwnerReferences = *owners
	}

	claimLabels := map[string]string{
		utils.KumoriKukuVolumeLabel: persistentVolume.KukuVolume.GetName(),
	}

	if labels != nil {
		keys := sort.SortKeys(labels)
		for _, key := range keys {
			claimLabels[key] = (*labels)[key]

		}
		template.Labels = claimLabels
	}

	if annotations != nil {
		template.Annotations = *annotations
	}

	return &template, nil
}

// calculateVolatileVolumeClaim calculates the volumeClaims for the volatile volumes. This
// is used only if usePvcForVolatileVolumes is true
func (generator *PodTemplateGenerator) calculateVolatileVolumeClaim(
	resourceName string,
	volatileVolume *context.Volume,
	labels *map[string]string,
	annotations *map[string]string,
	owners *[]metav1.OwnerReference,
) (*v1.PersistentVolumeClaimTemplate, error) {

	intSize, ok := volatileVolume.Size.AsInt64()
	if !ok {
		return nil, fmt.Errorf("cannot parse volume size '%v' as an integer", *volatileVolume.Size)
	}
	if intSize <= 0 {
		return nil, fmt.Errorf("volume '%s' size must be bigger than 0 and is '%v'", resourceName, intSize)
	}

	// var storageClassName string
	// if spec.FileSystem != nil {
	// 	storageClassName = string(generator.getStorageClassName(*spec.FileSystem))
	// } else {
	// 	storageClassName = string(generator.getStorageClassName("undefined"))
	// }
	storageClassName := volatileVolume.Type.StorageClass

	// PVC names must fit a DNS-1123 name: must consist of lower case alphanumeric characters or '-',
	// and must start and end with an alphanumeric character
	// preparedResourceName := generator.calculatePVCName(resourceName)

	// Volatile volumes are destroyed once its V3Deployment is destroyed. That can be
	// improved by adding the assigned POD as the owner but that can only be made once the
	//[resourceName] PVC is created, not now.
	volumeMode := v1.PersistentVolumeFilesystem
	template := v1.PersistentVolumeClaimTemplate{
		// ObjectMeta: metav1.ObjectMeta{
		// 	Name: preparedResourceName,
		// },
		Spec: v1.PersistentVolumeClaimSpec{
			StorageClassName: &storageClassName,
			VolumeMode:       &volumeMode,
			AccessModes:      []v1.PersistentVolumeAccessMode{v1.ReadWriteOnce},
			Resources: v1.ResourceRequirements{
				Requests: v1.ResourceList{
					"storage": *volatileVolume.Size,
				},
			},
		},
	}

	if owners != nil && *owners != nil && len(*owners) > 0 {
		template.ObjectMeta.OwnerReferences = *owners
	}

	if labels != nil {
		template.Labels = *labels
	}

	if annotations != nil {
		template.Annotations = *annotations
	}

	return &template, nil
}

// calculateTopologySpreadConstraints calculates the TopologySpreadConstraints for a given role
// pod template
func (generator *PodTemplateGenerator) calculateTopologySpreadConstraints() (
	[]v1.TopologySpreadConstraint,
	error,
) {
	role := generator.Role
	meth := fmt.Sprintf("generators.podtemplates.getTopologySpreadConstraints. Role: %s.", role)

	log.Debug(meth)

	// Topology spread constraints need labels to find out which nodes must be properly spread
	if (generator.Labels == nil) || (len(*generator.Labels) <= 0) {
		log.Warningf("%s. Labels map empty. Topology Spread Constraints cannot be applied without labels", meth)
		return nil, nil
	}

	// If the resilience is 0 and there is not a global max skew, there is no need to restrict
	// the pods spreading
	clusterMaxSkew := generator.Context.Config.TopologySpreadConstraintsMaxSkew
	roleInfo := (*generator.Context.RolesInfo)[role]
	roleResilience := roleInfo.RoleSize.Resilience
	if (roleResilience <= 0) && (clusterMaxSkew <= 0) {
		log.Infof("%s. Skipping section. This role resilience is not set or is '0' and cluster max skew is not set or is '0'", meth)
		return nil, nil
	}

	// Calculates this role max skew. It is calculated using the
	roleInstances := roleInfo.RoleSize.Instances
	maxSkew := clusterMaxSkew
	if roleResilience > 0 {
		maxSkew = int32(math.Max(math.Ceil(float64(roleInstances)/float64(roleResilience)-1), 1))
		if clusterMaxSkew > 0 && clusterMaxSkew < maxSkew {
			maxSkew = clusterMaxSkew
		}
	}

	// Key used to
	topologySoftKeys := generator.Context.Config.TopologySpreadConstraintsSoftLabelKeys
	var softKeys []string
	if len(topologySoftKeys) > 0 {
		softKeys = strings.Split(topologySoftKeys, ",")
	}
	topologyHardKeys := generator.Context.Config.TopologySpreadConstraintsHardLabelKeys
	var hardKeys []string
	if len(topologyHardKeys) > 0 {
		hardKeys = strings.Split(topologyHardKeys, ",")
	}

	topologySpreadConstraints := make([]v1.TopologySpreadConstraint, 0, len(softKeys)+len(hardKeys))

	// Adds hard constrains
	for _, key := range hardKeys {
		item := v1.TopologySpreadConstraint{
			LabelSelector: &metav1.LabelSelector{
				MatchLabels: *generator.SelectorLabels,
			},
			MaxSkew:           maxSkew,
			TopologyKey:       key,
			WhenUnsatisfiable: v1.DoNotSchedule,
		}
		topologySpreadConstraints = append(topologySpreadConstraints, item)
	}

	// Adds soft constrains
	for _, key := range softKeys {
		item := v1.TopologySpreadConstraint{
			LabelSelector: &metav1.LabelSelector{
				MatchLabels: *generator.SelectorLabels,
			},
			MaxSkew:           maxSkew,
			TopologyKey:       key,
			WhenUnsatisfiable: v1.ScheduleAnyway,
		}
		topologySpreadConstraints = append(topologySpreadConstraints, item)
	}

	log.Debugf("%s. MaxSkew: '%d'. TopologySoftKeys: '%s'. TopologyHardKeys: '%s'", meth, maxSkew, topologySoftKeys, topologyHardKeys)

	return topologySpreadConstraints, nil
}

// getContainers returns the objects declaring the containers required by each role
func (generator *PodTemplateGenerator) getContainers(
	role string,
	ports []v1.ContainerPort,
	volumeMountPaths []v1.VolumeMount,
) (
	containers *[]v1.Container,
	initContainers *[]v1.Container,
	err error,
) {
	meth := fmt.Sprintf("generators.podtemplates.getContainers. AppName: %s. Role: %s", generator.AppName, role)
	roleInfo, ok := (*generator.Context.RolesInfo)[generator.Role]
	if !ok {
		err = fmt.Errorf("role '%s' not found in context", generator.Role)
		return
	}

	if roleInfo.ContainersInfo == nil {
		err = fmt.Errorf("%s. Containers not defined", meth)
		// containers = &[]v1.Container{}
		// initContainers = &[]v1.Container{}
		return
	}

	// Creates the volume mounts used to expose parameters and secrets as files
	volumeMounts, err := generator.calculateVolumeMounts(&roleInfo, volumeMountPaths)
	if err != nil {
		return
	}

	// Creates the environment variable declarations used to expose parameters and secrets
	// as environment variables
	envVars, err := generator.calculateEnvVars(&roleInfo)
	if err != nil {
		return
	}

	// Crestes containers and init containers
	containers, err = generator.generateContainers(role, ports, volumeMounts, envVars, roleInfo.ContainersInfo)
	if err != nil {
		return
	}
	if (roleInfo.InitContainersInfo != nil) && (len(*roleInfo.InitContainersInfo) > 0) {
		initContainers, err = generator.generateContainers(role, ports, volumeMounts, envVars, roleInfo.InitContainersInfo)
		if err != nil {
			return
		}
	} else {
		initContainers = &[]v1.Container{}
	}

	return
}

func (generator *PodTemplateGenerator) generateContainers(
	role string,
	ports []v1.ContainerPort,
	volumeMounts *map[string][]v1.VolumeMount,
	envVars *map[string][]v1.EnvVar,
	containersInfo *map[string]context.ContainerInfo,
) (
	*[]v1.Container,
	error,
) {
	meth := fmt.Sprintf("generators.podtemplates.generateContainers. AppName: %s. Role: %s", generator.AppName, role)
	containers := make([]v1.Container, 0, len(*containersInfo))
	keys := sort.SortKeys(containersInfo)
	for _, name := range keys {
		containerInfo := (*containersInfo)[name]
		cpuRequestQuantity, _ := resource.ParseQuantity(containerInfo.Size.MinCPU)
		cpuLimitQuantity, _ := resource.ParseQuantity(containerInfo.Size.CPU)
		if cpuRequestQuantity.Cmp(cpuLimitQuantity) > 0 {
			message := fmt.Sprintf("MinCPU '%s' is greater than CPU '%s' in container %s. Skipping container...", containerInfo.Size.MinCPU, containerInfo.Size.CPU, name)
			log.Errorf("%s. %s", meth, message)
			return nil, fmt.Errorf(message)
		}
		memQuantity, _ := resource.ParseQuantity(containerInfo.Size.Memory)
		containerName := generator.getContainerName(&containerInfo)
		container := v1.Container{
			Name:  containerName,
			Image: containerInfo.Image,
			// Ticket 510: removed hardcoded "Always" image update if "latest" tag is used.
			// ImagePullPolicy: v1.PullIfNotPresent,
			Resources: v1.ResourceRequirements{
				Requests: v1.ResourceList{
					v1.ResourceCPU:    cpuRequestQuantity,
					v1.ResourceMemory: memQuantity,
				},
				Limits: v1.ResourceList{
					v1.ResourceCPU:    cpuLimitQuantity,
					v1.ResourceMemory: memQuantity,
				},
			},
		}

		if containerInfo.Size.Disk != nil {
			diskQuantity, _ := resource.ParseQuantity(*containerInfo.Size.Disk)
			container.Resources.Requests[v1.ResourceEphemeralStorage] = diskQuantity
			container.Resources.Limits[v1.ResourceEphemeralStorage] = diskQuantity
		}

		// Add lifecycle but only to non-init containers. Kubernetes forbids this sections in init
		// containers:
		// https://kubernetes.io/docs/concepts/workloads/pods/init-containers/#differences-from-regular-containers
		if containerInfo.Type != context.InitContainerType {
			container.Lifecycle = &v1.Lifecycle{
				PreStop: &v1.LifecycleHandler{
					Exec: &v1.ExecAction{
						Command: []string{
							"sleep",
							"20",
						},
					},
				},
			}
		}

		// Ticket510: image pull policy is set only if a value is also set in the container info
		// object. Otherwise, Kubernetes default configuration is used (if latest tag is used, pull
		// always, otherwise pull only if not present locally). Since currently the PullPolicy is
		// never set during the context generation, the default behaviour is always used in all pods.
		pullPolicy := generator.getImagePullPolicy(&containerInfo)
		if pullPolicy != nil {
			container.ImagePullPolicy = *pullPolicy
		}

		if containerInfo.Cmd != nil {
			container.Command = *containerInfo.Cmd
		}

		if containerInfo.Args != nil {
			container.Args = *containerInfo.Args
		}

		// If the process running in this container must be executed in a specific
		// user and/or group.
		if containerInfo.User != nil {
			if containerInfo.User.GroupID != nil {
				container.SecurityContext = &v1.SecurityContext{
					RunAsGroup: containerInfo.User.GroupID,
				}
			}
			if containerInfo.User.UserID != nil {
				if container.SecurityContext != nil {
					container.SecurityContext.RunAsUser = containerInfo.User.UserID
				} else {
					container.SecurityContext = &v1.SecurityContext{
						RunAsUser: containerInfo.User.UserID,
					}
				}
			}
		}

		// Add the volume mounts (if any)
		containerVolumeMounts := (*volumeMounts)[containerInfo.Name]
		if containerVolumeMounts != nil {
			container.VolumeMounts = containerVolumeMounts
		}

		// Add the environment variables (if any)
		containerEnvVars := (*envVars)[containerInfo.Name]
		if containerEnvVars != nil {
			container.Env = containerEnvVars
		}

		// Include the exposed ports (if any)
		if ports != nil {
			container.Ports = ports
		}

		// Include the startup and liveness probes (if any)
		startupProbe := generator.getKubeProbe(containerInfo.StartupProbe)
		if startupProbe != nil {
			container.StartupProbe = startupProbe
		}
		livenessProbe := generator.getKubeProbe(containerInfo.LivenessProbe)
		if livenessProbe != nil {
			container.LivenessProbe = livenessProbe
		}
		readinessProbe := generator.getKubeProbe(containerInfo.ReadinessProbe)
		if readinessProbe != nil {
			container.ReadinessProbe = readinessProbe
		} else if livenessProbe != nil {
			container.ReadinessProbe = livenessProbe.DeepCopy()
		}

		containers = append(containers, container)
	}

	return &containers, nil
}

func (generator *PodTemplateGenerator) getKubeProbe(kumoProbe *context.Probe) *v1.Probe {
	if kumoProbe == nil {
		return nil
	}

	// Calculates the probe generic information
	probe := v1.Probe{
		FailureThreshold:    kumoProbe.FailureThreshold,
		PeriodSeconds:       int32(math.Ceil(kumoProbe.Period.Seconds())),
		InitialDelaySeconds: int32(math.Ceil(kumoProbe.InitialDelay.Seconds())),
		TimeoutSeconds:      int32(math.Ceil(kumoProbe.Timeout.Seconds())),
	}

	// If this is an HTTP probe, calculates the HTTP part of the probe
	if kumoProbe.HTTP != nil {
		probe.ProbeHandler.HTTPGet = &v1.HTTPGetAction{
			Path:   kumoProbe.HTTP.Path,
			Port:   intstr.FromInt(int(kumoProbe.HTTP.Port)),
			Scheme: v1.URIScheme(kumoProbe.HTTP.Scheme),
		}
	}

	// If this is a TCP probe, calculates the TCP part of the probe
	if kumoProbe.TCP != nil {
		probe.ProbeHandler.TCPSocket = &v1.TCPSocketAction{
			Port: intstr.FromInt(int(kumoProbe.TCP.Port)),
		}
	}

	// If this is an exec probe, calculates the exec part of the probe
	if kumoProbe.Exec != nil {
		probe.ProbeHandler.Exec = &v1.ExecAction{
			Command: kumoProbe.Exec.Command,
		}
	}

	return &probe
}

// getComponentPorts calculates the ports this role listens to. The ports are
// declared per role and not per container in the V3Deployment.
func (generator *PodTemplateGenerator) getComponentPorts(role string) (ports []v1.ContainerPort, err error) {
	meth := fmt.Sprintf("generators.podtemplates.getComponentPorts. Role: %s", role)
	roleInfo, ok := (*generator.Context.RolesInfo)[generator.Role]
	if !ok {
		return nil, fmt.Errorf("role '%s' not found in context", generator.Role)
	}

	if roleInfo.ChannelsInfo == nil {
		return []v1.ContainerPort{}, nil
	}

	ports = make([]v1.ContainerPort, 0, len(*roleInfo.ChannelsInfo))
	keys := sort.SortKeys(roleInfo.ChannelsInfo)
	for _, key := range keys {
		channelInfo := (*roleInfo.ChannelsInfo)[key]
		if (channelInfo.RoleInConnector == context.ServerChannel) || (channelInfo.RoleInConnector == context.DuplexChannel) {
			name := naming.CreateName(channelInfo.Name)
			generator.Translations[name] = channelInfo.Name
			if channelInfo.Port == nil {
				log.Warnf("%s. Channel '%s' port missing", meth, name)
				continue
			}
			port := *channelInfo.Port
			protocol := utils.CalculateKumoriProtocol(channelInfo.Protocol)
			ports = append(ports, v1.ContainerPort{
				Name:          name,
				ContainerPort: port,
				Protocol:      protocol,
			})
		}
	}

	return
}

// calculateFileVolumes calculates the volumes to expose files
func (generator *PodTemplateGenerator) calculateFileVolumes(
	roleInfo *context.RoleInfo,
	configMapName string,
) ([]v1.Volume, error) {
	meth := fmt.Sprintf("generators.podtemplates.getComponentPorts. AppName: %s, Role: %s ConfigMapName: %s", generator.AppName, roleInfo.Name, configMapName)

	containersSet := []*map[string]context.ContainerInfo{}
	numContainers := 0
	if roleInfo.ContainersInfo != nil {
		containersSet = append(containersSet, roleInfo.ContainersInfo)
		numContainers += len(*roleInfo.ContainersInfo)
	}
	if roleInfo.InitContainersInfo != nil {
		containersSet = append(containersSet, roleInfo.InitContainersInfo)
		numContainers += len(*roleInfo.InitContainersInfo)
	}

	if len(containersSet) <= 0 {
		log.Infof("%s. Containers list is empty", meth)
		return []v1.Volume{}, nil
	}

	items := make([]v1.KeyToPath, 0, 1)
	volumes := make([]v1.Volume, 0, numContainers+1)
	// The key is the Secrets name and the value is an array with the used keys inside that object
	usedSecrets := map[string][]string{}

	for _, containersInfo := range containersSet {
		keys := sort.SortKeys(containersInfo)
		for _, name := range keys {
			containerInfo := (*containersInfo)[name]
			if (containerInfo.Files == nil) || (len(containerInfo.Files) == 0) {
				continue
			}
			files := make([]string, 0, len(containerInfo.Files))
			indexes := make([]string, 0, len(containerInfo.Files))
			for fileIndex, fileInfo := range containerInfo.Files {

				// If the file picks up the data from a secret, just register which secret and which key is
				// used and skip the rest.
				if fileInfo.Secret != nil {
					if _, ok := usedSecrets[fileInfo.Secret.Name]; !ok {
						usedSecrets[fileInfo.Secret.Name] = []string{}
					}
					alreadyInList := false
					for _, elem := range usedSecrets[fileInfo.Secret.Name] {
						if elem == *fileInfo.SecretKey {
							alreadyInList = true
							break
						}
					}
					if !alreadyInList {
						usedSecrets[fileInfo.Secret.Name] = append(usedSecrets[fileInfo.Secret.Name], *fileInfo.SecretKey)
					}
					// This will enable this file hot updates only if the file is not flagged
					// to reboot the POD if it is updated
					if !fileInfo.RebootOnUpdate {
						secretInfo := (*generator.Context.SecretsInfo)[fileInfo.Secret.Name]
						keys := sort.SortKeys(&secretInfo.Data)
						flexVolume := v1.Volume{
							Name: getSecretFlexVolumeName(name, &secretInfo),
							VolumeSource: v1.VolumeSource{
								FlexVolume: &v1.FlexVolumeSource{
									Driver: "kumori/kuvolume",
									Options: map[string]string{
										"container": generator.getContainerName(&containerInfo),
										"volume":    getSecretVolumeName(&secretInfo),
										"files":     strings.Join(keys, ","),
										"indexes":   strconv.Itoa(fileIndex),
										"plugin":    "kubernetes.io~secret",
									},
								},
							},
						}
						volumes = append(volumes, flexVolume)
					}
					continue
				}

				// This step is only needed if the file data is stored in a ConfigMap.
				item := v1.KeyToPath{
					Key:  fileInfo.Name,
					Path: fileInfo.Name,
				}

				if (fileInfo.Perms != nil) && (fileInfo.Perms.Mode != nil) {
					item.Mode = fileInfo.Perms.Mode
				}
				items = append(items, item)

				// This will enable this file hot updates only if the file is not flagged
				// to reboot the POD if it is updated
				if !fileInfo.RebootOnUpdate {
					files = append(files, fileInfo.Name)
					indexes = append(indexes, strconv.Itoa(fileIndex))
				}
			}

			// This volume is used to allow hot updates. The files data is stored in a ConfigMap and that
			// ConfigMap is exposed as a volume. Unfortunately, the ConfigMap volume type not allow hot
			// updates if the files are exposed using `subPath` in the VolumeMount, which is the case to
			// avoid removing the existing files in the path where the file is exposed in the container.
			// The FlexVolume needs to know which files are exposed and in which order are declared in
			// the Container VolumeMount section.
			volumeName := getFileDataVolumeName(roleInfo)
			flexVolume := v1.Volume{
				Name: getFlexVolumeName(name),
				VolumeSource: v1.VolumeSource{
					FlexVolume: &v1.FlexVolumeSource{
						Driver: "kumori/kuvolume",
						Options: map[string]string{
							"container": generator.getContainerName(&containerInfo),
							"volume":    volumeName,
							"files":     strings.Join(files, ","),
							"indexes":   strings.Join(indexes, ","),
						},
					},
				},
			}
			volumes = append(volumes, flexVolume)
		}
	}

	// This volume will expose the ConfigMap keys storing the files data.
	defaultPermsMode := int32(0644)
	volumeName := getFileDataVolumeName(roleInfo)
	volume := v1.Volume{
		Name: volumeName,
		VolumeSource: v1.VolumeSource{
			ConfigMap: &v1.ConfigMapVolumeSource{
				DefaultMode: &defaultPermsMode,
				LocalObjectReference: v1.LocalObjectReference{
					Name: configMapName,
				},
				Items: items,
			},
		},
	}
	volumes = append(volumes, volume)

	// Add the secret volumes
	secretNames := sort.SortKeys(&usedSecrets)
	for _, secretName := range secretNames {
		secretInfo := (*generator.Context.SecretsInfo)[secretName]
		volname := getSecretVolumeName(&secretInfo)
		keys := usedSecrets[secretName]
		items := make([]v1.KeyToPath, 0, len(keys))
		for _, key := range keys {
			items = append(items, v1.KeyToPath{
				Key:  key,
				Path: key,
			})
		}
		name := CreateOpaqueSecretName(generator.Context.ReleaseName, secretInfo.ShortName)
		secretvolume := v1.Volume{
			Name: volname,
			VolumeSource: v1.VolumeSource{
				Secret: &v1.SecretVolumeSource{
					DefaultMode: &defaultPermsMode,
					SecretName:  name,
					Items:       items,
				},
			},
		}
		volumes = append(volumes, secretvolume)
	}

	return volumes, nil
}

func getSecretFlexVolumeName(containerName string, secretsInfo *context.SecretsInfo) string {
	containerNameHash := naming.CreateName(containerName)
	return fmt.Sprintf("%s-%s-flexvol", containerNameHash, secretsInfo.ShortName)
}

func getFlexVolumeName(containerName string) string {
	containerNameHash := naming.CreateName(containerName)
	return fmt.Sprintf("%s-flexvol", containerNameHash)
}

// getSecretVolumeName calculates the name of a volume used to expose a secret
// as a file.
func getSecretVolumeName(secretsInfo *context.SecretsInfo) string {
	return fmt.Sprintf("k-%s-volume", secretsInfo.ShortName)
}

// getFileDataVolumeName calculates the name of a volume used to expose data in a file
func getFileDataVolumeName(roleInfo *context.RoleInfo) string {
	roleNameHash := naming.CreateName(roleInfo.Name)
	return fmt.Sprintf("%s-volume", roleNameHash)
}

// getContainerName creates a name for a container
func (generator *PodTemplateGenerator) getContainerName(containerInfo *context.ContainerInfo) string {
	name := naming.CreateName(containerInfo.Name)
	generator.Translations[name] = containerInfo.Name
	return name
}

// getFlexVolumePath returns the paths used to expose the flexvolumes in containers.
// We have to mount those volume to force the volume plugin execution. The volume
// itself is not used at all.
func getFlexVolumePath(containerName string) string {
	return path.Join("/", "kumori")
}

// calculateVolumeMounts calculates the volume mounts for a given role gathered per container
// name. The volume mounts are used to expose properties and secrets as files in
// containers.
func (generator *PodTemplateGenerator) calculateVolumeMounts(
	roleInfo *context.RoleInfo,
	sharedVolumes []v1.VolumeMount,
) (*map[string][]v1.VolumeMount, error) {
	meth := fmt.Sprintf("generators.podtemplates.calculateVolumeMounts. AppName: %s, Role: %s", generator.AppName, roleInfo.Name)

	containersSet := []*map[string]context.ContainerInfo{}
	numContainers := 0
	if roleInfo.ContainersInfo != nil {
		containersSet = append(containersSet, roleInfo.ContainersInfo)
		numContainers += len(*roleInfo.ContainersInfo)
	}
	if roleInfo.InitContainersInfo != nil {
		containersSet = append(containersSet, roleInfo.InitContainersInfo)
		numContainers += len(*roleInfo.InitContainersInfo)
	}

	if len(containersSet) <= 0 {
		log.Infof("%s. Containers list is empty", meth)
		return &map[string][]v1.VolumeMount{}, nil
	}

	volumeMounts := make(map[string][]v1.VolumeMount, numContainers)

	for _, containersInfo := range containersSet {
		keys := sort.SortKeys(containersInfo)
		for _, name := range keys {
			containerInfo := (*containersInfo)[name]
			numMounts := 1
			if containerInfo.Files != nil {
				numMounts = 1 + len(containerInfo.Files)
			}
			if containerInfo.VolumeMounts != nil {
				numMounts += len(containerInfo.VolumeMounts)
			}
			if sharedVolumes != nil {
				numMounts = numMounts + len(sharedVolumes)
			}
			// A volume mount will be generated per exposed file plus a volume mount to launch the
			// flexVolume in charge of propagating the ConfigMap updates to files
			containerVolumeMounts := make([]v1.VolumeMount, 0, numMounts+1)
			containerVolumeMounts = append(containerVolumeMounts, sharedVolumes...)
			if containerInfo.Files != nil {
				secretsFlexVolumes := make([]v1.VolumeMount, 0, len(containerInfo.Files))
				for _, fileInfo := range containerInfo.Files {
					if fileInfo.Data != nil {
						volumeName := getFileDataVolumeName(roleInfo)
						volumeMount := v1.VolumeMount{
							Name:      volumeName,
							MountPath: fileInfo.Path,
							SubPath:   path.Base(fileInfo.Name),
						}
						containerVolumeMounts = append(containerVolumeMounts, volumeMount)
						continue
					}
					if (fileInfo.Secret != nil) && (fileInfo.SecretKey != nil) {
						secretInfo := (*generator.Context.SecretsInfo)[fileInfo.Secret.Name]
						volumeName := getSecretVolumeName(&secretInfo)
						volumeMount := v1.VolumeMount{
							Name:      volumeName,
							MountPath: fileInfo.Path,
							SubPath:   *fileInfo.SecretKey,
						}
						containerVolumeMounts = append(containerVolumeMounts, volumeMount)
						if !fileInfo.RebootOnUpdate {
							flexname := getSecretFlexVolumeName(name, &secretInfo)
							watcherFileName := fmt.Sprintf(".%s.%s", flexname, "watcher")
							flexVolumeMount := v1.VolumeMount{
								Name:      flexname,
								MountPath: path.Join(getFlexVolumePath(name), watcherFileName),
								SubPath:   ".watcher",
							}
							secretsFlexVolumes = append(secretsFlexVolumes, flexVolumeMount)
						}
						continue
					}

					return nil, fmt.Errorf("cannot mount '%s'. Role: '%s' Container: '%s'", fileInfo.Path, roleInfo.Name, name)
				}

				watcherFileName := fmt.Sprintf(".%s.%s", name, "watcher")
				flexVolumeMount := v1.VolumeMount{
					Name:      getFlexVolumeName(name),
					MountPath: path.Join(getFlexVolumePath(name), watcherFileName),
					SubPath:   ".watcher",
				}
				containerVolumeMounts = append(containerVolumeMounts, flexVolumeMount)
				containerVolumeMounts = append(containerVolumeMounts, secretsFlexVolumes...)
			}
			if containerInfo.VolumeMounts != nil {
				for _, volumeMount := range containerInfo.VolumeMounts {
					preparedResourceName := generator.calculateVolumeName(volumeMount.Volume.Name)
					volumeMount := v1.VolumeMount{
						Name:      preparedResourceName,
						MountPath: volumeMount.Path,
					}
					containerVolumeMounts = append(containerVolumeMounts, volumeMount)
				}
			}
			volumeMounts[containerInfo.Name] = containerVolumeMounts
		}

	}

	return &volumeMounts, nil
}

// calculateEnvVars declares the environment variables set in containers to expose parameters
// and secrets.
func (generator *PodTemplateGenerator) calculateEnvVars(
	roleInfo *context.RoleInfo,
) (*map[string][]v1.EnvVar, error) {
	meth := fmt.Sprintf("generators.podtemplates.calculateEnvVars. AppName: %s, Role: %s", generator.AppName, roleInfo.Name)

	containersSet := []*map[string]context.ContainerInfo{}
	numContainers := 0
	if roleInfo.ContainersInfo != nil {
		containersSet = append(containersSet, roleInfo.ContainersInfo)
		numContainers += len(*roleInfo.ContainersInfo)
	}
	if roleInfo.InitContainersInfo != nil {
		containersSet = append(containersSet, roleInfo.InitContainersInfo)
		numContainers += len(*roleInfo.InitContainersInfo)
	}

	if len(containersSet) <= 0 {
		log.Infof("%s. Containers list is empty", meth)
		return &map[string][]v1.EnvVar{}, nil
	}

	// Environment variable storing the instance ID.
	// TODO: Keeped for historical reasons.
	instanceIDEnv := v1.EnvVar{
		Name: "KUMORI_INSTANCE_ID",
		ValueFrom: &v1.EnvVarSource{
			FieldRef: &v1.ObjectFieldSelector{
				APIVersion: "v1",
				FieldPath:  "metadata.name",
			},
		},
	}

	// Environment variable storing the instance ID
	deprecatedInstanceIDEnv := v1.EnvVar{
		Name: "INSTANCE_ID",
		ValueFrom: &v1.EnvVarSource{
			FieldRef: &v1.ObjectFieldSelector{
				APIVersion: "v1",
				FieldPath:  "metadata.name",
			},
		},
	}

	// Environment variable storing the deployment internal ID
	deploymentEnv := v1.EnvVar{
		Name:  "KUMORI_DEPLOYMENT",
		Value: generator.Context.ReleaseName,
	}

	// Environment variable storing the POD role
	roleEnv := v1.EnvVar{
		Name:  "KUMORI_ROLE",
		Value: roleInfo.Name,
	}

	// Environment variable storing the solution domain/name
	solutionEnv := v1.EnvVar{
		Name:  "KUMORI_SOLUTION_NAME",
		Value: fmt.Sprintf("%s/%s", generator.Context.SolutionDomain, generator.Context.SolutionName),
	}

	// Environment variable storing the solution domain/name
	registrationEnv := v1.EnvVar{
		Name:  "KUMORI_DEPLOYMENT_NAME",
		Value: fmt.Sprintf("%s/%s", generator.Context.RegistrationDomain, generator.Context.RegistrationName),
	}

	dnsSubdomain := generator.calculateRoleSubdomain()
	dnsSubdomainEnv := v1.EnvVar{
		Name:  "KUMORI_ROLE_SUBDOMAIN",
		Value: dnsSubdomain,
	}

	envVars := make(map[string][]v1.EnvVar, numContainers+4)

	for _, containersInfo := range containersSet {
		keys := sort.SortKeys(containersInfo)
		for _, name := range keys {
			containerInfo := (*containersInfo)[name]
			// There are 4 KUMORI_* environment variables plus an extra KUMORI_FILES_HASH if the container
			// mounts files with a rebootOnUpdate flag set to true and plus the environment variables
			// defined in the container manifest
			numEnvs := 8
			if containerInfo.Envs != nil {
				numEnvs += len(containerInfo.Envs)
			}

			// Environment variable storing the has calculated with the mounted files requiring a reboot
			filesHash, err := generator.calculateFilesHash(&containerInfo)
			if err != nil {
				return nil, err
			}
			envsHash, err := generator.calculateEnvsHash(&containerInfo)
			if err != nil {
				return nil, err
			}
			containerEnvVars := make([]v1.EnvVar, 0, numEnvs)
			if filesHash != "" {
				filesHashEnv := v1.EnvVar{
					Name:  "KUMORI_FILES_HASH",
					Value: filesHash,
				}
				containerEnvVars = append(containerEnvVars, filesHashEnv)
			}
			if envsHash != "" {
				envsHashEnv := v1.EnvVar{
					Name:  "KUMORI_ENVS_HASH",
					Value: envsHash,
				}
				containerEnvVars = append(containerEnvVars, envsHashEnv)
			}
			containerEnvVars = append(containerEnvVars, deprecatedInstanceIDEnv, instanceIDEnv, deploymentEnv, registrationEnv, roleEnv, solutionEnv, dnsSubdomainEnv)

			if containerInfo.Envs != nil {
				for _, envVarInfo := range containerInfo.Envs {
					if envVarInfo.Value != nil {
						envVar := v1.EnvVar{
							Name:  envVarInfo.Name,
							Value: *envVarInfo.Value,
						}
						containerEnvVars = append(containerEnvVars, envVar)
						continue
					}
					if (envVarInfo.Secret != nil) && (envVarInfo.SecretKey != nil) {
						secretName := CreateOpaqueSecretName(generator.Context.ReleaseName, envVarInfo.Secret.ShortName)
						envVar := v1.EnvVar{
							Name: envVarInfo.Name,
							ValueFrom: &v1.EnvVarSource{
								SecretKeyRef: &v1.SecretKeySelector{
									LocalObjectReference: v1.LocalObjectReference{
										Name: secretName,
									},
									Key: *envVarInfo.SecretKey,
								},
							},
						}
						containerEnvVars = append(containerEnvVars, envVar)
						continue
					}
					return nil, fmt.Errorf("the environment variable '%s' value is unknown. Role: '%s' Container: '%s'", envVarInfo.Name, roleInfo.Name, containerInfo.Name)
				}
			}
			envVars[containerInfo.Name] = containerEnvVars
		}
	}

	return &envVars, nil
}

// calculateFilesHash calculates a hash using the container files name, path and data. Only
// the files requiring a POD reboot are taken into account. If none of the files require
// that, an empty string is returned.
func (generator *PodTemplateGenerator) calculateFilesHash(containerInfo *context.ContainerInfo) (string, error) {
	envHash := ""
	if containerInfo.Files != nil {
		var h hash.Hash
		for _, fileInfo := range containerInfo.Files {
			if fileInfo.RebootOnUpdate {
				if h == nil {
					h = sha1.New()
				}
				_, err := io.WriteString(h, fileInfo.Name)
				if err != nil {
					return "", err
				}
				_, err = io.WriteString(h, fileInfo.Path)
				if err != nil {
					return "", err
				}
				if fileInfo.Data != nil {
					_, err = h.Write(fileInfo.Data)
					if err != nil {
						return "", err
					}
				}
				if fileInfo.Secret != nil {
					dataNames := sort.SortKeys(&fileInfo.Secret.Data)
					for _, dataName := range dataNames {
						dataBytes := fileInfo.Secret.Data[dataName]
						_, err = h.Write(dataBytes)
						if err != nil {
							return "", err
						}
					}
				}
			}
		}
		if h != nil {
			hashBytes := h.Sum(nil)
			envHash = fmt.Sprintf("%x", hashBytes)
		}
	}
	return envHash, nil
}

// calculateEnvsHash calculates a hash using the envs name and data. Only the environment variables
// filled using secrets are considered since environment variables filled from parameters will be
// rebooted by kubernetes controllers if the environment value changes.
func (generator *PodTemplateGenerator) calculateEnvsHash(containerInfo *context.ContainerInfo) (string, error) {
	envHash := ""
	if containerInfo.Envs != nil {
		var h hash.Hash
		for _, envInfo := range containerInfo.Envs {
			if envInfo.Secret != nil {
				dataNames := sort.SortKeys(&envInfo.Secret.Data)
				for _, dataName := range dataNames {
					if h == nil {
						h = sha1.New()
					}
					dataBytes := envInfo.Secret.Data[dataName]
					_, err := h.Write(dataBytes)
					if err != nil {
						return "", err
					}
				}
			}
		}
		if h != nil {
			hashBytes := h.Sum(nil)
			envHash = fmt.Sprintf("%x", hashBytes)
		}
	}
	return envHash, nil
}

// calculateImagePullSecrets creates the references to the secrets used to pull images
// from private docker hubs.
func (generator *PodTemplateGenerator) calculateImagePullSecrets(
	roleInfo *context.RoleInfo,
) ([]v1.LocalObjectReference, error) {
	meth := fmt.Sprintf("generators.podtemplates.calculateImagePullSecrets. AppName: %s, Role: %s", generator.AppName, roleInfo.Name)

	containersSet := []*map[string]context.ContainerInfo{}
	numContainers := 0
	if roleInfo.ContainersInfo != nil {
		containersSet = append(containersSet, roleInfo.ContainersInfo)
		numContainers += len(*roleInfo.ContainersInfo)
	}
	if roleInfo.InitContainersInfo != nil {
		containersSet = append(containersSet, roleInfo.InitContainersInfo)
		numContainers += len(*roleInfo.InitContainersInfo)
	}

	if len(containersSet) <= 0 {
		log.Infof("%s. Containers list is empty", meth)
		return []v1.LocalObjectReference{}, nil
	}

	imagePullSecrets := make([]v1.LocalObjectReference, 0, 1)

	for _, containersInfo := range containersSet {
		keys := sort.SortKeys(containersInfo)
		for _, name := range keys {
			containerInfo := (*containersInfo)[name]
			if containerInfo.Secret != nil {
				secret := *containerInfo.Secret
				secretName := CreateDockerConfigSecretName(generator.Context.ReleaseName, secret.ShortName)
				found := false
				for _, existing := range imagePullSecrets {
					if existing.Name == secret.Name {
						found = true
						break
					}
				}
				if !found {
					imagePullSecret := v1.LocalObjectReference{
						Name: secretName,
					}
					imagePullSecrets = append(imagePullSecrets, imagePullSecret)
				}
			}
		}
	}

	if len(imagePullSecrets) == 0 {
		return nil, nil
	}
	return imagePullSecrets, nil
}

func (generator *PodTemplateGenerator) calculateVolumeName(original string) string {
	// pvcname := strings.ReplaceAll(original, ".", "-")
	// pvcname = strings.ReplaceAll(pvcname, "_", "-")
	// pvcname = strings.ToLower(pvcname)
	pvcname := naming.CreateName(original)
	generator.Translations[pvcname] = original
	return pvcname
}

func (generator *PodTemplateGenerator) calculatePVCName(volume *context.Volume) string {
	return fmt.Sprintf("%s-shared-pvc", volume.KukuVolume.GetName())
}

// calculateDNSConfig returns the custom DNS configuration to enable the TAG.CHANNEL name
// resolution for this role.
func (generator *PodTemplateGenerator) calculateDNSConfig() *v1.PodDNSConfig {

	// We add to this role the domain <ROLE>.<DEPLOYMENT>.kumori.dep.cluster.local. This
	// domain will be resolve by our own CoreDNS plugin.
	dnsDomain := generator.calculateRoleSubdomain()

	// ¿Why? See https://gitlab.com/kumori/kv3/project-management/-/issues/13#note_288402230
	// We set ndots to 3 because we need the internal DNS to resolve set.<TAG>.<CHANNEL>
	ndots := "3"

	dnsConfig := v1.PodDNSConfig{
		Options: []v1.PodDNSConfigOption{
			{
				Name:  "ndots",
				Value: &ndots,
			},
		},
		Searches: []string{
			dnsDomain,
		},
	}

	return &dnsConfig
}

// calculateRoleSubdomain returns the subdomain used to resolve channel names
func (generator *PodTemplateGenerator) calculateRoleSubdomain() string {
	// <ROLE>.<DEPLOYMENT>.kumori.dep.cluster.local
	return fmt.Sprintf("%s.%s.kumori.dep.cluster.local", hashutil.Hash(generator.Role), generator.Context.Name)

}

// generateAnnotations returns the annotations needed to restrict the bandwidth using the
// values declated in the V3Deployment
func (generator *PodTemplateGenerator) generateAnnotations() *map[string]string {

	meth := "generators.podtemplates.generateAnnotations"

	var annotations *map[string]string

	if generator.Annotations == nil {
		annotations = &map[string]string{}
	} else {
		annotations = generator.Annotations
	}

	if generator.Context.RolesInfo == nil {
		return nil
	}
	roleInfo, ok := (*generator.Context.RolesInfo)[generator.Role]
	if !ok {
		return nil
	}

	if len(generator.Translations) > 0 {
		content, err := json.Marshal(generator.Translations)
		if err != nil {
			log.Errorf("%s. Error creating template annotation: %s", meth, err.Error())
		} else {
			(*annotations)[utils.KumoriTranslations] = string(content)
		}
	}

	(*annotations)["kubernetes.io/egress-bandwidth"] = roleInfo.RoleSize.Bandwidth
	(*annotations)["kubernetes.io/ingress-bandwidth"] = roleInfo.RoleSize.Bandwidth

	return annotations
}

func (generator *PodTemplateGenerator) getImagePullPolicy(containerInfo *context.ContainerInfo) *v1.PullPolicy {

	meth := "generators.podtemplates.getImagePullPolicy"

	if (containerInfo == nil) || (containerInfo.PullPolicy == nil) {
		return nil
	}

	var pullPolicy v1.PullPolicy

	switch *containerInfo.PullPolicy {
	case context.AlwaysPullPolicy:
		pullPolicy = v1.PullAlways
	case context.NeverPullPolicy:
		pullPolicy = v1.PullNever
	case context.IfNotPresentPullPolicy:
		pullPolicy = v1.PullIfNotPresent
	default:
		log.Errorf("%s. Unknown pull policy type '%s'. Returning default pull policy.", meth, string(*containerInfo.PullPolicy))
	}

	return &pullPolicy
}
