/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package generators

import (
	"encoding/json"
	"fmt"
	"kuku-controller/pkg/utils"
	hashutil "kuku-controller/pkg/utils/hash"

	context "kuku-controller/pkg/controllers/deployment/context"

	log "github.com/sirupsen/logrus"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// SecretGenerator generates the secrets required by a given role of a given V3Deployment.
type SecretGenerator struct {
	Context        *context.Context
	ControllerName string
	Owner          string
	Translations   map[string]string
}

// CreateOpaqueSecretName calculates the name of the ConfigMap of a given role
func CreateOpaqueSecretName(appName string, secretName string) string {
	return fmt.Sprintf("%s-%s-secret", appName, secretName)
}

// CreateDockerConfigSecretName calculates the name of the ConfigMap of a given role
func CreateDockerConfigSecretName(appName string, secretName string) string {
	return fmt.Sprintf("%s-%s-dockerconfig", appName, secretName)
}

// NewSecretGenerator returns a new generator to create the kubernetes Secrets required by
// a given role of a given V3Deployment
func NewSecretGenerator(
	kcontext *context.Context,
	controllerName string,
	owner string,
) (generator *SecretGenerator) {
	meth := fmt.Sprintf("generators.secrets.NewSecretGenerator. ControllerName: %s Owner: %s", controllerName, owner)
	log.Debugf(meth)

	return &SecretGenerator{
		Context:        kcontext,
		ControllerName: controllerName,
		Owner:          owner,
		Translations:   map[string]string{},
	}
}

// Generate creates all the Secrets required by a given role from a given V3Deployment. An opaque secret
// will be created per KukuSecret referenced from a environment variable or mounted file. A docker
// config file will be created per KukuSecret used to access a private hub
func (generator *SecretGenerator) Generate() (*[]v1.Secret, error) {
	meth := fmt.Sprintf("generators.secrets.Generate. Namespace: %s ReleaseName: %s", generator.Context.Namespace, generator.Context.ReleaseName)
	log.Debugf(meth)
	labels := generator.generateLabels()
	annotations := generator.generateAnnotations()
	return generator.calculateSecrets(labels, annotations)
}

// calculateSecrets creates the Secret objects used to expose secrets in files and environment variables
func (generator *SecretGenerator) calculateSecrets(
	labels *map[string]string,
	annotations *map[string]string,
) (*[]v1.Secret, error) {

	// If this deployment is not using secrets, the secrets list is empty
	if (generator.Context.SecretsInfo == nil) || (len(*generator.Context.SecretsInfo) <= 0) {
		return &[]v1.Secret{}, nil
	}

	// Create the secrets. It will create a kubernetes.io/dockerconfigjson secret per SecretInfo with the
	// InDockerHub flag set to true. It will create an Opaque secret per SecretInfo with InEnv or InFile
	// flags set to trye
	secrets := make([]v1.Secret, 0, len(*generator.Context.SecretsInfo))
	for _, secretsInfo := range *generator.Context.SecretsInfo {
		if secretsInfo.InDockerHub {
			name := CreateDockerConfigSecretName(generator.Context.ReleaseName, secretsInfo.ShortName)
			secret := v1.Secret{
				ObjectMeta: metav1.ObjectMeta{
					Name:            name,
					Namespace:       generator.Context.Namespace,
					OwnerReferences: *generator.Context.Owners,
				},
				Data: map[string][]byte{
					context.DockerConfigSecretKey: secretsInfo.Data[context.SecretsInfoDefaultKey],
				},
				Type: v1.SecretTypeDockerConfigJson,
			}
			if labels != nil {
				secret.ObjectMeta.Labels = *labels
			}
			if annotations != nil {
				secret.ObjectMeta.Annotations = *annotations
			}
			secrets = append(secrets, secret)
		}
		if secretsInfo.InFile || secretsInfo.InEnv {
			name := CreateOpaqueSecretName(generator.Context.ReleaseName, secretsInfo.ShortName)
			secret := v1.Secret{
				ObjectMeta: metav1.ObjectMeta{
					Name:            name,
					Namespace:       generator.Context.Namespace,
					OwnerReferences: *generator.Context.Owners,
				},
				Data: map[string][]byte{},
				Type: v1.SecretTypeOpaque,
			}
			for key, value := range secretsInfo.Data {
				secret.Data[key] = value
			}
			if labels != nil {
				secret.ObjectMeta.Labels = *labels
			}
			if annotations != nil {
				secret.ObjectMeta.Annotations = *annotations
			}
			secrets = append(secrets, secret)
		}
	}
	// content, _ := json.MarshalIndent(secrets, "", "  ")
	// fmt.Printf("\n\n------->SECRETS: %s", string(content))
	return &secrets, nil
}

// generateLabels generate the labels shared by all generated secrets
func (generator *SecretGenerator) generateLabels() *map[string]string {
	labels := map[string]string{
		utils.KumoriControllerLabel:   generator.ControllerName,
		utils.KumoriDeploymentIdLabel: generator.Context.ReleaseName,
	}
	if generator.Context.KumoriOwner != nil {
		labels[utils.KumoriOwnerLabel] = hashutil.Hash(*generator.Context.KumoriOwner)
	}
	return &labels
}

// generateAnnotations generate the annotations shared by all generated secrets
func (generator *SecretGenerator) generateAnnotations() *map[string]string {

	meth := "generators.secrets.generateAnnotations"

	annotations := map[string]string{}
	if generator.Context.KumoriOwner != nil {
		annotations[utils.KumoriOwnerLabel] = *generator.Context.KumoriOwner
	}

	if len(generator.Translations) > 0 {
		content, err := json.Marshal(generator.Translations)
		if err != nil {
			log.Errorf("%s. Error creating annotation: %s", meth, err.Error())
		} else {
			annotations[utils.KumoriTranslations] = string(content)
		}
	}

	return &annotations
}
