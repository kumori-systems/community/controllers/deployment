/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package generators

import (
	"encoding/json"
	"fmt"
	"kuku-controller/pkg/utils"
	hashutil "kuku-controller/pkg/utils/hash"

	log "github.com/sirupsen/logrus"
	"k8s.io/apimachinery/pkg/util/intstr"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	context "kuku-controller/pkg/controllers/deployment/context"

	v1 "k8s.io/api/core/v1"
)

// ServiceGenerator generates a connector service from Kuku objects. If the "Balancer" flag is set
// to "true" then the resulting services will NOT have a cluster IP assigned and, hence, the
// service name will be resolved by the DNS with the final connector targets (an IP and PORT
// records). If "headless" is set to "false" then the service name is resolved to the Cluster IP.
type ServiceGenerator struct {
	Balancer        bool
	ConnectorID     string
	Context         *context.Context
	ControllerName  string
	Name            *string
	Owner           string
	Port            int32
	PublishNonReady bool
	Translations    map[string]string
}

// ServiceConnectorAnnotation contains the structure of the JSON document stored in the
// `connector` annotation.
// * Name: the original connector name, without the tag
// * Tag: the tag name
// * Kind: the connector type ("full" or "lb")
// * Service: the connector service application name
// * Deployment: the connector deployment name
// * Clients: the client channels (role, channel)
// * Servers: the server channels (role, channel)
// * Duplex: the duplex channels (role, channel)
type ServiceConnectorAnnotation struct {
	Name          string                               `json:"name"`
	Tag           string                               `json:"tag"`
	Kind          string                               `json:"kind"`
	Service       string                               `json:"service"`
	Deployment    string                               `json:"deployment"`
	Clients       []ServiceConnectorAnnotationEndpoint `json:"clients"`
	Servers       []ServiceConnectorAnnotationEndpoint `json:"servers"`
	LinkedClients []ServiceConnectorAnnotationEndpoint `json:"linkedClients"`
	LinkedServers []ServiceConnectorAnnotationEndpoint `json:"linkedServers"`
	Duplex        []ServiceConnectorAnnotationEndpoint `json:"duplex"`
}

// ServiceConnectorAnnotationEndpoint contains information about a channel endpoint
type ServiceConnectorAnnotationEndpoint struct {
	Deployment *string `json:"deployment,omitempty"`
	Role       *string `json:"role,omitempty"`
	Channel    string  `json:"channel"`
	Port       *int32  `json:"port,omitempty"`
}

// CreateConnectorServiceName calculates the name of the service related to a connector. The serice
// name will variate depending on if it is used to provided a list of targets or to balance
// balance requests.
func CreateConnectorServiceName(releaseName string, connectorID string, balancer bool, publishNonReady bool) string {
	connectorIDHash := hashutil.Hash(connectorID)
	if balancer {
		return fmt.Sprintf("%s-%s-lb-service", releaseName, connectorIDHash)
	} else if !publishNonReady {
		return fmt.Sprintf("%s-%s-ready-service", releaseName, connectorIDHash)
	}
	return fmt.Sprintf("%s-%s-service", releaseName, connectorIDHash)
}

// NewHeadlessServiceGenerator creates a new ServiceGenerator to register a given
// connector targets (IP + PORT). If a name is not provided, it will be calculated from
// the release name and the connector ID
func NewHeadlessServiceGenerator(
	connectorID string,
	kcontext *context.Context,
	controllerName string,
	owner string,
	port int32,
	name *string,
	publishNonReady bool,
) (generator *ServiceGenerator) {
	meth := fmt.Sprintf("generators.services.NewHeadlessServiceGenerator. ConnectorID: %s ControllerName: %s Owner: %s Port: %d", connectorID, controllerName, owner, port)
	log.Debugf(meth)
	return &ServiceGenerator{
		Balancer:        false,
		ConnectorID:     connectorID,
		Context:         kcontext,
		ControllerName:  controllerName,
		Name:            name,
		Owner:           owner,
		Port:            port,
		PublishNonReady: publishNonReady,
		Translations:    map[string]string{},
	}
}

// NewBalancerServiceGenerator creates a new ServiceGenerator to balance among a given connector
// targets. If a name is not provided, it will be calculated from the release name and the connector ID
func NewBalancerServiceGenerator(
	connectorID string,
	kcontext *context.Context,
	controllerName string,
	owner string,
	port int32,
	name *string,
) (generator *ServiceGenerator) {
	meth := fmt.Sprintf("generators.services.NewBalancerServiceGenerator. ConnectorID: %s ControllerName: %s Owner: %s Port: %d", connectorID, controllerName, owner, port)
	log.Debugf(meth)
	return &ServiceGenerator{
		Balancer:        true,
		ConnectorID:     connectorID,
		Context:         kcontext,
		ControllerName:  controllerName,
		Name:            name,
		Owner:           owner,
		Port:            port,
		PublishNonReady: false,
		Translations:    map[string]string{},
	}
}

// Generate generates a service from Kuku objects
func (generator *ServiceGenerator) Generate() (*v1.Service, error) {
	meth := fmt.Sprintf("generators.services.Generate. Namespace: %s ReleaseName: %s", generator.Context.Namespace, generator.Context.ReleaseName)
	log.Debugf(meth)
	if generator.Name == nil {
		name := CreateConnectorServiceName(generator.Context.ReleaseName, generator.ConnectorID, generator.Balancer, generator.PublishNonReady)
		generator.Name = &name
	}
	labels := generator.generateLabels()
	annotations := generator.generateAnnotations()
	return generator.calculateService(labels, annotations)
}

func (generator *ServiceGenerator) calculateService(
	labels *map[string]string,
	annotations *map[string]string,
) (service *v1.Service, err error) {
	meth := fmt.Sprintf("generators.services.calculateService. Namespace: %s ReleaseName: %s", generator.Context.Namespace, generator.Context.ReleaseName)
	log.Debugf(meth)
	name := *generator.Name
	connectorInfo, ok := (*generator.Context.ConnectorsInfo)[generator.ConnectorID]
	if !ok {
		return nil, fmt.Errorf("cannot retrieve information about connector %s", generator.ConnectorID)
	}
	protocol, err := generator.calculateConnectorTagProtocol(&connectorInfo)
	if err != nil {
		return nil, err
	}
	targetPort := intstr.FromInt(int(generator.Port))
	ipFamilyPolicy := v1.IPFamilyPolicySingleStack
	service = &v1.Service{
		ObjectMeta: metav1.ObjectMeta{
			Name:            name,
			Namespace:       generator.Context.Namespace,
			OwnerReferences: *generator.Context.Owners,
		},
		Spec: v1.ServiceSpec{
			Type:            v1.ServiceTypeClusterIP,
			SessionAffinity: "None",
			IPFamilies:      []v1.IPFamily{v1.IPv4Protocol},
			IPFamilyPolicy:  &ipFamilyPolicy,
			Ports: []v1.ServicePort{
				{
					Port:       generator.Port,
					TargetPort: targetPort,
					Protocol:   protocol,
					Name:       connectorInfo.ShortName,
				},
			},
		},
	}

	if !generator.Balancer {
		service.Spec.ClusterIP = "None"
	}

	if labels != nil {
		service.ObjectMeta.Labels = *labels
	}

	if annotations != nil {
		service.ObjectMeta.Annotations = *annotations
	}

	if generator.PublishNonReady {
		service.Spec.PublishNotReadyAddresses = true
	}

	// content, _ := json.MarshalIndent(*service, "", "  ")
	// fmt.Printf("\n\n------->SERVICE: %s", string(content))

	return service, nil
}

// generateAnnotations returns the annotations to be added to the services.
// Currently, a single `connector` annotation is added with a JSON document
// constructed from a `ServiceConnectorAnnotation`
func (generator *ServiceGenerator) generateAnnotations() *map[string]string {

	meth := "generators.services.generateAnnotations"

	if generator.Context.ConnectorsInfo == nil {
		return nil
	}
	connectorInfo, ok := (*generator.Context.ConnectorsInfo)[generator.ConnectorID]
	if !ok {
		return nil
	}
	connector := (*generator.Context.ConnectorsInfo)[generator.ConnectorID]
	clients := make([]ServiceConnectorAnnotationEndpoint, 0, len(connector.Clients))
	servers := make([]ServiceConnectorAnnotationEndpoint, 0, len(connector.Servers))
	linkedClients := make([]ServiceConnectorAnnotationEndpoint, 0, len(connector.LinkedClients))
	linkedServers := make([]ServiceConnectorAnnotationEndpoint, 0, len(connector.LinkedServers))
	duplex := []ServiceConnectorAnnotationEndpoint{}
	var kind string

	if connector.Type == context.LoadBalancerConnector {
		kind = "lb"
	} else {
		kind = "full"
	}

	for _, client := range connector.Clients {
		endpoint := ServiceConnectorAnnotationEndpoint{
			Role:    client.Role,
			Channel: client.ChannelName,
		}
		if client.ChannelType == context.DuplexChannel {
			endpoint.Port = client.Port
			duplex = append(duplex, endpoint)
			continue
		}
		clients = append(clients, endpoint)
	}

	for _, client := range connector.LinkedClients {
		deployment := client.Deployment
		endpoint := ServiceConnectorAnnotationEndpoint{
			Deployment: &deployment,
			Role:       client.Role,
			Channel:    client.ChannelName,
		}
		linkedClients = append(linkedClients, endpoint)
	}

	for _, server := range connector.Servers {
		endpoint := ServiceConnectorAnnotationEndpoint{
			Role:    server.Role,
			Channel: server.ChannelName,
			Port:    server.Port,
		}
		if server.ChannelType != context.DuplexChannel {
			servers = append(servers, endpoint)
		}
	}

	for _, server := range connector.LinkedServers {
		deployment := server.Deployment
		endpoint := ServiceConnectorAnnotationEndpoint{
			Deployment: &deployment,
			Role:       server.Role,
			Channel:    server.ChannelName,
			Port:       server.Port,
		}
		if server.ChannelType != context.DuplexChannel {
			linkedServers = append(linkedServers, endpoint)
		}
	}

	connectorAnnotation := ServiceConnectorAnnotation{
		Name:          connector.ConnectorName,
		Tag:           connector.TagName,
		Kind:          kind,
		Service:       generator.Context.ServiceInfo.Reference.Name,
		Deployment:    generator.Context.Name,
		Clients:       clients,
		Servers:       servers,
		LinkedClients: linkedClients,
		LinkedServers: linkedServers,
		Duplex:        duplex,
	}

	content, _ := json.MarshalIndent(connectorAnnotation, "", "  ")
	annotations := map[string]string{
		utils.KumoriConnectorDataLabel: string(content),
		utils.KumoriConnectorLabel:     connectorInfo.ConnectorName,
	}

	if generator.Context.KumoriOwner != nil {
		annotations[utils.KumoriOwnerLabel] = *generator.Context.KumoriOwner
	}

	if len(generator.Translations) > 0 {
		content, err := json.Marshal(generator.Translations)
		if err != nil {
			log.Errorf("%s. Error creating annotation: %s", meth, err.Error())
		} else {
			annotations[utils.KumoriTranslations] = string(content)
		}
	}

	return &annotations
}

// generateLabels returns the labels to be included in each Service representing
// a given container tag
func (generator *ServiceGenerator) generateLabels() *map[string]string {
	if generator.Context.ConnectorsInfo == nil {
		return nil
	}
	connectorInfo, ok := (*generator.Context.ConnectorsInfo)[generator.ConnectorID]
	if !ok {
		return nil
	}
	labels := map[string]string{
		utils.KumoriDeploymentIdLabel: generator.Context.ReleaseName,
		utils.KumoriConnectorLabel:    hashutil.Hash(connectorInfo.ConnectorName),
		utils.KumoriControllerLabel:   generator.ControllerName,
		utils.KumoriTagLabel:          connectorInfo.TagName,
	}

	theLinkedEndpoints := [][]*context.ConnectorEndpoint{connectorInfo.LinkedClients, connectorInfo.LinkedServers}

	for _, linkedEndpoints := range theLinkedEndpoints {
		if len(linkedEndpoints) > 0 {
			for _, linkedEndpoint := range linkedEndpoints {
				if linkedEndpoint.Links != nil {
					for _, link := range linkedEndpoint.Links {
						labels[link] = "link-in-use"
					}
				}
			}
		}
	}

	if generator.Context.KumoriOwner != nil {
		labels[utils.KumoriOwnerLabel] = hashutil.Hash(*generator.Context.KumoriOwner)
	}

	return &labels
}
