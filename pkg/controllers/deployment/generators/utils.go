/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package generators

import (
	"fmt"
	context "kuku-controller/pkg/controllers/deployment/context"
	"kuku-controller/pkg/utils"

	log "github.com/sirupsen/logrus"
	v1 "k8s.io/api/core/v1"
)

func copyStringMap(source *map[string]string) *map[string]string {
	if source == nil {
		return nil
	}
	target := make(map[string]string, len(*source))
	for key, value := range *source {
		target[key] = value
	}
	return &target
}

func (generator *ServiceGenerator) calculateConnectorTagProtocol(connectorTag *context.ConnectorTagInfo) (v1.Protocol, error) {
	meth := fmt.Sprintf("generators.services.calculateProtocol. V3Deployment: %s. Connector: %s. Tag: %s", generator.Context.Deployment.Name, connectorTag.ConnectorName, connectorTag.TagName)

	var protocol string
	for _, server := range connectorTag.Servers {

		if protocol == "" {
			protocol = server.Protocol
		} else {
			if server.Protocol != protocol {
				message := fmt.Sprintf("All server/duplex channels must declare the same protocol")
				log.Errorf("%s. %s", meth, message)
				return v1.ProtocolTCP, fmt.Errorf(message)
			}
		}
	}

	if protocol == "" {
		message := fmt.Sprintf("Connector protocol unknown")
		return v1.ProtocolTCP, fmt.Errorf(message)
	}

	return utils.CalculateKumoriProtocol(protocol), nil
}
