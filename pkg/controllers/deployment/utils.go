/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package deployment

import (
	"fmt"
	"reflect"

	context "kuku-controller/pkg/controllers/deployment/context"

	log "github.com/sirupsen/logrus"
	viper "github.com/spf13/viper"
)

// InterfaceToSlice converts an interface{} to []interface{} if obj is an array or slice.
// It also returns a boolan indicating if the conversion was successfull or not.
func InterfaceToSlice(obj interface{}) ([]interface{}, bool) {
	v := reflect.ValueOf(obj)
	if (v.Kind() == reflect.Array) || (v.Kind() == reflect.Slice) {
		list := make([]interface{}, v.Len())
		for i := 0; i < v.Len(); i++ {
			list[i] = v.Index(i).Interface()
		}
		return list, true
	}
	return nil, false
}

// CalculateServiceChannelLabelName returns the name of the label to be used in a connector
// service. This label is used to know which service connector has a given service channel linked.
func CalculateServiceChannelLabelName(name string) string {
	return fmt.Sprintf("service-channel-%s", name)
}

// CalculateChannelConnectorLabel returns the name and value of the label representing the role
// of a channel in its connector.
func CalculateChannelConnectorLabel(connectorName string, chanelRole context.ChannelType) (key string, value string) {
	key = fmt.Sprintf("%s%s", connectorName, chanelRole)
	value = "true"
	return
}

// GetChannelPort finds the channel port, including children for combo channels
func GetChannelPort(ch *context.ChannelInfo, channels *map[string]context.ChannelInfo) *int32 {
	if ch == nil {
		return nil
	}
	return ch.Port
}

// UpdateLogLevel from configuration
func UpdateLogLevel() {
	logLevel := viper.GetString("logLevel")
	log.Infof("Setting log level to %s", logLevel)
	switch logLevel {
	case "debug":
		log.SetLevel(log.DebugLevel)
	case "info":
		log.SetLevel(log.InfoLevel)
	case "warn":
		log.SetLevel(log.WarnLevel)
	case "error":
		log.SetLevel(log.ErrorLevel)
	default:
		log.Warnf("Unknown log level %s. Ignored", logLevel)
	}
}
