/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package context

import kumoriv1 "gitlab.com/kumori-systems/community/libraries/client-go/pkg/apis/kumori/v1"

// calculateChannelsInfo returns information about a given channel in a given role
func (context *Context) calculateChannelsInfo(
	role string,
	srv *kumoriv1.MicroService,
) (*map[string]ChannelInfo, error) {
	channelsInfo := map[string]ChannelInfo{}

	if srv == nil {
		return &channelsInfo, nil
	}

	if srv.Clients != nil {
		for name, clientInfo := range *srv.Clients {
			channelInfo, err := context.calculateClientsInfo(name, &clientInfo, role)
			if err != nil {
				return nil, err
			}
			channelsInfo[name] = *channelInfo
		}
	}

	if srv.Servers != nil {
		for name, serverInfo := range *srv.Servers {
			channelInfo, err := context.calculateServerInfo(name, &serverInfo, role)
			if err != nil {
				return nil, err
			}
			channelsInfo[name] = *channelInfo
		}
	}

	if srv.Duplex != nil {
		for name, duplexInfo := range *srv.Duplex {
			channelInfo, err := context.calculateDuplexInfo(name, &duplexInfo, role)
			if err != nil {
				return nil, err
			}
			channelsInfo[name] = *channelInfo
		}
	}

	return &channelsInfo, nil
}

// calculateClientsInfo returns information about a role client channel
func (context *Context) calculateClientsInfo(
	clientName string,
	client *kumoriv1.Client,
	role string,
) (*ChannelInfo, error) {
	var err error
	channelRole := ClientChannel
	if role == "self" {
		channelRole = ServerChannel
	}
	if err != nil {
		return nil, err
	}
	channelInfo := ChannelInfo{
		Name:            clientName,
		Type:            ClientChannel,
		Protocol:        string(client.Protocol),
		RoleInConnector: channelRole,
	}

	return &channelInfo, nil
}

// calculateClientsInfo returns information about a role server channel.
func (context *Context) calculateServerInfo(
	serverName string,
	server *kumoriv1.Server,
	role string,
) (*ChannelInfo, error) {
	var err error
	channelRole := ServerChannel
	if role == "self" {
		channelRole = ClientChannel
	}
	if err != nil {
		return nil, err
	}
	port := server.Port
	channelInfo := ChannelInfo{
		Name:            serverName,
		Type:            ServerChannel,
		Protocol:        string(server.Protocol),
		Port:            &port,
		RoleInConnector: channelRole,
	}
	return &channelInfo, nil
}

// calculateClientsInfo returns information about a role duplex channel.
func (context *Context) calculateDuplexInfo(
	duplexName string,
	duplex *kumoriv1.Duplex,
	role string,
) (*ChannelInfo, error) {
	port := duplex.Port
	channelInfo := ChannelInfo{
		Name:            duplexName,
		Type:            DuplexChannel,
		Protocol:        string(duplex.Protocol),
		Port:            &port,
		RoleInConnector: DuplexChannel,
	}

	return &channelInfo, nil
}
