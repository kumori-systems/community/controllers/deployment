/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package context

import (
	"fmt"
	"strconv"

	log "github.com/sirupsen/logrus"
	kumoriv1 "gitlab.com/kumori-systems/community/libraries/client-go/pkg/apis/kumori/v1"
)

// calculateServiceInfo calcula la información del servicio del despliegue
//
//	ServiceInfo: contiene la siguiente información en relación con el servicio:
//
// * Name: nombre del servicio
// * ChannelsInfo: es un mapa que contiene la siguiente información sobre los canales de servicio (clave: nombre canal):
func (context *Context) calculateServiceInfo() (*ServiceInfo, error) {
	meth := "context.services.calculateServiceInfo"
	// If the deployment hasn't a service assignet yet then we cannot retrieve information about the service
	if (context.Deployment.Spec.Artifact == nil) ||
		(context.Deployment.Spec.Artifact.Description == nil) {
		log.Infof("%s. Service description not found for V3Deployment '%s'. Skipping", meth, context.Deployment.Name)
		return nil, nil
	}

	// If the deployment has a builtin service then we cannot retrieve information about the service
	if context.Deployment.Spec.Artifact.Description.Builtin {
		log.Infof("%s. Built-in service found for V3Deployment '%s'. Skipping", meth, context.Deployment.Name)
		return nil, nil
	}

	serviceInfo := ServiceInfo{
		Reference: Reference{
			Name:    string(context.Deployment.Spec.Artifact.Ref.Name),
			Domain:  string(context.Deployment.Spec.Artifact.Ref.Domain),
			Version: []int16(context.Deployment.Spec.Artifact.Ref.Version),
		},
	}

	if context.Deployment.Spec.Artifact.Ref.Prerelease != nil {
		serviceInfo.Reference.Prerelease = context.Deployment.Spec.Artifact.Ref.Prerelease
	}

	// Retrieves information about the service channels.
	channelsInfo, err := context.calculateChannelsInfo("self", context.Deployment.Spec.Artifact.Description.Srv)
	if err != nil {
		return nil, err
	}
	if channelsInfo != nil {
		serviceInfo.ChannelsInfo = channelsInfo
	}

	// Return the information gathered about this deployment service.
	return &serviceInfo, nil
}

// ConnectorsInfo: contiene información de los conectores (la clave es el nombre del conector):
// * Name: nombre del conector.
// * ShortName: es el nombre inventado por el Context (connector0). Se utiliza en NetworkPolicies, Services y Pods.
// * Port: puerto en el que escucha el conector (80 por defecto)
// * Type: tipo del conector
// DEPRECATED: Now calculateTags in connector.go is used
func (context *Context) calculateConnectorsInfo_DEPRECATED() (*map[string]ConnectorTagInfo, error) {
	meth := "context.services.calculateConnectorsInfo"

	// If the deployment hasn't a service assignet yet then we cannot retrieve information about the service connectors
	if (context.Deployment.Spec.Artifact == nil) ||
		(context.Deployment.Spec.Artifact.Description == nil) {
		log.Infof("%s. Service description not found for V3Deployment '%s'. Skipping", meth, context.Deployment.Name)
		return nil, nil
	}

	// If the deployment has a builtin service then we cannot retrieve information about the service
	if context.Deployment.Spec.Artifact.Description.Builtin {
		log.Infof("%s. Built-in service found for V3Deployment '%s'. Skipping", meth, context.Deployment.Name)
		return nil, nil
	}

	// If the connectors list is empty, skip this step
	if context.Deployment.Spec.Artifact.Description.Connectors == nil {
		log.Infof("%s.Connectors not found for V3Deployment '%s'. Skipping", meth, context.Deployment.Name)
		return nil, nil
	}
	connectors := *context.Deployment.Spec.Artifact.Description.Connectors

	// Get the client and server channels (including duplex) connected to each connector. It
	// also returns the client and server channels from other deploymens that can be
	// indirectly reached through inter-service links.
	clients, servers, linkedClients, linkedServers, err := context.getConnectorsChannels()
	if err != nil {
		return nil, err
	}

	connectorsInfo := map[string]ConnectorTagInfo{}

	// Gets an ordered list of connector name to desterministically calculate the connectors short name
	connectorNames := context.GetOrderedConnectorNames()
	port := int32(80)
	counter := 0
	for _, connectorName := range connectorNames {

		connectorData, ok := connectors[connectorName]
		if !ok {
			log.Errorf("%s. Error retrieving connector %s in V3Deployment '%s'. Skipping", meth, connectorName, context.Deployment.Name)
			continue
		}

		if connectorData.Servers == nil || len(connectorData.Servers) <= 0 {
			log.Infof("%s. Tags not found for connector '%s'. Skipping", meth, context.Deployment.Name)
			continue
		}

		for j := range connectorData.Servers {
			tagName := strconv.Itoa(j)
			taggedConnectorName := GetTaggedConnectorName(connectorName, tagName)
			shortName := fmt.Sprintf(`connector%d`, counter)
			connectorInfo := ConnectorTagInfo{
				Name:          taggedConnectorName,
				ShortName:     shortName,
				ConnectorName: connectorName,
				TagName:       tagName,
				Port:          port,
				Type:          getConnectorType(connectorData.Kind),
				Servers:       (*servers)[taggedConnectorName],
				Clients:       (*clients)[taggedConnectorName],
				LinkedServers: (*linkedServers)[taggedConnectorName],
				LinkedClients: (*linkedClients)[taggedConnectorName],
			}
			connectorsInfo[taggedConnectorName] = connectorInfo
			counter++
		}

	}

	return &connectorsInfo, nil
}

// getConnectorChannels returns four maps. The first one, contains the client channels (including
// duplex) connected to each connector-tag. The second one, the server channels (including duplex)
// connected to each connector-tag. The third one the client channels (including duplex) reachable
// through links for each connector tags. The fourth one the server channels (including duplex)
// reachable through links for each connector tags. Service channels type is switched (server are
// added as clients and the other way arround) to represent their role in the connector, not in the
// service interface. The lists also contain the role/channels pairs from linked deployments.
func (context *Context) getConnectorsChannels() (
	clients *map[string][]*ConnectorEndpoint,
	servers *map[string][]*ConnectorEndpoint,
	linkedClients *map[string][]*ConnectorEndpoint,
	linkedServers *map[string][]*ConnectorEndpoint,
	err error,
) {
	meth := "context.services.getConnectorsChannels"

	clients = &map[string][]*ConnectorEndpoint{}
	servers = &map[string][]*ConnectorEndpoint{}
	linkedClients = &map[string][]*ConnectorEndpoint{}
	linkedServers = &map[string][]*ConnectorEndpoint{}

	connectors := *context.Deployment.Spec.Artifact.Description.Connectors

	for name, data := range connectors {

		// Skips the connector if it has not server tags defined since we can not split that connector
		// in connector-tag tuples.
		if data.Servers == nil || len(data.Servers) <= 0 {
			log.Warnf("%s. Tags not found for connector '%s'. Skipping", meth, name)
			continue
		}

		// Calculates the connector client endpoints for this connector. All connector-tag tuples will
		// have those connectors plus the duplex channels which depend on the tags.
		var connectorClients []*ConnectorEndpoint
		var connectorLinkedClients []*ConnectorEndpoint
		if (data.Clients != nil) && (len(data.Clients) > 0) {
			connectorClients = make([]*ConnectorEndpoint, 0, len(data.Clients))
			connectorLinkedClients = make([]*ConnectorEndpoint, 0, 1)
			for _, client := range data.Clients {

				// Gets the channel information from the channels declaration in the manifest. Service
				// channels type is switched to represent their role in the connector instead of their
				// role in the service.
				endpoint, err := getConnectorEndpoint(client.Role, client.Channel, context.Deployment, nil)
				if err != nil {
					log.Errorf("%s. Skipping connector '%s'. Error geting endpoint for role '%s' and channel '%s': %s", meth, name, client.Role, client.Channel, err.Error())
					continue
				}

				// Duplex channels can only be connected as servers and, hence, are skipped if appear as
				// clients.
				if endpoint.ChannelType == DuplexChannel {
					log.Warnf("%s. Channel '%s' from tole '%s' in deployment '%s' is declared as a duplex channel and cannot be connected in the clients side. Skipping", meth, client.Channel, client.Role, context.Deployment.Name)
					continue
				}

				// Adds the endpoint in the clients lists but only if it has not been added before
				connectorClients = appendEndpoint(connectorClients, endpoint)

				// Gets role/channels from linked deployments if the endpoint is a service channel
				if endpoint.Role == nil {
					linkedEndpoints, err := context.getLinkedEndpoints(endpoint)
					if err != nil {
						log.Errorf("%s. Error geting linked endpoints for role '%s' and channel '%s'. Skipping", meth, client.Role, client.Channel)
						continue
					}
					if linkedEndpoints != nil {
						for _, linkedEndpoint := range linkedEndpoints {
							connectorLinkedClients = appendEndpoint(connectorLinkedClients, linkedEndpoint)
						}
					}
				}
			}
		} else {
			connectorClients = []*ConnectorEndpoint{}
			connectorLinkedClients = []*ConnectorEndpoint{}
		}

		// Calculates the servers endpoints for each connector-tag tuple. If one of those endpoints is
		// a duplex channel, it is also added as a client endpoint to that connector-tag tuple
		for i, tag := range data.Servers {

			// Connectors are splitted in one connector per connector and tag
			connectorName := GetTaggedConnectorName(name, strconv.Itoa(i))

			// This will contains the client channels for this connector-tag, which are the clients
			// for this connector plus the duplex channels in this tag.
			tagClients := make([]*ConnectorEndpoint, len(connectorClients))
			copy(tagClients, connectorClients)
			tagLinkedClients := make([]*ConnectorEndpoint, len(connectorLinkedClients))
			copy(tagLinkedClients, connectorLinkedClients)
			if tag.Links == nil || len(tag.Links) <= 0 {
				log.Warnf("%s. Links list empty for connector '%s' and tag '%d'", meth, name, i)
				(*servers)[connectorName] = []*ConnectorEndpoint{}
				continue
			}

			// This will contains the server channels for this connector-tag
			tagServers := make([]*ConnectorEndpoint, len(tag.Links))
			tagLinkedServers := make([]*ConnectorEndpoint, 0, 1)
			for j, server := range tag.Links {

				// Gets the channel information from the channels declaration in the manifest. Service
				// channels type is switched to represent their role in the connector instead of their
				// role in the service.
				endpoint, err := getConnectorEndpoint(server.Role, server.Channel, context.Deployment, nil)
				if err != nil {
					log.Errorf("%s. Skipping server '%d'. Error geting endpoint for role '%s' and channel '%s': %s", meth, i, server.Role, server.Channel, err.Error())
					continue
				}

				// Adds the endpoint to the server channels list for this tag
				tagServers[j] = endpoint

				// If the channel is a duplex channels, adds it to this connector-tag client channels list.
				if endpoint.ChannelType == DuplexChannel {
					tagClients = appendEndpoint(tagClients, endpoint)
				}

				// Gets the role/channels from linked deployments if the endpoint is a service channel.
				// TBD: not needed right now. This information is requiered to enable ingress network and
				//      policies we are not creating ingress network policies for other deployments roles.
				// Gets role/channels from linked deployments if the endpoint is a service channel
				if endpoint.Role == nil {
					linkedEndpoints, err := context.getLinkedEndpoints(endpoint)
					if err != nil {
						log.Errorf("%s. Error geting linked endpoints for role '%s' and channel '%s'. Skipping", meth, server.Role, server.Channel)
						continue
					}
					if linkedEndpoints != nil {
						for _, linkedEndpoint := range linkedEndpoints {
							tagLinkedServers = appendEndpoint(tagLinkedServers, linkedEndpoint)
							if linkedEndpoint.ChannelType == DuplexChannel {
								tagLinkedClients = appendEndpoint(tagLinkedClients, linkedEndpoint)
							}
						}
					}
				}
			}

			// Adds the client and server tags for this connector-tag
			(*clients)[connectorName] = tagClients
			(*servers)[connectorName] = tagServers
			(*linkedClients)[connectorName] = tagLinkedClients
			(*linkedServers)[connectorName] = tagLinkedServers
		}
	}

	return clients, servers, linkedClients, linkedServers, nil
}

// getConnectorEndpoint returns the information about a given endpoint (role, channel and type).
// The endpoint role is the name of the role exposing that channel or "self" if the channel belongs
// to the service. The endpoint channel is the channel name as declared in the V3Deployment
// manifest. The channe type is the original type as declared in the V3Deployment if the channel
// belongs to a role and the opposite type if it belongs to the service. Note thta, from a
// connector point of view, the service channel adopt the inverse role (i.e., if the channel is
// declared as a "server" in the service manifest, it will be connected to the connector as a
// "client")
func getConnectorEndpoint(
	role string,
	channel string,
	v3Deployment *kumoriv1.V3Deployment,
	link *kumoriv1.KukuLink,
) (*ConnectorEndpoint, error) {
	var srv *kumoriv1.MicroService

	// Get the role or service channels manifest
	if role == "self" {
		srv = v3Deployment.Spec.Artifact.Description.Srv
	} else {
		if v3Deployment.Spec.Artifact.Description.Roles != nil {
			roleManifest, ok := (*v3Deployment.Spec.Artifact.Description.Roles)[role]
			if !ok {
				return nil, fmt.Errorf("unknown role '%s' references in service links", role)
			}
			if (roleManifest.Artifact == nil) || (roleManifest.Artifact.Description == nil) {
				return nil, fmt.Errorf("role '%s' has not an assigned component", role)
			}
			srv = roleManifest.Artifact.Description.Srv
		}
	}

	if srv == nil {
		return nil, fmt.Errorf("channel '%s' not found for role '%s'", channel, role)
	}

	// Calculate the endpoint
	var endpoint ConnectorEndpoint

	// Checks if it is a server endpoint
	if srv.Servers != nil {
		if server, ok := (*srv.Servers)[channel]; ok {
			port := server.Port
			if role != "self" {
				roleName := role
				endpoint = ConnectorEndpoint{
					ChannelName: channel,
					ChannelType: ServerChannel,
					Role:        &roleName,
					Deployment:  v3Deployment.Name,
					Protocol:    string(server.Protocol),
					Port:        &port,
				}
				if link != nil {
					linkName := link.GetName()
					endpoint.Links = []string{linkName}
				} else {
					endpoint.Links = []string{}
				}
				return &endpoint, nil
			}
			endpoint = ConnectorEndpoint{
				ChannelName: channel,
				ChannelType: ClientChannel,
				Deployment:  v3Deployment.Name,
				Protocol:    string(server.Protocol),
				Port:        &port,
			}
			if link != nil {
				linkName := link.GetName()
				endpoint.Links = []string{linkName}
			} else {
				endpoint.Links = []string{}
			}
			return &endpoint, nil
		}
	}

	// Checks if it is a client endpoint
	if srv.Clients != nil {
		if client, ok := (*srv.Clients)[channel]; ok {
			if role != "self" {
				roleName := role
				endpoint = ConnectorEndpoint{
					ChannelName: channel,
					ChannelType: ClientChannel,
					Role:        &roleName,
					Deployment:  v3Deployment.Name,
					Protocol:    string(client.Protocol),
				}
				if link != nil {
					linkName := link.GetName()
					endpoint.Links = []string{linkName}
				} else {
					endpoint.Links = []string{}
				}
				return &endpoint, nil
			}
			endpoint = ConnectorEndpoint{
				ChannelName: channel,
				ChannelType: ServerChannel,
				Deployment:  v3Deployment.Name,
				Protocol:    string(client.Protocol),
			}
			if link != nil {
				linkName := link.GetName()
				endpoint.Links = []string{linkName}
			} else {
				endpoint.Links = []string{}
			}
			return &endpoint, nil
		}
	}

	// It must be a duplex channel then
	if srv.Duplex == nil {
		return nil, fmt.Errorf("channel '%s' not found for role '%s'", channel, role)
	}

	duplex, ok := (*srv.Duplex)[channel]
	if !ok {
		return nil, fmt.Errorf("channel '%s' not found for role '%s'", channel, role)
	}

	port := duplex.Port
	endpoint = ConnectorEndpoint{
		ChannelName: channel,
		ChannelType: DuplexChannel,
		Deployment:  v3Deployment.Name,
		Protocol:    string(duplex.Protocol),
		Port:        &port,
	}
	if role != "self" {
		roleName := role
		endpoint.Role = &roleName
	}
	if link != nil {
		linkName := link.GetName()
		endpoint.Links = []string{linkName}
	} else {
		endpoint.Links = []string{}
	}
	return &endpoint, nil
}

// getConnectorType returns the connector type
func getConnectorType(rawtype kumoriv1.ConnectorType) ConnectorType {
	switch rawtype {
	case kumoriv1.LBConnectorType:
		return LoadBalancerConnector
	case kumoriv1.FullConnectorType:
		return FullConnector
	default:
		return ""
	}
}

func containsEndpoint(endpoints []*ConnectorEndpoint, newEndpoint *ConnectorEndpoint) bool {
	if endpoints == nil || newEndpoint == nil {
		return false
	}
	for _, endpoint := range endpoints {
		if endpoint.ChannelName != newEndpoint.ChannelName {
			continue
		}
		if endpoint.ChannelType != newEndpoint.ChannelType {
			continue
		}
		if endpoint.Role == nil && newEndpoint.Role != nil {
			continue
		}
		if endpoint.Role != nil && newEndpoint.Role == nil {
			continue
		}

		if endpoint.Role != nil && *endpoint.Role != *newEndpoint.Role {
			continue
		}

		if endpoint.Deployment != newEndpoint.Deployment {
			continue
		}

		return true
	}

	return false
}
