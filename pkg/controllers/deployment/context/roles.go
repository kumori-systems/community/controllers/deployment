/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package context

import (
	"fmt"

	"github.com/Jeffail/gabs/v2"
	log "github.com/sirupsen/logrus"
	kumoriv1 "gitlab.com/kumori-systems/community/libraries/client-go/pkg/apis/kumori/v1"
)

// calculateRolesInfo returns all the information gathered about a given role, including its configuration
// and assigned resources.
func (context *Context) calculateRolesInfo() (*map[string]RoleInfo, error) {

	meth := "context.roles.calculateRolesInfo"

	// If the deployment hasn't a service assignet yet then we cannot retrieve information about the service roles
	if (context.Deployment.Spec.Artifact == nil) ||
		(context.Deployment.Spec.Artifact.Description == nil) {
		log.Infof("%s. Service description not found for V3Deployment '%s'. Skipping", meth, context.Deployment.Name)
		return nil, nil
	}

	// If the service is a built-in service, skip this step (it will not declare the internal roles)
	if context.Deployment.Spec.Artifact.Description.Builtin {
		log.Infof("%s. Builtin service declared in V3Deployment '%s'. Skipping", meth, context.Deployment.Name)
		return nil, nil
	}

	// If the roles list is empty, skip also this step
	if (context.Deployment.Spec.Artifact.Description.Roles == nil) || (len(*context.Deployment.Spec.Artifact.Description.Roles) == 0) {
		log.Infof("%s. Service roles not found for V3Deployment '%s'. Skipping", meth, context.Deployment.Name)
		return nil, nil
	}

	roles := *context.Deployment.Spec.Artifact.Description.Roles
	rolesInfo := make(map[string]RoleInfo, len(roles))
	for roleName, roleData := range roles {
		var mininstances int32
		if roleData.Artifact.Description.Config.Resilience <= 0 {
			mininstances = 0
		} else {
			mininstances = roleData.Artifact.Description.Config.Resilience + 1
		}

		isRoleAutoscaled, err := context.isRoleAutoScaled(&roleData)
		if err != nil {
			log.Errorf("%s. Error checking if role '%s' is autoscaled or not", meth, roleName)
		}

		// Maxinstances removed from the manifest
		// maxinstances := roleData.Size.MaxInstances
		instances := int32(1)
		if roleData.Artifact.Description.Config.Scale.HSize != nil {
			instances = *roleData.Artifact.Description.Config.Scale.HSize
		}

		roleSize := RoleSize{
			Instances: instances,
			// Maxinstances: &maxinstances,
			Mininstances: &mininstances,
			Resilience:   roleData.Artifact.Description.Config.Resilience,
		}
		if roleData.Artifact != nil && roleData.Artifact.Description != nil {
			roleDataSize := roleData.Artifact.Description.Size
			roleSize.Bandwidth = fmt.Sprintf("%d%s", roleDataSize.Bandwidth.Size, roleDataSize.Bandwidth.Unit)
		}
		isRoleStateful, err := context.isRoleStateful(roleName)
		if err != nil {
			return nil, err
		}
		roleInfo := RoleInfo{
			RoleSize:   &roleSize,
			Name:       roleName,
			Stateful:   isRoleStateful,
			Autoscaled: isRoleAutoscaled,
		}
		if roleData.Artifact != nil {
			roleInfo.ComponentReference = &Reference{
				Name:    string(roleData.Artifact.Ref.Name),
				Domain:  string(roleData.Artifact.Ref.Domain),
				Version: []int16(roleData.Artifact.Ref.Version),
			}

			if roleData.Artifact.Ref.Prerelease != nil {
				roleInfo.ComponentReference.Prerelease = roleData.Artifact.Ref.Prerelease
			}

			if roleData.Artifact.Description != nil {

				// Gets the volumes referenced in the configuration section
				volumes, err := context.getVolumes(roleName)
				if err != nil {
					return nil, err
				}

				// Gets information about the channels declared in the role component
				channelsInfo, err := context.calculateChannelsInfo(roleName, roleData.Artifact.Description.Srv)
				if err != nil {
					return nil, err
				}

				// Gets information about the containers used by the role component
				containersInfo, initContainersInfo, err := context.calculateContainersInfo(roleName, volumes)
				if err != nil {
					return nil, err
				}

				// Updates the role information
				roleInfo.ChannelsInfo = channelsInfo
				roleInfo.ContainersInfo = containersInfo
				roleInfo.InitContainersInfo = initContainersInfo
				roleInfo.Volumes = volumes
			}
		}

		rolesInfo[roleName] = roleInfo
	}
	return &rolesInfo, nil
}

// isRoleStateful returns "true" if the role is stateful and false otherwise. A role
// is currently considered stateful if it is connected to a full connector, since
// their instances identity is significative in that case.
func (context *Context) isRoleStateful(
	roleName string,
) (bool, error) {

	kukuVolumes := (*context.kukuVolumes)[roleName]

	if context.Deployment.Spec.Artifact.Description.Connectors != nil {
		connectors := *context.Deployment.Spec.Artifact.Description.Connectors
		// The role is stateful if a full connector is connected to any of its channel
		for _, connectorData := range connectors {
			if connectorData.Kind != kumoriv1.FullConnectorType {
				continue
			}
			for _, tag := range connectorData.Servers {
				for _, server := range tag.Links {
					if server.Role == roleName {
						return true, nil
					}
				}
			}
			// Removed due to ticket306
			// for _, client := range connectorData.Clients {
			// 	if client.Role == roleName {
			// 		return true, nil
			// 	}
			// }
		}
	}

	// If persistent volumes are disabled then the role is stateless
	if context.Config.DisablePersistentVolumes {
		return false, nil
	}

	// If the deployment is not using any persistent volume, it is stateless
	if kukuVolumes == nil {
		return false, nil
	}

	roles := *context.Deployment.Spec.Artifact.Description.Roles

	var err error

	// The role is stateful if it uses persistent volumes
	if role, ok := roles[roleName]; ok {

		// If the role has not assigned resources is stateless
		if (role.Artifact.Description.Config == nil) || (role.Artifact.Description.Config.Resource == nil) {
			return false, nil
		}

		// If the role artifact is not declared, or it is a built-in role
		// or it has not any container declared, is stateless
		if role.Artifact == nil ||
			role.Artifact.Description == nil ||
			role.Artifact.Description.Builtin ||
			role.Artifact.Description.Codes == nil {
			return false, nil
		}

		// Checks if any container is using a persistent volume
		for _, code := range *role.Artifact.Description.Codes {

			// If the container is not mapping any volume, skip
			if code.Mapping == nil || code.Mapping.FileSystem == nil {
				continue
			}

			// Checks if the container is mounting a persistent volume
			for _, file := range *code.Mapping.FileSystem {
				if file.Volume != nil {

					// Gets the referenced resource. It exists only if the volume is a perisstent volume
					if kvolume, ok := kukuVolumes[*file.Volume]; ok && (kvolume != nil) {
						return true, err
					}
				}
			}
		}
	}

	// Otherwise, the role is stateless
	return false, err
}

func (context *Context) getRoleConfigResource(
	roleName string,
	resourceName string,
) (resource *kumoriv1.ConfigurationResource, err error) {

	resources, err := context.getRoleConfigResources(roleName)
	if err != nil {
		return
	}
	if resources == nil {
		err = fmt.Errorf("Resource '%s' not found in role '%s'", resourceName, roleName)
		return
	}

	res, ok := (*resources)[resourceName]
	if !ok {
		err = fmt.Errorf("Resource '%s' not found in role '%s'", resourceName, roleName)
		return
	}

	resource = &res

	return
}

func (context *Context) getRoleConfigSecret(
	roleName string,
	resourceName string,
) (resource *kumoriv1.ConfigurationResource, err error) {
	resource, err = context.getRoleConfigResource(roleName, resourceName)
	if err != nil {
		return
	}

	if resource.Secret == nil {
		err = fmt.Errorf("Resource '%s' is not a secret", resourceName)
	}

	return
}

func (context *Context) getRoleConfigResources(
	roleName string,
) (resources *map[string]kumoriv1.ConfigurationResource, err error) {

	if (context.Deployment.Spec.Artifact == nil) ||
		(context.Deployment.Spec.Artifact.Description == nil) ||
		(context.Deployment.Spec.Artifact.Description.Roles == nil) {
		err = fmt.Errorf("Role '%s' not found", roleName)
		return
	}

	roleData, ok := (*context.Deployment.Spec.Artifact.Description.Roles)[roleName]
	if !ok {
		err = fmt.Errorf("Role '%s' not found", roleName)
		return
	}

	if (roleData.Artifact == nil) || (roleData.Artifact.Description == nil) {
		return
	}

	if roleData.Artifact.Description.Config == nil {
		return
	}

	resources = roleData.Artifact.Description.Config.Resource

	return
}

// isRoleAutoScaled returns "true" if this role is autoscaled by the HPA and
// false otherwise. A role is autoscaled by HPA if:
//   - The kucontroller "hpaEnabledLabelKey" parameter is not empty. This parameter contains
//     a path to be checked in the role metadata
//   - The role metadata contains the path indicated in "hpaEnabledLabelKey" and this bath
//     contains a boolean label set ads "true". For example, if hpaEnabledLabelKey=hpa.enabled,
//     it will check: { "hpa": { "enabled": true } }
func (context *Context) isRoleAutoScaled(roleData *kumoriv1.Role) (bool, error) {

	meth := fmt.Sprintf("context.roles.calculateRolesInfo. Role: %s", roleData.Name)

	// If an HTP label is not configured the role is trivialt not autoscaled
	hpaEnabledLabelKey := context.Config.HpaEnabledLabelKey
	if len(hpaEnabledLabelKey) <= 0 {
		return false, nil
	}
	log.Debugf("%s. hpaEnabledLabelKey='%s'", meth, hpaEnabledLabelKey)

	// If the role metadata is empty or nil, the role is trivialy not autoscaled
	if (roleData.Meta == nil) || (len(roleData.Meta.Raw) <= 0) {
		return false, nil
	}

	// Processes metadata as a JSON document
	meta, err := gabs.ParseJSON(roleData.Meta.Raw)
	if err != nil {
		return false, err
	}

	// Checks if the path configured in hpaEnabledLabelKey exists in metadata. If not
	// exists the role is not autoscaled
	if !meta.ExistsP(hpaEnabledLabelKey) {
		return false, nil
	}

	// If the path indicated in "hpaEnabledLabelKey" configuration parameter not contains a
	// boolean value, prints a warning and returns false
	hpaEnabled, ok := meta.Path(hpaEnabledLabelKey).Data().(bool)
	if !ok {
		log.Warnf("%s. Path '%s' exists in role metadata but not contains a boolean value. Skipping...", meth, hpaEnabledLabelKey)
		return false, nil
	}

	// Returns the value contained in "hpaEnabledLabelKey" path
	return hpaEnabled, nil
}
