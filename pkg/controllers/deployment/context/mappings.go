/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package context

import (
	"encoding/json"
	"fmt"
	"kuku-controller/pkg/utils/sort"
	ktemplate "kuku-controller/pkg/utils/template"

	log "github.com/sirupsen/logrus"
	kumoriv1 "gitlab.com/kumori-systems/community/libraries/client-go/pkg/apis/kumori/v1"
	"gopkg.in/yaml.v2"
)

// getEnvs returns information about environment variables to be exposed in a container.
// It expects a list of environment variables declarations and the list of secrets
// referenced in those environment variables (if any)
func (context *Context) getEnvs(
	roleName string,
	envs *map[string]kumoriv1.Env,
) ([]EnvInfo, error) {
	if envs == nil {
		return nil, nil
	}
	envVars := make([]EnvInfo, 0, len(*envs))
	keys := sort.SortKeys(envs)
	for _, name := range keys {
		envData := (*envs)[name]
		var env EnvInfo
		if envData.Value != nil {

			value := *envData.Value
			isTemplate := context.isATemplate(envData.EnableTemplate)
			if isTemplate && context.marshalledResources != nil {
				if marshalledMap, ok := (*context.marshalledResources)[roleName]; ok {
					resourcesMap := map[string]map[string]string{
						"Resource": marshalledMap,
					}

					var err error
					value, err = ktemplate.Execute(value, resourcesMap)
					if err != nil {
						return nil, err
					}
				}
			}
			env = EnvInfo{
				Name:  name,
				Value: &value,
			}
		} else if envData.Secret != nil {
			secretResource, err := context.getRoleConfigSecret(roleName, *envData.Secret)
			if err != nil {
				return nil, err
			}
			resourceName := context.getResourceCompleteName(*secretResource.Secret)
			secretInfo, ok := (*context.SecretsInfo)[resourceName]
			if !ok {
				return nil, fmt.Errorf("secret '%s' not found for environment variable '%s'", resourceName, name)
			}
			key := SecretsInfoDefaultKey
			secretInfo.InEnv = true
			env = EnvInfo{
				Name:      name,
				Secret:    &secretInfo,
				SecretKey: &key,
			}
			(*context.SecretsInfo)[resourceName] = secretInfo
		} else {
			data, err := context.calculateResourceData(roleName, &envData.ResourceRef)
			if err != nil {
				return nil, err
			}
			value := string(data)
			env = EnvInfo{
				Name:  name,
				Value: &value,
			}
		}
		envVars = append(envVars, env)
	}
	return envVars, nil
}

// getFiles returns information about files to be exposed in a container. It expects a list
// of the mountpoints declarations in that container in the V3Deployment and the list of
// secrets referenced in those environment variables (if any)
func (context *Context) getFiles(
	roleName string,
	prefix string,
	mountPoints *map[string]kumoriv1.MountPoint,
) (*[]FileMountInfo, error) {
	if mountPoints == nil {
		return nil, nil
	}
	files := make([]FileMountInfo, 0, len(*mountPoints))
	fileprefix := fmt.Sprintf("%sf", prefix)
	index := 0
	mountPointNames := sort.SortKeys(mountPoints)
	for _, fileName := range mountPointNames {
		fileInfo := (*mountPoints)[fileName]
		if (fileInfo.Data.Secret == nil) &&
			(fileInfo.Data.Port == nil) &&
			(fileInfo.Data.Domain == nil) &&
			(fileInfo.Data.Certificate == nil) &&
			(fileInfo.Data.CA == nil) &&
			(fileInfo.Data.Value == nil) {
			continue
		}
		name := fmt.Sprintf("%s%d", fileprefix, index)
		index++

		// The POD will be rebooted when the file content changes if this flag is set to true
		reboot := false
		if context.Config.ForceRebootOnUpdate || (fileInfo.RebootOnUpdate != nil && *fileInfo.RebootOnUpdate) {
			reboot = true
		}
		file := FileMountInfo{
			Name:           name,
			Path:           fileInfo.Path,
			RebootOnUpdate: reboot,
		}

		// If the file data is included in the V3Deployment manifest, just register that data for that
		// file in the context. If the file data is stored in a secret, add a reference to that secret.
		// NOTE: currently, a KukuSecret stores a single secret but the context Secrets objects can store
		// several secrets since it contains a key/value store. Previously, we have created one Secrets
		// object per KukuSecret, using as a Key allways the same name, stored in SecretsInfoDefaultKey
		// constant. When a file content is stored in a secret, we reference which secret and key stores
		// that content. The secret name is the one declared in the V3Deployment manifest. The key is
		// the default key used when the Secrets objects is declared. Recall that Secrets objects in the
		// context and the original KukuSecret currently share de same name.
		if fileInfo.Data.Value != nil {
			if fileInfo.Format == nil {
				return nil, fmt.Errorf("unknown format for file '%s'", fileInfo.Path)
			}
			content, err := context.getDataValueContent(roleName, fileInfo.Data.Value.Raw, *fileInfo.Format, fileInfo.EnableTemplate)
			if err != nil {
				return nil, err
			}
			file.Data = content
		} else if fileInfo.Data.Secret != nil {
			secretResource, err := context.getRoleConfigSecret(roleName, *fileInfo.Data.Secret)
			if err != nil {
				return nil, err
			}
			resourceName := context.getResourceCompleteName(*secretResource.Secret)
			s, ok := (*context.SecretsInfo)[resourceName]
			if !ok {
				return nil, fmt.Errorf("secret '%s' not found for mounted file '%s'", resourceName, fileInfo.Path)
			}
			s.InFile = true
			(*context.SecretsInfo)[resourceName] = s
			skey := SecretsInfoDefaultKey
			file.Secret = &s
			file.SecretKey = &skey
		} else {
			data, err := context.calculateResourceData(roleName, &fileInfo.Data.ResourceRef)
			if err != nil {
				return nil, err
			}
			file.Data = data
		}

		if (fileInfo.Mode != nil) || (fileInfo.User != nil) || (fileInfo.Group != nil) {
			perms := Permissions{}
			file.Perms = &perms
		}
		if fileInfo.Mode != nil {
			file.Perms.Mode = fileInfo.Mode
		}
		if (fileInfo.User != nil) || (fileInfo.Group != nil) {
			file.Perms.User = &Owner{}
			if fileInfo.Group != nil {
				file.Perms.User.GroupID = fileInfo.Group
			}
			if fileInfo.User != nil {
				file.Perms.User.UserID = fileInfo.User
			}
		}
		files = append(files, file)
	}

	return &files, nil
}

// getVolumeMounts returns information about the volumes to be mounted in a formation as
// declared in the V3Deployment.
func (context *Context) getVolumeMounts(
	prefix string,
	mountPoints *map[string]kumoriv1.MountPoint,
	volumes *map[string]*Volume,
) (*[]VolumeMountInfo, error) {
	// If this container is not exposing anything in its filesystem then there aren't exposed volumes
	if mountPoints == nil {
		return nil, nil
	}

	// The number of exposed volumes will never be as long as the number of exposed elements in the filesystem
	volumeMounts := make([]VolumeMountInfo, 0, len(*mountPoints))

	var kcerr error

	for _, fileInfo := range *mountPoints {

		// If this element is not a volume, skip it
		if fileInfo.Volume == nil {
			continue
		}

		volume, ok := (*volumes)[*fileInfo.Volume]
		if !ok {
			kcerr = fmt.Errorf("volume resource '%s' referenced in V3Deplotment '%s' but not found", *fileInfo.Volume, context.ReleaseName)
			continue
		}

		// Gets the volume to be mounted form the component declaration
		// volumename := fmt.Sprintf("%svolume%d", prefix, index)

		// Target path in the container filesystem
		// fpath := fileInfo.Path

		// // If the volume mounted is persistent
		// if fileInfo.Volume != nil {
		// 	if !context.Controller.config.DisablePersistentVolumes {
		// 		persistentVolume, ok := (*persistentVolumes)[*fileInfo.Volume]
		// 		if !ok {
		// 			kcerr = fmt.Errorf("Persitent volume '%s' referenced in V3Deplotment '%s' but not found", *fileInfo.Volume, context.ReleaseName)
		// 			continue
		// 		}
		// 		volume = &KukuVolume{
		// 			Name:             volumename,
		// 			RegistrationName: *fileInfo.Volume,
		// 			KukuVolumeName:   persistentVolume.Name,
		// 			MountPoint:       fpath,
		// 			Spec:             *persistentVolume.Spec.DeepCopy(),
		// 		}
		// 	}
		// 	// If the volume is volatile
		// } else {
		// 	var unit string
		// 	if fileInfo.Unit == nil {
		// 		unit = "G"
		// 	} else {
		// 		unit = *fileInfo.Unit
		// 	}

		// 	ftype := kumoriv1.XFS
		// 	size, err := resource.ParseQuantity(fmt.Sprintf("%d%s", *fileInfo.Size, unit))
		// 	if err != nil {
		// 		return nil, err
		// 	}
		// 	volume = &KukuVolatileVolume{
		// 		Name: volumename,
		// 		Spec: KukuVolatileVolumeSpec{
		// 			FileSystem: &ftype,
		// 			MountPoint: &fpath,
		// 			Size:       size,
		// 		},
		// 	}
		// }

		// Appends the new volume mount info
		volumeMount := VolumeMountInfo{
			Path:   fileInfo.Path,
			Volume: volume,
		}
		volumeMounts = append(volumeMounts, volumeMount)

	}

	if kcerr != nil {
		return &volumeMounts, kcerr
	}

	// Returns the mounted volumes
	return &volumeMounts, nil
	// return nil, fmt.Errorf("Not implemented")
}

// getDataContent returns the content of a exposed file.
func (context *Context) getDataValueContent(
	roleName string,
	data []byte,
	format string,
	template *bool,
) ([]byte, error) {
	meth := fmt.Sprintf("Context.getDataValueContent. Role: %s. Format: %s", roleName, format)

	// Parses the fileInfo data in case it is a go template
	// getRoleConfigResource
	resourcesMap := map[string]map[string]string{}
	if context.stringifiedResources != nil {
		if stringifiedMap, ok := (*context.stringifiedResources)[roleName]; ok {
			resourcesMap = map[string]map[string]string{
				"Resource": stringifiedMap,
			}

			// result, err := ktemplate.Execute(string(data), resourcesMap)
			// if err != nil {
			// 	return nil, err
			// }
			// data = []byte(result)
		}
	}
	isTemplate := context.isATemplate(template)
	if isTemplate {
		result, err := ktemplate.Execute(string(data), resourcesMap)
		if err != nil {
			return nil, err
		}
		data = []byte(result)
	}

	switch format {
	case "text":
		var text string
		if err := json.Unmarshal(data, &text); err != nil {
			log.Errorf("%s. Error unmarshaling mapped file data to format 'text'.\n    Data: %s\n    Error: %v", meth, data, err)
			return data, nil
		}
		return []byte(text), nil
	case "json":
		var obj interface{}
		if err := json.Unmarshal(data, &obj); err != nil {
			return nil, fmt.Errorf("Error unmarshaling mapped file data to format 'json' in role %s. Data: %s. Error: %v", roleName, data, err)
		}
		content, err := json.MarshalIndent(obj, "", "  ")
		if err != nil {
			return nil, fmt.Errorf("Error unmarshaling mapped file data to format 'json' in role %s. Data: %s. Error: %v", roleName, data, err)
		}
		return content, nil
	case "yaml":
		var obj interface{}
		if err := json.Unmarshal(data, &obj); err != nil {
			return nil, fmt.Errorf("Error unmarshaling mapped file data to format 'yaml' in role %s. Data: %s. Error: %v", roleName, data, err)
		}
		content, err := yaml.Marshal(obj)
		if err != nil {
			return nil, fmt.Errorf("Error unmarshaling mapped file data to format 'yaml' in role %s. Data: %s. Error: %v", roleName, data, err)
		}
		return content, nil
	default:
		return nil, fmt.Errorf("unknown format '%s'", format)
	}
}

func (context *Context) calculateResourceData(
	roleName string,
	ref *kumoriv1.ResourceRef,
) ([]byte, error) {

	if context.marshalledResources == nil {
		return nil, fmt.Errorf("marshalled resources missing for role %s", roleName)
	}

	marshalledResources, ok := (*context.marshalledResources)[roleName]
	if !ok {
		return nil, fmt.Errorf("marshalled resources missing for role %s", roleName)
	}

	if ref.Port != nil {
		data, ok := marshalledResources[*ref.Port]
		if !ok {
			return nil, fmt.Errorf("marshalled resource %s missing for role %s", *ref.Port, roleName)
		}
		return []byte(data), nil
	}

	if ref.Domain != nil {
		data, ok := marshalledResources[*ref.Domain]
		if !ok {
			return nil, fmt.Errorf("marshalled domain %s missing for role %s", *ref.Domain, roleName)
		}
		return []byte(data), nil
	}

	if ref.Certificate != nil {
		data, ok := marshalledResources[*ref.Certificate]
		if !ok {
			return nil, fmt.Errorf("marshalled certificate %s missing for role %s", *ref.Certificate, roleName)
		}
		return []byte(data), nil
	}

	if ref.CA != nil {
		data, ok := marshalledResources[*ref.CA]
		if !ok {
			return nil, fmt.Errorf("marshalled CA %s missing for role %s", *ref.CA, roleName)
		}
		return []byte(data), nil
	}

	if ref.Secret != nil {
		data, ok := marshalledResources[*ref.Secret]
		if !ok {
			return nil, fmt.Errorf("marshalled secret %s missing for role %s", *ref.Secret, roleName)
		}
		return []byte(data), nil
	}

	return nil, fmt.Errorf("unknown resource format: %v", ref)
}

// isATemplate is used to find out if a given mapping element must be processed as
// a go template or not. If a mapping elements (environment variables and mounted files)
// includes a "template" flag set, the environment variable or file content is processed
// as a go template using the declared resources as available values in the template.
func (context *Context) isATemplate(templateFlag *bool) bool {
	defaultIsTemplate := context.Config.DefaultMappingEnableTemplate
	if templateFlag == nil {
		return defaultIsTemplate
	}

	return *templateFlag
}
