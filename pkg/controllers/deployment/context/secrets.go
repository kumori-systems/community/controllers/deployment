/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package context

import (
	"fmt"
	"kuku-controller/pkg/utils/kumori"
	"kuku-controller/pkg/utils/sort"

	hashutils "kuku-controller/pkg/utils/hash"

	kumoriv1 "gitlab.com/kumori-systems/community/libraries/client-go/pkg/apis/kumori/v1"
)

// calculateSecretsInfo returns the significative information related to secret. Returns a map
// being the "key" the original name used in the V3Deployment reference and the value the
// gathered information.
func (context *Context) calculateSecretsInfo(
	ksecrets *map[string]map[string]*kumoriv1.KukuSecret,
) (*map[string]SecretsInfo, error) {
	if ksecrets == nil {
		return &map[string]SecretsInfo{}, nil
	}
	secrets := map[string]SecretsInfo{}
	roleNames := sort.SortKeys(ksecrets)
	index := 0
	for _, roleName := range roleNames {
		roleSecrets := (*ksecrets)[roleName]
		secretsNames := sort.SortKeys(&roleSecrets)
		for _, name := range secretsNames {
			ksecret := roleSecrets[name]
			secretName := ksecret.Annotations[kumori.KumoriNameLabel]
			secretDomain := ksecret.Annotations[kumori.KumoriDomainLabel]
			secretCompleteName := fmt.Sprintf("%s/%s", secretDomain, secretName)
			secretShortName := hashutils.Hash(fmt.Sprintf("%s/%s", roleName, name))
			// secretShortName := fmt.Sprintf("%d", index)
			if _, ok := secrets[secretCompleteName]; !ok {
				secrets[secretCompleteName] = SecretsInfo{
					Name:      secretCompleteName,
					ShortName: secretShortName,
					Data: map[string][]byte{
						SecretsInfoDefaultKey: ksecret.Data,
					},
					InDockerHub: false,
					InFile:      false,
					InEnv:       false,
				}
				index++
			}
		}
	}

	return &secrets, nil
}
