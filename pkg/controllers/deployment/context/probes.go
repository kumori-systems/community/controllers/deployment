/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package context

import (
	"encoding/json"
	"fmt"
	"math"
	"strconv"
	"strings"
	"time"

	"github.com/google/shlex"
	log "github.com/sirupsen/logrus"
	kumoriv1 "gitlab.com/kumori-systems/community/libraries/client-go/pkg/apis/kumori/v1"
	"k8s.io/apimachinery/pkg/runtime"
)

// calculateContainerProbes returns information about the probe assigned to a given container
// in a given component, if any
func (context *Context) calculateContainerProbes(
	component *kumoriv1.ComponentArtifact,
	containerName string,
) (startupProbe *Probe, livenessProbe *Probe, readinessProbe *Probe, err error) {

	meth := fmt.Sprintf("context.probes.calculateContainerProbes. Component: %s/%s ContainerName: %s", component.Ref.Domain, component.Ref.Name, containerName)

	// Skips this section if probes are not defined for this component
	if component.Description.Probes == nil {
		return
	}

	// Gets the container data. Skips if the container not exists
	data, ok := (*component.Description.Codes)[containerName]
	if !ok {
		err = fmt.Errorf("container %s not found", containerName)
		return
	}

	// Skips if the container is an init container
	if (data.Init != nil) && (*data.Init) {
		log.Debugf("%s. This container is an init container. Skipping probes...", meth)
		return
	}

	// Gets the container probes. Skips if this container has not probes defined
	probes, ok := (*component.Description.Probes)[containerName]
	if !ok {
		return
	}

	if probes.Liveness != nil {
		livenessProbe, err = context.calculateLivenessProbe(probes.Liveness)
		if err != nil {
			return nil, nil, nil, err
		}
		startupProbe, err = context.calculateStartupProbe(probes.Liveness)
		if err != nil {
			return nil, nil, nil, err
		}

	}

	if probes.Readiness != nil {
		// Liveness and readiness probe share the same initial delay. If the liveness probe
		// is not defined, the readiness probe will use the default liveness initial delay
		initialDelay := context.Config.DefaultReadinessInitialDelay
		if livenessProbe != nil {
			initialDelay = livenessProbe.InitialDelay
		}
		readinessProbe, err = context.calculateReadinessProbe(probes.Readiness, initialDelay)
		if err != nil {
			return nil, nil, nil, err
		}
	}

	if probes.PrometheusMetrics != nil {
		log.Warnf("%s. Prometheus metrics probe not supported. Skipping", meth)

	}

	return
}

// calculateProbePeriod a probe period. The probe period can be defined as a string containing "high",
// "medium" or "low" or as an integer indicating the number of milliseconds. This converts this value to
// a time.Duration
func (context *Context) calculateProbePeriod(
	rawFrequency *runtime.RawExtension,
	defaultPeriod time.Duration,
	highPeriod time.Duration,
	mediumPeriod time.Duration,
	lowPeriod time.Duration,
) (time.Duration, error) {

	if rawFrequency == nil {
		return defaultPeriod, nil
	}

	freqraw := string(rawFrequency.Raw)
	freqint, err := strconv.Atoi(freqraw)

	// The period is indicated as a number of millis
	if err == nil {
		period, err := time.ParseDuration(fmt.Sprintf("%dms", freqint))
		if err != nil {
			return defaultPeriod, err
		}
		return period, nil
	}

	// Unexpected error. Should not happen
	if !strings.HasSuffix(err.Error(), "invalid syntax") {
		return defaultPeriod, fmt.Errorf("unexpected error marshalling period %s: %s", freqraw, err)
	}

	// Gets the string
	var frequency string
	json.Unmarshal(rawFrequency.Raw, &frequency)

	// Returns the period corresponding to the specified value
	switch frequency {
	case "high":
		return highPeriod, nil
	case "medium":
		return mediumPeriod, nil
	case "low":
		return lowPeriod, nil
	default:
		return defaultPeriod, fmt.Errorf("unknown period '%s'", frequency)
	}
}

// calculateProbeThreshold calculates the threshold as ceil(timeout/frequency) plus one.
// Examples (P = probe run, <-XXX-> means "after XXX time"):
// * Timeout = 3000ms, Frequency = 1000ms, Threshold = 4 (P <-1000ms-> P <-1000ms-> P <-1000ms-> P)
// * Timeout = 1500ms, Frequency = 1000ms, Threshold = 3 (P <-1000ms-> P <-1000ms-> P)
func (context *Context) calculateProbeThreshold(
	frequency time.Duration,
	timeout time.Duration,
) int32 {
	meth := fmt.Sprintf("context.probes.calculateProbeThreshold. Frequency: %s. Tiemout: %s", frequency.String(), timeout.String())

	// If the specified timeout is lower than the specified frequency, the frequency is
	// taken as the timeout and the threshold is 2 (the first execution of the probe and
	// the last execution)
	if frequency > timeout {
		log.Warnf("%s. Frequency is greater than timeout. The frequency will be considered as the timeout.", meth)
		return 2
	}

	// Returns ceil(timeout/frequency) + 1
	// Nothe that kubernetes granularity for frequency (periodSeconds) is seconds,
	// not millis like in Kumori service model
	timeoutInSecs := math.Ceil(float64(timeout.Seconds()))
	frequencyInSecs := math.Ceil(float64(frequency.Seconds()))
	return int32(math.Ceil(timeoutInSecs/frequencyInSecs)) + 1
}

// calculateLivenessProbe calculates the liveness probe from the V3Deployment
// raw data
func (context *Context) calculateLivenessProbe(
	attributes *kumoriv1.LivenessProbeAttributes,
) (livenessProbe *Probe, err error) {
	meth := "context.probes.calculateLivenessProbe"

	// Calculates the liveness period considering the frequency declared in the
	// probe in Kumori format
	livenessPeriod, err := context.calculateProbePeriod(
		attributes.Frequency,
		context.Config.DefaultLivenessPeriod,
		context.Config.LivenessHighFrequencyPeriod,
		context.Config.LivenessMediumFrequencyPeriod,
		context.Config.LivenessLowFrequencyPeriod,
	)

	// The liveness timeout is always the default value configured
	livenessTimeout := context.Config.DefaultLivenessTimeout

	// The threshold is calculated considering the frequency and total tiemout
	// provided in kumori probe
	livenessFailureThreshold := context.Config.DefaultLivenessFailureThreshold
	if attributes.Timeout != nil {
		livenessFailureThreshold = context.calculateProbeThreshold(livenessPeriod, time.Duration(*attributes.Timeout)*time.Millisecond)
	}

	// Calculates the initial delay. Uses the default value unless the startup probe is
	// disabled for this liveness probe or in the controller configuration
	livenessInitialDelay := context.Config.DefaultLivenessInitialDelay
	if !attributes.StartupGraceWindow.Probe || !context.Config.EnableStartupProbe {
		switch attributes.StartupGraceWindow.Unit {
		case kumoriv1.AttemptsStartupGraceWindowUnit:
			livenessInitialDelay = context.Config.DefaultStartupPeriod * time.Duration(attributes.StartupGraceWindow.Duration)
		case kumoriv1.MillisStartupGraceWindowUnit:
			livenessInitialDelay = time.Duration(attributes.StartupGraceWindow.Duration) * time.Millisecond
		default:
			// If an unknown unit is provided, we use the default values for startup period and threshold
			log.Errorf("%s. Wrong liveness grace window unit '%s'. Using default values for liveness initial delay", meth, attributes.StartupGraceWindow.Unit)
		}
	}

	// Initialize LivenessProbe
	livenessProbe = &Probe{
		InitialDelay:     livenessInitialDelay,
		FailureThreshold: livenessFailureThreshold,
		Period:           livenessPeriod,
		Timeout:          livenessTimeout,
	}

	// Depending on the probe type (HTTP or TCP), add to liveness probe
	// the required data
	protocolDone := false
	if attributes.Protocol.HTTP != nil {
		livenessProbe.HTTP = &HTTPProbe{
			Path:   attributes.Protocol.HTTP.Path,
			Port:   int32(attributes.Protocol.HTTP.Port),
			Scheme: "HTTP",
		}
		protocolDone = true
	}
	if attributes.Protocol.TCP != nil {
		if protocolDone {
			err = fmt.Errorf("liveness probe has more than one protocols defined")
			return
		}
		livenessProbe.TCP = &TCPProbe{
			Port: int32(attributes.Protocol.TCP.Port),
		}
		protocolDone = true
	}
	if attributes.Protocol.Exec != nil {
		if protocolDone {
			err = fmt.Errorf("%s. Liveness probe has more than one protocols defined", meth)
			return
		}
		command, err := shlex.Split(attributes.Protocol.Exec.Path)
		if err != nil {
			return nil, fmt.Errorf("error processing exec probe command %s: %s", attributes.Protocol.Exec.Path, err.Error())
		}
		livenessProbe.Exec = &ExecProbe{
			Command: command,
		}
		protocolDone = true
	}
	if !protocolDone {
		err = fmt.Errorf("liveness probe type unknown")
	}
	return
}

// calculateReadinessProbe calculates the readiness probe from the V3Deployment
// raw data
func (context *Context) calculateReadinessProbe(
	attributes *kumoriv1.ReadinessProbeAttributes,
	initialDelay time.Duration,
) (readinessProbe *Probe, err error) {
	meth := "context.probes.calculateReadinessProbe"

	// Calculates the readiness period considering the frequency declared in the
	// probe in Kumori format
	readinessPeriod, err := context.calculateProbePeriod(
		attributes.Frequency,
		context.Config.DefaultReadinessPeriod,
		context.Config.ReadinessHighFrequencyPeriod,
		context.Config.ReadinessMediumFrequencyPeriod,
		context.Config.ReadinessLowFrequencyPeriod,
	)

	// The readiness timeout is always the default value configured
	readinessTimeout := context.Config.DefaultReadinessTimeout

	// The threshold is calculated considering the frequency and total tiemout
	// provided in kumori probe
	readinessFailureThreshold := context.Config.DefaultReadinessFailureThreshold
	if attributes.Timeout != nil {
		readinessFailureThreshold = context.calculateProbeThreshold(readinessPeriod, time.Duration(*attributes.Timeout)*time.Millisecond)
	}

	// Initialize ReadinessProbe
	readinessProbe = &Probe{
		InitialDelay:     initialDelay,
		FailureThreshold: readinessFailureThreshold,
		Period:           readinessPeriod,
		Timeout:          readinessTimeout,
	}

	// Depending on the probe type (HTTP or TCP), add to readiness and startup probes
	// the required data
	protocolDone := false
	if attributes.Protocol.HTTP != nil {
		readinessProbe.HTTP = &HTTPProbe{
			Path:   attributes.Protocol.HTTP.Path,
			Port:   int32(attributes.Protocol.HTTP.Port),
			Scheme: "HTTP",
		}
		protocolDone = true
	}
	if attributes.Protocol.TCP != nil {
		if protocolDone {
			err = fmt.Errorf("readiness probe has more than one protocols defined")
			return
		}
		readinessProbe.TCP = &TCPProbe{
			Port: int32(attributes.Protocol.TCP.Port),
		}
		protocolDone = true
	}
	if attributes.Protocol.Exec != nil {
		if protocolDone {
			err = fmt.Errorf("%s. readiness probe has more than one protocols defined", meth)
			return
		}
		command, err := shlex.Split(attributes.Protocol.Exec.Path)
		if err != nil {
			return nil, fmt.Errorf("error processing exec probe command %s: %s", attributes.Protocol.Exec.Path, err.Error())
		}
		readinessProbe.Exec = &ExecProbe{
			Command: command,
		}
		protocolDone = true
	}
	if !protocolDone {
		err = fmt.Errorf("readiness probe type unknown")
	}
	return
}

// calculateStartupProbe calculates the startup probe from the V3Deployment
// raw data
func (context *Context) calculateStartupProbe(
	attributes *kumoriv1.LivenessProbeAttributes,
) (startupProbe *Probe, err error) {
	meth := "context.probes.calculateStartupProbe"

	var startupFailureThreshold int32
	var startupPeriod time.Duration
	var startupTimeout time.Duration
	startupInitialDelay := context.Config.DefaultStartupInitialDelay

	// Skip the startup probe generation if this liveness probe has the "StartupGraceWindow.Probe"
	// flag to false or if startup probes are disables by configuration
	if !attributes.StartupGraceWindow.Probe || !context.Config.EnableStartupProbe {
		return
	}

	startupTimeout = context.Config.DefaultStartupTimeout
	startupPeriod = context.Config.DefaultStartupPeriod
	switch attributes.StartupGraceWindow.Unit {
	case kumoriv1.AttemptsStartupGraceWindowUnit:
		// If startup is provided in attempts, we use the default startup period
		startupFailureThreshold = int32(attributes.StartupGraceWindow.Duration)
	case kumoriv1.MillisStartupGraceWindowUnit:
		// If the startup time is provided in milles, we calculate the period considering the default threshold for startup.
		// BEFORE: the period is calculated dividing the startup time per the default failure threshold
		// NOW: the failureThreshold is calculated dividing the startup time per the default period.
		// msperiod := int64(math.Ceil(float64(attributes.StartupGraceWindow.Duration) / float64(uint(context.Controller.config.DefaultStartupFailureThreshold))))
		// startupPeriod = time.Duration(msperiod) * time.Millisecond
		// startupFailureThreshold = context.Controller.config.DefaultStartupFailureThreshold
		startupFailureThreshold = int32(math.Ceil(float64(attributes.StartupGraceWindow.Duration) / float64(uint(context.Config.DefaultStartupPeriod.Milliseconds()))))
	default:
		// If an unknown unit is provided, we use the default values for startup period and threshold
		log.Errorf("%s. Wrong startup grace window unit '%s'. Using default values for startup period and failure threshold", meth, attributes.StartupGraceWindow.Unit)
		startupFailureThreshold = context.Config.DefaultStartupFailureThreshold
	}

	// Initialize StartupProbe
	startupProbe = &Probe{
		InitialDelay:     startupInitialDelay,
		FailureThreshold: startupFailureThreshold,
		Period:           startupPeriod,
		Timeout:          startupTimeout,
	}

	// Depending on the probe type (HTTP or TCP), add to startup probe
	// the required data
	protocolDone := false
	if attributes.Protocol.HTTP != nil {
		startupProbe.HTTP = &HTTPProbe{
			Path:   attributes.Protocol.HTTP.Path,
			Port:   int32(attributes.Protocol.HTTP.Port),
			Scheme: "HTTP",
		}
		protocolDone = true
	}
	if attributes.Protocol.TCP != nil {
		if protocolDone {
			err = fmt.Errorf("liveness probe has more than one protocols defined")
			return
		}
		startupProbe.TCP = &TCPProbe{
			Port: int32(attributes.Protocol.TCP.Port),
		}
		protocolDone = true
	}
	if attributes.Protocol.Exec != nil {
		if protocolDone {
			err = fmt.Errorf("%s. Liveness probe has more than one protocols defined", meth)
			return
		}
		command, err := shlex.Split(attributes.Protocol.Exec.Path)
		if err != nil {
			return nil, fmt.Errorf("error processing exec probe command %s: %s", attributes.Protocol.Exec.Path, err.Error())
		}
		startupProbe.Exec = &ExecProbe{
			Command: command,
		}
		protocolDone = true
	}
	if !protocolDone {
		err = fmt.Errorf("liveness probe type unknown")
	}
	return

}
