package context

import (
	"fmt"
	"kuku-controller/pkg/utils/kumori"
	"kuku-controller/pkg/utils/tags"
	"strconv"

	log "github.com/sirupsen/logrus"
)

func (c *Context) calculateTags() (
	connectorsTags *map[string]ConnectorTagInfo,
	err error,
) {
	meth := "Context.calculateTags"
	log.Infof(meth)

	counter := 0
	connectorCounter := map[string]int{}
	connectorsTags = &map[string]ConnectorTagInfo{}

	for _, tagBreadcrumn := range c.tagsTools.VSetTagBreadcrumbs {

		if tagBreadcrumn.V3Deployment.Name != c.tagsTools.RootV3D.Name {
			continue
		}

		branches := c.tagsTools.GetTags(tagBreadcrumn)
		log.Debugf("%s. %d branches generated for breadcrumb %s\n", meth, len(branches), tagBreadcrumn.GetName())

		connectorName := tagBreadcrumn.ConnectorName
		if _, ok := connectorCounter[connectorName]; !ok {
			connectorCounter[connectorName] = 0
		}

		// Calculates the clients connected to this connector tag
		clients, err := c.getClients(tagBreadcrumn)
		if err != nil {
			return nil, err
		}

		// Calculates the servers connected to this connector tag through links (if any)
		linkedClients, err := c.getLinkedClients(tagBreadcrumn)
		if err != nil {
			return nil, err
		}

		// Calculate tags
		for _, branch := range branches {
			tagName := strconv.Itoa(connectorCounter[connectorName])
			taggedConnectorName := GetTaggedConnectorName(tagBreadcrumn.ConnectorName, fmt.Sprintf("%d", connectorCounter[connectorName]))
			shortName := fmt.Sprintf("connector%d", counter)
			port := int32(80)

			// Calculates the servers connected to this connector tag
			servers, err := c.getServers(branch)
			if err != nil {
				return nil, err
			}

			// Calculates the servers connected to this connector tag through links (if any)
			var linkedServers []*ConnectorEndpoint
			if len(branch) > 1 {
				linkedServers, err = c.getLinkedServers(branch)
				if err != nil {
					return nil, err
				}
			}

			if linkedServers == nil {
				linkedServers = []*ConnectorEndpoint{}
			}

			connectorTag := ConnectorTagInfo{
				Name:          taggedConnectorName,
				ShortName:     shortName,
				ConnectorName: tagBreadcrumn.ConnectorName,
				TagName:       tagName,
				Port:          port,
				Type:          getConnectorType(tagBreadcrumn.Connector.Kind),
				Servers:       servers,
				Clients:       clients,
				LinkedServers: linkedServers,
				LinkedClients: linkedClients,
			}

			counter++
			connectorCounter[connectorName]++
			(*connectorsTags)[taggedConnectorName] = connectorTag
		}
	}

	return
}

func (c *Context) getServers(
	branch []tags.Breadcrumb,
) (endpoints []*ConnectorEndpoint, err error) {
	meth := fmt.Sprintf("Context.GetServers. V3Deployment: %s. Breadcrumb: %s",
		c.tagsTools.RootV3D.Name, branch[0].GetName())
	log.Infof(meth)

	// The branch is empty
	if len(branch) <= 0 {
		err = fmt.Errorf("empty branch")
		return
	}

	breadcrumb, ok := branch[0].(*tags.VSetTagBreadcrumb)
	if !ok {
		err = fmt.Errorf("the first element in a tag branch must be a connector tag but it is a %s", breadcrumb.GetType())
	}

	endpoints = []*ConnectorEndpoint{}

	// This branch is a final sheet. Add the connector tag server channels
	if len(branch) == 1 {

		for _, link := range breadcrumb.Tag.Links {
			endpoint, err2 := getConnectorEndpoint(link.Role, link.Channel, breadcrumb.V3Deployment, nil)
			if err2 != nil {
				err = fmt.Errorf("%s. Error adding endpoint %s/%s: %v", meth, link.Role, link.Channel, err2)
				continue
			}

			if endpoint.Role != nil {
				endpoints = append(endpoints, endpoint)
			}

		}

		return
	}

	// This branch is not a final sheet. The next breadcrumb must be a link breadcrumb. Add the
	// link source endpoint as the server and the link as this endpoint links
	linkBreadcrumb, ok := branch[1].(*tags.LinkBreadcrumb)
	if !ok {
		err = fmt.Errorf("the second element in a tag branch must be a link but it is a %s", branch[1].GetType())
		return
	}

	endpoint, err := getConnectorEndpoint("self", linkBreadcrumb.SourceChannel, breadcrumb.V3Deployment, nil)
	endpoint.Links = []string{linkBreadcrumb.Link.GetName()}

	endpoints = append(endpoints, endpoint)
	return

}

func (c *Context) getLinkedServers(
	branch []tags.Breadcrumb,
) (endpoints []*ConnectorEndpoint, err error) {
	meth := fmt.Sprintf("Context.getLinkedServers. V3Deployment: %s. Breadcrumb: %s",
		c.tagsTools.RootV3D.Name, branch[0].GetName())
	log.Infof(meth)

	// The branch is empty
	if len(branch) <= 0 {
		err = fmt.Errorf("empty branch")
		return
	}

	// If this branch contains a single elements, there are no linked endpoints
	if len(branch) == 1 {
		endpoints = []*ConnectorEndpoint{}
		return
	}

	// If the branch is larger and the last breadcrumb is a connector tag, add the connector tag
	// server channels as linked servers
	if breadcrumb, ok := branch[len(branch)-1].(*tags.VSetTagBreadcrumb); ok {
		for _, link := range breadcrumb.Tag.Links {
			endpoint, err2 := getConnectorEndpoint(link.Role, link.Channel, breadcrumb.V3Deployment, nil)
			if err2 != nil {
				err = fmt.Errorf("%s. Error adding endpoint %s/%s: %v", meth, link.Role, link.Channel, err2)
				continue
			}

			if endpoint.Role != nil {
				endpoints = append(endpoints, endpoint)
			}

		}

		return
	}

	// If the branch is larger and the last breadcrumb is not a connector tag, then this branch
	// is invalid.
	log.Warnf("%s. This tag is not connected to any role. Skipping...", meth)
	return
}

func (c *Context) getClients(
	breadcrumb *tags.VSetTagBreadcrumb,
) (endpoints []*ConnectorEndpoint, err error) {
	meth := fmt.Sprintf("Context.GetClients. V3Deployment: %s. Connector: %s",
		c.tagsTools.RootV3D.Name, breadcrumb.ConnectorName)
	log.Infoln(meth)

	v3dname, ok := breadcrumb.V3Deployment.Annotations[kumori.KumoriNameLabel]
	if !ok || (v3dname == "") {
		err = fmt.Errorf("%s. Annotation %s not found in V3Deployment", meth, kumori.KumoriNameLabel)
		return
	}

	v3ddomain, ok := breadcrumb.V3Deployment.Annotations[kumori.KumoriDomainLabel]
	if !ok || (v3ddomain == "") {
		err = fmt.Errorf("%s. Annotation %s not found in V3Deployment", meth, kumori.KumoriDomainLabel)
		return
	}

	if breadcrumb.Connector == nil {
		return
	}

	endpoints = []*ConnectorEndpoint{}

	// Adds the connector client channels as endpoints
	for _, client := range breadcrumb.Connector.Clients {
		endpoint, err2 := getConnectorEndpoint(client.Role, client.Channel, breadcrumb.V3Deployment, nil)
		if err2 != nil {
			err = fmt.Errorf("%s. Error adding endpoint %s/%s: %v", meth, client.Role, client.Channel, err2)
			continue
		}

		if endpoint.Role == nil {
			for _, parent := range breadcrumb.Parents {
				if parent.GetType() != tags.LinkBreadcrumbType {
					continue
				}

				linkBreadcrumb := parent.(*tags.LinkBreadcrumb)

				for _, linkEndpoint := range linkBreadcrumb.Link.Spec.Endpoints {
					if linkEndpoint.Name == v3dname &&
						linkEndpoint.Domain == v3ddomain &&
						linkEndpoint.Channel == client.Channel {
						endpoint.Links = append(endpoint.Links, linkBreadcrumb.Link.GetName())
					}
				}
			}
		}

		endpoints = append(endpoints, endpoint)

	}

	// Duplex channels
	for _, link := range breadcrumb.Tag.Links {
		endpoint, err2 := getConnectorEndpoint(link.Role, link.Channel, breadcrumb.V3Deployment, nil)

		if endpoint.ChannelType != DuplexChannel {
			continue
		}

		if err2 != nil {
			err = fmt.Errorf("%s. Error adding endpoint %s/%s: %v", meth, link.Role, link.Channel, err2)
			continue
		}

		if endpoint.Role == nil {
			for _, kid := range breadcrumb.Kids {
				if kid.GetType() != tags.LinkBreadcrumbType {
					continue
				}

				linkBreadcrumb := kid.(*tags.LinkBreadcrumb)

				for _, linkEndpoint := range linkBreadcrumb.Link.Spec.Endpoints {
					if linkEndpoint.Name == v3dname &&
						linkEndpoint.Domain == v3ddomain &&
						linkEndpoint.Channel == link.Channel {
						endpoint.Links = append(endpoint.Links, linkBreadcrumb.Link.GetName())
					}
				}
			}
		}

		endpoints = append(endpoints, endpoint)
	}

	return
}

func (c *Context) getLinkedClients(
	breadcrumb *tags.VSetTagBreadcrumb,
) (endpoints []*ConnectorEndpoint, err error) {
	meth := fmt.Sprintf("Context.getLinkedClients. V3Deployment: %s. Connector: %s",
		c.tagsTools.RootV3D.Name, breadcrumb.ConnectorName)
	log.Infof(meth)

	endpoints = []*ConnectorEndpoint{}

	if len(breadcrumb.GetParents()) <= 0 {
		return
	}

	pendingBreadcrumbs := []tags.Breadcrumb{}
	processedBreadcrumbs := []tags.Breadcrumb{}

	pendingBreadcrumbs = append(pendingBreadcrumbs, breadcrumb.GetParents()...)

	for len(pendingBreadcrumbs) > 0 {
		currentBreadcrumb := pendingBreadcrumbs[0]
		pendingBreadcrumbs = pendingBreadcrumbs[1:]
		processedBreadcrumbs = append(processedBreadcrumbs, currentBreadcrumb)

		// Checks if this breadcrumb has been already processed (skips loops)
		for _, processed := range processedBreadcrumbs {
			if (processed.GetType() == currentBreadcrumb.GetType()) &&
				(processed.GetName() == currentBreadcrumb.GetName()) {
				continue
			}
		}

		if currentBreadcrumb.GetType() == tags.VSetTagBreadcrumbType {
			currentEndpoints, err := c.getClients(currentBreadcrumb.(*tags.VSetTagBreadcrumb))
			if err != nil {
				return endpoints, err
			}

			// Adds this connector endpoints to the endpoints links but only if they are
			// role endpoints since we are only interested in real destinations.
			for _, endpoint := range currentEndpoints {
				if endpoint.Role == nil {
					continue
				}

				// Adds endpoint to the list of available endpoint if it has not been added
				// preeviously
				processed := false
				for _, existing := range endpoints {
					if (endpoint.ChannelName == existing.ChannelName) &&
						(endpoint.ChannelType == existing.ChannelType) &&
						(*endpoint.Role == *existing.Role) &&
						(endpoint.Deployment == existing.Deployment) {
						processed = true
						break
					}
				}
				if !processed {
					endpoints = append(endpoints, endpoint)
				}
			}
		}

		// Adds parents as breadcrumbs pending to be processed but only if they have not been
		// processed yet or have been already added in the pending list
		if len(currentBreadcrumb.GetParents()) > 0 {
			for _, parent := range currentBreadcrumb.GetParents() {
				if (tags.SearchBreadcrumb(processedBreadcrumbs, parent) == nil) &&
					(tags.SearchBreadcrumb(pendingBreadcrumbs, parent) == nil) {
					pendingBreadcrumbs = append(pendingBreadcrumbs, parent)
				}
			}
		}
	}

	return
}
