/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package context

import (
	"fmt"

	log "github.com/sirupsen/logrus"
)

// calculateContainersInfo returns agregated information about a role containers.
func (context *Context) calculateContainersInfo(
	roleName string,
	volumes *map[string]*Volume,
) (
	containersInfo *map[string]ContainerInfo,
	initContainersInfo *map[string]ContainerInfo,
	err error,
) {

	// Return nil if there aren't any containers defined for this role
	componentData := *(*context.Deployment.Spec.Artifact.Description.Roles)[roleName].Artifact
	if componentData.Description.Codes == nil {
		return
	}

	// Gets the containers list
	auxMap := make(map[string]ContainerInfo, len(*componentData.Description.Codes))
	containersInfo = &auxMap
	initContainersInfo = &map[string]ContainerInfo{}

	prefix := "c"
	i := 0
	roles := *context.Deployment.Spec.Artifact.Description.Roles
	roleData := roles[roleName]

	// This is the FileMountInfo object containing information about Kumori's configuration
	// file included in each container. This file indicates which tags are available per client
	// channel
	configFileInfo, err := context.calculateConfigFile(roleName)
	if err != nil {
		return
	}

	// Gathers containers info
	keys := context.GetOrderedContainersName(roleName)
	for _, name := range keys {
		codeElement := (*componentData.Description.Codes)[name]
		cprefix := fmt.Sprintf("%s%d", prefix, i)
		i++

		// Calculates the image name
		var image string
		if codeElement.Image.Hub != nil && codeElement.Image.Hub.Name != "" {
			image = fmt.Sprintf("%s/%s", codeElement.Image.Hub.Name, codeElement.Image.Tag)
		} else {
			image = codeElement.Image.Tag
		}

		// Gathers the container size (CPU, memory, ...)
		containerSize := ContainerSize{
			Iopsintensive: false,
		}
		if roleData.Artifact.Description.Profile != nil {
			containerSize.Iopsintensive = roleData.Artifact.Description.Profile.Iopsintensive
		}
		if codeElement.Size != nil {
			containerSize.CPU = fmt.Sprintf("%d%s", codeElement.Size.CPU.Size, codeElement.Size.CPU.Unit)
			containerSize.MinCPU = fmt.Sprintf("%d%s", codeElement.Size.MinCPU, codeElement.Size.CPU.Unit)
			containerSize.Memory = fmt.Sprintf("%d%s", codeElement.Size.Memory.Size, codeElement.Size.Memory.Unit)
			containerSize.Iopsintensive = roleData.Artifact.Description.Profile.Iopsintensive
		}

		// NOTE: this is left out of the previous if statement until Disk is supported in Kumori Service Model
		if context.Config.DefaultDiskRequestSize != 0 && context.Config.DefaultDiskRequestSize != -1 {
			diskSize := fmt.Sprintf("%d%s", context.Config.DefaultDiskRequestSize, context.Config.DefaultDiskRequestUnit)
			containerSize.Disk = &diskSize
		}

		// Creates the core container info
		containerInfo := ContainerInfo{
			Name:  name,
			Image: image,
			Size:  &containerSize,
		}

		// If a secret is required to download the container image, includes the secret in the container info
		if (codeElement.Image.Hub != nil) && (codeElement.Image.Hub.Secret != nil) && (len(*codeElement.Image.Hub.Secret) > 0) {

			secretResource, err := context.getRoleConfigSecret(roleName, *codeElement.Image.Hub.Secret)
			if err != nil {
				return nil, nil, err
			}
			secretResourceName := *secretResource.Secret
			secretCompleteResourceName := context.getResourceCompleteName(secretResourceName)
			if secretsInfo, ok := (*context.SecretsInfo)[secretCompleteResourceName]; ok {
				key := SecretsInfoDefaultKey
				secretsInfo.InDockerHub = true
				(*context.SecretsInfo)[secretCompleteResourceName] = secretsInfo
				containerInfo.Secret = &secretsInfo
				containerInfo.SecretKey = &key
			} else {
				return nil, nil, fmt.Errorf("secret '%s' not found. Role: '%s'. Container: %s", *codeElement.Image.Hub.Secret, roleName, name)
			}
		}

		// If the entrypoint is overrided, includes the new entrypoint in the container info
		if (codeElement.Entrypoint != nil) && (len(*codeElement.Entrypoint) > 0) {
			containerInfo.Cmd = codeElement.Entrypoint
		}

		// If the cmd is overrided, includes the new entrypoint arguments in the container info
		if (codeElement.Cmd != nil) && (len(*codeElement.Cmd) > 0) {
			containerInfo.Args = codeElement.Cmd
		}

		// Calculates the files mounted in this container
		var files []FileMountInfo
		if codeElement.Mapping != nil {
			if codeElement.Mapping.Env != nil {
				envs, err := context.getEnvs(roleName, codeElement.Mapping.Env)
				if err != nil {
					return nil, nil, err
				}
				containerInfo.Envs = envs
			}
			if codeElement.Mapping.FileSystem != nil {
				mappedFiles, err := context.getFiles(roleName, cprefix, codeElement.Mapping.FileSystem)
				if err != nil {
					return nil, nil, err
				}
				files = *mappedFiles

				volumesMounts, err := context.getVolumeMounts(cprefix, codeElement.Mapping.FileSystem, volumes)
				if err != nil {
					return nil, nil, err
				}
				if files != nil {
					containerInfo.VolumeMounts = *volumesMounts
				}
			}
		}
		if files == nil {
			files = []FileMountInfo{}
		}

		// Adds the file containing information about channels as a file to be mounted
		containerInfo.Files = append(files, *configFileInfo)

		// Adds the startup and liveness probes
		startupProbe, livenessProbe, readinessProbe, err := context.calculateContainerProbes(&componentData, name)
		if startupProbe != nil {
			containerInfo.StartupProbe = startupProbe
		}
		if livenessProbe != nil {
			containerInfo.LivenessProbe = livenessProbe
		}
		if readinessProbe != nil {
			containerInfo.ReadinessProbe = readinessProbe
		}

		// Sets the pull policy
		// Ticket510: currently, a pull policy cannot be set for containers in Kumori Service
		// model. This commented code is added only to show how this value should be set in the
		// container context
		// if codeElement.PullPolicy != nil {
		// 	pullPolicy := context.calculatePullPolicy(codeElement.PullPolicy)
		// 	containerInfo.PullPolicy = pullPolicy
		// }

		// If defined, adds the user and group ids used to run the container process
		if codeElement.User != nil {
			containerInfo.User = &Owner{
				UserID:  &codeElement.User.UserID,
				GroupID: &codeElement.User.GroupID,
			}
		}

		if codeElement.Init != nil && *codeElement.Init {
			containerInfo.Type = InitContainerType
			(*initContainersInfo)[name] = containerInfo
		} else {
			containerInfo.Type = DefaultContainerType
			(*containersInfo)[name] = containerInfo
		}
		// (*containersInfo)[name] = containerInfo

		if err != nil {
			return nil, nil, err
		}
	}

	return
}

// calculateConfigFile returns all the required information to mount in containers the
// file /kumori/config.json containing information about the client channels tags.
func (context *Context) calculateConfigFile(roleName string) (*FileMountInfo, error) {
	meth := fmt.Sprintf("Context.calculateConfigFile. V3Deployment: %s. Role: %s", context.Name, roleName)
	log.Infof(meth)

	data, err := context.tagsTools.GetRoleConfig(roleName)
	if err != nil {
		return nil, err
	}

	// Creates and returns the FileMountInfo declaring that the config file must be mounted in
	// in /kumori/config.json
	fileMountInfo := FileMountInfo{
		Name:           "kumoriconfig",
		Path:           "/kumori/config.json",
		Data:           data,
		RebootOnUpdate: context.Config.ForceRebootOnUpdate,
	}

	log.Debugf("%s. ConfigFile:\n%s\n", meth, string(data))

	return &fileMountInfo, nil
}
