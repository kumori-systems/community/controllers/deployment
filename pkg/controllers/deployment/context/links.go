/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package context

import (
	"fmt"
	kerrors "kuku-controller/pkg/utils/errors"

	log "github.com/sirupsen/logrus"
)

// getLinkedEndpoints finds out which role endpoints (endpoints with role != nil) from other deployments
// are reachable from a given service endpoint (endpoint representing a service channel and, hence, with
// role == nil). To that end, it follows the existing links connecting the deployment being processed to
// other deployments through the provided endpoint. NOTE: an endpoint is a channel.
func (context *Context) getLinkedEndpoints(endpoint *ConnectorEndpoint) ([]*ConnectorEndpoint, error) {

	meth := fmt.Sprintf("context.links.getLinkedEndpoints. Endpoint: %v", endpoint)

	log.Debugf(meth)

	// Returns an empty endpoints list if the given endpoint represents a role channel. Those channels are
	// not directly connected to a link
	if endpoint.Role != nil {
		return nil, nil
	}

	var kerr error

	// List of service endpoints to process. Starts with the endpoint provided as
	// a parameter but other endpoints might be added, but it is higly improbale
	// (this can only happen if service channels are found in the client and server
	// sides of a connector)
	pendingEndpoints := []*ConnectorEndpoint{endpoint}
	processedEndpoints := make([]*ConnectorEndpoint, 0, 1)
	endpoints := []*ConnectorEndpoint{}

	// Pops the next endpoint to process
	for len(pendingEndpoints) > 0 {
		currentEndpoint := pendingEndpoints[0]
		pendingEndpoints = pendingEndpoints[1:]
		processedEndpoints = append(processedEndpoints, currentEndpoint)

		// Gets the links linking the provided endpoint. If this channel is not linked then return the empty
		// list.
		linkedDeployments, kerr := context.kukuLinkTools.GetLinkedDeployments(context.Namespace, currentEndpoint.Deployment, currentEndpoint.ChannelName)

		if kerr != nil {
			log.Errorf("%s. Name: %s Channel: %s. Error: %s", meth, currentEndpoint.Deployment, currentEndpoint.ChannelName, kerr.Error())
		}

		// Calculates the reachable endpoints through each linked deployment
		for _, linkedDeployment := range linkedDeployments {

			// Skip if the linked deployment is nil, it hasn't a service declared or declares a built-in
			// service or it not declares connectors.
			linkedV3Deployment := linkedDeployment.V3Deployment
			if (linkedV3Deployment == nil) ||
				(linkedV3Deployment.Spec.Artifact == nil) ||
				(linkedV3Deployment.Spec.Artifact.Description) == nil ||
				(linkedV3Deployment.Spec.Artifact.Description.Builtin) ||
				(linkedV3Deployment.Spec.Artifact.Description.Connectors == nil) {
				continue
			}

			linkedEndpoint, err := getConnectorEndpoint("self", linkedDeployment.Channel, linkedV3Deployment, linkedDeployment.Link)
			if err != nil {
				log.Errorf("%s. Deployment: Role: 'self', '%s' Channel: '%s'. Error getting endpoint info: %+v", meth, linkedDeployment.Channel, linkedV3Deployment.Name, err)
				continue
			}

			connectors := *linkedV3Deployment.Spec.Artifact.Description.Connectors
			for _, data := range connectors {

				/******************************************************/
				/* Checks if this connector uses this linked endpoint */
				/******************************************************/

				// usesLinkedChannel will be true if the linked channel appears in this connector
				usesLinkedChannel := false

				// tag will contain the connector tag using the linked channel if this channel is a
				// server or a duplex
				var tag int

				// If the channel type is client we must check if the linked channel is in client
				// channels list. Otherwise (it is a server or a duplex) we must check the servers
				// list
				if linkedEndpoint.ChannelType == ClientChannel {
					for _, clientData := range data.Clients {
						if clientData.Channel != linkedDeployment.Channel || clientData.Role != "self" {
							continue
						}

						usesLinkedChannel = true
					}
				} else {
					for serverIndex, serverData := range data.Servers {
						for _, linkData := range serverData.Links {
							if linkData.Channel != linkedDeployment.Channel || linkData.Role != "self" {
								continue
							}

							tag = serverIndex
							usesLinkedChannel = true
						}
					}
				}

				// If the linked channel is not in this connector, skip it
				if !usesLinkedChannel {
					continue
				}

				/********************************/
				/* Gets the connector endpoints */
				/********************************/

				// The endpoints are the pairs role/channel reachable from the linked service
				// channel. If the reachable pair is another service channel (role= "self") then
				// this endpoint is not added to the results list but to the list of linked
				// endpoints pending to be processed

				// If the linked channel is a client channel then all connected server channels are
				// reachable. If the linked channel is a duplex channel, then only the connected server
				// channels of the same tag are reachable.
				if linkedEndpoint.ChannelType == ClientChannel || linkedEndpoint.ChannelType == DuplexChannel {
					for serverIndex, serverData := range data.Servers {

						// If the channel type is a duplex, only the endpoints in its own tag are
						// reachable
						if linkedEndpoint.ChannelType == DuplexChannel && serverIndex != tag {
							continue
						}

						for _, linkData := range serverData.Links {

							// If the linked channel type is a duplex, it will appear in the servers list and
							// must be skipped as a reachable endpoint
							if (linkedEndpoint.ChannelType == DuplexChannel) &&
								(linkData.Channel == linkedEndpoint.ChannelName) &&
								(linkData.Role == "self") {
								continue
							}

							// Calculates the endpoint information
							newEndpoint, err := getConnectorEndpoint(linkData.Role, linkData.Channel, linkedV3Deployment, linkedDeployment.Link)
							if err != nil {
								kerr = kerrors.NewKukuError(kerrors.Major, err)
								log.Errorf("%s. Error: cannot calculate the endpoint. Channel: '%s', role: '%s', deployment: '%s'. Error: %s", meth, linkData.Channel, linkData.Role, linkedV3Deployment.Name, err.Error())
								continue
							}

							// If the endpoint is a role channel then add it to the list of reachable endpoints.
							// Otherwise it is a service channel and must be added to the list of service endpoints
							// to be processed.
							if newEndpoint.Role != nil {
								endpoints = appendEndpoint(endpoints, newEndpoint)
							} else {
								// Checks if this service endpoint has been already processed or it is already in the
								// pending list (there is a links loop).
								if containsEndpoint(processedEndpoints, newEndpoint) ||
									containsEndpoint(pendingEndpoints, newEndpoint) {
									kerr = kerrors.NewKukuError(kerrors.Major, fmt.Errorf("links loop detected in deployment %s channel %s", newEndpoint.Deployment, newEndpoint.ChannelName))
								} else {
									pendingEndpoints = append(pendingEndpoints, newEndpoint)
								}
							}
						}
					}
				}

				// If the linked channel is a server or a duplex then all connected client channels are
				// reachable
				if linkedEndpoint.ChannelType == ServerChannel || linkedEndpoint.ChannelType == DuplexChannel {

					for _, clientData := range data.Clients {

						// Calculates the endpoint information
						newEndpoint, err := getConnectorEndpoint(clientData.Role, clientData.Channel, linkedV3Deployment, linkedDeployment.Link)
						if err != nil {
							kerr = kerrors.NewKukuError(kerrors.Major, err)
							log.Errorf("%s. Error: cannot calculate the endpoint. Channel: '%s', role: '%s', deployment: '%s'. Error: %s", meth, clientData.Channel, clientData.Role, linkedV3Deployment.Name, err.Error())
							continue
						}

						// If the endpoint is a role channel then add it to the list of reachable endpoints.
						// Otherwise it is a service channel and must be added to the list of service endpoints
						// to be processed.
						if newEndpoint.Role != nil {
							endpoints = appendEndpoint(endpoints, newEndpoint)
						} else {
							// Checks if this service endpoint has been already processed or it is already in the
							// pending list (there is a links loop).
							if containsEndpoint(processedEndpoints, newEndpoint) ||
								containsEndpoint(pendingEndpoints, newEndpoint) {
								kerr = kerrors.NewKukuError(kerrors.Major, fmt.Errorf("links loop detected in deployment %s channel %s", newEndpoint.Deployment, newEndpoint.ChannelName))
							} else {
								pendingEndpoints = append(pendingEndpoints, newEndpoint)
							}
						}
					}
				}
			}
		}
	}

	if kerr != nil {
		return endpoints, kerr
	}

	return endpoints, nil
}
