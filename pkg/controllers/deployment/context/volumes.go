/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package context

import (
	"encoding/json"
	"fmt"
	kerrors "kuku-controller/pkg/utils/errors"
	"strings"

	log "github.com/sirupsen/logrus"
	kumoriv1 "gitlab.com/kumori-systems/community/libraries/client-go/pkg/apis/kumori/v1"
	resource "k8s.io/apimachinery/pkg/api/resource"
)

// getVolumes returns the volumes used by a given role
func (context *Context) getVolumes(
	roleName string,
) (
	*map[string]*Volume,
	error,
) {
	meth := "context.volumes.getVolumes"
	log.Debug(meth)

	kukuVolumes := (*context.kukuVolumes)[roleName]

	volumes := map[string]*Volume{}
	var kcerr *kerrors.KukuError

	v3Deployment := context.Deployment

	// Checks if the deployment service is not empty. If that's the case,
	if (v3Deployment.Spec.Artifact == nil) ||
		(v3Deployment.Spec.Artifact.Description == nil) ||
		(v3Deployment.Spec.Artifact.Description.Builtin) ||
		(v3Deployment.Spec.Artifact.Description.Roles == nil) {
		return &volumes, nil
	}

	// Gets role data
	roleData, ok := (*v3Deployment.Spec.Artifact.Description.Roles)[roleName]
	if !ok {
		message := fmt.Sprintf("Role %s not dound in deployment", roleName)
		log.Warnf("%s. %s", meth, message)
		kcerr = &kerrors.KukuError{Severity: kerrors.Minor, Parent: fmt.Errorf(message)}
		return nil, kcerr
	}

	// If the component is not declared, or it is a builtin component, or it hasn't
	// any resource assigned, skip it. It will never use volume resources.
	if (roleData.Artifact.Description.Config == nil) ||
		(roleData.Artifact.Description.Config.Resource == nil) {
		return nil, nil
	}

	// Search for volumes
	for resourceName, resourceData := range *roleData.Artifact.Description.Config.Resource {

		// Skip non-volume resources
		if resourceData.Volume == nil {
			continue
		}

		// Calculates the resourceName
		// volumeName := CreateHashName(resourceName)
		volumeName := resourceName

		// The volume is a persistent volume
		if !context.isInlineVolume(&resourceData) {
			if !context.Config.DisablePersistentVolumes {
				var key string
				json.Unmarshal(resourceData.Volume.Raw, &key)
				if kukuVolumes == nil {
					message := fmt.Sprintf("KukuVolume '%s' not found (1)", key)
					log.Warnf("%s. %s", meth, message)
					kcerr = &kerrors.KukuError{Severity: kerrors.Minor, Parent: fmt.Errorf(message)}
					continue
				}
				kvolume, ok := kukuVolumes[resourceName]
				if !ok {
					message := fmt.Sprintf("KukuVolume '%s' not found (2)", key)
					log.Warnf("%s. %s", meth, message)
					kcerr = &kerrors.KukuError{Severity: kerrors.Minor, Parent: fmt.Errorf(message)}
					continue
				}

				volumeTypeKinds := make([]VolumeTypeKind, 0, 2)

				// Checks if the volume is persistent
				if isPersistent, err := context.kukuVolumeHasProperty(kvolume, KukuVolumePersistentProperty); err != nil {
					log.Errorf("%s. Error checking if KukuVolume '%s' is persistent: %v", meth, resourceName, err)
					kcerr = &kerrors.KukuError{Severity: kerrors.Minor, Parent: err}
					continue
				} else if isPersistent {
					volumeTypeKinds = append(volumeTypeKinds, PersistentVolumeType)
				}

				// Checks if the volume is shared
				if isShared, err := context.kukuVolumeHasProperty(kvolume, KukuVolumeSharedProperty); err != nil {
					log.Errorf("%s. Error checking if KukuVolume '%s' is shared: %v", meth, resourceName, err)
					kcerr = &kerrors.KukuError{Severity: kerrors.Minor, Parent: err}
					continue
				} else if isShared {
					volumeTypeKinds = append(volumeTypeKinds, SharedVolumeType)
				}

				// Registered KukuVolumes are never volatile. Therefore, we are not going to check it

				volumeType, err := context.getVolumeType(kvolume.Spec.Type, volumeTypeKinds)
				if err != nil {
					message := fmt.Sprintf("Error processing resource '%s': %s", resourceName, err.Error())
					log.Errorf("%s. %s", meth, message)
					kcerr = &kerrors.KukuError{Severity: kerrors.Fatal, Parent: fmt.Errorf(message)}
					continue
				}

				volume := Volume{
					Name:         volumeName,
					ResourceName: resourceName,
					Type:         *volumeType,
					KukuVolume:   kvolume,
				}
				volumes[resourceName] = &volume
			}
		} else {

			// Unmarshal the volatile volume data
			var volatileData kumoriv1.VolatileVolumeSpec
			err := json.Unmarshal(resourceData.Volume.Raw, &volatileData)
			if err != nil {
				message := fmt.Sprintf("Error processing resource '%s': %s", resourceName, err.Error())
				log.Warnf("%s. %s", meth, message)
				kcerr = &kerrors.KukuError{Severity: kerrors.Minor, Parent: fmt.Errorf(message)}
				continue
			}

			// The volume is a volatile volume
			if volatileData.Size != nil {
				var unit string
				if volatileData.Unit == nil {
					unit = "G"
				} else {
					unit = *volatileData.Unit
				}
				size, err := resource.ParseQuantity(fmt.Sprintf("%d%s", *volatileData.Size, unit))
				if err != nil {
					message := fmt.Sprintf("Error processing size %d.: %s", *volatileData.Size, err.Error())
					log.Warnf("%s. %s", meth, message)
					kcerr = &kerrors.KukuError{Severity: kerrors.Minor, Parent: fmt.Errorf(message)}
					continue
				}

				volumeType, err := context.getVolumeType(context.Config.DefaultVolatileVolumesType, []VolumeTypeKind{VolatileVolumeType})
				if err != nil {
					message := fmt.Sprintf("Error processing resource '%s': %s", resourceName, err.Error())
					log.Errorf("%s. %s", meth, message)
					kcerr = &kerrors.KukuError{Severity: kerrors.Fatal, Parent: fmt.Errorf(message)}
					continue
				}

				volume := Volume{
					Name:         volumeName,
					ResourceName: resourceName,
					Type:         *volumeType,
					Size:         &size,
				}
				volumes[resourceName] = &volume
			}
		}
	}

	// To avoid https://golang.org/doc/faq#nil_error
	if kcerr == nil {
		return &volumes, nil
	}

	return &volumes, kcerr
}

func (context *Context) getVolumeType(volumeKind string, requiredProperties []VolumeTypeKind) (*VolumeType, error) {
	meth := fmt.Sprintf("context.volumes.getVolumeType. VolumeKind: '%s' RequiredProperties: '%v'", volumeKind, requiredProperties)
	log.Debugf(meth)

	var volumeType VolumeType

	// If PVC is no used for volatile volumes and the volme is volatile, don't look
	// into the cluster configuration and just return a volatile volume type
	if !context.Config.UsePvcForVolatileVolumes {
		for _, requiredProperty := range requiredProperties {
			if requiredProperty == VolatileVolumeType {
				if len(volumeKind) == 0 {
					volumeKind = "defaultvolatile"
				}
				volumeType = VolumeType{
					Name:         volumeKind,
					StorageClass: "",
					Properties:   []VolumeTypeKind{VolatileVolumeType},
				}
				return &volumeType, nil
			}
		}
	}

	// Returns an error if volume types are not defined in the cluster configuration.
	if context.clusterConfig.VolumeTypes == nil {
		return nil, fmt.Errorf("VolumeType '%s' not found in cluster configuration", volumeKind)
	}

	clusterConfigVolumeType, ok := (*context.clusterConfig.VolumeTypes)[volumeKind]
	if !ok {
		return nil, fmt.Errorf("VolumeType '%s' not found in cluster configuration", volumeKind)
	}

	// Checks if the volume type properties match the properties required
	volumeTypeProperties := strings.Split(clusterConfigVolumeType.Properties, ",")
	properties := make([]VolumeTypeKind, 0, len(volumeTypeProperties))
	for _, requiredProperty := range requiredProperties {
		match := false
		for _, volumeTypeProperty := range volumeTypeProperties {
			if volumeTypeProperty == string(requiredProperty) {
				match = true
				break
			}
		}
		if !match {
			return nil, fmt.Errorf("volume declared as '%s' but VolumeType '%s' not contains that property", requiredProperty, volumeKind)
		}
	}

	// Adds the volume type properties
	for _, volumeTypeProperty := range volumeTypeProperties {
		switch volumeTypeProperty {
		case "persistent":
			properties = append(properties, PersistentVolumeType)
		case "volatile":
			properties = append(properties, VolatileVolumeType)
		case "shared":
			properties = append(properties, SharedVolumeType)
		default:
			log.Debugf("%s. Unknown volume type property '%s'. Ignoring...", meth, volumeTypeProperty)
		}

	}

	// Returns the volumeType
	volumeType = VolumeType{
		Name:         volumeKind,
		StorageClass: clusterConfigVolumeType.StorageClass,
		Properties:   properties,
	}

	return &volumeType, nil
}
