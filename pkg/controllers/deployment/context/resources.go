/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package context

import (
	"encoding/json"
	"fmt"
	kerrors "kuku-controller/pkg/utils/errors"
	kjson "kuku-controller/pkg/utils/json"
	"kuku-controller/pkg/utils/kumori"
	kukucautils "kuku-controller/pkg/utils/kumori/kukucas"
	kukucertutils "kuku-controller/pkg/utils/kumori/kukucerts"
	kukudomainutils "kuku-controller/pkg/utils/kumori/kukudomains"
	kukuportutils "kuku-controller/pkg/utils/kumori/kukuports"
	kukusecretutils "kuku-controller/pkg/utils/kumori/kukusecrets"
	kukuvolumeutils "kuku-controller/pkg/utils/kumori/kukuvolumes"
	v3dutils "kuku-controller/pkg/utils/kumori/v3deployments"
	"strings"

	log "github.com/sirupsen/logrus"
	kumoriv1 "gitlab.com/kumori-systems/community/libraries/client-go/pkg/apis/kumori/v1"
)

// getKukuResources returns the Kumori objects representing the resources assigned per role in a
// given deployment.
func (context *Context) getKukuResources(v3Deployment *kumoriv1.V3Deployment) (
	cas *map[string]map[string]*kumoriv1.KukuCa,
	certs *map[string]map[string]*kumoriv1.KukuCert,
	domains *map[string]map[string]*kumoriv1.KukuDomain,
	ports *map[string]map[string]*kumoriv1.KukuPort,
	secrets *map[string]map[string]*kumoriv1.KukuSecret,
	volumes *map[string]map[string]*kumoriv1.KukuVolume,
	err error,
) {
	meth := "context.resources.getKukuResources"
	log.Debug(meth)

	cas = &map[string]map[string]*kumoriv1.KukuCa{}
	certs = &map[string]map[string]*kumoriv1.KukuCert{}
	domains = &map[string]map[string]*kumoriv1.KukuDomain{}
	ports = &map[string]map[string]*kumoriv1.KukuPort{}
	secrets = &map[string]map[string]*kumoriv1.KukuSecret{}
	volumes = &map[string]map[string]*kumoriv1.KukuVolume{}

	// Checks if this deployment declares any resources and any role
	if (v3Deployment.Spec.Artifact == nil) ||
		(v3Deployment.Spec.Artifact.Description == nil) ||
		(v3Deployment.Spec.Artifact.Description.Builtin) ||
		(v3Deployment.Spec.Artifact.Description.Roles == nil) ||
		(v3Deployment.Spec.Config == nil) ||
		(v3Deployment.Spec.Config.Resource == nil) ||
		(len(*v3Deployment.Spec.Config.Resource) <= 0) {
		return
	}

	// Namespace and owner used to retrieve the kumori objects representing resources
	namespace := context.Namespace
	ownerDomain, rawerr := v3dutils.GetOwnerDomain(v3Deployment)
	if rawerr != nil {
		err = &kerrors.KukuError{Severity: kerrors.Major, Parent: rawerr}
		return
	}

	// Search for reources in components
	for roleName, roleData := range *v3Deployment.Spec.Artifact.Description.Roles {

		(*cas)[roleName] = map[string]*kumoriv1.KukuCa{}
		(*certs)[roleName] = map[string]*kumoriv1.KukuCert{}
		(*domains)[roleName] = map[string]*kumoriv1.KukuDomain{}
		(*ports)[roleName] = map[string]*kumoriv1.KukuPort{}
		(*secrets)[roleName] = map[string]*kumoriv1.KukuSecret{}
		(*volumes)[roleName] = map[string]*kumoriv1.KukuVolume{}

		// If the component or is not declared, skip it. It will not use KukuSecrets
		if (roleData.Artifact == nil) || (roleData.Artifact.Description == nil) {
			continue
		}

		// Skip this role if it is a built-in role, not declares any container or not contains
		// any resource
		if (roleData.Artifact.Description.Builtin) ||
			(roleData.Artifact.Description.Codes == nil) ||
			(roleData.Artifact.Description.Config == nil) ||
			(roleData.Artifact.Description.Config.Resource == nil) ||
			(len(*roleData.Artifact.Description.Config.Resource) <= 0) {
			continue
		}

		resources := roleData.Artifact.Description.Config.Resource

		// Retrieves the resources used by this role
		for resourceKey, resourceData := range *resources {
			// If it is a secret resource
			if (resourceData.Secret != nil) && (*resourceData.Secret != "") {
				resourceName := *resourceData.Secret
				resource, kerr := kukusecretutils.GetByRegistrationReference(context.kukuSecretsLister, namespace, resourceName, ownerDomain)
				if kerr != nil {
					log.Warnf("%s. Skipping secret '%s': Error: %s", meth, resourceKey, kerr.Error())
					if kerr == nil {
						err = &kerrors.KukuError{Severity: kerrors.Minor, Parent: kerr}
					}
				} else {
					(*secrets)[roleName][resourceKey] = resource
				}
				continue
			}

			// If it is a port resource
			if resourceData.Port != nil && (*resourceData.Port != "") {
				resourceName := *resourceData.Port
				resource, kerr := kukuportutils.GetByRegistrationReference(context.kukuPortsLister, namespace, resourceName, ownerDomain)
				if kerr != nil {
					log.Warnf("%s. Skipping port '%s': Error: %s", meth, resourceKey, kerr.Error())
					if err == nil {
						err = &kerrors.KukuError{Severity: kerrors.Minor, Parent: kerr}
					}
				} else {
					(*ports)[roleName][resourceKey] = resource
				}
				continue
			}

			// If it is a domain resource
			if (resourceData.Domain != nil) && (*resourceData.Domain != "") {
				resourceName := *resourceData.Domain
				resource, kerr := kukudomainutils.GetByRegistrationReference(context.kukuDomainsLister, namespace, resourceName, ownerDomain)
				if kerr != nil {
					log.Warnf("%s. Skipping domain '%s': Error: %s", meth, resourceKey, kerr.Error())
					if err == nil {
						err = &kerrors.KukuError{Severity: kerrors.Minor, Parent: kerr}
					}
				} else {
					(*domains)[roleName][resourceKey] = resource
				}
				continue
			}

			// If it is a certificate resource
			if (resourceData.Certificate != nil) && (*resourceData.Certificate != "") {
				resourceName := *resourceData.Certificate
				resource, kerr := kukucertutils.GetByRegistrationReference(context.kukuCertsLister, namespace, resourceName, ownerDomain)
				if kerr != nil {
					log.Warnf("%s. Skipping certificate '%s': Error: %s", meth, resourceKey, kerr.Error())
					if err == nil {
						err = &kerrors.KukuError{Severity: kerrors.Minor, Parent: kerr}
					}
				} else {
					(*certs)[roleName][resourceKey] = resource
				}
				continue
			}

			// If it is a CA resource
			if (resourceData.CA != nil) && (*resourceData.CA != "") {
				resourceName := *resourceData.CA
				resource, kerr := kukucautils.GetByRegistrationReference(context.kukuCasLister, namespace, resourceName, ownerDomain)
				if kerr != nil {
					log.Warnf("%s. Skipping CA '%s': Error: %s", meth, resourceKey, kerr.Error())
					if err == nil {
						err = &kerrors.KukuError{Severity: kerrors.Minor, Parent: kerr}
					}
				} else {
					(*cas)[roleName][resourceKey] = resource
				}
				continue
			}

			// If it is a volume resource
			if resourceData.Volume != nil {
				if !context.isInlineVolume(&resourceData) && !context.Config.DisablePersistentVolumes {
					var resourceName string
					json.Unmarshal(resourceData.Volume.Raw, &resourceName)
					resource, kerr := kukuvolumeutils.GetByRegistrationReference(context.kukuVolumesLister, namespace, resourceName, ownerDomain)
					if kerr != nil {
						log.Warnf("%s. Skipping volume '%s': Error: %s", meth, resourceKey, kerr.Error())
						if err == nil {
							err = &kerrors.KukuError{Severity: kerrors.Minor, Parent: kerr}
						}
					} else {
						(*volumes)[roleName][resourceKey] = resource
					}
					continue
				}
			}
		}
	}

	return
}

// marshalResources returns two maps. The first one contains a serialized description of the resources
// used in a given role. The second one contains the same content but serialized as a JSON string (i.e.,
// escapes characters like double quotes or new lines)
func (context *Context) marshalResources() (
	marshalled *map[string]map[string]string,
	stringified *map[string]map[string]string,
	err error,
) {

	meth := "context.resources.marshalResources"

	marshalled = &map[string]map[string]string{}
	stringified = &map[string]map[string]string{}

	// Serialize CAs
	if context.kukuCas != nil {
		for roleName, rolecas := range *context.kukuCas {
			if rolecas != nil {
				if _, ok := (*marshalled)[roleName]; !ok {
					(*marshalled)[roleName] = map[string]string{}
					(*stringified)[roleName] = map[string]string{}
				}
				for name, kukuca := range rolecas {
					var data []byte
					data, err = kjson.MarshalCA(kukuca)
					if err != nil {
						log.Errorf("%s. Cannot marshal CA '%s' in role '%s': %s", meth, name, roleName, err.Error())
						err = kerrors.NewKukuError(kerrors.Major, err)
						return
					}
					strdata := kjson.Stringify(data)
					(*marshalled)[roleName][name] = string(data)
					(*stringified)[roleName][name] = string(strdata)
				}
			}
		}
	}

	// Serialize certificates
	if context.kukuCerts != nil {
		for roleName, rolecerts := range *context.kukuCerts {
			if rolecerts != nil {
				if _, ok := (*marshalled)[roleName]; !ok {
					(*marshalled)[roleName] = map[string]string{}
					(*stringified)[roleName] = map[string]string{}
				}
				for name, kukucert := range rolecerts {
					var data []byte
					data, err = kjson.MarshalCert(kukucert)
					if err != nil {
						log.Errorf("%s. Cannot marshal certificate '%s' in role '%s': %s", meth, name, roleName, err.Error())
						err = kerrors.NewKukuError(kerrors.Major, err)
						return
					}
					strdata := kjson.Stringify(data)
					(*marshalled)[roleName][name] = string(data)
					(*stringified)[roleName][name] = string(strdata)
				}
			}
		}
	}

	// Serialize domains
	if context.kukuDomains != nil {
		for roleName, roledomains := range *context.kukuDomains {
			if roledomains != nil {
				if _, ok := (*marshalled)[roleName]; !ok {
					(*marshalled)[roleName] = map[string]string{}
					(*stringified)[roleName] = map[string]string{}
				}
				for name, kukudomain := range roledomains {
					var data []byte
					data, err = kjson.MarshalDomain(kukudomain)
					if err != nil {
						log.Errorf("%s. Cannot marshal domain '%s' in role '%s': %s", meth, name, roleName, err.Error())
						err = kerrors.NewKukuError(kerrors.Major, err)
						return
					}
					strdata := kjson.Stringify(data)
					(*marshalled)[roleName][name] = string(data)
					(*stringified)[roleName][name] = string(strdata)
				}
			}
		}
	}

	// Serialize ports
	if context.kukuPorts != nil {
		for roleName, roleports := range *context.kukuPorts {
			if roleports != nil {
				if _, ok := (*marshalled)[roleName]; !ok {
					(*marshalled)[roleName] = map[string]string{}
					(*stringified)[roleName] = map[string]string{}
				}
				for name, kukuport := range roleports {
					var data []byte
					data, err = kjson.MarshalPort(kukuport)
					if err != nil {
						log.Errorf("%s. Cannot marshal port '%s' in role '%s': %s", meth, name, roleName, err.Error())
						err = kerrors.NewKukuError(kerrors.Major, err)
						return
					}
					strdata := kjson.Stringify(data)
					(*marshalled)[roleName][name] = string(data)
					(*stringified)[roleName][name] = string(strdata)
				}
			}
		}
	}

	// Serialize secrets
	if context.kukuSecrets != nil {
		for roleName, rolesecrets := range *context.kukuSecrets {
			if rolesecrets != nil {
				if _, ok := (*marshalled)[roleName]; !ok {
					(*marshalled)[roleName] = map[string]string{}
					(*stringified)[roleName] = map[string]string{}
				}
				for name, kukusecret := range rolesecrets {
					var data []byte
					data, err = kjson.MarshalSecret(kukusecret)
					if err != nil {
						log.Errorf("%s. Cannot marshal secret '%s' in role '%s': %s", meth, name, roleName, err.Error())
						err = kerrors.NewKukuError(kerrors.Major, err)
						return
					}
					strdata := kjson.Stringify(data)
					(*marshalled)[roleName][name] = string(data)
					(*stringified)[roleName][name] = string(strdata)
				}
			}
		}
	}

	return
}

func (context *Context) getResourceCompleteName(resourceName string) string {
	domain := context.Deployment.Annotations[kumori.KumoriDomainLabel]
	parts := strings.Split(resourceName, "/")
	if len(parts) > 1 {
		return resourceName
	}
	return fmt.Sprintf("%s/%s", domain, resourceName)
}

func (context *Context) isInlineVolume(resource *kumoriv1.ConfigurationResource) bool {
	if (resource == nil) || (resource.Volume == nil) {
		return true
	}

	data := string(resource.Volume.Raw)

	if strings.HasPrefix(data, "{") && strings.HasSuffix(data, "}") {
		return true
	}

	return false
}

// kukuVolumeHasProperty checks if the volume type of a given KukuVolume contains a given property.
// Possible properties: persistent, volatile, shared
func (context *Context) kukuVolumeHasProperty(kukuVolume *kumoriv1.KukuVolume, volumeProperty VolumeProperty) (bool, error) {

	// Returns an error if volume types are not defined in the cluster configuration.
	if context.clusterConfig.VolumeTypes == nil {
		return false, fmt.Errorf("volume types not found in cluster configuration")
	}

	// Gets the volume type of the KukuVolume. Fails if it is not defined in the cluster configuration
	volumeTypeName := kukuVolume.Spec.Type
	volumeType, ok := (*context.clusterConfig.VolumeTypes)[volumeTypeName]
	if !ok {
		return false, fmt.Errorf("volume type '%s' not found in cluster configuration", volumeTypeName)
	}

	// Checks if the volume type contains the "persistent" property
	properties := strings.Split(volumeType.Properties, ",")
	for _, property := range properties {
		if property == string(volumeProperty) {
			// The volume is persistent
			return true, nil
		}
	}
	// The volume is not persistent
	return false, nil
}
