/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package context

import (
	"fmt"
	"sort"
)

// GetTaggedConnectorName returns the name associated to a connector/tag tuple
func GetTaggedConnectorName(connector, tag string) string {
	return fmt.Sprintf("%s-%s", connector, tag)
}

// GetOrderedConnectorNames returns an ordered array with the keys of a given map
func (context *Context) GetOrderedConnectorNames() []string {

	if context.orderedConnectorNames != nil && len(context.orderedConnectorNames) > 0 {
		return context.orderedConnectorNames
	}

	if context.Deployment.Spec.Artifact.Description.Connectors == nil {
		return []string{}
	}

	themap := *context.Deployment.Spec.Artifact.Description.Connectors
	keys := make([]string, 0, len(themap))
	for key := range themap {
		keys = append(keys, key)
	}
	sort.Strings(keys)
	context.orderedConnectorNames = keys
	return keys
}

// GetOrderedContainersName returns an ordered array with the keys of a given map
func (context *Context) GetOrderedContainersName(roleName string) []string {
	componentData := (*context.Deployment.Spec.Artifact.Description.Roles)[roleName].Artifact
	if componentData.Description.Codes == nil {
		return []string{}
	}
	themap := *componentData.Description.Codes
	keys := make([]string, 0, len(themap))
	for key := range themap {
		keys = append(keys, key)
	}
	sort.Strings(keys)
	return keys
}

func appendEndpoint(endpoints []*ConnectorEndpoint, newEndpoint *ConnectorEndpoint) []*ConnectorEndpoint {

	// Returns the current list if the appended endpoint is nil
	if newEndpoint == nil {
		return endpoints
	}

	// Returns a new list with the provided element if the original list
	// is nil or empty
	if (endpoints == nil) || (len(endpoints) <= 0) {
		return []*ConnectorEndpoint{newEndpoint}
	}

	// If the endpoint is already in the list, just append the links from
	// newEndpoint to the existint endpoint in the list
	for _, endpoint := range endpoints {
		if endpoint.ChannelName != newEndpoint.ChannelName {
			continue
		}
		if endpoint.ChannelType != newEndpoint.ChannelType {
			continue
		}
		if endpoint.Role == nil && newEndpoint.Role != nil {
			continue
		}
		if endpoint.Role != nil && newEndpoint.Role == nil {
			continue
		}

		if endpoint.Role != nil && *endpoint.Role != *newEndpoint.Role {
			continue
		}

		if endpoint.Deployment != newEndpoint.Deployment {
			continue
		}

		if endpoint.Links == nil {
			endpoint.Links = newEndpoint.Links
		} else {
			if newEndpoint.Links != nil {
				for _, newLink := range newEndpoint.Links {
					linkFound := false
					for _, link := range endpoint.Links {
						if link == newLink {
							linkFound = true
							break
						}
					}
					if !linkFound {
						endpoint.Links = append(endpoint.Links, newLink)
					}
				}
			}
		}
		return endpoints
	}

	// If the endpoint is not in the list, append it
	return append(endpoints, newEndpoint)
}
