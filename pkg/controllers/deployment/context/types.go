/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package context

import (
	"time"

	kumoriv1 "gitlab.com/kumori-systems/community/libraries/client-go/pkg/apis/kumori/v1"
	resource "k8s.io/apimachinery/pkg/api/resource"
	"k8s.io/apimachinery/pkg/runtime"
)

// ConnectorEndpoint represents an endpoint linked to a given connector. An endpoint is a service or role channel.
type ConnectorEndpoint struct {
	Deployment  string      `json:"deployment"`
	ChannelName string      `json:"channelName"`
	ChannelType ChannelType `json:"channelType"`
	Role        *string     `json:"role,omitempty"`
	Links       []string    `json:"links,omitempty"`
	Protocol    string      `json:"protocol,omitempty"`
	Port        *int32      `json:"port,omitempty"`
}

// ConnectorTagInfo contains information about a given connector, including its name and type:
// * Name: the connector-tag pair name. Currently as OriginalName-Tag
// * ShortName: a short name for internal purposes only
// * ConnectorName: the name of the connector declared in the V3Deployment
// * TagName: the tag used in this connector-tag pair
// * Port: the port exposed by the connector.
// * Type: the connector type ("full" or "lb")
// * Servers: who can receive connections.
// * Clients: who can connect
type ConnectorTagInfo struct {
	Name          string
	ShortName     string
	ConnectorName string
	TagName       string
	Port          int32
	NumPorts      *int32
	Type          ConnectorType
	Servers       []*ConnectorEndpoint
	Clients       []*ConnectorEndpoint
	LinkedServers []*ConnectorEndpoint
	LinkedClients []*ConnectorEndpoint
}

// ConnectorType is used to store connectors types.
type ConnectorType string

const (
	// LoadBalancerConnector balances requests between providers
	LoadBalancerConnector ConnectorType = "loadbalancer"
	// PublishSubscribeConnector sends messages to all subscribed requesters
	PublishSubscribeConnector ConnectorType = "pubsub"
	// FullConnector enables point to point communication between requesters
	FullConnector ConnectorType = "full"
)

// ChannelType is used to store the channel type
type ChannelType string

const (
	// ClientChannel is a connector channel
	ClientChannel ChannelType = "client"
	// ServerChannel is a binder channel
	ServerChannel ChannelType = "server"
	// DuplexChannel is a binder channel
	DuplexChannel ChannelType = "duplex"
)

// ChannelInfo stores information a channel used in a service.
type ChannelInfo struct {
	Name            string      `json:"name"`
	Type            ChannelType `json:"type"`
	Protocol        string      `json:"protocol"`
	Port            *int32      `json:"port,omitempty"`
	NumPorts        *int32      `json:"numports,omitempty"`
	RoleInConnector ChannelType `json:"role"`
}

type Reference struct {
	Name       string  `json:"name"`
	Domain     string  `json:"domain"`
	Version    []int16 `json:"version"`
	Prerelease *string `json:"prerelease,omitempty"`
}

// RoleInfo stores information about a role
type RoleInfo struct {
	ChannelsInfo          *map[string]ChannelInfo
	RoleSize              *RoleSize
	ComponentReference    *Reference
	ContainersInfo        *map[string]ContainerInfo
	InitContainersInfo    *map[string]ContainerInfo
	Name                  string
	Stateful              bool
	OrderedContainerNames []string
	Volumes               *map[string]*Volume `json:"volumes,omitempty"`
	Autoscaled            bool
}

// ContainerSize describes the resources assigned to a role component
type ContainerSize struct {
	CPU           string  `json:"cpu"`
	MinCPU        string  `json:"mincpu"`
	Memory        string  `json:"memory"`
	Disk          *string `json:"disk"`
	Ioperf        *string `json:"ioperf"`
	Iopsintensive bool    `json:"iopsintensive"`
	Bandwidth     *string `json:"bandwidth"`
}

// RoleSize sets the role elasticity boundaries.
type RoleSize struct {
	Instances    int32  `json:"instances"`
	Maxinstances *int32 `json:"maxinstances,omitempty"`
	Mininstances *int32 `json:"mininstances,omitempty"`
	Resilience   int32  `json:"resilience"`
	Bandwidth    string `json:"bandwitdth"`
	MinBandwidth string `json:"minbandwitdth"`
	MinCPU       string `json:"mincpu"`
}

// Owner represents the owner of a file, process, folder, ...
type Owner struct {
	UserID  *int64
	GroupID *int64
}

// ContainerInfo describes a container
type ContainerInfo struct {
	Name           string
	Image          string
	Cmd            *[]string
	Args           *[]string
	User           *Owner
	Files          []FileMountInfo
	Envs           []EnvInfo
	VolumeMounts   []VolumeMountInfo
	Secret         *SecretsInfo
	SecretKey      *string
	Size           *ContainerSize
	LivenessProbe  *Probe
	ReadinessProbe *Probe
	StartupProbe   *Probe
	CPU            string
	MinCPU         string
	Memory         string
	PullPolicy     *PullPolicyType
	Type           ContainerType
}

type ContainerType string

const (
	DefaultContainerType ContainerType = "default"
	InitContainerType    ContainerType = "init"
)

// PullPolicyType defines when a container image is pulled from its registry
type PullPolicyType string

const (
	// AlwaysPullPolicy is used when the images is pulled each time a new container is created
	AlwaysPullPolicy PullPolicyType = "always"
	// IfNotPresentPullPolicy is used when the images is pulled only if not exists locally
	IfNotPresentPullPolicy PullPolicyType = "ifNotPresent"
	// Never is used when images are never pulled from their registry
	NeverPullPolicy PullPolicyType = "never"
)

// Probe is used to check the container health
type Probe struct {
	HTTP             *HTTPProbe
	TCP              *TCPProbe
	Exec             *ExecProbe
	FailureThreshold int32
	Period           time.Duration
	InitialDelay     time.Duration
	Timeout          time.Duration
}

// HTTPProbe defines a HTTP probe to check the liveness/readiness of a container
// Scheme: defaults to HTTP
type HTTPProbe struct {
	Headers *map[string]string
	Path    string
	Port    int32
	Scheme  string
}

// TCPProbe defines a TCP probe to check the liveness/readiness of a container
type TCPProbe struct {
	Port int32
}

// ExecProbe defines an exec probe to check liveness/readiness of a container
type ExecProbe struct {
	Command []string
}

// Permissions contains the permissions for a given file
type Permissions struct {
	User *Owner
	Mode *int32
}

// FileMountInfo describes a file exposed in a container. If the Data section is nil then
// the Secret and SecretKey must be specified
type FileMountInfo struct {
	Name           string
	Path           string
	Data           []byte
	Secret         *SecretsInfo
	SecretKey      *string
	Perms          *Permissions
	RebootOnUpdate bool
}

// EnvInfo exposes an environment variable exposed in a container. If the Value
// is not set then Secret and Key must be set.
type EnvInfo struct {
	Name      string
	Value     *string
	Secret    *SecretsInfo
	SecretKey *string
}

// VolumeMountInfo exposes a volume in a path of a given container
type VolumeMountInfo struct {
	Path   string
	Volume *Volume
}

// ParameterInfo contains the information gathered for a given parameter
type ParameterInfo struct {
	Name  string
	Type  ParameterType
	Value interface{}
}

// ParameterType is the value type expected for a given parameter
type ParameterType string

const (
	// BooleanParameterType is the boolean type
	BooleanParameterType ParameterType = "boolean"
	// FileParameterType is the file type
	FileParameterType ParameterType = "file"
	// JSONParameterType is the json type
	JSONParameterType ParameterType = "json"
	// NumberParameterType is the number type
	NumberParameterType ParameterType = "number"
	// StringParameterType is the string type
	StringParameterType ParameterType = "string"
)

// ServiceInfo stores information about the deployment service
type ServiceInfo struct {
	ChannelsInfo *map[string]ChannelInfo
	Reference    Reference
}

// VolumeType contains the type of a given volume (volatile or persistent)
type VolumeTypeKind string

type VolumeType struct {
	Name         string
	StorageClass string
	Properties   []VolumeTypeKind
}

const (
	// VolatileVolumeType is the name used to identify volatile volumes
	VolatileVolumeType VolumeTypeKind = "volatile"
	// PersistentVolumeType is the name used to identify persistent volumes
	PersistentVolumeType VolumeTypeKind = "persistent"
	// PersistentVolumeType is the name used to identify persistent volumes
	SharedVolumeType VolumeTypeKind = "shared"
)

// Volume represents an element to store data. Can be volatile or persistent.
// If the volume is persistent, the KukuVolume will hold the referenced KukuVolume.
// If the volume is volatile, Size will include the volume size.
type Volume struct {
	KukuVolume   *kumoriv1.KukuVolume
	Name         string
	ResourceName string
	Size         *resource.Quantity
	Type         VolumeType
}

func (v *Volume) IsVolatile() bool {
	for _, property := range v.Type.Properties {
		if property == VolatileVolumeType {
			return true
		}
	}
	return false
}

func (v *Volume) IsPersistent() bool {
	for _, property := range v.Type.Properties {
		if property == PersistentVolumeType {
			return true
		}
	}
	return false
}

func (v *Volume) IsShared() bool {
	for _, property := range v.Type.Properties {
		if property == SharedVolumeType {
			return true
		}
	}
	return false
}

// StorageClassName represents a kubernetes volumes storage class
type StorageClassName string

const (
	// DefaultStorageClassName is the default storage class used for volumes
	// DefaultStorageClassName StorageClassName = "rook-ceph-block"
	DefaultStorageClassName StorageClassName = ""
	// Ext4StorageClassName is the storag class used for ext4 volumes
	// Ext4StorageClassName StorageClassName = "rook-ceph-block-ext4"
)

// KukuObjectKind is a kind of a Kuku object
type KukuObjectKind string

const (
	// V3DeploymentKind is the kind of a V3 Kumori Deployment
	V3DeploymentKind = "V3Deployment"
)

// RoleConfig contains the configuration for a given role, which will be exposed in
// /kumori/config.json
type RoleConfig struct {
	Channels map[string]TagsConfig `json:"channels"`
}

// TagsConfig contains the metadata for tags.
type TagsConfig map[string]*runtime.RawExtension

// SecretsInfo contains information about a given secret
type SecretsInfo struct {
	Name        string
	ShortName   string
	Data        map[string][]byte
	InDockerHub bool
	InFile      bool
	InEnv       bool
}

const (
	// SecretsInfoDefaultKey contains the key used by default when the secrets key is not set.
	// Currently, a SecretsInfo object contains a single secret in "data" key. However, maybe
	// in the future more secrets can be stored in a single secrets object.
	SecretsInfoDefaultKey string = "data"

	// OpaqueSecretKey contains the key name to be used when an opaque secret stored is stored.
	OpaqueSecretKey string = "data"

	// DockerConfigSecretKey contains the key name to be used when the secret stored is a Docker
	// configuration file used to access docker hub images.
	DockerConfigSecretKey string = ".dockerconfigjson"
)

// ClusterConfig contains the configuration of the cluster running this kucontroller
type ClusterConfig struct {
	ClusterVersion             string                              `json:"clusterVersion" yaml:"clusterVersion"`
	ClusterName                string                              `json:"clusterName" yaml:"clusterName"`
	ReferenceDomain            string                              `json:"referenceDomain" yaml:"referenceDomain"`
	IngressDomain              string                              `json:"ingressDomain" yaml:"ingressDomain"`
	AdmissionDomain            string                              `json:"admissionDomain" yaml:"admissionDomain"`
	KumoriCtlSupportedVersions []string                            `json:"kumorictlSupportedVersions" yaml:"kumorictlSupportedVersions"`
	KumoriMgrSupportedVersions []string                            `json:"kumorimgrSupportedVersions" yaml:"kumorimgrSupportedVersions"`
	VolumeTypes                *map[string]ClusterConfigVolumeType `json:"volumeTypes,omitempty" yaml:"volumeTypes,omitempty"`
}

// ClusterConfigVolumeType contains the configuration of a volume type as provided in the cluster configuration
type ClusterConfigVolumeType struct {
	StorageClass string `json:"storageClass" yaml:"storageClass"`
	Properties   string `json:"properties" yaml:"properties"`
	Shared       *bool  `json:"shared,omitempty" yaml:"shared,omitempty"`
}

type VolumeProperty string

const (
	// KukuVolumePersistentProperty appears in a volume type if creates persistent volumes
	KukuVolumePersistentProperty VolumeProperty = "persistent"
	// KukuVolumeVolatileProperty appears in a volume type if creates volatile volumes
	KukuVolumeVolatileProperty VolumeProperty = "volatile"
	// KukuVolumeSharedProperty appears in a volume type if creates shared volumes
	KukuVolumeSharedProperty VolumeProperty = "shared"
)
