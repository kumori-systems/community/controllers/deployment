/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package context

import (
	"encoding/json"
	"fmt"
	"kuku-controller/pkg/utils"
	kerrors "kuku-controller/pkg/utils/errors"
	cmutils "kuku-controller/pkg/utils/kube/configmaps"
	linkutils "kuku-controller/pkg/utils/kumori/kukulinks"
	v3dutils "kuku-controller/pkg/utils/kumori/v3deployments"
	"kuku-controller/pkg/utils/tags"

	config "kuku-controller/pkg/controllers/deployment/config"

	log "github.com/sirupsen/logrus"
	kumoriv1 "gitlab.com/kumori-systems/community/libraries/client-go/pkg/apis/kumori/v1"
	kumorilisters "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated/listers/kumori/v1"
	"gopkg.in/yaml.v2"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime/schema"
	corelisters "k8s.io/client-go/listers/core/v1"
	netlisters "k8s.io/client-go/listers/networking/v1"
)

// Context aggregates information about the V3Deployment being processed including:
// * The service topology
// * The roles configuration
// * The secrets and volumes used.
// * The links affecting this service.
// * Who owns the elements created to run the service
type Context struct {
	Deployment            *kumoriv1.V3Deployment
	ServiceInfo           *ServiceInfo
	Owners                *[]metav1.OwnerReference
	Namespace             string
	Name                  string
	ConnectorsInfo        *map[string]ConnectorTagInfo
	Loaded                bool
	ReleaseName           string
	RolesInfo             *map[string]RoleInfo
	SecretsInfo           *map[string]SecretsInfo
	RegistrationName      string
	RegistrationDomain    string
	SolutionName          string
	SolutionDomain        string
	KumoriOwner           *string
	kukuLinkTools         linkutils.KukuLinkTools
	tagsTools             *tags.Tags
	orderedConnectorNames []string
	kukuCas               *map[string]map[string]*kumoriv1.KukuCa
	kukuCerts             *map[string]map[string]*kumoriv1.KukuCert
	kukuDomains           *map[string]map[string]*kumoriv1.KukuDomain
	kukuPorts             *map[string]map[string]*kumoriv1.KukuPort
	kukuSecrets           *map[string]map[string]*kumoriv1.KukuSecret
	kukuVolumes           *map[string]map[string]*kumoriv1.KukuVolume
	marshalledResources   *map[string]map[string]string
	stringifiedResources  *map[string]map[string]string
	clusterConfig         *ClusterConfig
	Config                *config.Config
	v3DeploymentsLister   kumorilisters.V3DeploymentLister
	kukuLinksLister       kumorilisters.KukuLinkLister
	kukuSecretsLister     kumorilisters.KukuSecretLister
	kukuPortsLister       kumorilisters.KukuPortLister
	kukuDomainsLister     kumorilisters.KukuDomainLister
	kukuCertsLister       kumorilisters.KukuCertLister
	kukuCasLister         kumorilisters.KukuCaLister
	kukuVolumesLister     kumorilisters.KukuVolumeLister
	servicesLister        corelisters.ServiceLister
	networkPoliciesLister netlisters.NetworkPolicyLister
	configMapLister       corelisters.ConfigMapLister
}

// CreateContext creates and returns a new Context to process a V3Deployment and
// all relates Kuku objects.
func CreateContext(
	namespace string,
	name string,
	config *config.Config,
	v3DeploymentsLister kumorilisters.V3DeploymentLister,
	kukuLinksLister kumorilisters.KukuLinkLister,
	kukuSecretsLister kumorilisters.KukuSecretLister,
	kukuPortsLister kumorilisters.KukuPortLister,
	kukuDomainsLister kumorilisters.KukuDomainLister,
	kukuCertsLister kumorilisters.KukuCertLister,
	kukuCasLister kumorilisters.KukuCaLister,
	kukuVolumesLister kumorilisters.KukuVolumeLister,
	servicesLister corelisters.ServiceLister,
	networkPoliciesLister netlisters.NetworkPolicyLister,
	configMapLister corelisters.ConfigMapLister,
) (*Context, error) {
	// Gets the V3Deployment resource with this namespace/name
	meth := fmt.Sprintf("context.CreateContext. Namespace: %s. Name: %s", namespace, name)
	log.Debugf(meth)

	kukuLinkTools := linkutils.NewKukuLinkTools(v3DeploymentsLister, kukuLinksLister, servicesLister, networkPoliciesLister)

	newContext := Context{
		Config:                config,
		Namespace:             namespace,
		Name:                  name,
		Loaded:                false,
		kukuLinkTools:         kukuLinkTools,
		v3DeploymentsLister:   v3DeploymentsLister,
		kukuLinksLister:       kukuLinksLister,
		kukuSecretsLister:     kukuSecretsLister,
		kukuPortsLister:       kukuPortsLister,
		kukuDomainsLister:     kukuDomainsLister,
		kukuCertsLister:       kukuCertsLister,
		kukuCasLister:         kukuCasLister,
		kukuVolumesLister:     kukuVolumesLister,
		servicesLister:        servicesLister,
		networkPoliciesLister: networkPoliciesLister,
		configMapLister:       configMapLister,
	}

	// Loads the kuku objects in the kuku context
	if err := newContext.load(); err != nil {
		return nil, err
	}

	return &newContext, nil
}

// load fills the context with all required information. It will load all necessary
// objects from the cluster state and make all necessary calculations.
func (context *Context) load() error {
	var nonCriticalErr error

	/********************************/
	/* Load referenced kuku objects */
	/********************************/

	// Loads the original V3Deployment
	v3Deployment, err := context.getV3Deployment()
	if err != nil {
		return err
	}
	context.Deployment = v3Deployment
	context.ReleaseName = v3Deployment.GetName()

	// Gets the name and domain used when the V3Deployment was registered by the service provider
	registrationName, registrationDomain, err := context.getRegisteredNameAndDomain(v3Deployment)
	if kerrors.ShouldAbort(err) {
		return err
	}
	context.RegistrationName = registrationName
	context.RegistrationDomain = registrationDomain

	// Gets the solution name and domain used when the V3Deployment was registered by the service provider
	solutionName, solutionDomain, err := context.getSolutionNameAndDomain(v3Deployment)
	if kerrors.ShouldAbort(err) {
		return err
	}
	context.SolutionName = solutionName
	context.SolutionDomain = solutionDomain

	kowner, err := getKumoriOwner(v3Deployment)
	if kerrors.ShouldAbort(err) {
		return err
	}
	nonCriticalErr = kerrors.ProcessError(nonCriticalErr, err)
	if kowner != nil {
		context.KumoriOwner = kowner
	}

	// Loads the cluster configuration from the ConfigMap
	clusterConfig, err := context.getClusterConfig()
	if kerrors.ShouldAbort(err) {
		return err
	}

	// Loads the resources referenced in the V3Deployment
	kukuCas, kukuCerts, kukuDomains, kukuPorts, kukuSecrets, kukuVolumes, err := context.getKukuResources(v3Deployment)
	if kerrors.ShouldAbort(err) {
		return err
	}
	nonCriticalErr = kerrors.ProcessError(nonCriticalErr, err)
	context.kukuCas = kukuCas
	context.kukuCerts = kukuCerts
	context.kukuDomains = kukuDomains
	context.kukuPorts = kukuPorts
	context.kukuSecrets = kukuSecrets
	context.kukuVolumes = kukuVolumes
	context.clusterConfig = clusterConfig

	// Creates the tags generator
	tagsTools, err := tags.NewTags(v3Deployment, context.v3DeploymentsLister, context.kukuLinkTools)
	if kerrors.ShouldAbort(err) {
		return err
	}
	tagsTools.Generate()
	context.tagsTools = tagsTools

	/************************************/
	/* Calculate aggregated information */
	/************************************/

	// Serializes context resources
	marshalledResources, stringifiedResources, err := context.marshalResources()
	if kerrors.ShouldAbort(err) {
		return err
	}
	nonCriticalErr = kerrors.ProcessError(nonCriticalErr, err)
	context.marshalledResources = marshalledResources
	context.stringifiedResources = stringifiedResources

	// Gets the know information about the references secrets
	secretsInfo, err := context.calculateSecretsInfo(kukuSecrets)
	if kerrors.ShouldAbort(err) {
		return err
	}
	nonCriticalErr = kerrors.ProcessError(nonCriticalErr, err)
	context.SecretsInfo = secretsInfo

	// Calculates service aggregated information
	serviceInfo, err := context.calculateServiceInfo()
	if kerrors.ShouldAbort(err) {
		return err
	}
	nonCriticalErr = kerrors.ProcessError(nonCriticalErr, err)

	// Calculates the owner reference to be added to Kube objects
	owners, err := context.calculateOwnerReferences(v3Deployment)
	if kerrors.ShouldAbort(err) {
		return err
	}
	nonCriticalErr = kerrors.ProcessError(nonCriticalErr, err)

	// Calculates the connectors aggregated information
	// connectorsInfo, err := context.calculateConnectorsInfo()
	connectorsInfo, err := context.calculateTags()
	if kerrors.ShouldAbort(err) {
		return err
	}
	nonCriticalErr = kerrors.ProcessError(nonCriticalErr, err)

	// Calculates the roles aggregated information
	rolesInfo, err := context.calculateRolesInfo()
	if kerrors.ShouldAbort(err) {
		return err
	}
	nonCriticalErr = kerrors.ProcessError(nonCriticalErr, err)

	/****************/
	/* Fill context */
	/****************/

	context.Deployment = v3Deployment
	context.ServiceInfo = serviceInfo
	context.RolesInfo = rolesInfo
	context.ConnectorsInfo = connectorsInfo
	context.SecretsInfo = secretsInfo
	context.Owners = owners
	context.Loaded = true

	return nonCriticalErr
}

func (context *Context) getV3Deployment() (*kumoriv1.V3Deployment, error) {
	namespace := context.Namespace
	name := context.Name
	meth := fmt.Sprintf("context.getV3Deployment. Namespace: %s. Name: %s.", namespace, name)
	log.Debugf(meth)

	v3Deployment, err := v3dutils.GetByName(context.v3DeploymentsLister, namespace, name)
	if err != nil {
		if errors.IsNotFound(err) {
			log.Warnf("%s. V3Deployment not found", meth)
			return nil, &kerrors.KukuError{Severity: kerrors.Fatal, Parent: err}
		}
		return nil, &kerrors.KukuError{Severity: kerrors.Major, Parent: err}
	}

	if !v3Deployment.DeletionTimestamp.IsZero() {
		return nil, &kerrors.KukuError{Severity: kerrors.Skip, Parent: fmt.Errorf("V3Deployment deletion timestamp set")}
	}

	if v3Deployment.Spec.Artifact != nil &&
		v3Deployment.Spec.Artifact.Description != nil &&
		v3Deployment.Spec.Artifact.Description.Builtin {
		return nil, &kerrors.KukuError{Severity: kerrors.Skip, Parent: fmt.Errorf("V3Deployment contains a built-in service")}
	}

	v3Deployment = v3Deployment.DeepCopy()
	return v3Deployment, nil

}

// getRegisteredNameAndDomain returns the domain and name used when the deployment was registered
func (context *Context) getRegisteredNameAndDomain(v3Deployment *kumoriv1.V3Deployment) (name string, domain string, err error) {
	annotations := v3Deployment.GetAnnotations()
	name, okname := annotations[utils.KumoriNameLabel]
	domain, okdomain := annotations[utils.KumoriDomainLabel]
	if !okname && !okdomain {
		err := &kerrors.KukuError{Severity: kerrors.Fatal, Parent: fmt.Errorf("Name and domain annotations not found in V3Deployment '%s'", v3Deployment.GetName())}
		return "", "", err
	}

	if !okname {
		err := &kerrors.KukuError{Severity: kerrors.Fatal, Parent: fmt.Errorf("Name annotation not found in V3Deployment '%s'", v3Deployment.GetName())}
		return "", "", err
	}

	if !okdomain {
		err := &kerrors.KukuError{Severity: kerrors.Fatal, Parent: fmt.Errorf("Domain annotation not found in V3Deployment '%s'", v3Deployment.GetName())}
		return "", "", err
	}

	return name, domain, nil
}

func (context *Context) getSolutionNameAndDomain(v3Deployment *kumoriv1.V3Deployment) (name string, domain string, err error) {
	annotations := v3Deployment.GetAnnotations()
	name, okname := annotations[utils.KumoriSolutionNameLabel]
	domain, okdomain := annotations[utils.KumoriSolutionDomainLabel]
	if !okname && !okdomain {
		err := &kerrors.KukuError{Severity: kerrors.Fatal, Parent: fmt.Errorf("Solution name and domain annotations not found in V3Deployment '%s'", v3Deployment.GetName())}
		return "", "", err
	}

	if !okname {
		err := &kerrors.KukuError{Severity: kerrors.Fatal, Parent: fmt.Errorf("Solution name annotation not found in V3Deployment '%s'", v3Deployment.GetName())}
		return "", "", err
	}

	if !okdomain {
		err := &kerrors.KukuError{Severity: kerrors.Fatal, Parent: fmt.Errorf("Solution domain annotation not found in V3Deployment '%s'", v3Deployment.GetName())}
		return "", "", err
	}

	return name, domain, nil
}

// calculateOwnerReferences returns the Kubernetes OwnerReferences to be added to the objects generated during
// the V3Deployment processing
func (context *Context) calculateOwnerReferences(v3Deployment *kumoriv1.V3Deployment) (*[]metav1.OwnerReference, error) {
	if v3Deployment == nil {
		return nil, &kerrors.KukuError{Severity: kerrors.Major, Parent: fmt.Errorf("Deployment is null")}
	}
	group := kumoriv1.SchemeGroupVersion.Group
	version := kumoriv1.SchemeGroupVersion.Version
	// kind := kukuDeployment.Kind
	references := []metav1.OwnerReference{
		*metav1.NewControllerRef(v3Deployment, schema.GroupVersionKind{
			Group:   group,
			Version: version,
			// Kind:    kind,
			Kind: V3DeploymentKind,
		}),
	}

	return &references, nil
}

// getKumoriOwner returns the name of the deployment owner in Kumori platform
func getKumoriOwner(v3Deployment *kumoriv1.V3Deployment) (*string, error) {
	annotations := v3Deployment.Annotations
	if kowner, ok := annotations[utils.KumoriOwnerLabel]; ok {
		return &kowner, nil
	}
	return nil, nil
}

// getClusterConfig gets the ConfigMap containing the cluster configuration
func (context *Context) getClusterConfig() (*ClusterConfig, error) {
	namespace := context.Config.ClusterConfigMapNamespace
	name := context.Config.ClusterConfigMapName
	key := context.Config.ClusterConfigMapKey
	meth := fmt.Sprintf("context.getClusterConfig. Namespace: '%s'. Name: '%s'. Key: '%s'", namespace, name, key)
	log.Debugf(meth)

	configMap, err := cmutils.GetByName(context.configMapLister, namespace, name)
	if err != nil {
		if errors.IsNotFound(err) {
			return nil, &kerrors.KukuError{Severity: kerrors.Fatal, Parent: fmt.Errorf("ConfigMap '%s' not found", name)}
		}
		return nil, &kerrors.KukuError{Severity: kerrors.Major, Parent: err}
	}

	clusterConfigRaw, ok := configMap.Data[key]
	if !ok {
		return nil, &kerrors.KukuError{Severity: kerrors.Fatal, Parent: fmt.Errorf("key '%s' not found in ConfigMap %s", key, name)}
	}

	var clusterConfig ClusterConfig
	if err := yaml.Unmarshal([]byte(clusterConfigRaw), &clusterConfig); err != nil {
		return nil, &kerrors.KukuError{Severity: kerrors.Fatal, Parent: err}
	}

	generated, _ := json.MarshalIndent(clusterConfig, "", "  ")
	log.Debugf("%s. Cluster configuration:\n%s\n", meth, string(generated))

	return &clusterConfig, nil
}
