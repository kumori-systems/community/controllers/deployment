package tags

import (
	"fmt"
	"sort"

	"kuku-controller/pkg/utils/kumori"
	kusort "kuku-controller/pkg/utils/sort"

	log "github.com/sirupsen/logrus"
	kumoriv1 "gitlab.com/kumori-systems/community/libraries/client-go/pkg/apis/kumori/v1"

	v3dutils "kuku-controller/pkg/utils/kumori/v3deployments"
)

// calculatePaths calculates all reachable elements (connector/tags, links and V3Deployments)
// from a given V3Deployment. Each one of the elements is named a "breadcrumb". This function
// returns the "breadcrumb" representing the V3Deployment. Each breacrumb contains a list of:
//
// * Kids: breadcrumbs reachable downstream
// * Parents: breadcrumbs reachable upstream
func (t *Tags) generateBreadcrumbs(v3d *kumoriv1.V3Deployment) error {
	meth := fmt.Sprintf("Tags.generateBreadcrumbs. V3Deployment: %s", v3d.GetName())
	log.Infoln(meth)

	// Skip breadcrumbs if this V3Deployment not includes an artifact description
	if (v3d.Spec.Artifact == nil) ||
		(v3d.Spec.Artifact.Description == nil) {
		return nil
	}

	pendingBreadcrumbs := []Breadcrumb{}
	processedBreadcrumbs := []Breadcrumb{}

	// Add the V3Deployment service server channels (acting as client in connectors) in the inital
	// list of breadcrumbs to be processed
	if (v3d.Spec.Artifact.Description.Srv != nil) &&
		(v3d.Spec.Artifact.Description.Srv.Servers != nil) {
		serverNames := kusort.SortKeys(v3d.Spec.Artifact.Description.Srv.Servers)
		for _, channelName := range serverNames {
			channelBreadcrumb := &DeploymentChannelBreadcrumb{
				Name:         fmt.Sprintf("%s-self-%s", v3d.GetName(), channelName),
				Meta:         v3d.Spec.Meta,
				V3Deployment: v3d,
				Kids:         []Breadcrumb{},
				Channel:      channelName,
			}

			log.Debugf("%s. Breadcrumb %s/%s created", meth, channelBreadcrumb.GetType(), channelBreadcrumb.GetName())

			pendingBreadcrumbs = append(pendingBreadcrumbs, channelBreadcrumb)
			t.addDeploymentChannelBreadcrumb(channelBreadcrumb)
		}
	}

	// Add the V3Deployment connectors in the initial list of bradcrumbs to be processed
	if v3d.Spec.Artifact.Description.Connectors != nil {
		connectorNames := kusort.SortKeys(v3d.Spec.Artifact.Description.Connectors)
		for _, connectorName := range connectorNames {
			connectorData := (*v3d.Spec.Artifact.Description.Connectors)[connectorName]
			for tag, server := range connectorData.Servers {
				serverCopy := server
				tagBreadcrumb := &VSetTagBreadcrumb{
					Name:          fmt.Sprintf("%s-%s-%d", v3d.GetName(), connectorName, tag),
					Meta:          server.Meta,
					Parents:       []Breadcrumb{},
					V3Deployment:  v3d,
					Connector:     &connectorData,
					ConnectorName: connectorName,
					Tag:           &serverCopy,
				}

				log.Debugf("%s. Breadcrumb %s/%s created", meth, tagBreadcrumb.GetType(), tagBreadcrumb.GetName())

				pendingBreadcrumbs = append(pendingBreadcrumbs, tagBreadcrumb)
				t.addVSetTagBreadcrumb(tagBreadcrumb)
			}
		}
	}

	iteration := 0
	for len(pendingBreadcrumbs) > 0 {

		// If this loop reaches the maximum number of iterations, abort to avoid infinite loops
		if iteration >= MAX_BREADCRUMB_ITERATIONS {
			log.Errorf("%s. Infinite loop detected", meth)
			return fmt.Errorf("infinite loop detected")
		}

		// Gets the breadcrumb to be processed
		currentBreadcrumb := pendingBreadcrumbs[0]
		pendingBreadcrumbs = pendingBreadcrumbs[1:]
		processedBreadcrumbs = append(processedBreadcrumbs, currentBreadcrumb)

		switch currentBreadcrumb.GetType() {
		case VSetTagBreadcrumbType:
			currentTagBreadcrumb := currentBreadcrumb.(*VSetTagBreadcrumb)
			var err error
			pendingBreadcrumbs, processedBreadcrumbs, err = t.processVSetTagBreadcrumb(v3d, currentTagBreadcrumb, pendingBreadcrumbs, processedBreadcrumbs)
			if err != nil {
				log.Errorf("%s. Error processing VSetTagBreadcrumb %s: %v\n", meth, currentBreadcrumb.GetName(), err)
				return err
			}

		case LinkBreadcrumbType:
			currentLinkBreadcrumb := currentBreadcrumb.(*LinkBreadcrumb)
			var err error
			pendingBreadcrumbs, processedBreadcrumbs, err = t.processLinkBreadcrumb(v3d, currentLinkBreadcrumb, pendingBreadcrumbs, processedBreadcrumbs)
			if err != nil {
				log.Errorf("%s. Error processing LinkBreadcrumbType %s: %v\n", meth, currentBreadcrumb.GetName(), err)
				return err
			}

		case DeploymentChannelBreadcrumbType:
			currentDeploymentChannelBreadcrumb := currentBreadcrumb.(*DeploymentChannelBreadcrumb)
			var err error
			pendingBreadcrumbs, processedBreadcrumbs, err = t.processDeploymentChannelBreadcrumb(v3d, currentDeploymentChannelBreadcrumb, pendingBreadcrumbs, processedBreadcrumbs)
			if err != nil {
				log.Errorf("%s. Error processing DeploymentChannelBreadcrumb %s: %v\n", meth, currentBreadcrumb.GetName(), err)
				return err
			}

		default:
			log.Errorf("%s. Unknown breadcrumb type %s. Skipping...", meth, currentBreadcrumb.GetType())
		}

		iteration++
	}

	return nil

}

// processVSetTagBreadcrumb calculates which new breadcrumbs can be created from a VSet tag breadcrumb.
// It adds a new LinkBreadcrumb kid per link connected to this connector tag server/duplex service channel.
// It adds a DeploymentChannelBreadcrumb parent per the V3Deployment server service channel connected to this breadcrumb connector.
func (t *Tags) processVSetTagBreadcrumb(
	v3d *kumoriv1.V3Deployment,
	breadcrumb *VSetTagBreadcrumb,
	pendingBreadcrumbs []Breadcrumb,
	processedBreadcrumbs []Breadcrumb,
) (
	newPendingBreadcrumbs []Breadcrumb,
	newProcessedBreadcrumbs []Breadcrumb,
	err error,
) {
	meth := fmt.Sprintf("Tags.processVSetTagBreadcrumb. V3Deployment: %s. Breadcrumb: %s. Pending: %d. Processed: %d", v3d.GetName(), breadcrumb.GetName(), len(pendingBreadcrumbs), len(processedBreadcrumbs))
	log.Infoln(meth)

	sourceV3d := breadcrumb.V3Deployment
	sourceTag := breadcrumb.Tag

	// Add as kids links connected to server/duplex service channels
	for _, endpoint := range sourceTag.Links {
		if endpoint.Role != "self" {
			continue
		}

		currentDomain, ok := sourceV3d.Annotations[kumori.KumoriDomainLabel]
		if !ok {
			err := fmt.Errorf("Annotation %s not found in V3Deployment %s", kumori.KumoriDomainLabel, sourceV3d.GetName())
			log.Errorf("%s. Error: %s\n", meth, err)
			return pendingBreadcrumbs, processedBreadcrumbs, err
		}

		currentName, ok := sourceV3d.Annotations[kumori.KumoriNameLabel]
		if !ok {
			err := fmt.Errorf("Annotation %s not found in V3Deployment %s", kumori.KumoriNameLabel, sourceV3d.GetName())
			log.Errorf("%s. Error: %s\n", meth, err)
			return pendingBreadcrumbs, processedBreadcrumbs, err
		}

		links, err := t.GetLinksByEndpoint(currentDomain, currentName, endpoint.Channel)
		if err != nil {
			log.Errorf("%s. Error: %s\n", meth, err)
			return pendingBreadcrumbs, processedBreadcrumbs, err
		}

		for _, link := range links {

			var targetV3d *kumoriv1.V3Deployment
			var targetChannel string

			for _, linkEndpoint := range link.Spec.Endpoints {
				if (linkEndpoint.Domain == currentDomain) &&
					(linkEndpoint.Name == currentName) &&
					(linkEndpoint.Channel == endpoint.Channel) {
					continue
				}
				targetV3d, err = t.GetDeploymentByDomainAndName(linkEndpoint.Domain, linkEndpoint.Name)
				if err != nil {
					return pendingBreadcrumbs, processedBreadcrumbs, err
				}
				targetChannel = linkEndpoint.Channel
			}

			if targetV3d == nil {
				err = fmt.Errorf("Target V3Deployment not found in link %s", link.GetName())
				log.Errorf("%s. Error. %v", meth, err)
				return pendingBreadcrumbs, processedBreadcrumbs, err
			}

			copylink := link

			linkTagBreadcrumb := &LinkBreadcrumb{
				Name:             link.GetName(),
				Meta:             link.Spec.Meta,
				Parents:          []Breadcrumb{breadcrumb},
				Link:             &copylink,
				SourceDeployment: sourceV3d,
				SourceChannel:    endpoint.Channel,
				TargetDeployment: targetV3d,
				TargetChannel:    targetChannel,
			}

			if existingBreadcrumb := SearchBreadcrumb(processedBreadcrumbs, Breadcrumb(linkTagBreadcrumb)); existingBreadcrumb != nil {
				breadcrumb.AddKid(existingBreadcrumb)
				log.Debugf("%s. Breadcrumb %s/%s added as kid to breadcrumb %s/%s", meth, existingBreadcrumb.GetType(), existingBreadcrumb.GetName(), breadcrumb.GetType(), breadcrumb.GetName())
				existingBreadcrumb.AddParent(breadcrumb)
				log.Debugf("%s. Breadcrumb %s/%s added as parent to breadcrumb %s/%s", meth, breadcrumb.GetType(), breadcrumb.GetName(), existingBreadcrumb.GetType(), existingBreadcrumb.GetName())
			} else if existingBreadcrumb := SearchBreadcrumb(pendingBreadcrumbs, Breadcrumb(linkTagBreadcrumb)); existingBreadcrumb != nil {
				breadcrumb.AddKid(existingBreadcrumb)
				log.Debugf("%s. Breadcrumb %s/%s added as kid to breadcrumb %s/%s", meth, existingBreadcrumb.GetType(), existingBreadcrumb.GetName(), breadcrumb.GetType(), breadcrumb.GetName())
				existingBreadcrumb.AddParent(breadcrumb)
				log.Debugf("%s. Breadcrumb %s/%s added as parent to breadcrumb %s/%s", meth, breadcrumb.GetType(), breadcrumb.GetName(), existingBreadcrumb.GetType(), existingBreadcrumb.GetName())
			} else {
				log.Debugf("%s. Breadcrumb %s/%s created", meth, linkTagBreadcrumb.GetType(), linkTagBreadcrumb.GetName())
				pendingBreadcrumbs = append(pendingBreadcrumbs, Breadcrumb(linkTagBreadcrumb))
				log.Debugf("%s. Breadcrumb %s/%s added as kid to breadcrumb %s/%s", meth, linkTagBreadcrumb.GetType(), linkTagBreadcrumb.GetName(), breadcrumb.GetType(), breadcrumb.GetName())
				breadcrumb.AddKid(linkTagBreadcrumb)
				log.Debugf("%s. Breadcrumb %s/%s added as parent to breadcrumb %s/%s", meth, breadcrumb.GetType(), breadcrumb.GetName(), linkTagBreadcrumb.GetType(), linkTagBreadcrumb.GetName())
				t.addLinkBreadcrumb(linkTagBreadcrumb)
			}

		}

	}

	// Adds a DeploymentChannelBreadcrumb per client service channel
	for _, endpoint := range breadcrumb.Connector.Clients {

		if endpoint.Role != "self" {
			continue
		}

		deploymentChannelBreadcrumb := &DeploymentChannelBreadcrumb{
			Name:         fmt.Sprintf("%s-self-%s", breadcrumb.V3Deployment.GetName(), endpoint.Channel),
			Meta:         breadcrumb.V3Deployment.Spec.Meta,
			Parents:      []Breadcrumb{},
			V3Deployment: breadcrumb.V3Deployment,
			Channel:      endpoint.Channel,
		}

		if existingBreadcrumb := SearchBreadcrumb(processedBreadcrumbs, Breadcrumb(deploymentChannelBreadcrumb)); existingBreadcrumb != nil {
			breadcrumb.AddParent(existingBreadcrumb)
			log.Debugf("%s. Breadcrumb %s/%s added as parent to breadcrumb %s/%s", meth, existingBreadcrumb.GetType(), existingBreadcrumb.GetName(), breadcrumb.GetType(), breadcrumb.GetName())
			existingBreadcrumb.AddKid(breadcrumb)
			log.Debugf("%s. Breadcrumb %s/%s added as kid to breadcrumb %s/%s", meth, breadcrumb.GetType(), breadcrumb.GetName(), existingBreadcrumb.GetType(), existingBreadcrumb.GetName())
		} else if existingBreadcrumb := SearchBreadcrumb(pendingBreadcrumbs, Breadcrumb(deploymentChannelBreadcrumb)); existingBreadcrumb != nil {
			breadcrumb.AddParent(existingBreadcrumb)
			log.Debugf("%s. Breadcrumb %s/%s added as parent to breadcrumb %s/%s", meth, existingBreadcrumb.GetType(), existingBreadcrumb.GetName(), breadcrumb.GetType(), breadcrumb.GetName())
			existingBreadcrumb.AddKid(breadcrumb)
			log.Debugf("%s. Breadcrumb %s/%s added as kid to breadcrumb %s/%s", meth, breadcrumb.GetType(), breadcrumb.GetName(), existingBreadcrumb.GetType(), existingBreadcrumb.GetName())
		} else {
			log.Debugf("%s. Breadcrumb %s/%s created", meth, deploymentChannelBreadcrumb.GetType(), deploymentChannelBreadcrumb.GetName())
			pendingBreadcrumbs = append(pendingBreadcrumbs, Breadcrumb(deploymentChannelBreadcrumb))
			breadcrumb.AddParent(deploymentChannelBreadcrumb)
			log.Debugf("%s. Breadcrumb %s/%s added as parent to breadcrumb %s/%s", meth, deploymentChannelBreadcrumb.GetType(), deploymentChannelBreadcrumb.GetName(), breadcrumb.GetType(), breadcrumb.GetName())
			deploymentChannelBreadcrumb.AddKid(breadcrumb)
			log.Debugf("%s. Breadcrumb %s/%s added as kid to breadcrumb %s/%s", meth, breadcrumb.GetType(), breadcrumb.GetName(), deploymentChannelBreadcrumb.GetType(), deploymentChannelBreadcrumb.GetName())
			t.addDeploymentChannelBreadcrumb(deploymentChannelBreadcrumb)
		}
	}

	return pendingBreadcrumbs, processedBreadcrumbs, nil
}

// processDeploymentChannelBreadcrumb calculates which new breadcrumbs can be created from a deployment breadcrumb.
// It adds a new VSetTagBreadcrumb kid per connector/tag from this V3Deployment connected to this breadcrumb endpoint.
// It adds a LinkBreadcrumb parent per link connected to the V3Deployment endpoint.
func (t *Tags) processDeploymentChannelBreadcrumb(
	v3d *kumoriv1.V3Deployment,
	breadcrumb *DeploymentChannelBreadcrumb,
	pendingBreadcrumbs []Breadcrumb,
	processedBreadcrumbs []Breadcrumb,
) (
	newPendingBreadcrumbs []Breadcrumb,
	newProcessedBreadcrumbs []Breadcrumb,
	err error,
) {
	meth := fmt.Sprintf("Tags.processDeploymentChannelBreadcrumb. V3Deployment: %s. Channel: %s. Breadcrumb: %s. Pending: %d. Processed: %d", v3d.GetName(), breadcrumb.Channel, breadcrumb.GetName(), len(pendingBreadcrumbs), len(processedBreadcrumbs))
	log.Infoln(meth)

	// Add as parents the links connected to this V3Deployment endpoint
	domain, ok := breadcrumb.V3Deployment.Annotations[kumori.KumoriDomainLabel]
	if !ok {
		err = fmt.Errorf("Annotation %s not found in V3Deployment %s", kumori.KumoriDomainLabel, breadcrumb.V3Deployment.GetName())
		log.Errorf("%s. Error: %s\n", meth, err)
		return pendingBreadcrumbs, processedBreadcrumbs, err
	}

	name, ok := breadcrumb.V3Deployment.Annotations[kumori.KumoriNameLabel]
	if !ok {
		err = fmt.Errorf("Annotation %s not found in V3Deployment %s", kumori.KumoriNameLabel, breadcrumb.V3Deployment.GetName())
		log.Errorf("%s. Error: %s\n", meth, err)
		return pendingBreadcrumbs, processedBreadcrumbs, err
	}

	currentV3d := breadcrumb.V3Deployment
	description := currentV3d.Spec.Artifact.Description

	// Adds connector tags as kid breadcrumbs
	if description.Connectors != nil {

		connectors := *description.Connectors

		connectorNames := kusort.SortKeys(&connectors)
		for _, connectorName := range connectorNames {

			connector := connectors[connectorName]

			// If this V3Deployment has been reached from a given service channel,
			// only the connector this channel is connected to will be added to the
			// list of reachable connectors in this V3Deployment
			skip := true
			for _, client := range connector.Clients {
				if (client.Role == "self") &&
					(client.Channel == breadcrumb.Channel) {
					skip = false
					break
				}
			}
			if skip {
				continue
			}

			connectorCopy := connector
			for tag, server := range connector.Servers {
				serverCopy := server
				tagBreadcrumb := &VSetTagBreadcrumb{
					Name:          fmt.Sprintf("%s-%s-%d", currentV3d.GetName(), connectorName, tag),
					Meta:          server.Meta,
					Parents:       []Breadcrumb{breadcrumb},
					V3Deployment:  currentV3d,
					Connector:     &connectorCopy,
					ConnectorName: connectorName,
					Tag:           &serverCopy,
				}
				if existingBreadcrumb := SearchBreadcrumb(processedBreadcrumbs, Breadcrumb(tagBreadcrumb)); existingBreadcrumb != nil {
					breadcrumb.AddKid(existingBreadcrumb)
					log.Debugf("%s. Breadcrumb %s/%s added as kid to breadcrumb %s/%s", meth, existingBreadcrumb.GetType(), existingBreadcrumb.GetName(), breadcrumb.GetType(), breadcrumb.GetName())
					existingBreadcrumb.AddParent(breadcrumb)
					log.Debugf("%s. Breadcrumb %s/%s added as parent to breadcrumb %s/%s", meth, breadcrumb.GetType(), breadcrumb.GetName(), existingBreadcrumb.GetType(), existingBreadcrumb.GetName())
				} else if existingBreadcrumb := SearchBreadcrumb(pendingBreadcrumbs, Breadcrumb(tagBreadcrumb)); existingBreadcrumb != nil {
					breadcrumb.AddKid(existingBreadcrumb)
					log.Debugf("%s. Breadcrumb %s/%s added as kid to breadcrumb %s/%s", meth, existingBreadcrumb.GetType(), existingBreadcrumb.GetName(), breadcrumb.GetType(), breadcrumb.GetName())
					existingBreadcrumb.AddParent(breadcrumb)
					log.Debugf("%s. Breadcrumb %s/%s added as parent to breadcrumb %s/%s", meth, breadcrumb.GetType(), breadcrumb.GetName(), existingBreadcrumb.GetType(), existingBreadcrumb.GetName())
				} else {
					log.Debugf("%s. Breadcrumb %s/%s created", meth, tagBreadcrumb.GetType(), tagBreadcrumb.GetName())
					pendingBreadcrumbs = append(pendingBreadcrumbs, Breadcrumb(tagBreadcrumb))
					breadcrumb.AddKid(tagBreadcrumb)
					log.Debugf("%s. Breadcrumb %s/%s added as kid to breadcrumb %s/%s", meth, tagBreadcrumb.GetType(), tagBreadcrumb.GetName(), breadcrumb.GetType(), breadcrumb.GetName())
					t.addVSetTagBreadcrumb(tagBreadcrumb)
					log.Debugf("%s. Breadcrumb %s/%s added as parent to breadcrumb %s/%s", meth, breadcrumb.GetType(), breadcrumb.GetName(), tagBreadcrumb.GetType(), tagBreadcrumb.GetName())
				}
			}
		}
	}

	// Adds links targeting this V3Deployment and channel as parent breadcrumbs
	links, err := t.GetLinksByEndpoint(domain, name, breadcrumb.Channel)
	if err != nil {
		log.Errorf("%s. Error getting links for Domain: %s, Name: %s, Channel: %s. %v", meth, domain, name, breadcrumb.Channel, err)
		return pendingBreadcrumbs, processedBreadcrumbs, err
	}

	for _, link := range links {
		var sourceV3d *kumoriv1.V3Deployment
		for _, endpoint := range link.Spec.Endpoints {
			if (endpoint.Domain == domain) &&
				(endpoint.Name == name) &&
				(endpoint.Channel == breadcrumb.Channel) {
				continue
			}
			sourceV3d, err = t.GetDeploymentByDomainAndName(endpoint.Domain, endpoint.Name)
			if err != nil {
				return pendingBreadcrumbs, processedBreadcrumbs, err
			}
			if sourceV3d == nil {
				return pendingBreadcrumbs, processedBreadcrumbs, fmt.Errorf("V3Deployment %s/%s not found", endpoint.Domain, endpoint.Name)
			}

			copylink := link

			linkTagBreadcrumb := &LinkBreadcrumb{
				Name:             link.GetName(),
				Meta:             link.Spec.Meta,
				Parents:          []Breadcrumb{},
				Link:             &copylink,
				SourceDeployment: sourceV3d,
				SourceChannel:    endpoint.Channel,
				TargetDeployment: currentV3d,
				TargetChannel:    breadcrumb.Channel,
			}

			if existingBreadcrumb := SearchBreadcrumb(processedBreadcrumbs, Breadcrumb(linkTagBreadcrumb)); existingBreadcrumb != nil {
				breadcrumb.AddParent(existingBreadcrumb)
				log.Debugf("%s. Breadcrumb %s/%s added as parent to breadcrumb %s/%s", meth, existingBreadcrumb.GetType(), existingBreadcrumb.GetName(), breadcrumb.GetType(), breadcrumb.GetName())
				existingBreadcrumb.AddKid(breadcrumb)
				log.Debugf("%s. Breadcrumb %s/%s added as kid to breadcrumb %s/%s", meth, breadcrumb.GetType(), breadcrumb.GetName(), existingBreadcrumb.GetType(), existingBreadcrumb.GetName())
			} else if existingBreadcrumb := SearchBreadcrumb(pendingBreadcrumbs, Breadcrumb(linkTagBreadcrumb)); existingBreadcrumb != nil {
				breadcrumb.AddParent(existingBreadcrumb)
				log.Debugf("%s. Breadcrumb %s/%s added as parent to breadcrumb %s/%s", meth, existingBreadcrumb.GetType(), existingBreadcrumb.GetName(), breadcrumb.GetType(), breadcrumb.GetName())
				existingBreadcrumb.AddKid(breadcrumb)
				log.Debugf("%s. Breadcrumb %s/%s added as kid to breadcrumb %s/%s", meth, breadcrumb.GetType(), breadcrumb.GetName(), existingBreadcrumb.GetType(), existingBreadcrumb.GetName())
			} else {
				log.Debugf("%s. Breadcrumb %s/%s created", meth, linkTagBreadcrumb.GetType(), linkTagBreadcrumb.GetName())
				pendingBreadcrumbs = append(pendingBreadcrumbs, Breadcrumb(linkTagBreadcrumb))
				linkTagBreadcrumb.AddKid(breadcrumb)
				log.Debugf("%s. Breadcrumb %s/%s added as kid to breadcrumb %s/%s", meth, breadcrumb.GetType(), breadcrumb.GetName(), linkTagBreadcrumb.GetType(), linkTagBreadcrumb.GetName())
				breadcrumb.AddParent(linkTagBreadcrumb)
				log.Debugf("%s. Breadcrumb %s/%s added as parent to breadcrumb %s/%s", meth, linkTagBreadcrumb.GetType(), linkTagBreadcrumb.GetName(), breadcrumb.GetType(), breadcrumb.GetName())
				t.addLinkBreadcrumb(linkTagBreadcrumb)
			}
		}

	}

	return pendingBreadcrumbs, processedBreadcrumbs, nil
}

// processLinkBreadcrumb calculates which new breadcrumbs can be created from a link breadcrumb.
// It adds the link target as a DeploymentBreadcumb kid.
// It adds the link sourcce as a VSetTagBreadcrumn parent.
func (t *Tags) processLinkBreadcrumb(
	v3d *kumoriv1.V3Deployment,
	breadcrumb *LinkBreadcrumb,
	pendingBreadcrumbs []Breadcrumb,
	processedBreadcrumbs []Breadcrumb,
) (
	newPendingBreadcrumbs []Breadcrumb,
	newProcessedBreadcrumbs []Breadcrumb,
	err error,
) {
	meth := fmt.Sprintf("Tags.processLinkBreadcrumb. V3Deployment: %s. Breadcrumb: %s. Pending: %d. Processed: %d", v3d.GetName(), breadcrumb.GetName(), len(pendingBreadcrumbs), len(processedBreadcrumbs))
	log.Infoln(meth)

	// Adds target deployment as a kid
	targetDeploymentChannelBreadcrumb := &DeploymentChannelBreadcrumb{
		Name:         fmt.Sprintf("%s-self-%s", breadcrumb.TargetDeployment.GetName(), breadcrumb.TargetChannel),
		Meta:         breadcrumb.TargetDeployment.Spec.Meta,
		Parents:      []Breadcrumb{breadcrumb},
		V3Deployment: breadcrumb.TargetDeployment,
		Channel:      breadcrumb.TargetChannel,
	}

	if existingBreadcrumb := SearchBreadcrumb(processedBreadcrumbs, Breadcrumb(targetDeploymentChannelBreadcrumb)); existingBreadcrumb != nil {
		breadcrumb.AddKid(existingBreadcrumb)
		log.Debugf("%s. Breadcrumb %s/%s added as kid to breadcrumb %s/%s", meth, existingBreadcrumb.GetType(), existingBreadcrumb.GetName(), breadcrumb.GetType(), breadcrumb.GetName())
		existingBreadcrumb.AddParent(breadcrumb)
		log.Debugf("%s. Breadcrumb %s/%s added as parent to breadcrumb %s/%s", meth, breadcrumb.GetType(), breadcrumb.GetName(), existingBreadcrumb.GetType(), existingBreadcrumb.GetName())
	} else if existingBreadcrumb := SearchBreadcrumb(pendingBreadcrumbs, Breadcrumb(targetDeploymentChannelBreadcrumb)); existingBreadcrumb != nil {
		breadcrumb.AddKid(existingBreadcrumb)
		log.Debugf("%s. Breadcrumb %s/%s added as kid to breadcrumb %s/%s", meth, existingBreadcrumb.GetType(), existingBreadcrumb.GetName(), breadcrumb.GetType(), breadcrumb.GetName())
		existingBreadcrumb.AddParent(breadcrumb)
		log.Debugf("%s. Breadcrumb %s/%s added as parent to breadcrumb %s/%s", meth, breadcrumb.GetType(), breadcrumb.GetName(), existingBreadcrumb.GetType(), existingBreadcrumb.GetName())
	} else {
		log.Debugf("%s. Breadcrumb %s/%s created", meth, targetDeploymentChannelBreadcrumb.GetType(), targetDeploymentChannelBreadcrumb.GetName())
		pendingBreadcrumbs = append(pendingBreadcrumbs, Breadcrumb(targetDeploymentChannelBreadcrumb))
		breadcrumb.AddKid(targetDeploymentChannelBreadcrumb)
		log.Debugf("%s. Breadcrumb %s/%s added as kid to breadcrumb %s/%s", meth, targetDeploymentChannelBreadcrumb.GetType(), targetDeploymentChannelBreadcrumb.GetName(), breadcrumb.GetType(), breadcrumb.GetName())
		t.addDeploymentChannelBreadcrumb(targetDeploymentChannelBreadcrumb)
		log.Debugf("%s. Breadcrumb %s/%s added as parent to breadcrumb %s/%s", meth, breadcrumb.GetType(), breadcrumb.GetName(), targetDeploymentChannelBreadcrumb.GetType(), targetDeploymentChannelBreadcrumb.GetName())
	}

	// Adds all connector tags linked to the source channel/deployment as parents
	sourceV3d := breadcrumb.SourceDeployment
	if (sourceV3d.Spec.Artifact != nil) &&
		(sourceV3d.Spec.Artifact.Description != nil) &&
		(!sourceV3d.Spec.Artifact.Description.Builtin) &&
		(sourceV3d.Spec.Artifact.Description.Connectors != nil) {
		// for connectorName, connectorData := range *sourceV3d.Spec.Artifact.Description.Connectors {
		connectorNames := kusort.SortKeys(sourceV3d.Spec.Artifact.Description.Connectors)
		for _, connectorName := range connectorNames {
			connectorData := (*sourceV3d.Spec.Artifact.Description.Connectors)[connectorName]
			for tag, server := range connectorData.Servers {
				skip := true
				serverCopy := server
				for _, link := range server.Links {
					if (link.Role == "self") && (link.Channel == breadcrumb.SourceChannel) {
						skip = false
					}
				}
				if skip {
					continue
				}
				tagBreadcrumb := &VSetTagBreadcrumb{
					Name:          fmt.Sprintf("%s-%s-%d", sourceV3d.GetName(), connectorName, tag),
					Meta:          server.Meta,
					Parents:       []Breadcrumb{breadcrumb},
					V3Deployment:  sourceV3d,
					Connector:     &connectorData,
					ConnectorName: connectorName,
					Tag:           &serverCopy,
				}

				if existingBreadcrumb := SearchBreadcrumb(processedBreadcrumbs, Breadcrumb(tagBreadcrumb)); existingBreadcrumb != nil {
					breadcrumb.AddParent(existingBreadcrumb)
					log.Debugf("%s. Breadcrumb %s/%s added as parent to breadcrumb %s/%s", meth, existingBreadcrumb.GetType(), existingBreadcrumb.GetName(), breadcrumb.GetType(), breadcrumb.GetName())
					existingBreadcrumb.AddKid(breadcrumb)
					log.Debugf("%s. Breadcrumb %s/%s added as kid to breadcrumb %s/%s", meth, breadcrumb.GetType(), breadcrumb.GetName(), existingBreadcrumb.GetType(), existingBreadcrumb.GetName())
				} else if existingBreadcrumb := SearchBreadcrumb(pendingBreadcrumbs, Breadcrumb(tagBreadcrumb)); existingBreadcrumb != nil {
					breadcrumb.AddParent(existingBreadcrumb)
					log.Debugf("%s. Breadcrumb %s/%s added as parent to breadcrumb %s/%s", meth, existingBreadcrumb.GetType(), existingBreadcrumb.GetName(), breadcrumb.GetType(), breadcrumb.GetName())
					existingBreadcrumb.AddKid(breadcrumb)
					log.Debugf("%s. Breadcrumb %s/%s added as kid to breadcrumb %s/%s", meth, breadcrumb.GetType(), breadcrumb.GetName(), existingBreadcrumb.GetType(), existingBreadcrumb.GetName())
				} else {
					log.Debugf("%s. Breadcrumb %s/%s created", meth, tagBreadcrumb.GetType(), tagBreadcrumb.GetName())
					pendingBreadcrumbs = append(pendingBreadcrumbs, Breadcrumb(tagBreadcrumb))
					breadcrumb.AddParent(tagBreadcrumb)
					log.Debugf("%s. Breadcrumb %s/%s added as parent to breadcrumb %s/%s", meth, tagBreadcrumb.GetType(), tagBreadcrumb.GetName(), breadcrumb.GetType(), breadcrumb.GetName())
					tagBreadcrumb.AddKid(breadcrumb)
					log.Debugf("%s. Breadcrumb %s/%s added as kid to breadcrumb %s/%s", meth, breadcrumb.GetType(), breadcrumb.GetName(), tagBreadcrumb.GetType(), tagBreadcrumb.GetName())
					t.addVSetTagBreadcrumb(tagBreadcrumb)
				}
			}
		}
	}

	return pendingBreadcrumbs, processedBreadcrumbs, nil
}

func (t *Tags) GetLinksByEndpoint(domain, name, channel string) (
	links []kumoriv1.KukuLink,
	err error,
) {
	meth := fmt.Sprintf("Tags.GetLinksByEndpoint. Domain: %s. Name: %s. Channel: %s", domain, name, channel)
	log.Infoln(meth)

	namespace := t.RootV3D.Namespace
	v3d, err := v3dutils.GetByRegistrationNameAndDomain(t.v3DeploymentsLister, namespace, name, domain)
	if err != nil {
		return
	}

	links, err = t.linkstool.GetKukuLinks(v3d, channel)

	sort.SliceStable(links, func(i, j int) bool { return links[i].Name < links[j].Name })

	return
}
