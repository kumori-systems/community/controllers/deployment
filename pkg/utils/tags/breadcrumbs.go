package tags

import (
	kumoriv1 "gitlab.com/kumori-systems/community/libraries/client-go/pkg/apis/kumori/v1"
	"k8s.io/apimachinery/pkg/runtime"
)

type BreadcrumbType string

const (
	VSetTagBreadcrumbType           BreadcrumbType = "tag"
	LinkBreadcrumbType              BreadcrumbType = "link"
	DeploymentChannelBreadcrumbType BreadcrumbType = "deployment"
)

type Breadcrumb interface {
	AddKid(kid Breadcrumb)
	AddParent(parent Breadcrumb)
	GetKids() []Breadcrumb
	GetName() string
	GetMeta() *runtime.RawExtension
	GetParents() []Breadcrumb
	GetType() BreadcrumbType
}

type VSetTagBreadcrumb struct {
	Connector     *kumoriv1.Connector
	ConnectorName string
	Kids          []Breadcrumb
	Name          string
	Meta          *runtime.RawExtension
	Parents       []Breadcrumb
	Tag           *kumoriv1.ConnectorTag
	V3Deployment  *kumoriv1.V3Deployment
}

func (b *VSetTagBreadcrumb) GetName() string {
	return b.Name
}

func (b *VSetTagBreadcrumb) GetType() BreadcrumbType {
	return VSetTagBreadcrumbType
}

func (b *VSetTagBreadcrumb) GetMeta() *runtime.RawExtension {
	return b.Meta
}

func (b *VSetTagBreadcrumb) GetParents() []Breadcrumb {
	return b.Parents
}

func (b *VSetTagBreadcrumb) AddParent(parent Breadcrumb) {
	if breadcrumb := SearchBreadcrumb(b.Parents, parent); breadcrumb == nil {
		b.Parents = append(b.Parents, parent)
	}
}

func (b *VSetTagBreadcrumb) GetKids() []Breadcrumb {
	return b.Kids
}

func (b *VSetTagBreadcrumb) AddKid(kid Breadcrumb) {
	if breadcrumb := SearchBreadcrumb(b.Kids, kid); breadcrumb == nil {
		b.Kids = append(b.Kids, kid)
	}
}

type LinkBreadcrumb struct {
	Kids             []Breadcrumb
	Link             *kumoriv1.KukuLink
	Name             string
	Meta             *runtime.RawExtension
	Parents          []Breadcrumb
	SourceDeployment *kumoriv1.V3Deployment
	SourceChannel    string
	TargetDeployment *kumoriv1.V3Deployment
	TargetChannel    string
}

func (b *LinkBreadcrumb) GetName() string {
	return b.Name
}

func (b *LinkBreadcrumb) GetType() BreadcrumbType {
	return LinkBreadcrumbType
}

func (b *LinkBreadcrumb) GetMeta() *runtime.RawExtension {
	return b.Meta
}

func (b *LinkBreadcrumb) GetParents() []Breadcrumb {
	return b.Parents
}

func (b *LinkBreadcrumb) AddParent(parent Breadcrumb) {
	if breadcrumb := SearchBreadcrumb(b.Parents, parent); breadcrumb == nil {
		b.Parents = append(b.Parents, parent)
	}
}

func (b *LinkBreadcrumb) GetKids() []Breadcrumb {
	return b.Kids
}

func (b *LinkBreadcrumb) AddKid(kid Breadcrumb) {
	if breadcrumb := SearchBreadcrumb(b.Kids, kid); breadcrumb == nil {
		b.Kids = append(b.Kids, kid)
	}
}

type ChannelData struct {
	Name string
	Kind string
}

type DeploymentChannelBreadcrumb struct {
	Channel      string
	Kids         []Breadcrumb
	Name         string
	Meta         *runtime.RawExtension
	Parents      []Breadcrumb
	V3Deployment *kumoriv1.V3Deployment
}

func (b *DeploymentChannelBreadcrumb) GetName() string {
	return b.Name
}

func (b *DeploymentChannelBreadcrumb) GetType() BreadcrumbType {
	return DeploymentChannelBreadcrumbType
}

func (b *DeploymentChannelBreadcrumb) GetMeta() *runtime.RawExtension {
	return b.Meta
}

func (b *DeploymentChannelBreadcrumb) GetParents() []Breadcrumb {
	return b.Parents
}

func (b *DeploymentChannelBreadcrumb) AddParent(parent Breadcrumb) {
	if breadcrumb := SearchBreadcrumb(b.Parents, parent); breadcrumb == nil {
		b.Parents = append(b.Parents, parent)
	}
}

func (b *DeploymentChannelBreadcrumb) GetKids() []Breadcrumb {
	return b.Kids
}

func (b *DeploymentChannelBreadcrumb) AddKid(kid Breadcrumb) {
	if breadcrumb := SearchBreadcrumb(b.Kids, kid); breadcrumb == nil {
		b.Kids = append(b.Kids, kid)
	}
}
