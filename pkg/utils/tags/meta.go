package tags

import (
	"encoding/json"
	"fmt"
	"strconv"

	v3dutils "kuku-controller/pkg/utils/kumori/v3deployments"
	"kuku-controller/pkg/utils/sort"

	"github.com/Jeffail/gabs/v2"
	log "github.com/sirupsen/logrus"
	kumoriv1 "gitlab.com/kumori-systems/community/libraries/client-go/pkg/apis/kumori/v1"
	"k8s.io/apimachinery/pkg/runtime"
)

func (t *Tags) GetRoleConfig(role string) (data []byte, err error) {
	meth := fmt.Sprintf("Tags.GetRoleConfig. V3Deployment: %s. Role: %s", t.RootV3D.Name, role)
	log.Infoln(meth)

	v3d, err := t.GetRootV3D()
	if err != nil {
		return nil, err
	}

	if (v3d.Spec.Artifact == nil) ||
		(v3d.Spec.Artifact.Description == nil) ||
		(v3d.Spec.Artifact.Description.Roles == nil) {
		return nil, fmt.Errorf("role not found")
	}

	roles := *v3d.Spec.Artifact.Description.Roles

	if _, ok := roles[role]; !ok {
		return nil, fmt.Errorf("role not found")
	}

	connectorsPerChannel, err := t.getRoleConnectorsPerChannel(role)
	if err != nil {
		return
	}

	connectorBreadcrumbs := make(map[string][]*VSetTagBreadcrumb, len(t.VSetTagBreadcrumbs))

	for _, tagBreadcrumb := range t.VSetTagBreadcrumbs {

		if tagBreadcrumb.V3Deployment.GetName() != t.RootV3D.Name {
			continue
		}

		if _, ok := connectorBreadcrumbs[tagBreadcrumb.ConnectorName]; !ok {
			connectorBreadcrumbs[tagBreadcrumb.ConnectorName] = []*VSetTagBreadcrumb{tagBreadcrumb}
		} else {
			connectorBreadcrumbs[tagBreadcrumb.ConnectorName] = append(connectorBreadcrumbs[tagBreadcrumb.ConnectorName], tagBreadcrumb)
		}

	}

	channelNames := sort.SortKeys(&connectorsPerChannel)

	branchesPerChannel := map[string]map[string][]*runtime.RawExtension{}

	for _, channelName := range channelNames {
		branchesPerChannel[channelName] = map[string][]*runtime.RawExtension{}
		for _, connectorName := range connectorsPerChannel[channelName] {
			if connectorBreadcrumbs, ok := connectorBreadcrumbs[connectorName]; ok {
				for _, breadcrumb := range connectorBreadcrumbs {
					branches := t.GetTags(breadcrumb)
					log.Debugf("%s. %d branches generated for breadcrumb %s/%s\n", meth, len(branches), breadcrumb.GetType(), breadcrumb.GetName())
					for numbranch, branch := range branches {
						log.Debugf("%s. %d breadcrumbs in branch %v of breadcrumb %s/%s\n", meth, len(branch), numbranch, breadcrumb.GetType(), breadcrumb.GetName())

						metas := []*runtime.RawExtension{}
						for i, breadcrumb := range branch {
							meta := breadcrumb.GetMeta()
							if meta == nil {
								meta = &runtime.RawExtension{
									Raw: []byte("{}"),
								}

							}

							switch breadcrumb.GetType() {
							case VSetTagBreadcrumbType:
								var rawslice []*runtime.RawExtension
								err := json.Unmarshal(meta.Raw, &rawslice)
								if err != nil {
									log.Errorf("Error printing meta: %v\n", err)
									continue
								}
								metas = append(metas, rawslice...)
							case LinkBreadcrumbType:
								auto := "{}"
								if len(branch) > (i + 2) {
									next := branch[i+1]
									if next.GetType() == DeploymentChannelBreadcrumbType {
										nextV3dChannel := next.(*DeploymentChannelBreadcrumb)

										ref, err := getDeploymentRef(nextV3dChannel.V3Deployment)
										if err != nil {
											return nil, err
										}

										depName, err := getDeploymentRegistrationName(nextV3dChannel.V3Deployment)
										if err != nil {
											return nil, err
										}

										auto = fmt.Sprintf(`{
											"servRef": %s,
											"deploymentName": "%s"
										}`, ref, depName)
										nextMeta := next.GetMeta()
										if nextMeta != nil {
											meta, err = joinMetas(meta, next.GetMeta())
											if err != nil {
												return nil, err
											}
										}
									}
								}

								user := string(meta.Raw)
								metaWithUser := &runtime.RawExtension{
									Raw: []byte(fmt.Sprintf(`{ 
										"auto": %s,
										"user": %s
									}`, auto, user)),
								}
								metas = append(metas, metaWithUser)
								// case DeploymentChannelBreadcrumbType:
								// 	metas = append(metas, meta)
								// default:
								// 	metas = append(metas, meta)
							}
						}
						nexttag := strconv.Itoa(len(branchesPerChannel[channelName]))
						branchesPerChannel[channelName][nexttag] = metas
					}
				}
			}
		}
	}

	configObject := map[string]map[string]map[string][]*runtime.RawExtension{
		"channels": branchesPerChannel,
	}
	data, err = json.MarshalIndent(&configObject, "", "  ")
	return
}

func (t *Tags) getRoleConnectorsPerChannel(role string) (connectors map[string][]string, err error) {
	meth := fmt.Sprintf("Tags.getRoleConnectorsPerChannel. Role: %s", role)
	log.Infoln(meth)

	v3d, err := t.GetRootV3D()
	if err != nil {
		return nil, err
	}

	if v3d.Spec.Artifact.Description.Connectors == nil {
		return nil, fmt.Errorf("connectors not found")
	}

	specConnectors := v3d.Spec.Artifact.Description.Connectors

	connectors = map[string][]string{}
	connectorNames := sort.SortKeys(specConnectors)
	for _, connectorName := range connectorNames {
		connectorData := (*specConnectors)[connectorName]

		// Load balancer connector. Look for client array
		if connectorData.Kind == "lb" {
			if len(connectorData.Clients) <= 0 {
				continue
			}

			for _, client := range connectorData.Clients {
				if client.Role == role {
					connectors[client.Channel] = appendIfExists(connectors[client.Channel], connectorName)
					break
				}
			}
			continue
		}

		// Full connector.
		if connectorData.Kind == "full" {
			// Full connector without clients. Look for servers array
			if len(connectorData.Clients) <= 0 {
				if len(connectorData.Servers) <= 0 {
					continue
				}

				for _, server := range connectorData.Servers {
					found := false
					for _, tag := range server.Links {
						if tag.Role == role {
							connectors[tag.Channel] = appendIfExists(connectors[tag.Channel], connectorName)
							found = true
							break
						}
					}
					if found {
						break
					}
				}
			} else {
				for _, client := range connectorData.Clients {
					if client.Role == role {
						connectors[client.Channel] = appendIfExists(connectors[client.Channel], connectorName)
						break
					}
				}
			}
			continue
		}
	}

	return

}

func (t *Tags) generateBranches(breadcrumb Breadcrumb) (branches [][]Breadcrumb) {
	meth := fmt.Sprintf("Tags.generateBranches. Breadcrumb: %s", breadcrumb.GetName())
	log.Infoln(meth)

	branches = make([][]Breadcrumb, 0, len(breadcrumb.GetKids()))
	pending := [][]Breadcrumb{{breadcrumb}}

	for len(pending) > 0 {

		current := pending[0]
		pending = pending[1:]

		last := current[len(current)-1]
		if len(last.GetKids()) == 0 {
			branches = append(branches, current)
			continue
		}

		for _, kid := range last.GetKids() {
			if SearchBreadcrumb(current, kid) == nil {
				cloned := make([]Breadcrumb, len(current), len(current)+1)
				copy(cloned, current)
				cloned = append(cloned, kid)
				pending = append(pending, cloned)
			}
		}
	}

	return
}

func joinMetas(metas ...*runtime.RawExtension) (*runtime.RawExtension, error) {
	// &runtime.RawExtension{Raw: []byte("{}")}
	result := gabs.New()
	for _, meta := range metas {
		newData, err := gabs.ParseJSON(meta.Raw)
		if err != nil {
			return nil, err
		}

		dataMap := newData.ChildrenMap()
		names := sort.SortKeys(&dataMap)
		for _, name := range names {
			elem := dataMap[name]
			if !result.Exists(name) {
				result.Set(elem, name)
			}
		}
	}

	resultData := &runtime.RawExtension{Raw: []byte(result.String())}
	return resultData, nil
}

func getDeploymentRef(v3d *kumoriv1.V3Deployment) (string, error) {

	if v3d.Spec.Artifact == nil {
		return "", fmt.Errorf("Artifact not found in V3Deployment %s", v3d.GetName())
	}

	domain := v3d.Spec.Artifact.Ref.Domain
	kind := v3d.Spec.Artifact.Ref.Kind
	local := true
	name := v3d.Spec.Artifact.Ref.Name
	module := v3d.Spec.Artifact.Ref.Module
	versionMajor := v3d.Spec.Artifact.Ref.Version[0]
	versionMinor := v3d.Spec.Artifact.Ref.Version[1]
	versionPatch := v3d.Spec.Artifact.Ref.Version[2]

	ref := fmt.Sprintf(`{
		"domain": "%s",
		"kind": "%s",
		"local": %t,
		"name": "%s",
		"module": "%s"
	`, domain, kind, local, name, module)

	if v3d.Spec.Artifact.Ref.Prerelease != nil {
		prerelease := *v3d.Spec.Artifact.Artifact.Ref.Prerelease
		ref = fmt.Sprintf(`%s,
		"prerelease": "%s"
	`, ref, prerelease)
	}

	ref = fmt.Sprintf(`%s,
		"version": [
			%d, %d, %d
		]
	}`, ref, versionMajor, versionMinor, versionPatch)

	return ref, nil

}

func getDeploymentRegistrationName(v3d *kumoriv1.V3Deployment) (string, error) {

	name, err := v3dutils.GetRegistrationName(v3d)
	if err != nil {
		return "", err
	}

	domain, err := v3dutils.GetOwnerDomain(v3d)
	if err != nil {
		return "", err
	}

	return fmt.Sprintf("%s/%s", domain, name), nil

	// GetRegistrationName
}
