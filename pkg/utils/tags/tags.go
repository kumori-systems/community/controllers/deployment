package tags

import (
	"fmt"

	linkutils "kuku-controller/pkg/utils/kumori/kukulinks"
	v3dutils "kuku-controller/pkg/utils/kumori/v3deployments"

	log "github.com/sirupsen/logrus"
	kumoriv1 "gitlab.com/kumori-systems/community/libraries/client-go/pkg/apis/kumori/v1"
	kumorilisters "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated/listers/kumori/v1"
	"k8s.io/apimachinery/pkg/runtime"
)

type Tags struct {
	Meta                         *runtime.RawExtension
	RootV3D                      *kumoriv1.V3Deployment
	v3DeploymentsLister          kumorilisters.V3DeploymentLister
	kukuLinksLister              kumorilisters.KukuLinkLister
	V3ds                         map[string]*kumoriv1.V3Deployment
	linkstool                    *linkutils.KukuLinkTools
	DeploymentChannelBreadcrumbs []*DeploymentChannelBreadcrumb
	VSetTagBreadcrumbs           []*VSetTagBreadcrumb
	LinkBreadcrumbs              []*LinkBreadcrumb
	Tags                         map[string][][]Breadcrumb
}

func NewTags(
	rootV3D *kumoriv1.V3Deployment,
	v3DeploymentsLister kumorilisters.V3DeploymentLister,
	linkstool linkutils.KukuLinkTools,
) (tags *Tags, err error) {
	meth := "Tags.NewTags"
	log.Infoln(meth)

	tags = &Tags{
		RootV3D:             rootV3D,
		v3DeploymentsLister: v3DeploymentsLister,
		linkstool:           &linkstool,
	}

	return
}

func (t *Tags) Generate() error {
	meth := "Tags.Generate"
	log.Infoln(meth)

	if err := t.check(); err != nil {
		return err
	}

	v3d, err := t.GetRootV3D()
	if err != nil {
		return err
	}

	t.Meta = v3d.Spec.Meta

	// Calculates this deployment breadcrumb paths
	err = t.generateBreadcrumbs(v3d)
	if err != nil {
		return err
	}

	// Calculates this deployment connector/tags
	err = t.generateTags()
	if err != nil {
		return err
	}

	return nil
}

func (t *Tags) GetRootV3D() (*kumoriv1.V3Deployment, error) {
	return t.RootV3D, nil
}

func (t *Tags) GetDeploymentByDomainAndName(domain string, name string) (*kumoriv1.V3Deployment, error) {

	v3d, err := v3dutils.GetByRegistrationNameAndDomain(t.v3DeploymentsLister, t.RootV3D.Namespace, name, domain)
	if err != nil {
		return nil, err
	}

	return v3d, nil
}

func (t *Tags) GetTags(breadcrumb *VSetTagBreadcrumb) [][]Breadcrumb {
	meth := fmt.Sprintf("Tags.GetTags. Root V3Deployment: %s. Breadcrumb: %s/%s", t.RootV3D.Name, breadcrumb.GetType(), breadcrumb.GetName())
	log.Infoln(meth)

	if breadcrumb.V3Deployment.Name != t.RootV3D.Name {
		return [][]Breadcrumb{}
	}
	return t.Tags[breadcrumb.GetName()]
}

func (t *Tags) generateTags() (err error) {
	meth := "Tags.generateTags"
	log.Infof(meth)

	t.Tags = make(map[string][][]Breadcrumb, len(t.VSetTagBreadcrumbs))

	for _, tagBreadcrumn := range t.VSetTagBreadcrumbs {

		if tagBreadcrumn.V3Deployment.Name != t.RootV3D.Name {
			continue
		}

		branches := t.generateBranches(tagBreadcrumn)
		t.Tags[tagBreadcrumn.Name] = branches
		log.Debugf("%s. %d tags generated for breadcrumb %s/%s\n", meth, len(branches), tagBreadcrumn.GetType(), tagBreadcrumn.GetName())
		for i, branch := range branches {
			log.Debugf("%s. %d breadcrumbs in branch %d from breadcrumb %s/%s\n", meth, len(branch), i, tagBreadcrumn.GetType(), tagBreadcrumn.GetName())
		}
	}

	return nil
}

func (t *Tags) check() error {
	meth := "Tags.check"
	log.Infoln(meth)

	v3d, err := t.GetRootV3D()
	if err != nil {
		return err
	}

	err = t.checkV3Deployment(v3d)
	if err != nil {
		return nil
	}

	return nil
}

func (t *Tags) checkV3Deployment(v3d *kumoriv1.V3Deployment) error {
	meth := fmt.Sprintf("Tags.checkV3Deployment. V3Deployment: %s", v3d.GetName())
	log.Infoln(meth)

	if v3d.Spec.Artifact == nil {
		return fmt.Errorf("V3Deployment %s artifact section missing", t.RootV3D.Name)
	}

	if v3d.Spec.Artifact.Description == nil {
		return fmt.Errorf("V3Deployment %s description section missing", t.RootV3D.Name)
	}

	return nil
}

func (t *Tags) addDeploymentChannelBreadcrumb(newBreadcrumb *DeploymentChannelBreadcrumb) {
	meth := fmt.Sprintf("Tags.addDeploymentChannelBreadcrumb. V3Deployment: %s. Breadcrumb: %s", t.RootV3D.Name, newBreadcrumb.GetName())
	log.Infoln(meth)

	if t.DeploymentChannelBreadcrumbs == nil {
		t.DeploymentChannelBreadcrumbs = []*DeploymentChannelBreadcrumb{}
	}

	for _, breadcrumb := range t.DeploymentChannelBreadcrumbs {
		if breadcrumb.GetName() == newBreadcrumb.GetName() {
			return
		}
	}

	t.DeploymentChannelBreadcrumbs = append(t.DeploymentChannelBreadcrumbs, newBreadcrumb)
}

func (t *Tags) addVSetTagBreadcrumb(newBreadcrumb *VSetTagBreadcrumb) {
	meth := fmt.Sprintf("Tags.addVSetTagBreadcrumb. V3Deployment: %s. Breadcrumb: %s", t.RootV3D.Name, newBreadcrumb.GetName())
	log.Infoln(meth)

	if t.VSetTagBreadcrumbs == nil {
		t.VSetTagBreadcrumbs = []*VSetTagBreadcrumb{}
	}

	for _, breadcrumb := range t.VSetTagBreadcrumbs {
		if breadcrumb.GetName() == newBreadcrumb.GetName() {
			return
		}
	}

	t.VSetTagBreadcrumbs = append(t.VSetTagBreadcrumbs, newBreadcrumb)
}

func (t *Tags) addLinkBreadcrumb(newBreadcrumb *LinkBreadcrumb) {
	meth := fmt.Sprintf("Tags.addLinkBreadcrumb. V3Deployment: %s. Breadcrumb: %s", t.RootV3D.Name, newBreadcrumb.GetName())
	log.Infoln(meth)

	if t.VSetTagBreadcrumbs == nil {
		t.LinkBreadcrumbs = []*LinkBreadcrumb{}
	}

	for _, breadcrumb := range t.VSetTagBreadcrumbs {
		if breadcrumb.GetName() == newBreadcrumb.GetName() {
			return
		}
	}

	t.LinkBreadcrumbs = append(t.LinkBreadcrumbs, newBreadcrumb)
}
