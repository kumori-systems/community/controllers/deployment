package tags

type ChannelType string

const (
	ClientChannel ChannelType = "client"
	ServerChannel ChannelType = "server"
	DuplexChannel ChannelType = "duplex"
)
