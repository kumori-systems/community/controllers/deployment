package tags

func SearchBreadcrumb(list []Breadcrumb, newBreadcrumb Breadcrumb) Breadcrumb {
	if len(list) <= 0 {
		return nil
	}

	for _, breadcrumb := range list {
		if breadcrumb.GetType() != newBreadcrumb.GetType() {
			continue
		}

		if breadcrumb.GetName() != newBreadcrumb.GetName() {
			continue
		}

		return breadcrumb
	}

	return nil
}

func appendIfExists(source []string, new string) []string {
	if source == nil {
		return []string{new}
	}
	for _, name := range source {
		if name == new {
			return source
		}
	}
	source = append(source, new)
	return source
}
