/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package utils

const (
	KumoriAutoscaledLabel          string = "kumori/autoscaled"
	KumoriDomainLabel              string = "kumori/domain"
	KumoriNameLabel                string = "kumori/name"
	KumoriSolutionDomainLabel      string = "kumori/solution.domain"
	KumoriSolutionNameLabel        string = "kumori/solution.name"
	KumoriControllerLabel          string = "kumori/controller"
	KumoriRoleLabel                string = "kumori/role"
	KumoriDeploymentIdLabel        string = "kumori/deployment.id"
	KumoriDeploymentNameLabel      string = "kumori/deployment.name"
	KumoriDeploymentDomainLabel    string = "kumori/deployment.domain"
	KumoriOwnerLabel               string = "kumori/owner"
	KumoriServiceNameLabel         string = "kumori/service.name"
	KumoriServiceDomainLabel       string = "kumori/service.domain"
	KumoriServiceVersionLabel      string = "kumori/service.version"
	KumoriComponentNameLabel       string = "kumori/component.name"
	KumoriComponentDomainLabel     string = "kumori/component.domain"
	KumoriComponentVersionLabel    string = "kumori/component.version"
	KumoriConnectorLabel           string = "kumori/connector"
	KumoriConnectorDataLabel       string = "kumori/connector.data"
	KumoriToIdLabel                string = "kumori/to.id"
	KumoriToRoleLabel              string = "kumori/to.role"
	KumoriToChannelLabel           string = "kumori/to.channel"
	KumoriFromIdLabel              string = "kumori/from.id"
	KumoriFromRoleLabel            string = "kumori/from.role"
	KumoriFromChannelLabel         string = "kumori/from.channel"
	KumoriFromExternalServiceLabel string = "kumori/from.external"
	KumoriKukuVolumeLabel          string = "kumori/kukuvolume"
	KumoriTagLabel                 string = "kumori/tag"
	KumoriTranslations             string = "kumori/translations"
	KumoriWorkerNodeValue          string = "worker"
)

const (
	// RevisionHistoryAnnotationKey is the key used to store a given revision history. Each item in the
	// history represents the moment a given revision was set
	KumoriRevisionHistoryAnnotation = "kumori/history"
	// CommentAnnotationKey is the key used to store the update comment in the revision
	KumoriCommentAnnotation = "kumori/comment"
)
