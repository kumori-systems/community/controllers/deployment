/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package json

import (
	"encoding/json"
	"fmt"

	kumoriv1 "gitlab.com/kumori-systems/community/libraries/client-go/pkg/apis/kumori/v1"
)

func MarshalDomain(kukudomain *kumoriv1.KukuDomain) ([]byte, error) {
	return []byte(kukudomain.Spec.Domain), nil
}

func MarshalCA(kukuca *kumoriv1.KukuCa) ([]byte, error) {
	return kukuca.Spec.Ca, nil
}

func MarshalCert(kukucert *kumoriv1.KukuCert) ([]byte, error) {
	data, err := json.Marshal(kukucert.Data)
	if err != nil {
		return nil, err
	}
	return data, nil
}

func MarshalPort(kukuport *kumoriv1.KukuPort) ([]byte, error) {
	data := []byte(fmt.Sprintf("%d", kukuport.Spec.ExternalPort))
	return data, nil
}

func MarshalSecret(kukusecret *kumoriv1.KukuSecret) ([]byte, error) {
	return kukusecret.Data, nil
}

func Stringify(source []byte) []byte {
	data, _ := json.Marshal(string(source))
	data = data[1 : len(data)-1]
	return data
}
