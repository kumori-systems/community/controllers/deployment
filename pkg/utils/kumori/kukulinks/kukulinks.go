/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package kukulinks

import (
	"fmt"

	"kuku-controller/pkg/utils"
	kerrors "kuku-controller/pkg/utils/errors"

	hashutil "kuku-controller/pkg/utils/hash"
	v3dutils "kuku-controller/pkg/utils/kumori/v3deployments"

	kumoriv1 "gitlab.com/kumori-systems/community/libraries/client-go/pkg/apis/kumori/v1"
	kumorilisters "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated/listers/kumori/v1"
	labels "k8s.io/apimachinery/pkg/labels"
	corelisters "k8s.io/client-go/listers/core/v1"
	netlisters "k8s.io/client-go/listers/networking/v1"
)

// KukuLinkTools includes functions related to kukulinks
type KukuLinkTools struct {
	v3DeploymentsLister   kumorilisters.V3DeploymentLister
	kukuLinksLister       kumorilisters.KukuLinkLister
	servicesLister        corelisters.ServiceLister
	networkPoliciesLister netlisters.NetworkPolicyLister
}

// LinkedV3Deployment contains information about a linked V3Deployment
type LinkedV3Deployment struct {
	Channel      string
	V3Deployment *kumoriv1.V3Deployment
	Link         *kumoriv1.KukuLink
}

// linkEndpoint is internally used to store information about an endpoint link
type linkDeploymentEndpoint struct {
	Name       string
	Domain     string
	Channel    string
	Deployment *kumoriv1.V3Deployment
}

const (
	// DeploymentKukuLinkEndpointType is the type of a KukuLink endpoint referencing a V3Deployment
	DeploymentKukuLinkEndpointType string = "deployment"
	SolutionKukuLinkEndpointType   string = "solution"
)

// NewKukuLinkTools returns a new KukuLinkTools
func NewKukuLinkTools(
	v3DeploymentsLister kumorilisters.V3DeploymentLister,
	kukuLinksLister kumorilisters.KukuLinkLister,
	servicesLister corelisters.ServiceLister,
	networkPoliciesLister netlisters.NetworkPolicyLister,
) (c KukuLinkTools) {
	c = KukuLinkTools{
		v3DeploymentsLister:   v3DeploymentsLister,
		kukuLinksLister:       kukuLinksLister,
		servicesLister:        servicesLister,
		networkPoliciesLister: networkPoliciesLister,
	}
	return
}

// GetLinkDeployments returns the kukuDeployment related to
// the kukulink.
// Will return:
// - 2 kukudeployments, if it is a regular link
func (k *KukuLinkTools) GetLinkDeployments(kukuLink *kumoriv1.KukuLink) (
	v3Deployment1 *kumoriv1.V3Deployment,
	channel1 string,
	v3Deployment2 *kumoriv1.V3Deployment,
	channel2 string,
	err error,
) {
	v3Deployment1 = nil
	v3Deployment2 = nil
	err = nil

	endpoint1 := kukuLink.Spec.Endpoints[0]
	endpoint2 := kukuLink.Spec.Endpoints[1]

	channel1 = endpoint1.Channel
	v3Deployment1, err = k.getLinkedDeployment(
		kukuLink.Name,
		kukuLink.Namespace,
		endpoint1.Kind,
		endpoint1.Domain,
		endpoint1.Name,
	)
	if err != nil {
		return
	}

	channel2 = endpoint2.Channel
	v3Deployment2, err = k.getLinkedDeployment(
		kukuLink.Name,
		kukuLink.Namespace,
		endpoint2.Kind,
		endpoint2.Domain,
		endpoint2.Name,
	)
	if err != nil {
		return
	}

	return
}

// GetLinkedDeployments returns the KukuLinks linking a given V3Deployment
func (k *KukuLinkTools) GetLinkedDeployments(
	namespace string,
	deploymentName string,
	channelName string,
) (
	[]LinkedV3Deployment,
	error,
) {

	// Checks parameters
	if deploymentName == "" || channelName == "" {
		return nil, kerrors.NewKukuError(kerrors.Major, fmt.Errorf("Deployment or channel name is null"))
	}

	// Gets the deployment
	deployment, err := k.v3DeploymentsLister.V3Deployments(namespace).Get(deploymentName)
	if err != nil {
		return nil, kerrors.NewKukuError(kerrors.Major, err)
	}
	registrationName, ok := deployment.Annotations[utils.KumoriNameLabel]
	if !ok {
		return nil, kerrors.NewKukuError(kerrors.Major, fmt.Errorf("V3Deployment %s has not a name label", deploymentName))
	}
	registrationDomain, ok := deployment.Annotations[utils.KumoriDomainLabel]
	if !ok {
		return nil, kerrors.NewKukuError(kerrors.Major, fmt.Errorf("V3Deployment %s has not a domain label", deploymentName))
	}

	// Calculates the labels selector to find the links
	endpoints := make([]LinkedV3Deployment, 0, 1)
	matchLabels1 := map[string]string{
		"deployment1": deploymentName,
		"channel1":    hashutil.Hash(channelName),
	}
	matchLabels2 := map[string]string{
		"deployment2": deploymentName,
		"channel2":    hashutil.Hash(channelName),
	}

	var kcerr *kerrors.KukuError
	for _, matchLabels := range []map[string]string{matchLabels1, matchLabels2} {
		options := labels.SelectorFromSet(matchLabels)
		if links, err := k.kukuLinksLister.KukuLinks(namespace).List(options); err == nil {
			for _, link := range links {
				link = link.DeepCopy()
				for _, endpoint := range link.Spec.Endpoints {
					if (endpoint.Kind != DeploymentKukuLinkEndpointType) &&
						(endpoint.Kind != SolutionKukuLinkEndpointType) {
						continue
					}

					if (endpoint.Domain == registrationDomain) && (endpoint.Name == registrationName) {
						continue
					}

					linkedDeployment, err := k.getLinkedDeployment(link.Name, namespace, endpoint.Kind, endpoint.Domain, endpoint.Name)
					if err != nil {
						message := fmt.Sprintf("Error retrieving linked V3Deployment %s/%s: %+v", endpoint.Domain, endpoint.Name, err)
						kcerr = kerrors.NewKukuError(kerrors.Minor, fmt.Errorf(message))
						continue
					}
					if linkedDeployment == nil {
						message := fmt.Sprintf("Error retrieving linked V3Deployment %s/%s: not found", endpoint.Domain, endpoint.Name)
						kcerr = kerrors.NewKukuError(kerrors.Minor, fmt.Errorf(message))
						continue
					}

					endpoints = append(endpoints, LinkedV3Deployment{
						V3Deployment: linkedDeployment,
						Channel:      endpoint.Channel,
						Link:         link,
					})

				}
			}
		} else {
			message := fmt.Sprintf("Error retrieving links: %v", err)
			kcerr = kerrors.NewKukuError(kerrors.Minor, fmt.Errorf(message))
		}
	}

	// To avoid https://golang.org/doc/faq#nil_error
	if kcerr == nil {
		return endpoints, nil
	}
	return endpoints, kcerr
}

// getLinkDeployment returns the kukuHttpInbound or kukuDeployment related to
// one part of kukulink
func (k *KukuLinkTools) getLinkedDeployment(
	linkName, linkNs, depKind, depDomain, depName string,
) (
	v3Deployment *kumoriv1.V3Deployment,
	kerr error,
) {
	v3Deployment = nil
	kerr = nil

	matchLabels := map[string]string{
		utils.KumoriDomainLabel: hashutil.Hash(depDomain),
		utils.KumoriNameLabel:   hashutil.Hash(depName),
	}
	labelSelector := labels.SelectorFromSet(matchLabels)

	list, err := k.v3DeploymentsLister.V3Deployments(linkNs).List(labelSelector)
	if err != nil {
		kerr = kerrors.NewKukuError(kerrors.Major, err)
	} else if len(list) == 0 {
		kerr = kerrors.NewKukuError(kerrors.Fatal, fmt.Sprintf("Deployment "+depDomain+"/"+depName+" not found"))
		return
	} else if len(list) > 1 {
		kerr = kerrors.NewKukuError(kerrors.Fatal, fmt.Sprintf("There are several deployments "+depDomain+"/"+depName))
	} else {
		v3Deployment = list[0].DeepCopy()
	}

	return
}

// LinkToString creates a string using kukulink info (logging) purposes)
func (k *KukuLinkTools) LinkToString(kukuLink *kumoriv1.KukuLink) string {
	endpoint1 := kukuLink.Spec.Endpoints[0]
	endpoint2 := kukuLink.Spec.Endpoints[1]
	return kukuLink.Name + ":" +
		endpoint1.Domain + "/" + endpoint1.Name + ":" + endpoint1.Channel +
		"<=>" +
		endpoint2.Domain + "/" + endpoint2.Name + ":" + endpoint2.Channel
}

// GetReachableDeployments returns the V3Deployments reachable from a given V3Deployment
// and channel through links.
func (k *KukuLinkTools) GetReachableDeployments(
	v3Deployment *kumoriv1.V3Deployment,
	channelName string,
) ([]*kumoriv1.V3Deployment, error) {

	// Gets the names used when the deployment was registered by the service provider
	registeredName, ok := v3Deployment.Annotations[utils.KumoriNameLabel]
	if !ok {
		return nil, fmt.Errorf("V3Deployment '%s' has not a name label", v3Deployment.GetName())
	}
	registeredDomain, ok := v3Deployment.Annotations[utils.KumoriDomainLabel]
	if !ok {
		return nil, fmt.Errorf("V3Deployment '%s' has not a domain label", v3Deployment.GetName())
	}

	var kcerr error

	// This list contains the link endpoints pending to be followed. It is initially
	// filled with the provided deployment. Each endpoint is a deployment and a service channel
	// and indicates that we want to find out wich other endpoints we can reach following
	// the links affecting this deployment and service channel. If the service channel is the
	// empty string then we are insterested on following all links affecting the V3Deployment
	endpoints := []linkDeploymentEndpoint{{
		Name:       registeredName,
		Domain:     registeredDomain,
		Channel:    channelName,
		Deployment: v3Deployment,
	}}

	// This is the list of endpoints we have already processed. It is used to avoid processing
	// the same endpoint twice.
	processedEndpoints := []kumoriv1.KukuEndpoint{}

	// This list contains the deployments linked to the one provided as a parameter
	linkedDeployments := []*kumoriv1.V3Deployment{}

	// Processes all endpoints. This list can grow but it is highly improbable. It will grow
	// if a given endpoint is linked to a service channel in another deployment which, at the
	// same time, is connected to a connector with another service channel connected.
	for len(endpoints) > 0 {

		// Pops the next endpoint
		currentEndpoint := endpoints[0]
		endpoints = endpoints[1:]

		// Gets the kukulinks linking this endpoint
		kukuLinks, err := k.GetKukuLinks(currentEndpoint.Deployment, currentEndpoint.Channel)
		if err != nil {
			kcerr = err
			continue
		}

		// Follow each links to find out which V3Deployments are linked in the other side
		for _, link := range kukuLinks {

			// For each endpoint
			for _, linkEndpoint := range link.Spec.Endpoints {

				// Skips it if its not a deployment endpoint
				if linkEndpoint.Kind != DeploymentKukuLinkEndpointType {
					continue
				}

				// Skips it if this is the endpoint being processed
				if (linkEndpoint.Domain == currentEndpoint.Domain) &&
					(linkEndpoint.Name == currentEndpoint.Name) &&
					((linkEndpoint.Channel == currentEndpoint.Channel) || (currentEndpoint.Channel == "")) {
					continue
				}

				// Skips it if this endpoint has been already processed
				processed := false
				for _, processedEndpoint := range processedEndpoints {
					if (processedEndpoint.Domain == linkEndpoint.Domain) &&
						(processedEndpoint.Name == linkEndpoint.Name) &&
						(processedEndpoint.Channel == linkEndpoint.Channel) {
						processed = true
						break
					}
				}
				if processed {
					continue
				}

				// Adds the endpoint to the list of processed endpoints
				processedEndpoints = append(processedEndpoints, linkEndpoint)

				// Gets the endpoint V3Deployment using the name and domain
				newV3Deployment, err := v3dutils.GetByRegistrationNameAndDomain(k.v3DeploymentsLister, v3Deployment.Namespace, linkEndpoint.Name, linkEndpoint.Domain)
				if err != nil {
					kcerr = err
					continue
				}

				// Skips this V3Deployment if it is the V3Deployment provided as a parameter
				// or it has been already added to the linded deployments list
				if newV3Deployment.Name != v3Deployment.Name {
					added := false
					for _, addedDeployment := range linkedDeployments {
						if addedDeployment.Name == newV3Deployment.Name {
							added = true
							break
						}
					}
					if !added {
						linkedDeployments = append(linkedDeployments, newV3Deployment)
					}
				}

				// Calculates if this endpoint is connected to another service endpoint in this
				// deployment. If this is the case (hihly unlikely), adds that service endpoint as
				// a new endpoint pending to be processed.
				if newV3Deployment.Spec.Artifact.Description.Connectors != nil {
					for _, connector := range *newV3Deployment.Spec.Artifact.Description.Connectors {
						for _, clientData := range connector.Clients {
							if clientData.Role == "self" {
								processed := false
								for _, processedEndpoint := range processedEndpoints {
									if processedEndpoint.Name == linkEndpoint.Name && processedEndpoint.Domain == linkEndpoint.Domain && processedEndpoint.Channel != clientData.Channel {
										processed = true
										break
									}
								}
								if !processed {
									endpoints = append(endpoints, linkDeploymentEndpoint{
										Name:       linkEndpoint.Name,
										Domain:     linkEndpoint.Domain,
										Channel:    clientData.Channel,
										Deployment: newV3Deployment,
									})
								}
							}
						}
					}
				}
			}

			// Adds the V3Deployment with services or networkpolicies labeled with this link
			// but not found during this search. This means that a link has been changed
			labeledV3Deployments, err := k.GetLabeledV3Deployments(&link)
			if err != nil {
				kcerr = err
				continue
			}
			for _, labeledDeployment := range labeledV3Deployments {
				labeledName := labeledDeployment.GetName()
				alreadyQueued := false
				for _, linkedDeployment := range linkedDeployments {
					if labeledName == linkedDeployment.GetName() {
						alreadyQueued = true
						break
					}
				}
				if !alreadyQueued {
					linkedDeployments = append(linkedDeployments, labeledDeployment)
				}
			}
		}
	}

	if kcerr != nil {
		return linkedDeployments, kcerr
	}

	return linkedDeployments, nil
}

// getKukuLinks returns the KukuLinks linking a given V3Deployment
func (k *KukuLinkTools) GetKukuLinks(v3Deployment *kumoriv1.V3Deployment, channel string) (
	[]kumoriv1.KukuLink,
	error,
) {
	if v3Deployment == nil {
		kerr := kerrors.NewKukuError(kerrors.Major, fmt.Errorf("Deployment is null"))
		return nil, kerr
	}
	namespace := v3Deployment.Namespace
	kukuLinks := make([]kumoriv1.KukuLink, 0, 1)
	matchLabels1 := map[string]string{
		"deployment1": v3Deployment.GetName(),
	}
	matchLabels2 := map[string]string{
		"deployment2": v3Deployment.GetName(),
	}
	if channel != "" {
		matchLabels1["channel1"] = hashutil.Hash(channel)
		matchLabels2["channel2"] = hashutil.Hash(channel)
	}
	var kcerr *kerrors.KukuError
	for _, matchLabels := range []map[string]string{matchLabels1, matchLabels2} {
		options := labels.SelectorFromSet(matchLabels)
		if links, err := k.kukuLinksLister.KukuLinks(namespace).List(options); err == nil {
			for _, elem := range links {
				link := *elem.DeepCopy()
				kukuLinks = append(kukuLinks, link)
			}
		} else {
			message := fmt.Sprintf("Error retrieving links: %v", err)
			kcerr = kerrors.NewKukuError(kerrors.Minor, fmt.Errorf(message))
		}
	}

	// To avoid https://golang.org/doc/faq#nil_error
	if kcerr == nil {
		return kukuLinks, nil
	}
	return kukuLinks, kcerr
}

// GetLabeledV3Deployments returns the list of deployments containing a _Service_ or
// _NetworkPolicy_ labeled with the given link
func (k *KukuLinkTools) GetLabeledV3Deployments(
	kukuLink *kumoriv1.KukuLink,
) ([]*kumoriv1.V3Deployment, error) {

	labeledDeployments := []*kumoriv1.V3Deployment{}

	linkName := kukuLink.GetName()
	linkNamespace := kukuLink.GetNamespace()
	matchLabels := map[string]string{
		linkName: "link-in-use",
	}
	options := labels.SelectorFromSet(matchLabels)

	var kcerr error

	// Enqueue deployments of _Service_ objects containing a label with the link name
	services, err := k.servicesLister.Services(linkNamespace).List(options)
	if err != nil {
		kcerr = kerrors.NewKukuError(kerrors.Minor, err)
	} else {
		for _, service := range services {
			serviceDeployment, ok := service.Labels[utils.KumoriDeploymentIdLabel]
			if !ok {
				continue
			}
			if v3Deployment, err := v3dutils.GetByName(k.v3DeploymentsLister, linkNamespace, serviceDeployment); err == nil {
				labeledDeployments = append(labeledDeployments, v3Deployment)
			} else {
				kcerr = kerrors.NewKukuError(kerrors.Minor, err)
			}
		}
	}

	// Enqueue deployments of _NetworkPolicy_ objects containing a label with the link name
	networkPolicies, err := k.networkPoliciesLister.NetworkPolicies(linkNamespace).List(options)
	if err != nil {
		kcerr = kerrors.NewKukuError(kerrors.Minor, err)
	} else {
		for _, networkPolicy := range networkPolicies {
			networkPolicyDeployment, ok := networkPolicy.Labels[utils.KumoriDeploymentIdLabel]
			if !ok {
				continue
			}
			if v3Deployment, err := v3dutils.GetByName(k.v3DeploymentsLister, linkNamespace, networkPolicyDeployment); err == nil {
				labeledDeployments = append(labeledDeployments, v3Deployment)
			} else {
				kcerr = kerrors.NewKukuError(kerrors.Minor, err)
			}
		}
	}

	if kcerr != nil {
		return labeledDeployments, kcerr
	}

	return labeledDeployments, nil
}
