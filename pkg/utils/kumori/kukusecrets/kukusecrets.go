/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package kukusecrets

import (
	"fmt"
	"strings"

	"kuku-controller/pkg/utils"
	hashutil "kuku-controller/pkg/utils/hash"

	kumoriv1 "gitlab.com/kumori-systems/community/libraries/client-go/pkg/apis/kumori/v1"
	kumorilisters "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated/listers/kumori/v1"
	"k8s.io/apimachinery/pkg/labels"
)

// Get recovers from the Kumori cluster the current version of a KukuSecret, if any
func Get(
	lister kumorilisters.KukuSecretLister,
	secret *kumoriv1.KukuSecret,
) (*kumoriv1.KukuSecret, error) {
	if secret == nil {
		return nil, fmt.Errorf("Secret is nil")
	}
	return GetByName(lister, secret.GetNamespace(), secret.GetName())
}

// GetByName using the object domain and name
func GetByName(
	lister kumorilisters.KukuSecretLister,
	namespace string,
	name string,
) (*kumoriv1.KukuSecret, error) {
	return lister.KukuSecrets(namespace).Get(name)
}

// GetByRegistrationReference returns the KukuSecret with a given domain/name. The name
// and domain are the ones provided by the service provider when the KukuSecret is
// registered
func GetByRegistrationReference(
	lister kumorilisters.KukuSecretLister,
	namespace string,
	ref string,
	ownerDomain string,
) (*kumoriv1.KukuSecret, error) {
	parts := strings.Split(ref, "/")

	var name, domain string

	if len(parts) == 1 {
		name = parts[0]
		domain = ownerDomain
	} else if len(parts) == 2 {
		domain = parts[0]
		name = parts[1]
	} else {
		return nil, fmt.Errorf("The string '%s' is not a correct KukuSecret reference. Expected format: <DOMAIN>/<NAME> or just <NAME>", ref)
	}

	return GetByRegistrationNameAndDomain(lister, namespace, name, domain)
}

// GetByRegistrationNameAndDomain returns the KukuSecret with a given name and domain. The name
// and domain are the ones provided by the service provider when the KukuSecret is
// registered
func GetByRegistrationNameAndDomain(
	lister kumorilisters.KukuSecretLister,
	namespace string,
	name string,
	domain string,
) (*kumoriv1.KukuSecret, error) {

	// Creates the labels filter to find the KukuSecret with the given name and domain
	nameHash := hashutil.Hash(name)
	domainHash := hashutil.Hash(domain)
	matchLabels := map[string]string{
		utils.KumoriDomainLabel: domainHash,
		utils.KumoriNameLabel:   nameHash,
	}
	options := labels.SelectorFromSet(matchLabels)

	// Searches for the specified KukuSecret
	secrets, err := lister.KukuSecrets(namespace).List(options)

	// Returns the error if anything wrong happens
	if err != nil {
		return nil, err
	}

	// Returns an error if a KukuSecret is not found with this name and domain
	if len(secrets) == 0 {
		return nil, fmt.Errorf("Not found a KukuSecret with name '%s' ('%s') and domain '%s' ('%s') in namespace '%s'", name, nameHash, domain, domainHash, namespace)
	}

	// Returns an error if more than one KukuSecret is found with this name and domain
	if len(secrets) > 1 {
		return nil, fmt.Errorf("Found more than one KukuSecret with name '%s' ('%s') and domain '%s' ('%s') in namespace '%s'", name, nameHash, domain, domainHash, namespace)
	}

	// Returns the KukuSecret found
	return secrets[0].DeepCopy(), nil
}
