/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package v3Deployments

import (
	"context"
	"fmt"
	"kuku-controller/pkg/utils"
	"sort"

	hashutil "kuku-controller/pkg/utils/hash"

	log "github.com/sirupsen/logrus"
	kumoriv1 "gitlab.com/kumori-systems/community/libraries/client-go/pkg/apis/kumori/v1"
	clientset "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated/clientset/versioned"
	kumorilisters "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated/listers/kumori/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/labels"
)

// Create is used to create a new configmap in a Kubernetes cluster using a client-go
// instance.
func Create(
	client clientset.Interface,
	deployment *kumoriv1.V3Deployment,
) (*kumoriv1.V3Deployment, error) {
	if deployment == nil {
		return nil, fmt.Errorf("V3Deployment map is nil")
	}
	log.Debugf("v3deployments.Create. ConfigMap: %s", deployment.Name)
	ctx := context.TODO()
	options := metav1.CreateOptions{}
	return client.KumoriV1().V3Deployments(deployment.GetNamespace()).Create(ctx, deployment, options)
}

// Update is used to update an existing configmap in a Kubernetes cluster using a client-go
// instance.
func Update(
	client clientset.Interface,
	deployment *kumoriv1.V3Deployment,
) (*kumoriv1.V3Deployment, error) {
	if deployment == nil {
		return nil, fmt.Errorf("V3Deployment map is nil")
	}
	log.Debugf("v3deployments.Update. ConfigMap: %s", deployment.Name)
	ctx := context.TODO()
	options := metav1.UpdateOptions{}
	return client.KumoriV1().V3Deployments(deployment.GetNamespace()).Update(ctx, deployment, options)
}

// Delete is used to delete an existing configmap in a Kubernetes cluster using a client-go
// instance.
func Delete(
	client clientset.Interface,
	deployment *kumoriv1.V3Deployment,
) error {
	if deployment == nil {
		return fmt.Errorf("V3Deployment map is nil")
	}
	log.Debugf("v3deployments.Delete. ConfigMap: %s", deployment.Name)
	ctx := context.TODO()
	policy := metav1.DeletePropagationForeground
	options := metav1.DeleteOptions{
		PropagationPolicy: &policy,
	}
	return client.KumoriV1().V3Deployments(deployment.GetNamespace()).Delete(ctx, deployment.GetName(), options)
}

// Get recovers from the Kumori cluster the current version of a V3Deployment, if any
func Get(
	lister kumorilisters.V3DeploymentLister,
	deployment *kumoriv1.V3Deployment,
) (*kumoriv1.V3Deployment, error) {
	if deployment == nil {
		return nil, fmt.Errorf("V3Deployment is nil")
	}
	return GetByName(lister, deployment.GetNamespace(), deployment.GetName())
}

// GetByNameAndDomain using the object domain and name
func GetByName(
	lister kumorilisters.V3DeploymentLister,
	namespace string,
	name string,
) (*kumoriv1.V3Deployment, error) {
	return lister.V3Deployments(namespace).Get(name)
}

// GetByNameAndDomain returns the V3Deployment with a given name and domain. The name
// and domain are the ones provided by the service provider when the V3Deployment is
// registered
func GetByRegistrationNameAndDomain(
	lister kumorilisters.V3DeploymentLister,
	namespace string,
	name string,
	domain string,
) (*kumoriv1.V3Deployment, error) {

	// Creates the labels filter to find the V3Deployment with the given name and domain
	nameHash := hashutil.Hash(name)
	domainHash := hashutil.Hash(domain)
	matchLabels := map[string]string{
		utils.KumoriDomainLabel: domainHash,
		utils.KumoriNameLabel:   nameHash,
	}
	options := labels.SelectorFromSet(matchLabels)

	// Searches for the specified V3Deployment
	v3Deployments, err := lister.V3Deployments(namespace).List(options)

	// Returns the error if anything wrong happens
	if err != nil {
		return nil, err
	}

	// Returns an error if a V3Deployment is not found with this name and domain
	if len(v3Deployments) == 0 {
		return nil, fmt.Errorf("Not found a V3Deployment with name '%s' ('%s') and domain '%s' ('%s') in namespace '%s'", name, nameHash, domain, domainHash, namespace)
	}

	// Returns an error if more than one V3Deployment is found with this name and domain
	if len(v3Deployments) > 1 {
		return nil, fmt.Errorf("Found more than one V3Deployment with name '%s' ('%s') and domain '%s' ('%s') in namespace '%s'", name, nameHash, domain, domainHash, namespace)
	}

	// Returns the found V3Deployment
	return v3Deployments[0], nil
}

// GetChannelConnectorNames returns the connectors using a given channel of a given role. If the
// role is "self" then the channel is considered a service channel
func GetChannelConnectorNames(
	v3Deployment *kumoriv1.V3Deployment,
	role string,
	channel string,
	channelType string,
) []string {
	if v3Deployment == nil ||
		v3Deployment.Spec.Artifact == nil ||
		v3Deployment.Spec.Artifact.Description == nil ||
		v3Deployment.Spec.Artifact.Description.Connectors == nil ||
		len(*v3Deployment.Spec.Artifact.Description.Connectors) <= 0 {
		return []string{}
	}

	connectors := *v3Deployment.Spec.Artifact.Description.Connectors
	channelConnectorNames := make([]string, 0, 1)

	for connectorName, connectorData := range connectors {
		if channelType == "client" {
			for _, clientData := range connectorData.Clients {
				if clientData.Role == role && clientData.Channel == channel {
					channelConnectorNames = append(channelConnectorNames, connectorName)
					break
				}
			}
		} else {
			for _, serverData := range connectorData.Servers {
				for _, linkData := range serverData.Links {
					if linkData.Role == role && linkData.Channel == channel {
						channelConnectorNames = append(channelConnectorNames, connectorName)
						break
					}
				}
			}
		}
	}

	sort.Strings(channelConnectorNames)

	return channelConnectorNames
}

// UpdateStatus is used to update an existing V3Deployment in a Kubernetes cluster using a client-go
// instance.
func UpdateStatus(
	client clientset.Interface,
	deployment *kumoriv1.V3Deployment,
) (*kumoriv1.V3Deployment, error) {
	if deployment == nil {
		return nil, fmt.Errorf("V3Deployment map is nil")
	}
	log.Debugf("v3deployments.UpdateStatus. V3Deployment: %s", deployment.Name)
	ctx := context.TODO()
	options := metav1.UpdateOptions{
		FieldManager: "kumori-kucontroller",
	}
	return client.KumoriV1().V3Deployments(deployment.Namespace).UpdateStatus(ctx, deployment, options)
}

// GetOwnerDomain returns the domain of the V3Deployment owner
func GetOwnerDomain(
	deployment *kumoriv1.V3Deployment,
) (string, error) {
	log.Debugf("v3deployments.GetOwnerDomain. V3Deployment: %s", deployment.Name)

	owner, ok := deployment.Annotations[utils.KumoriDomainLabel]
	if !ok {
		return "", fmt.Errorf("Domain not found for deployment %s", deployment.Name)
	}

	return owner, nil
}

// GetRegistrationName returns the name of the V3Deployment
func GetRegistrationName(
	deployment *kumoriv1.V3Deployment,
) (string, error) {
	log.Debugf("v3deployments.GetRegistrationName. V3Deployment: %s", deployment.Name)

	name, ok := deployment.Annotations[utils.KumoriNameLabel]
	if !ok {
		return "", fmt.Errorf("Name not found for deployment %s", deployment.Name)
	}

	return name, nil
}
