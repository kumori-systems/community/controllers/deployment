/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package kukuvolumes

import (
	"fmt"
	"strings"

	"kuku-controller/pkg/utils"
	hashutil "kuku-controller/pkg/utils/hash"

	kumoriv1 "gitlab.com/kumori-systems/community/libraries/client-go/pkg/apis/kumori/v1"
	kumorilisters "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated/listers/kumori/v1"
	"k8s.io/apimachinery/pkg/labels"
)

// Get recovers from the Kumori cluster the current version of a KukuVolume, if any
func Get(
	lister kumorilisters.KukuVolumeLister,
	persistentVolume *kumoriv1.KukuVolume,
) (*kumoriv1.KukuVolume, error) {
	if persistentVolume == nil {
		return nil, fmt.Errorf("PersistentVolume is nil")
	}
	return GetByName(lister, persistentVolume.GetNamespace(), persistentVolume.GetName())
}

// GetByNameAndDomain using the object domain and name
func GetByName(
	lister kumorilisters.KukuVolumeLister,
	namespace string,
	name string,
) (*kumoriv1.KukuVolume, error) {
	return lister.KukuVolumes(namespace).Get(name)
}

// GetByRegistrationReference returns the KukuVolume with a given domain/name. The name
// and domain are the ones provided by the service provider when the KukuVolume is
// registered
func GetByRegistrationReference(
	lister kumorilisters.KukuVolumeLister,
	namespace string,
	ref string,
	ownerDomain string,
) (*kumoriv1.KukuVolume, error) {
	parts := strings.Split(ref, "/")

	var name, domain string
	if len(parts) == 1 {
		name = parts[0]
		domain = ownerDomain
	} else if len(parts) == 2 {
		domain = parts[0]
		name = parts[1]
	} else {
		return nil, fmt.Errorf("The string '%s' is not a correct KukuVolume reference. Expected format: <DOMAIN>/<NAME> or just <NAME>", ref)
	}

	return GetByRegistrationNameAndDomain(lister, namespace, name, domain)
}

// GetByRegistrationNameAndDomain returns the KukuVolume with a given name and domain. The name
// and domain are the ones provided by the service provider when the KukuVolume is
// registered
func GetByRegistrationNameAndDomain(
	lister kumorilisters.KukuVolumeLister,
	namespace string,
	name string,
	domain string,
) (*kumoriv1.KukuVolume, error) {

	// Creates the labels filter to find the KukuVolume with the given name and domain
	nameHash := hashutil.Hash(name)
	domainHash := hashutil.Hash(domain)
	matchLabels := map[string]string{
		utils.KumoriDomainLabel: domainHash,
		utils.KumoriNameLabel:   nameHash,
	}
	options := labels.SelectorFromSet(matchLabels)

	// Searches for the specified KukuVolume
	persistentVolumes, err := lister.KukuVolumes(namespace).List(options)

	// Returns the error if anything wrong happens
	if err != nil {
		return nil, err
	}

	// Returns an error if a KukuVolume is not found with this name and domain
	if len(persistentVolumes) == 0 {
		return nil, fmt.Errorf("Not found a KukuVolume with name '%s' ('%s') and domain '%s' ('%s') in namespace '%s'", name, nameHash, domain, domainHash, namespace)
	}

	// Returns an error if more than one KukuVolume is found with this name and domain
	if len(persistentVolumes) > 1 {
		return nil, fmt.Errorf("Found more than one KukuVolume with name '%s' ('%s') and domain '%s' ('%s') in namespace '%s'", name, nameHash, domain, domainHash, namespace)
	}

	// Returns the KukuVolume found
	return persistentVolumes[0].DeepCopy(), nil
}
