/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package utils

import (
	"fmt"
)

const (
	// Skip error means that the Kuku-Element must not be processed
	Skip = "skip"
	// Fatal error means that the Kuku-Element processing cannot be ended
	// and cannot be requeued
	Fatal = "fatal"
	// Major error means that the Kuku-Element processing cannot be ended
	// but be requeued
	Major = "major"
	// Minor error means that the Kuku-Element processing can continue but
	// must be requeued because there might be some pieces missing
	Minor = "minor"
)

// Text related to a KukuError created from an invalid parent
const (
	InvalidSource = "Error constructed with invalid parent"
)

// KukuErrorSeverity is the severity of a KukuError
type KukuErrorSeverity string

// KukuError is an error produced while a Kuku was created. The
// Message represents the error and the Severity the impact on the Kuku-Element
// processing.
type KukuError struct {
	Parent   error
	Severity KukuErrorSeverity
}

// Error message returns a string indicating the error severity and the parent
// error message.
func (e *KukuError) Error() string {
	return fmt.Sprintf("Severity: %s. Parent: %s", e.Severity, e.Parent.Error())
}

// NewKukuError returns a new KukuError object
func NewKukuError(severity KukuErrorSeverity, source interface{}) *KukuError {
	var parent error
	switch source.(type) {
	case error:
		parent = source.(error)
	case string:
		parent = fmt.Errorf(source.(string))
	default:
		parent = fmt.Errorf(InvalidSource)
	}
	return &KukuError{Severity: severity, Parent: parent}
}

// CheckError analyze the error and returns (ShouldAbort, ShouldRetry bool):
//   - ShouldAbort = true if is a KukuError with severity = Fatal / Major,
//     or isnt a KukuError
//   - ShouldRetry = true if is a kukuError with severity = Major / Minor
//     or isnt a KukuError
func CheckError(err error) (bool, bool) {
	switch err.(type) {
	case nil:
		return false, false
	case *KukuError:
		kerr := err.(*KukuError)
		switch kerr.Severity {
		case Fatal:
			return true, false
		case Major:
			return true, true
		case Minor:
			return false, true
		}
	}
	return true, true
}

// CheckErrorEx analyze the error, like CheckError, but in more suitable
// format for the controller flow. Returns if flow should be aborted and the
// error to be used in this case
func CheckErrorEx(err error, pendingRetryErrors *bool) (bool, error) {
	shouldAbort, shouldRetry := CheckError(err)
	if shouldRetry {
		*pendingRetryErrors = shouldRetry
	}
	if shouldAbort {
		if shouldRetry {
			return true, err
		}
		return true, nil
	}
	return false, nil
}

// ShouldAbort returns true if the severity is not minor and false oherwirse
func ShouldSkip(err error) bool {
	switch err.(type) {
	case nil:
		return false
	case *KukuError:
		kerr := err.(*KukuError)
		if kerr.Severity == Skip {
			return true
		}
		return false
	default:
		return false
	}
}

// ShouldAbort returns true if the severity is not minor and false oherwirse
func ShouldAbort(err error) bool {
	switch err.(type) {
	case nil:
		return false
	case *KukuError:
		kerr := err.(*KukuError)
		if kerr.Severity == Minor {
			return false
		}
		return true
	default:
		return true
	}
}

// ShouldRetry returns false if the severity fatal and true otherwise
func ShouldRetry(err error) bool {
	switch err.(type) {
	case nil:
		return false
	case *KukuError:
		kerr := err.(*KukuError)
		if kerr.Severity == Fatal || kerr.Severity == Skip {
			return false
		}
		return true
	default:
		return true
	}
}

// ProcessError creates a new error composing an existing parent error and a new one.
func ProcessError(parent error, err error) error {
	switch parent.(type) {
	case nil:
		return err
	default:
		if err == nil {
			return nil
		}
		message := fmt.Sprintf("%s\n%s", parent.Error(), err.Error())
		return &KukuError{Severity: Minor, Parent: fmt.Errorf(message)}
	}
}

// AsKukuError checks if an error is a KukuError. In that case, returns
// the same error. Otherwise, returns a KukuError with the given severity and
// the original error as parent.
func AsKukuError(err error, severity KukuErrorSeverity) *KukuError {
	switch err.(type) {
	case *KukuError:
		kerr := err.(*KukuError)
		return kerr
	default:
		return &KukuError{Severity: severity, Parent: err}
	}
}
