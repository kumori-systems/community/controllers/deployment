/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package utils

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"reflect"
	"strings"

	v1 "k8s.io/api/core/v1"
	apiequality "k8s.io/apimachinery/pkg/api/equality"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/yaml"
)

// SameElements checks if two slices contain the same values, even if they are ordered
// differently.
func SameElements(x, y any) bool {
	return SameElementsCustom(x, y, reflect.DeepEqual)
}

// func SameElements2[T interface{}](x, y [])

// SameElementsCustom checks if two slices contain the same values, even if they are ordered
// differently. Uses a custom equality function
func SameElementsCustom(x, y any, equalFunc func(a, b any) bool) bool {
	if x == nil || y == nil {
		return x == y
	}
	v1 := reflect.ValueOf(x)
	v2 := reflect.ValueOf(y)

	if v1.Type() != v2.Type() {
		return false
	}

	// if v1.IsNil() != v2.IsNil() {
	// 	fmt.Printf("\n\n NIL %v %v %v %v\n\n", v1.IsNil(), v2.IsNil(), x, y)
	// 	return false
	// }

	if v1.Len() != v2.Len() {
		return false
	}

	if v1.Pointer() == v2.Pointer() {
		return true
	}

	for i := 0; i < v1.Len(); i++ {
		found := false
		for j := 0; j < v2.Len(); j++ {
			if equalFunc(v1.Index(i).Interface(), v2.Index(j).Interface()) {
				found = true
				break
			}
		}
		if !found {
			return false
		}
	}

	return true
}

// InterfaceToSlice converts an interface{} to []interface{} if obj is an array or slice.
// It also returns a boolan indicating if the conversion was successfull or not.
func InterfaceToSlice(obj interface{}) ([]interface{}, bool) {
	v := reflect.ValueOf(obj)
	if (v.Kind() == reflect.Array) || (v.Kind() == reflect.Slice) {
		list := make([]interface{}, v.Len())
		for i := 0; i < v.Len(); i++ {
			list[i] = v.Index(i).Interface()
		}
		return list, true
	}
	return nil, false
}

// CalculateKumoriProtocol returns a Kubernetes understandable protocol type from a string
func CalculateKumoriProtocol(name string) v1.Protocol {
	// proto := strings.ToUpper(fmt.Sprintf("%X", name))
	proto := strings.ToUpper(name)
	switch proto {
	case "TCP":
		return v1.ProtocolTCP
	case "SCTP":
		return v1.ProtocolSCTP
	case "UDP":
		return v1.ProtocolUDP
	default:
		return v1.ProtocolTCP
	}
}

// Operation is a code of an operation over a NetworkPolicy
type Operation string

const (
	// CreatedOperation is the code representing a NetworkPolicy creation
	CreatedOperation Operation = "created"
	// UpdatedOperation is the code representing a NetworkPolicy updating
	UpdatedOperation Operation = "updated"
	// DeletedOperation is the code representing a NetworkPolicy deletion
	DeletedOperation Operation = "deleted"
	// NoneOperation is the code representing that nothing has been done
	NoneOperation Operation = "nothing"
)

// LoadObjectFromYAML loads an object from a yaml file
func LoadObjectFromYAML(v interface{}, p string) error {
	yamlData, err := ioutil.ReadFile(p)
	if err != nil {
		return err
	}
	jsonData, err := yaml.YAMLToJSON(yamlData)
	if err != nil {
		return err
	}
	return json.Unmarshal(jsonData, v)
}

// const (
// 	// NormalEventType represents a normal event
// 	NormalEventType string = "Normal"
// 	// ErrorEventType represents an error event
// 	ErrorEventType string = "Error"
// )

const (
	// UpdatedReason represents an event updating an object
	UpdatedReason string = "Updated"
	// CreatedReason represents an event creating an object
	CreatedReason string = "Created"
	// DeletedReason represents an event deleting an object
	DeletedReason string = "Deleted"
	// RequeuedReason represents an event deleting an object
	RequeuedReason string = "Requeued"
	// AbortedReason represents an event deleting an object
	AbortedReason string = "Aborted"
)

// SameProbe checks if to probes are equal
func SameProbe(from *v1.Probe, to *v1.Probe) bool {
	if from == nil || to == nil {
		return from == to
	}

	if !apiequality.Semantic.DeepEqual(from.HTTPGet, to.HTTPGet) {
		return false
	}

	if !apiequality.Semantic.DeepEqual(from.TCPSocket, to.TCPSocket) {
		return false
	}

	if !apiequality.Semantic.DeepEqual(from.Exec, to.Exec) {
		return false
	}

	if from.InitialDelaySeconds != to.InitialDelaySeconds {
		return false
	}

	if from.PeriodSeconds != to.PeriodSeconds {
		return false
	}

	if from.FailureThreshold != to.FailureThreshold {
		return false
	}

	if from.TimeoutSeconds != to.TimeoutSeconds {
		return false
	}

	return true
}

// ContainersAreEqual checks if two Deployment containers are equal (or equivalent).
func ContainersAreEqual(from []v1.Container, to []v1.Container) (bool, error) {

	var err error

	equalFunc := func(a, b interface{}) bool {
		fromContainer, ok := a.(v1.Container)
		if !ok {
			err = fmt.Errorf("parameter is of type %v a corev1.Container", reflect.TypeOf(from))
			return false
		}
		toContainer, ok := b.(v1.Container)
		if !ok {
			err = fmt.Errorf("parameter is of type %v a corev1.Container", reflect.TypeOf(to))
			return false
		}
		if (fromContainer.Image != toContainer.Image) ||
			(fromContainer.Name != toContainer.Name) ||
			// (fromContainer.ImagePullPolicy != toContainer.ImagePullPolicy) ||
			!SameElements(fromContainer.VolumeMounts, toContainer.VolumeMounts) ||
			!apiequality.Semantic.DeepEqual(fromContainer.Env, toContainer.Env) ||
			!apiequality.Semantic.DeepEqual(fromContainer.Resources, toContainer.Resources) ||
			!apiequality.Semantic.DeepEqual(fromContainer.Command, toContainer.Command) ||
			!apiequality.Semantic.DeepEqual(fromContainer.Args, toContainer.Args) ||
			!apiequality.Semantic.DeepEqual(fromContainer.Lifecycle, toContainer.Lifecycle) ||
			!apiequality.Semantic.DeepEqual(fromContainer.SecurityContext, toContainer.SecurityContext) ||
			!SameProbe(fromContainer.StartupProbe, toContainer.StartupProbe) ||
			!SameProbe(fromContainer.LivenessProbe, toContainer.LivenessProbe) ||
			!SameProbe(fromContainer.ReadinessProbe, toContainer.ReadinessProbe) ||
			!SameElements(fromContainer.Ports, toContainer.Ports) {
			return false
		}
		return true
	}

	areEqual := SameElementsCustom(from, to, equalFunc)
	if err != nil {
		return false, err
	}
	return areEqual, nil
}

func IsAutoscaled(obj *metav1.ObjectMeta) bool {
	if obj == nil {
		return false
	}

	labels := obj.Labels

	autoscaled, ok := labels[KumoriAutoscaledLabel]
	if !ok {
		return false
	}

	if strings.ToLower(autoscaled) == "true" {
		return true
	}

	return false
}
