/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package poddisruptionbudgets

import (
	"context"
	"fmt"

	apiequality "k8s.io/apimachinery/pkg/api/equality"

	"k8s.io/apimachinery/pkg/api/errors"

	utils "kuku-controller/pkg/utils"

	log "github.com/sirupsen/logrus"
	v1 "k8s.io/api/policy/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	labels "k8s.io/apimachinery/pkg/labels"
	clientset "k8s.io/client-go/kubernetes"
	listers "k8s.io/client-go/listers/policy/v1"
)

// Create is used to create a new poddisruptionbudget in a Kubernetes cluster using a client-go
// instance.
func Create(
	client clientset.Interface,
	podDisruptionBudget *v1.PodDisruptionBudget,
) (*v1.PodDisruptionBudget, error) {
	if podDisruptionBudget == nil {
		return nil, fmt.Errorf("Config map is nil")
	}
	log.Debugf("poddisruptionbudgets.Create. PodDisruptionBudget: %s", podDisruptionBudget.Name)
	ctx := context.TODO()
	options := metav1.CreateOptions{
		FieldManager: "kumori-kucontroller",
	}
	return client.PolicyV1().PodDisruptionBudgets(podDisruptionBudget.GetNamespace()).Create(ctx, podDisruptionBudget, options)
}

// Update is used to update an existing poddisruptionbudget in a Kubernetes cluster using a client-go
// instance.
func Update(
	client clientset.Interface,
	podDisruptionBudget *v1.PodDisruptionBudget,
) (*v1.PodDisruptionBudget, error) {
	if podDisruptionBudget == nil {
		return nil, fmt.Errorf("Config map is nil")
	}
	log.Debugf("poddisruptionbudgets.Update. PodDisruptionBudget: %s", podDisruptionBudget.Name)
	ctx := context.TODO()
	options := metav1.UpdateOptions{
		FieldManager: "kumori-kucontroller",
	}
	return client.PolicyV1().PodDisruptionBudgets(podDisruptionBudget.GetNamespace()).Update(ctx, podDisruptionBudget, options)
}

// Delete is used to delete an existing poddisruptionbudget in a Kubernetes cluster using a client-go
// instance.
func Delete(
	client clientset.Interface,
	podDisruptionBudget *v1.PodDisruptionBudget,
) error {
	if podDisruptionBudget == nil {
		return fmt.Errorf("Config map is nil")
	}
	log.Debugf("poddisruptionbudgets.Delete. PodDisruptionBudget: %s", podDisruptionBudget.Name)
	ctx := context.TODO()
	policy := metav1.DeletePropagationForeground
	options := metav1.DeleteOptions{
		PropagationPolicy: &policy,
	}
	return client.PolicyV1().PodDisruptionBudgets(podDisruptionBudget.GetNamespace()).Delete(ctx, podDisruptionBudget.GetName(), options)
}

// Get recovers from the Kumori cluster the current version of a PodDisruptionBudget, if any
func Get(
	lister listers.PodDisruptionBudgetLister,
	podDisruptionBudget *v1.PodDisruptionBudget,
) (*v1.PodDisruptionBudget, error) {
	if podDisruptionBudget == nil {
		return nil, fmt.Errorf("Config map is nil")
	}
	return lister.PodDisruptionBudgets(podDisruptionBudget.GetNamespace()).Get(podDisruptionBudget.GetName())
}

// CreateOrUpdate checks if the given config map exists in the cluster and updates it
// if exists or creates it otherwirse. Returns also a flag indicating if the object has
// been created (true) or updated (false).
func CreateOrUpdate(
	client clientset.Interface,
	lister listers.PodDisruptionBudgetLister,
	podDisruptionBudget *v1.PodDisruptionBudget,
) (*v1.PodDisruptionBudget, utils.Operation, error) {
	if podDisruptionBudget == nil {
		return nil, utils.NoneOperation, fmt.Errorf("Config map is nil")
	}
	currentPodDisruptionBudget, err := Get(lister, podDisruptionBudget)
	if errors.IsNotFound(err) {
		finalPodDisruptionBudget, err := Create(client, podDisruptionBudget)
		return finalPodDisruptionBudget, utils.CreatedOperation, err
	} else if err != nil {
		return podDisruptionBudget, utils.NoneOperation, err
	}
	currentPodDisruptionBudget = currentPodDisruptionBudget.DeepCopy()
	finalPodDisruptionBudget, changed, err := Apply(client, podDisruptionBudget, currentPodDisruptionBudget)
	if changed {
		return finalPodDisruptionBudget, utils.UpdatedOperation, err
	}

	return finalPodDisruptionBudget, utils.NoneOperation, err
}

// CreateOrUpdateList creates or updates a list of config maps in the cluster. Returns
// the list of created config maps, the list of updated config maps and the list of errors
// found during the process.
func CreateOrUpdateList(
	client clientset.Interface,
	lister listers.PodDisruptionBudgetLister,
	podDisruptionBudgetList []v1.PodDisruptionBudget,
) (created []v1.PodDisruptionBudget, updated []v1.PodDisruptionBudget, errors []error) {
	listLength := len(podDisruptionBudgetList)
	errors = make([]error, 0, 3)
	created = make([]v1.PodDisruptionBudget, 0, listLength)
	updated = make([]v1.PodDisruptionBudget, 0, listLength)
	if listLength <= 0 {
		return
	}
	for _, podDisruptionBudget := range podDisruptionBudgetList {
		newPodDisruptionBudget, operation, err := CreateOrUpdate(client, lister, &podDisruptionBudget)
		if err != nil {
			errors = append(errors, err)
			continue
		}
		if operation == utils.NoneOperation {
			continue
		}
		if operation == utils.CreatedOperation {
			created = append(created, *newPodDisruptionBudget)
			continue
		}
		if operation == utils.UpdatedOperation {
			updated = append(updated, *newPodDisruptionBudget)
			continue
		}

		errors = append(errors, fmt.Errorf("Unexpected applied operation %s for podDisruptionBudget %s. Expected operations are %s or %s", operation, podDisruptionBudget.GetName(), utils.CreatedOperation, utils.UpdatedOperation))
	}
	return
}

// CreateUpdateOrDeleteList receives a list of podDisruptionBudgets and creates all elements in that
// list not currently part of the cluster state, updates those which are part of the cluster
// state but are outdated and deletes those currently existing in the cluster state but not
// in the list
func CreateUpdateOrDeleteList(
	client clientset.Interface,
	lister listers.PodDisruptionBudgetLister,
	podDisruptionBudgetsList []v1.PodDisruptionBudget,
	selector labels.Selector,
	namespace string,
) (
	created []v1.PodDisruptionBudget,
	updated []v1.PodDisruptionBudget,
	deleted []v1.PodDisruptionBudget,
	errors []error,
) {
	created, updated, errors = CreateOrUpdateList(client, lister, podDisruptionBudgetsList)
	currentPodDisruptionBudgetsList, err := lister.PodDisruptionBudgets(namespace).List(selector)
	if err != nil {
		errors = append(errors, err)
		deleted = []v1.PodDisruptionBudget{}
		return
	}
	if (currentPodDisruptionBudgetsList != nil) && (len(currentPodDisruptionBudgetsList) > 0) {
		var errs []error
		deleted, errs = DeleteMissing(client, currentPodDisruptionBudgetsList, podDisruptionBudgetsList)
		if (errs != nil) && (len(errs) > 0) {
			errors = append(errors, errs...)
		}
	} else {
		deleted = []v1.PodDisruptionBudget{}
	}
	return
}

// Apply updates "to" poddisruptionbudget with "from" values if any change is detected. The changes
// are also applied to a kubernetes cluster
func Apply(
	client clientset.Interface,
	from *v1.PodDisruptionBudget,
	to *v1.PodDisruptionBudget,
) (*v1.PodDisruptionBudget, bool, error) {

	if from == nil {
		return to, false, fmt.Errorf("From config map is null")
	}

	if to == nil {
		return to, false, fmt.Errorf("From config map is null")
	}

	changed := false

	if !apiequality.Semantic.DeepEqual(from.OwnerReferences, to.OwnerReferences) {
		log.Debugf("poddisruptionbudgets.Apply. Owner references differ for %s", from.GetName())
		changed = true
		to.OwnerReferences = from.OwnerReferences
	}

	for key, newValue := range (*from).GetLabels() {
		if currentValue, ok := to.Labels[key]; !ok || (currentValue != newValue) {
			log.Debugf("poddisruptionbudgets.Apply. Label %s differ in %s", key, from.GetName())
			changed = true
			to.Labels[key] = newValue
		}
	}

	for key, newValue := range (*from).GetAnnotations() {
		if currentValue, ok := to.Annotations[key]; !ok || (currentValue != newValue) {
			log.Debugf("poddisruptionbudgets.Apply. Annotation %s differ in %s", key, from.GetName())
			changed = true
			if to.Annotations == nil {
				to.Annotations = map[string]string{}
			}
			to.Annotations[key] = newValue
		}
	}

	if !apiequality.Semantic.DeepEqual(from.Spec.MaxUnavailable, to.Spec.MaxUnavailable) {
		log.Debugf("poddisruptionbudgets.Apply. MaxUnavailable differ in %s", from.GetName())
		changed = true
		to.Spec.MaxUnavailable = from.Spec.MaxUnavailable
	}

	if !apiequality.Semantic.DeepEqual(from.Spec.Selector, to.Spec.Selector) {
		log.Debugf("poddisruptionbudgets.Apply. Selector differ in %s", from.GetName())
		changed = true
		to.Spec.Selector = from.Spec.Selector
	}

	if changed {
		newPodDisruptionBudget, err := Update(client, to)
		return newPodDisruptionBudget, changed, err
	}

	return to, false, nil
}

// DeleteIfMissing deletes a podDisruptionBudget in a cluster if cannot be found in a list of podDisruptionBudgets.
// Returns true if the podDisruptionBudget has been deleted and false otherwise
func DeleteIfMissing(
	client clientset.Interface,
	podDisruptionBudget *v1.PodDisruptionBudget,
	expectedList []v1.PodDisruptionBudget,
) (bool, error) {
	if podDisruptionBudget == nil {
		return false, fmt.Errorf("Config map is nil")
	}
	for _, expectedPodDisruptionBudget := range expectedList {
		if (podDisruptionBudget.GetNamespace() == expectedPodDisruptionBudget.GetNamespace()) &&
			(podDisruptionBudget.GetName() == expectedPodDisruptionBudget.GetName()) {
			return false, nil
		}
	}
	if err := Delete(client, podDisruptionBudget); err != nil {
		return false, err
	}
	return true, nil
}

// DeleteMissing deletes from currentList all PodDisruptionBudgets not found in existingList.
// A PodDisruptionBudgetis in existingList if there is an element in that list with the same name
// and namespace.
func DeleteMissing(
	client clientset.Interface,
	currentList []*v1.PodDisruptionBudget,
	expectedList []v1.PodDisruptionBudget,
) (deleted []v1.PodDisruptionBudget, errors []error) {
	deleted = make([]v1.PodDisruptionBudget, 0, len(currentList))
	errors = make([]error, 0, len(currentList))
	for _, current := range currentList {
		if current == nil {
			continue
		}
		del, err := DeleteIfMissing(client, current, expectedList)
		if err != nil {
			errors = append(errors, err)
			continue
		}
		if del {
			deleted = append(deleted, *current)
		}
	}
	return deleted, errors
}
