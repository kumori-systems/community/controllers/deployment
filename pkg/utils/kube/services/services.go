/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package services

import (
	"context"
	"fmt"

	"k8s.io/apimachinery/pkg/api/errors"

	apiequality "k8s.io/apimachinery/pkg/api/equality"

	utils "kuku-controller/pkg/utils"

	log "github.com/sirupsen/logrus"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	labels "k8s.io/apimachinery/pkg/labels"
	clientset "k8s.io/client-go/kubernetes"
	listers "k8s.io/client-go/listers/core/v1"
)

// Create is used to create a new configmap in a Kubernetes cluster using a client-go
// instance.
func Create(
	client clientset.Interface,
	service *v1.Service,
) (*v1.Service, error) {
	if service == nil {
		return nil, fmt.Errorf("Service is nil")
	}
	log.Debugf("services.Create. Service: %s", service.Name)
	ctx := context.TODO()
	options := metav1.CreateOptions{
		FieldManager: "kumori-kucontroller",
	}
	return client.CoreV1().Services(service.GetNamespace()).Create(ctx, service, options)
}

// Update is used to update an existing configmap in a Kubernetes cluster using a client-go
// instance.
func Update(
	client clientset.Interface,
	service *v1.Service,
) (*v1.Service, error) {
	if service == nil {
		return nil, fmt.Errorf("Service is nil")
	}
	log.Debugf("services.Update. Service %s", service.Name)
	ctx := context.TODO()
	options := metav1.UpdateOptions{
		FieldManager: "kumori-kucontroller",
	}
	return client.CoreV1().Services(service.GetNamespace()).Update(ctx, service, options)
}

// Delete is used to delete an existing configmap in a Kubernetes cluster using a client-go
// instance.
func Delete(
	client clientset.Interface,
	service *v1.Service,
) error {
	if service == nil {
		return fmt.Errorf("Service is nil")
	}
	log.Debugf("services.Delete. Service %s", service.Name)
	ctx := context.TODO()
	policy := metav1.DeletePropagationForeground
	options := metav1.DeleteOptions{
		PropagationPolicy: &policy,
	}
	return client.CoreV1().Services(service.GetNamespace()).Delete(ctx, service.GetName(), options)
}

// Get recovers from the Kumori cluster the current version of a Service, if any
func Get(
	lister listers.ServiceLister,
	service *v1.Service,
) (*v1.Service, error) {
	if service == nil {
		return nil, fmt.Errorf("Service is nil")
	}
	return lister.Services(service.GetNamespace()).Get(service.GetName())
}

// CreateOrUpdate checks if the given config map exists in the cluster and updates it
// if exists or creates it otherwirse. Returns also a flag indicating if the object has
// been created (true) or updated (false).
func CreateOrUpdate(
	client clientset.Interface,
	lister listers.ServiceLister,
	service *v1.Service,
) (*v1.Service, utils.Operation, error) {
	if service == nil {
		return nil, utils.NoneOperation, fmt.Errorf("Service is nil")
	}
	currentService, err := Get(lister, service)
	if errors.IsNotFound(err) {
		finalService, err := Create(client, service)
		return finalService, utils.CreatedOperation, err
	} else if err != nil {
		return service, utils.NoneOperation, err
	}
	currentService = currentService.DeepCopy()
	finalService, changed, err := Apply(client, service, currentService)
	if changed {
		return finalService, utils.UpdatedOperation, err
	}

	return finalService, utils.NoneOperation, err
}

// CreateOrUpdateList creates or updates a list of config maps in the cluster. Returns
// the list of created config maps, the list of updated config maps and the list of errors
// found during the process.
func CreateOrUpdateList(
	client clientset.Interface,
	lister listers.ServiceLister,
	serviceList []v1.Service,
) (created []v1.Service, updated []v1.Service, errors []error) {
	listLength := len(serviceList)
	errors = make([]error, 0, 3)
	created = make([]v1.Service, 0, listLength)
	updated = make([]v1.Service, 0, listLength)
	if listLength <= 0 {
		return
	}
	for _, service := range serviceList {
		newService, operation, err := CreateOrUpdate(client, lister, &service)
		if err != nil {
			errors = append(errors, err)
			continue
		}
		if operation == utils.NoneOperation {
			continue
		}
		if operation == utils.CreatedOperation {
			created = append(created, *newService)
			continue
		}
		if operation == utils.UpdatedOperation {
			updated = append(updated, *newService)
			continue
		}

		errors = append(errors, fmt.Errorf("Unexpected applied operation %s for service %s. Expected operations are %s or %s", operation, service.GetName(), utils.CreatedOperation, utils.UpdatedOperation))
	}
	return
}

// CreateUpdateOrDeleteList receives a list of services and creates all elements in that
// list not currently part of the cluster state, updates those which are part of the cluster
// state but are outdated and deletes those currently existing in the cluster state but not
// in the list
func CreateUpdateOrDeleteList(
	client clientset.Interface,
	lister listers.ServiceLister,
	servicesList []v1.Service,
	selector labels.Selector,
	namespace string,
) (
	created []v1.Service,
	updated []v1.Service,
	deleted []v1.Service,
	errors []error,
) {
	created, updated, errors = CreateOrUpdateList(client, lister, servicesList)
	currentServicesList, err := List(lister, selector, namespace)
	if err != nil {
		errors = append(errors, err)
		deleted = []v1.Service{}
		return
	}
	if (currentServicesList != nil) && (len(currentServicesList) > 0) {
		var errs []error
		deleted, errs = DeleteMissing(client, currentServicesList, servicesList)
		if (errs != nil) && (len(errs) > 0) {
			errors = append(errors, errs...)
		}
	} else {
		deleted = []v1.Service{}
	}
	return
}

// List returns the services in a given namespace matching a set of labels.
func List(
	lister listers.ServiceLister,
	selector labels.Selector,
	namespace string,
) ([]*v1.Service, error) {
	return lister.Services(namespace).List(selector)
}

// Apply updates "to" configmap with "from" values if any change is detected. The changes
// are also applied to a kubernetes cluster
func Apply(
	client clientset.Interface,
	from *v1.Service,
	to *v1.Service,
) (*v1.Service, bool, error) {

	if from == nil {
		return to, false, fmt.Errorf("From service is null")
	}

	if to == nil {
		return to, false, fmt.Errorf("From service is null")
	}

	changed := false

	if !apiequality.Semantic.DeepEqual(from.OwnerReferences, to.OwnerReferences) {
		log.Debugf("services.Apply. Owner References differ in %s", from.GetName())
		changed = true
		to.OwnerReferences = from.OwnerReferences
	}

	// Labels changed if the new version of the Service object
	// contains labels not included in the old version or included with
	// a different value
	for key, newValue := range (*from).GetLabels() {
		if currentValue, ok := to.Labels[key]; !ok || (currentValue != newValue) {
			log.Debugf("services.Apply. Label %s differ in %s", key, from.GetName())
			changed = true
			to.Labels[key] = newValue
		}
	}

	// Labels change if a link labeled has disappeared in the new version
	// of the object.
	for key, currentValue := range (*to).GetLabels() {
		if currentValue != "link-in-use" {
			continue
		}
		if _, ok := (*from).Labels[key]; !ok {
			log.Debugf("services.Apply. Link-in-use label %s differ in %s", key, from.GetName())
			changed = true
			delete(to.Labels, key)
		}
	}

	for key, newValue := range (*from).GetAnnotations() {
		if currentValue, ok := to.Annotations[key]; !ok || (currentValue != newValue) {
			log.Debugf("services.Apply. Annotations differ in %s", from.GetName())
			changed = true
			if to.Annotations == nil {
				to.Annotations = map[string]string{}
			}
			to.Annotations[key] = newValue
		}
	}

	// If the ClusterIP is "" then Kubernetes will assign an IP. Then, if the
	// expected ClusterIP == "" and the current ClusterIP has an IP assigned,
	// everything is OK. The problem comes if both have a value different than
	// "" and that value is different.
	if (from.Spec.ClusterIP != "") && (to.Spec.ClusterIP != "") && (from.Spec.ClusterIP != to.Spec.ClusterIP) {
		log.Debugf("services.Apply. ClusterIP differ in %s", from.GetName())
		to.Spec.ClusterIP = from.Spec.ClusterIP
		changed = true
	}

	if from.Spec.Type != to.Spec.Type {
		log.Debugf("services.Apply. Type differ in %s", from.GetName())
		changed = true
		to.Spec.Type = from.Spec.Type
	}

	if from.Spec.PublishNotReadyAddresses != to.Spec.PublishNotReadyAddresses {
		log.Debugf("services.Apply. PublishNotReadyAddresses differ in %s", from.GetName())
		changed = true
		to.Spec.PublishNotReadyAddresses = from.Spec.PublishNotReadyAddresses
	}

	if !apiequality.Semantic.DeepEqual(from.Spec.Ports, to.Spec.Ports) {
		log.Debugf("services.Apply. Ports differ\n'%v'\n'%v'\n", from.Spec.Ports, to.Spec.Ports)
		// log.Debugf("services.Apply. Ports differ in %s", from.GetName())
		changed = true
		to.Spec.Ports = from.Spec.Ports
	}

	for key, newValue := range from.Spec.Selector {
		if currentValue, ok := to.Spec.Selector[key]; !ok || (currentValue != newValue) {
			log.Debugf("services.Apply. Selector label %s differ in %s", key, from.GetName())
			changed = true
			to.Spec.Selector[key] = newValue
		}
	}

	for key := range to.Spec.Selector {
		if _, ok := from.Spec.Selector[key]; !ok {
			log.Debugf("services.Apply. Selector label %s deleted in %s", key, to.GetName())
			changed = true
			delete(to.Spec.Selector, key)
		}
	}

	if changed {
		newService, err := Update(client, to)
		return newService, changed, err
	}

	return to, false, nil
}

// DeleteIfMissing deletes a service in a cluster if cannot be found in a list of services.
// Returns true if the service has been deleted and false otherwise
func DeleteIfMissing(
	client clientset.Interface,
	service *v1.Service,
	expectedList []v1.Service,
) (bool, error) {
	if service == nil {
		return false, fmt.Errorf("Service is nil")
	}
	for _, expectedService := range expectedList {
		if (service.GetNamespace() == expectedService.GetNamespace()) &&
			(service.GetName() == expectedService.GetName()) {
			return false, nil
		}
	}
	if err := Delete(client, service); err != nil {
		return false, err
	}
	return true, nil
}

// DeleteMissing deletes from currentList all Services not found in existingList.
// A Serviceis in existingList if there is an element in that list with the same name
// and namespace.
func DeleteMissing(
	client clientset.Interface,
	currentList []*v1.Service,
	expectedList []v1.Service,
) (deleted []v1.Service, errors []error) {
	deleted = make([]v1.Service, 0, len(currentList))
	errors = make([]error, 0, len(currentList))
	for _, current := range currentList {
		if current == nil {
			continue
		}
		del, err := DeleteIfMissing(client, current, expectedList)
		if err != nil {
			errors = append(errors, err)
			continue
		}
		if del {
			deleted = append(deleted, *current)
		}
	}
	return deleted, errors
}
