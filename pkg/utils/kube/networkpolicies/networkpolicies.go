/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package networkpolicies

import (
	"context"
	"fmt"

	"k8s.io/apimachinery/pkg/api/errors"

	apiequality "k8s.io/apimachinery/pkg/api/equality"

	utils "kuku-controller/pkg/utils"

	log "github.com/sirupsen/logrus"
	v1 "k8s.io/api/networking/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	labels "k8s.io/apimachinery/pkg/labels"
	clientset "k8s.io/client-go/kubernetes"
	listers "k8s.io/client-go/listers/networking/v1"
)

// Create is used to create a new configmap in a Kubernetes cluster using a client-go
// instance.
func Create(
	client clientset.Interface,
	networkPolicy *v1.NetworkPolicy,
) (*v1.NetworkPolicy, error) {
	if networkPolicy == nil {
		return nil, fmt.Errorf("Network policy is nil")
	}
	log.Debugf("networkpolicies.Create. NetworkPolicy: %s", networkPolicy.Name)
	ctx := context.TODO()
	options := metav1.CreateOptions{
		FieldManager: "kumori-kucontroller",
	}
	return client.NetworkingV1().NetworkPolicies(networkPolicy.GetNamespace()).Create(ctx, networkPolicy, options)
}

// Update is used to update an existing configmap in a Kubernetes cluster using a client-go
// instance.
func Update(
	client clientset.Interface,
	networkPolicy *v1.NetworkPolicy,
) (*v1.NetworkPolicy, error) {
	if networkPolicy == nil {
		return nil, fmt.Errorf("Network policy is nil")
	}
	log.Debugf("networkpolicies.Update. NetworkPolicy: %s", networkPolicy.Name)
	ctx := context.TODO()
	options := metav1.UpdateOptions{
		FieldManager: "kumori-kucontroller",
	}
	return client.NetworkingV1().NetworkPolicies(networkPolicy.GetNamespace()).Update(ctx, networkPolicy, options)
}

// Delete is used to delete an existing configmap in a Kubernetes cluster using a client-go
// instance.
func Delete(
	client clientset.Interface,
	networkPolicy *v1.NetworkPolicy,
) error {
	if networkPolicy == nil {
		return fmt.Errorf("Network policy is nil")
	}
	log.Debugf("networkpolicies.Delete. NetworkPolicy: %s", networkPolicy.Name)
	ctx := context.TODO()
	policy := metav1.DeletePropagationForeground
	options := metav1.DeleteOptions{
		PropagationPolicy: &policy,
	}
	return client.NetworkingV1().NetworkPolicies(networkPolicy.GetNamespace()).Delete(ctx, networkPolicy.GetName(), options)
}

// Get recovers from the Kumori cluster the current version of a NetworkPolicy, if any
func Get(
	lister listers.NetworkPolicyLister,
	networkPolicy *v1.NetworkPolicy,
) (*v1.NetworkPolicy, error) {
	if networkPolicy == nil {
		return nil, fmt.Errorf("Network policy is nil")
	}
	return lister.NetworkPolicies(networkPolicy.GetNamespace()).Get(networkPolicy.GetName())
}

// CreateOrUpdate checks if the given config map exists in the cluster and updates it
// if exists or creates it otherwirse. Returns also a flag indicating if the object has
// been created (true) or updated (false).
func CreateOrUpdate(
	client clientset.Interface,
	lister listers.NetworkPolicyLister,
	networkPolicy *v1.NetworkPolicy,
) (*v1.NetworkPolicy, utils.Operation, error) {
	if networkPolicy == nil {
		return nil, utils.NoneOperation, fmt.Errorf("Network policy is nil")
	}
	currentNetworkPolicy, err := Get(lister, networkPolicy)
	if errors.IsNotFound(err) {
		finalNetworkPolicy, err := Create(client, networkPolicy)
		return finalNetworkPolicy, utils.CreatedOperation, err
	} else if err != nil {
		return networkPolicy, utils.NoneOperation, err
	}
	currentNetworkPolicy = currentNetworkPolicy.DeepCopy()
	finalNetworkPolicy, changed, err := Apply(client, networkPolicy, currentNetworkPolicy)
	if changed {
		return finalNetworkPolicy, utils.UpdatedOperation, err
	}

	return finalNetworkPolicy, utils.NoneOperation, err
}

// CreateOrUpdateList creates or updates a list of config maps in the cluster. Returns
// the list of created config maps, the list of updated config maps and the list of errors
// found during the process.
func CreateOrUpdateList(
	client clientset.Interface,
	lister listers.NetworkPolicyLister,
	networkPolicyList []v1.NetworkPolicy,
) (created []v1.NetworkPolicy, updated []v1.NetworkPolicy, errors []error) {
	listLength := len(networkPolicyList)
	errors = make([]error, 0, 3)
	created = make([]v1.NetworkPolicy, 0, listLength)
	updated = make([]v1.NetworkPolicy, 0, listLength)
	if listLength <= 0 {
		return
	}
	for _, networkPolicy := range networkPolicyList {
		newNetworkPolicy, operation, err := CreateOrUpdate(client, lister, &networkPolicy)
		if err != nil {
			errors = append(errors, err)
			continue
		}
		if operation == utils.NoneOperation {
			continue
		}
		if operation == utils.CreatedOperation {
			created = append(created, *newNetworkPolicy)
			continue
		}
		if operation == utils.UpdatedOperation {
			updated = append(updated, *newNetworkPolicy)
			continue
		}
		errors = append(errors, fmt.Errorf("Unexpected applied operation %s for networkPolicy %s. Expected operations are %s or %s", operation, networkPolicy.GetName(), utils.CreatedOperation, utils.UpdatedOperation))
	}
	return
}

// CreateUpdateOrDeleteList receives a list of networkPolicies and creates all elements in that
// list not currently part of the cluster state, updates those which are part of the cluster
// state but are outdated and deletes those currently existing in the cluster state but not
// in the list
func CreateUpdateOrDeleteList(
	client clientset.Interface,
	lister listers.NetworkPolicyLister,
	networkPoliciesList []v1.NetworkPolicy,
	selector labels.Selector,
	namespace string,
) (
	created []v1.NetworkPolicy,
	updated []v1.NetworkPolicy,
	deleted []v1.NetworkPolicy,
	errors []error,
) {
	created, updated, errors = CreateOrUpdateList(client, lister, networkPoliciesList)
	currentNetworkPoliciesList, err := lister.NetworkPolicies(namespace).List(selector)
	if err != nil {
		errors = append(errors, err)
		deleted = []v1.NetworkPolicy{}
		return
	}
	if (currentNetworkPoliciesList != nil) && (len(currentNetworkPoliciesList) > 0) {
		var errs []error
		deleted, errs = DeleteMissing(client, currentNetworkPoliciesList, networkPoliciesList)
		if (errs != nil) && (len(errs) > 0) {
			errors = append(errors, errs...)
		}
	} else {
		deleted = []v1.NetworkPolicy{}
	}
	return
}

// Apply updates "to" configmap with "from" values if any change is detected. The changes
// are also applied to a kubernetes cluster
func Apply(
	client clientset.Interface,
	from *v1.NetworkPolicy,
	to *v1.NetworkPolicy,
) (*v1.NetworkPolicy, bool, error) {

	if from == nil {
		return to, false, fmt.Errorf("From config map is null")
	}

	if to == nil {
		return to, false, fmt.Errorf("From config map is null")
	}

	changed := false

	if !apiequality.Semantic.DeepEqual(from.OwnerReferences, to.OwnerReferences) {
		log.Debugf("networkpolicies.Apply. Owner References differ in %s", from.GetName())
		changed = true
		to.OwnerReferences = from.OwnerReferences
	}

	for key, newValue := range (*from).GetLabels() {
		if currentValue, ok := to.Labels[key]; !ok || (currentValue != newValue) {
			log.Debugf("networkpolicies.Apply. Label %s differ in %s", key, from.GetName())
			changed = true
			to.Labels[key] = newValue
		}
	}

	for key, newValue := range (*from).GetAnnotations() {
		if currentValue, ok := to.Annotations[key]; !ok || (currentValue != newValue) {
			log.Debugf("networkpolicies.Apply. Annotation %s differ in %s", key, from.GetName())
			changed = true
			if to.Annotations == nil {
				to.Annotations = map[string]string{}
			}
			to.Annotations[key] = newValue
		}
	}

	if !apiequality.Semantic.DeepEqual(from.Spec, to.Spec) {
		log.Debugf("networkpolicies.Apply. Spec differ in %s", from.GetName())
		changed = true
		to.Spec = from.Spec
	}

	if changed {
		newNetworkPolicy, err := Update(client, to)
		return newNetworkPolicy, changed, err
	}

	return to, false, nil
}

// DeleteIfMissing deletes a networkPolicy in a cluster if cannot be found in a list of networkPolicies.
// Returns true if the networkPolicy has been deleted and false otherwise
func DeleteIfMissing(
	client clientset.Interface,
	networkPolicy *v1.NetworkPolicy,
	expectedList []v1.NetworkPolicy,
) (bool, error) {
	if networkPolicy == nil {
		return false, fmt.Errorf("Network policy is nil")
	}
	for _, expectedNetworkPolicy := range expectedList {
		if (networkPolicy.GetNamespace() == expectedNetworkPolicy.GetNamespace()) &&
			(networkPolicy.GetName() == expectedNetworkPolicy.GetName()) {
			return false, nil
		}
	}
	if err := Delete(client, networkPolicy); err != nil {
		return false, err
	}
	return true, nil
}

// DeleteMissing deletes from currentList all NetworkPolicies not found in existingList.
// A NetworkPolicyis in existingList if there is an element in that list with the same name
// and namespace.
func DeleteMissing(
	client clientset.Interface,
	currentList []*v1.NetworkPolicy,
	expectedList []v1.NetworkPolicy,
) (deleted []v1.NetworkPolicy, errors []error) {
	deleted = make([]v1.NetworkPolicy, 0, len(currentList))
	errors = make([]error, 0, len(currentList))
	for _, current := range currentList {
		if current == nil {
			continue
		}
		del, err := DeleteIfMissing(client, current, expectedList)
		if err != nil {
			errors = append(errors, err)
			continue
		}
		if del {
			deleted = append(deleted, *current)
		}
	}
	return deleted, errors
}
