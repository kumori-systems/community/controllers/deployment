/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package statefulsets

import (
	"context"
	"fmt"
	"reflect"

	"k8s.io/apimachinery/pkg/api/errors"

	apiequality "k8s.io/apimachinery/pkg/api/equality"

	utils "kuku-controller/pkg/utils"

	log "github.com/sirupsen/logrus"
	v1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	labels "k8s.io/apimachinery/pkg/labels"
	clientset "k8s.io/client-go/kubernetes"
	listers "k8s.io/client-go/listers/apps/v1"
)

// Create is used to create a new configmap in a Kubernetes cluster using a client-go
// instance.
func Create(
	client clientset.Interface,
	statefulSet *v1.StatefulSet,
) (*v1.StatefulSet, error) {
	if statefulSet == nil {
		return nil, fmt.Errorf("Stateful set is nil")
	}
	log.Debugf("statefulsets.Create. StatefulSet: %s", statefulSet.Name)
	ctx := context.TODO()
	options := metav1.CreateOptions{
		FieldManager: "kumori-kucontroller",
	}
	return client.AppsV1().StatefulSets(statefulSet.GetNamespace()).Create(ctx, statefulSet, options)
}

// Update is used to update an existing configmap in a Kubernetes cluster using a client-go
// instance.
func Update(
	client clientset.Interface,
	statefulSet *v1.StatefulSet,
) (*v1.StatefulSet, error) {
	if statefulSet == nil {
		return nil, fmt.Errorf("Stateful set is nil")
	}
	log.Debugf("statefulsets.Update. StatefulSet: %s", statefulSet.Name)
	ctx := context.TODO()
	options := metav1.UpdateOptions{
		FieldManager: "kumori-kucontroller",
	}
	return client.AppsV1().StatefulSets(statefulSet.GetNamespace()).Update(ctx, statefulSet, options)
}

// Delete is used to delete an existing configmap in a Kubernetes cluster using a client-go
// instance.
func Delete(
	client clientset.Interface,
	statefulSet *v1.StatefulSet,
) error {
	if statefulSet == nil {
		return fmt.Errorf("Stateful set is nil")
	}
	log.Debugf("statefulsets.Delete. StatefulSet: %s", statefulSet.Name)
	ctx := context.TODO()
	policy := metav1.DeletePropagationForeground
	options := metav1.DeleteOptions{
		PropagationPolicy: &policy,
	}
	return client.AppsV1().StatefulSets(statefulSet.GetNamespace()).Delete(ctx, statefulSet.GetName(), options)
}

// Get recovers from the Kumori cluster the current version of a StatefulSet, if any
func Get(
	lister listers.StatefulSetLister,
	statefulSet *v1.StatefulSet,
) (*v1.StatefulSet, error) {
	if statefulSet == nil {
		return nil, fmt.Errorf("Stateful set is nil")
	}
	return lister.StatefulSets(statefulSet.GetNamespace()).Get(statefulSet.GetName())
}

// CreateOrUpdate checks if the given config map exists in the cluster and updates it
// if exists or creates it otherwirse. Returns also a flag indicating if the object has
// been created (true) or updated (false).
func CreateOrUpdate(
	client clientset.Interface,
	lister listers.StatefulSetLister,
	statefulSet *v1.StatefulSet,
) (*v1.StatefulSet, utils.Operation, error) {
	if statefulSet == nil {
		return nil, utils.NoneOperation, fmt.Errorf("Stateful set is nil")
	}
	currentStatefulSet, err := Get(lister, statefulSet)
	if errors.IsNotFound(err) {
		finalStatefulSet, err := Create(client, statefulSet)
		return finalStatefulSet, utils.CreatedOperation, err
	} else if err != nil {
		return statefulSet, utils.NoneOperation, err
	}
	currentStatefulSet = currentStatefulSet.DeepCopy()
	finalStatefulSet, changed, err := Apply(client, statefulSet, currentStatefulSet)
	if changed {
		return finalStatefulSet, utils.UpdatedOperation, err
	}

	return finalStatefulSet, utils.NoneOperation, err
}

// CreateOrUpdateList creates or updates a list of config maps in the cluster. Returns
// the list of created config maps, the list of updated config maps and the list of errors
// found during the process.
func CreateOrUpdateList(
	client clientset.Interface,
	lister listers.StatefulSetLister,
	statefulSetList []v1.StatefulSet,
) (created []v1.StatefulSet, updated []v1.StatefulSet, errors []error) {
	listLength := len(statefulSetList)
	errors = make([]error, 0, 3)
	created = make([]v1.StatefulSet, 0, listLength)
	updated = make([]v1.StatefulSet, 0, listLength)
	if listLength <= 0 {
		return
	}
	for _, statefulSet := range statefulSetList {
		newStatefulSet, operation, err := CreateOrUpdate(client, lister, &statefulSet)
		if err != nil {
			errors = append(errors, err)
			continue
		}
		if operation == utils.NoneOperation {
			continue
		}
		if operation == utils.CreatedOperation {
			created = append(created, *newStatefulSet)
			continue
		}
		if operation == utils.UpdatedOperation {
			updated = append(updated, *newStatefulSet)
			continue
		}

		errors = append(errors, fmt.Errorf("Unexpected applied operation %s for statefulSet %s. Expected operations are %s or %s", operation, statefulSet.GetName(), utils.CreatedOperation, utils.UpdatedOperation))
	}
	return
}

// CreateUpdateOrDeleteList receives a list of statefulSets and creates all elements in that
// list not currently part of the cluster state, updates those which are part of the cluster
// state but are outdated and deletes those currently existing in the cluster state but not
// in the list
func CreateUpdateOrDeleteList(
	client clientset.Interface,
	lister listers.StatefulSetLister,
	statefulSetsList []v1.StatefulSet,
	selector labels.Selector,
	namespace string,
) (
	created []v1.StatefulSet,
	updated []v1.StatefulSet,
	deleted []v1.StatefulSet,
	errors []error,
) {
	created, updated, errors = CreateOrUpdateList(client, lister, statefulSetsList)
	currentStatefulSetsList, err := lister.StatefulSets(namespace).List(selector)
	if err != nil {
		errors = append(errors, err)
		deleted = []v1.StatefulSet{}
		return
	}
	if (currentStatefulSetsList != nil) && (len(currentStatefulSetsList) > 0) {
		var errs []error
		deleted, errs = DeleteMissing(client, currentStatefulSetsList, statefulSetsList)
		if (errs != nil) && (len(errs) > 0) {
			errors = append(errors, errs...)
		}
	} else {
		deleted = []v1.StatefulSet{}
	}
	return
}

// Apply updates "to" configmap with "from" values if any change is detected. The changes
// are also applied to a kubernetes cluster
func Apply(
	client clientset.Interface,
	from *v1.StatefulSet,
	to *v1.StatefulSet,
) (*v1.StatefulSet, bool, error) {

	if from == nil {
		return to, false, fmt.Errorf("From config map is null")
	}

	if to == nil {
		return to, false, fmt.Errorf("From config map is null")
	}

	changed := false

	if !apiequality.Semantic.DeepEqual(from.OwnerReferences, to.OwnerReferences) {
		log.Debugf("statefulSets.Apply. Owner References differ in %s", from.GetName())
		changed = true
		to.OwnerReferences = from.OwnerReferences
	}

	for key, newValue := range from.GetLabels() {
		if currentValue, ok := to.Labels[key]; !ok || (currentValue != newValue) {
			log.Debugf("statefulSets.Apply. Label %s differ in %s", key, from.GetName())
			changed = true
			to.Labels[key] = newValue
		}
	}

	for key, newValue := range from.GetAnnotations() {
		if currentValue, ok := to.Annotations[key]; !ok || (currentValue != newValue) {
			log.Debugf("statefulSets.Apply. Label %s differ in %s", key, from.GetName())
			changed = true
			to.Annotations[key] = newValue
		}
	}

	for key, newValue := range from.Spec.Template.GetAnnotations() {
		if to.Spec.Template.Annotations == nil {
			to.Spec.Template.Annotations = map[string]string{}
		}
		if currentValue, ok := to.Spec.Template.Annotations[key]; !ok || (currentValue != newValue) {
			log.Debugf("statefulSets.Apply. Template annotation %s differ in %s", key, from.GetName())
			changed = true
			if to.Spec.Template.Annotations == nil {
				to.Spec.Template.Annotations = map[string]string{}
			}
			to.Spec.Template.Annotations[key] = newValue
		}
	}

	autoscaled := utils.IsAutoscaled(&from.ObjectMeta)
	if !autoscaled {
		if *from.Spec.Replicas != *to.Spec.Replicas {
			log.Debugf("statefulSets.Apply. Number of replicas differ in %s. From %d to %d", from.GetName(), *from.Spec.Replicas, *to.Spec.Replicas)
			changed = true
			to.Spec.Replicas = from.Spec.Replicas
		}
	}

	if !apiequality.Semantic.DeepEqual(from.Spec.Selector, to.Spec.Selector) {
		log.Debugf("statefulSets.Apply. Selector differ in %s", from.GetName())
		changed = true
		to.Spec.Selector = from.Spec.Selector
	}

	for key, newValue := range from.Spec.Template.GetLabels() {
		if currentValue, ok := to.Spec.Template.Labels[key]; !ok || (currentValue != newValue) {
			log.Debugf("statefulSets.Apply. Template label %s differ in %s", key, from.GetName())
			changed = true
			to.Spec.Template.Labels[key] = newValue
		}
	}

	areEqual, err := utils.ContainersAreEqual(from.Spec.Template.Spec.Containers, to.Spec.Template.Spec.Containers)
	if err != nil {
		log.Errorf("statefulSets.Apply. Error comparing containers: %v", err)
	}
	if !areEqual {
		log.Debugf("statefulSets.Apply. Containers differ in %s", from.GetName())
		to.Spec.Template.Spec.Containers = from.Spec.Template.Spec.Containers
		changed = true
	}

	areEqual, err = utils.ContainersAreEqual(from.Spec.Template.Spec.InitContainers, to.Spec.Template.Spec.InitContainers)
	if err != nil {
		log.Errorf("statefulSets.Apply. Error comparing init containers: %v", err)
	}
	if !areEqual {
		log.Debugf("statefulSets.Apply. Init containers differ in %s", from.GetName())
		to.Spec.Template.Spec.InitContainers = from.Spec.Template.Spec.InitContainers
		changed = true
	}

	if !statefulSetVolumesClaimsAreEqual(from, to) {
		log.Debugf("statefulSets.Apply. Volume Claims differ in %s", from.GetName())
		to.Spec.VolumeClaimTemplates = from.Spec.VolumeClaimTemplates
		changed = true
	}

	if !apiequality.Semantic.DeepEqual(from.Spec.Template.Spec.HostAliases, to.Spec.Template.Spec.HostAliases) {
		log.Debugf("statefulSets.Apply. Template HostAliases differs in %s", from.GetName())
		to.Spec.Template.Spec.HostAliases = from.Spec.Template.Spec.HostAliases
		changed = true
	}

	if !apiequality.Semantic.DeepEqual(from.Spec.Template.Spec.ImagePullSecrets, to.Spec.Template.Spec.ImagePullSecrets) {
		log.Debugf("statefulSets.Apply. Template ImagePullSecrets differs in %s", from.GetName())
		to.Spec.Template.Spec.ImagePullSecrets = from.Spec.Template.Spec.ImagePullSecrets
		changed = true
	}

	if !apiequality.Semantic.DeepEqual(from.Spec.Template.Spec.DNSConfig, to.Spec.Template.Spec.DNSConfig) {
		log.Debugf("statefulSets.Apply. Template DNSConfig differs in %s", from.GetName())
		to.Spec.Template.Spec.DNSConfig = from.Spec.Template.Spec.DNSConfig
		changed = true
	}

	if !apiequality.Semantic.DeepEqual(from.Spec.Template.Spec.TopologySpreadConstraints, to.Spec.Template.Spec.TopologySpreadConstraints) {
		log.Debugf("statefulSets.Apply. Template TopologySpreadConstraints differs in %s", from.GetName())
		to.Spec.Template.Spec.TopologySpreadConstraints = from.Spec.Template.Spec.TopologySpreadConstraints
		changed = true
	}

	if !apiequality.Semantic.DeepEqual(from.Spec.Template.Spec.Volumes, to.Spec.Template.Spec.Volumes) {
		log.Debugf("statefulSets.Apply. Template volumes differs in %s", from.GetName())
		// content, _ := json.MarshalIndent(from.Spec.Template.Spec.Volumes, "", "  ")
		// fmt.Printf("\n\n------->FROM VOLUMES: %s\n", string(content))
		// content, _ = json.MarshalIndent(to.Spec.Template.Spec.Volumes, "", "  ")
		// fmt.Printf("\n\n------->TO VOLUMES: %s\n", string(content))
		to.Spec.Template.Spec.Volumes = from.Spec.Template.Spec.Volumes
		changed = true
	}

	if !apiequality.Semantic.DeepEqual(from.Spec.Template.Spec.SecurityContext, to.Spec.Template.Spec.SecurityContext) {
		log.Debugf("deployments.Apply. Pod security context differs in %s", from.GetName())
		to.Spec.Template.Spec.SecurityContext = from.Spec.Template.Spec.SecurityContext
		changed = true
	}

	if from.Spec.Template.Spec.PriorityClassName != to.Spec.Template.Spec.PriorityClassName {
		log.Debugf("statefulSets.Apply. Priority class differ in %s. From %s to %s", from.GetName(), from.Spec.Template.Spec.PriorityClassName, to.Spec.Template.Spec.PriorityClassName)
		changed = true
		to.Spec.Template.Spec.PriorityClassName = from.Spec.Template.Spec.PriorityClassName
	}

	if changed {
		newStatefulSet, err := Update(client, to)
		return newStatefulSet, changed, err
	}

	return to, false, nil
}

// DeleteIfMissing deletes a statefulSet in a cluster if cannot be found in a list of statefulSets.
// Returns true if the statefulSet has been deleted and false otherwise
func DeleteIfMissing(
	client clientset.Interface,
	statefulSet *v1.StatefulSet,
	expectedList []v1.StatefulSet,
) (bool, error) {
	if statefulSet == nil {
		return false, fmt.Errorf("Stateful set is nil")
	}
	for _, expectedStatefulSet := range expectedList {
		if (statefulSet.GetNamespace() == expectedStatefulSet.GetNamespace()) &&
			(statefulSet.GetName() == expectedStatefulSet.GetName()) {
			return false, nil
		}
	}
	if err := Delete(client, statefulSet); err != nil {
		return false, err
	}
	return true, nil
}

// DeleteMissing deletes from currentList all StatefulSets not found in existingList.
// A StatefulSetis in existingList if there is an element in that list with the same name
// and namespace.
func DeleteMissing(
	client clientset.Interface,
	currentList []*v1.StatefulSet,
	expectedList []v1.StatefulSet,
) (deleted []v1.StatefulSet, errors []error) {
	deleted = make([]v1.StatefulSet, 0, len(currentList))
	errors = make([]error, 0, len(currentList))
	for _, current := range currentList {
		if current == nil {
			continue
		}
		del, err := DeleteIfMissing(client, current, expectedList)
		if err != nil {
			errors = append(errors, err)
			continue
		}
		if del {
			deleted = append(deleted, *current)
		}
	}
	return deleted, errors
}

func statefulSetVolumesClaimsAreEqual(from *v1.StatefulSet, to *v1.StatefulSet) bool {

	equalFunc := func(a, b any) bool {
		fromVolume, ok := a.(corev1.PersistentVolumeClaim)
		if !ok {
			log.Errorf("statefulSets.statefulSetVolumesClaimsAreEqual. Parameter is of type %v a v1.PersistentVolumeClaim", reflect.TypeOf(from))
			return false
		}
		toVolume, ok := b.(corev1.PersistentVolumeClaim)
		if !ok {
			log.Errorf("statefulSets.statefulSetVolumesClaimsAreEqual. Parameter is of type %v a v1.PersistentVolumeClaim", reflect.TypeOf(to))
			return false
		}
		if (fromVolume.GetName() != toVolume.GetName()) ||
			(fromVolume.GetNamespace() != toVolume.GetNamespace()) ||
			!utils.SameElements(fromVolume.GetOwnerReferences(), toVolume.GetOwnerReferences()) ||
			!apiequality.Semantic.DeepEqual(fromVolume.GetLabels(), toVolume.GetLabels()) ||
			(*fromVolume.Spec.StorageClassName != *toVolume.Spec.StorageClassName) ||
			!utils.SameElements(fromVolume.Spec.AccessModes, toVolume.Spec.AccessModes) ||
			!apiequality.Semantic.DeepEqual(fromVolume.Spec.Resources, toVolume.Spec.Resources) {
			return false
		}
		return true
	}

	return utils.SameElementsCustom(from.Spec.VolumeClaimTemplates, to.Spec.VolumeClaimTemplates, equalFunc)
}
