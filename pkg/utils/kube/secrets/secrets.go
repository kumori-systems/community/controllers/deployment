/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package secrets

import (
	"context"
	"fmt"

	apiequality "k8s.io/apimachinery/pkg/api/equality"

	"k8s.io/apimachinery/pkg/api/errors"

	utils "kuku-controller/pkg/utils"

	log "github.com/sirupsen/logrus"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	labels "k8s.io/apimachinery/pkg/labels"
	clientset "k8s.io/client-go/kubernetes"
	listers "k8s.io/client-go/listers/core/v1"
)

// Create is used to create a new secret in a Kubernetes cluster using a client-go
// instance.
func Create(
	client clientset.Interface,
	secret *v1.Secret,
) (*v1.Secret, error) {
	if secret == nil {
		return nil, fmt.Errorf("Config map is nil")
	}
	log.Debugf("secrets.Create. Secret: %s", secret.Name)
	ctx := context.TODO()
	options := metav1.CreateOptions{
		FieldManager: "kumori-kucontroller",
	}
	return client.CoreV1().Secrets(secret.GetNamespace()).Create(ctx, secret, options)
}

// Update is used to update an existing secret in a Kubernetes cluster using a client-go
// instance.
func Update(
	client clientset.Interface,
	secret *v1.Secret,
) (*v1.Secret, error) {
	if secret == nil {
		return nil, fmt.Errorf("Config map is nil")
	}
	log.Debugf("secrets.Update. Secret: %s", secret.Name)
	ctx := context.TODO()
	options := metav1.UpdateOptions{
		FieldManager: "kumori-kucontroller",
	}
	return client.CoreV1().Secrets(secret.GetNamespace()).Update(ctx, secret, options)
}

// Delete is used to delete an existing secret in a Kubernetes cluster using a client-go
// instance.
func Delete(
	client clientset.Interface,
	secret *v1.Secret,
) error {
	if secret == nil {
		return fmt.Errorf("Config map is nil")
	}
	log.Debugf("secrets.Delete. Secret: %s", secret.Name)
	ctx := context.TODO()
	policy := metav1.DeletePropagationForeground
	options := metav1.DeleteOptions{
		PropagationPolicy: &policy,
	}
	return client.CoreV1().Secrets(secret.GetNamespace()).Delete(ctx, secret.GetName(), options)
}

// Get recovers from the Kumori cluster the current version of a Secret, if any
func Get(
	lister listers.SecretLister,
	secret *v1.Secret,
) (*v1.Secret, error) {
	if secret == nil {
		return nil, fmt.Errorf("Config map is nil")
	}
	return lister.Secrets(secret.GetNamespace()).Get(secret.GetName())
}

// CreateOrUpdate checks if the given secret exists in the cluster and updates it
// if exists or creates it otherwirse. Returns also a flag indicating if the object has
// been created (true) or updated (false).
func CreateOrUpdate(
	client clientset.Interface,
	lister listers.SecretLister,
	secret *v1.Secret,
) (*v1.Secret, utils.Operation, error) {
	if secret == nil {
		return nil, utils.NoneOperation, fmt.Errorf("Config map is nil")
	}
	currentSecret, err := Get(lister, secret)
	if errors.IsNotFound(err) {
		finalSecret, err := Create(client, secret)
		return finalSecret, utils.CreatedOperation, err
	} else if err != nil {
		return secret, utils.NoneOperation, err
	}
	currentSecret = currentSecret.DeepCopy()
	finalSecret, changed, err := Apply(client, secret, currentSecret)
	if changed {
		return finalSecret, utils.UpdatedOperation, err
	}

	return finalSecret, utils.NoneOperation, err
}

// CreateOrUpdateList creates or updates a list of secrets in the cluster. Returns
// the list of created secrets, the list of updated secrets and the list of errors
// found during the process.
func CreateOrUpdateList(
	client clientset.Interface,
	lister listers.SecretLister,
	secretList []v1.Secret,
) (created []v1.Secret, updated []v1.Secret, errors []error) {
	listLength := len(secretList)
	errors = make([]error, 0, 3)
	created = make([]v1.Secret, 0, listLength)
	updated = make([]v1.Secret, 0, listLength)
	if listLength <= 0 {
		return
	}
	for _, secret := range secretList {
		newSecret, operation, err := CreateOrUpdate(client, lister, &secret)
		if err != nil {
			errors = append(errors, err)
			continue
		}
		if operation == utils.NoneOperation {
			continue
		}
		if operation == utils.CreatedOperation {
			created = append(created, *newSecret)
			continue
		}
		if operation == utils.UpdatedOperation {
			updated = append(updated, *newSecret)
			continue
		}

		errors = append(errors, fmt.Errorf("Unexpected applied operation %s for secret %s. Expected operations are %s or %s", operation, secret.GetName(), utils.CreatedOperation, utils.UpdatedOperation))
	}
	return
}

// CreateUpdateOrDeleteList receives a list of secrets and creates all elements in that
// list not currently part of the cluster state, updates those which are part of the cluster
// state but are outdated and deletes those currently existing in the cluster state but not
// in the list
func CreateUpdateOrDeleteList(
	client clientset.Interface,
	lister listers.SecretLister,
	secretsList []v1.Secret,
	selector labels.Selector,
	namespace string,
) (
	created []v1.Secret,
	updated []v1.Secret,
	deleted []v1.Secret,
	errors []error,
) {
	created, updated, errors = CreateOrUpdateList(client, lister, secretsList)
	currentSecretsList, err := lister.Secrets(namespace).List(selector)
	if err != nil {
		errors = append(errors, err)
		deleted = []v1.Secret{}
		return
	}
	if (currentSecretsList != nil) && (len(currentSecretsList) > 0) {
		var errs []error
		deleted, errs = DeleteMissing(client, currentSecretsList, secretsList)
		if (errs != nil) && (len(errs) > 0) {
			errors = append(errors, errs...)
		}
	} else {
		deleted = []v1.Secret{}
	}
	return
}

// Apply updates "to" secret with "from" values if any change is detected. The changes
// are also applied to a kubernetes cluster
func Apply(
	client clientset.Interface,
	from *v1.Secret,
	to *v1.Secret,
) (*v1.Secret, bool, error) {

	if from == nil {
		return to, false, fmt.Errorf("From secret is null")
	}

	if to == nil {
		return to, false, fmt.Errorf("To secret is null")
	}

	changed := false

	if !apiequality.Semantic.DeepEqual(from.OwnerReferences, to.OwnerReferences) {
		log.Debugf("secrets.Apply. Owner references differ for %s", from.GetName())
		changed = true
		to.OwnerReferences = from.OwnerReferences
	}

	for key, newValue := range (*from).GetLabels() {
		if currentValue, ok := to.Labels[key]; !ok || (currentValue != newValue) {
			log.Debugf("secrets.Apply. Label %s differ in %s", key, from.GetName())
			changed = true
			to.Labels[key] = newValue
		}
	}

	for key, newValue := range (*from).GetAnnotations() {
		if currentValue, ok := to.Annotations[key]; !ok || (currentValue != newValue) {
			log.Debugf("secrets.Apply. Annotation %s differ in %s", key, from.GetName())
			changed = true
			if to.Annotations == nil {
				to.Annotations = map[string]string{}
			}
			to.Annotations[key] = newValue
		}
	}

	if !apiequality.Semantic.DeepEqual(from.Data, to.Data) {
		log.Debugf("secrets.Apply. Data differ in %s", from.GetName())
		// content, _ := json.MarshalIndent(from.BinaryData, "", "  ")
		// fmt.Printf("\n\n------->FROM DATA: %s", string(content))
		// content, _ = json.MarshalIndent(to.BinaryData, "", "  ")
		// fmt.Printf("\n\n------->TO DATA: %s", string(content))
		changed = true
		to.Data = from.Data
	}

	if changed {
		newSecret, err := Update(client, to)
		return newSecret, changed, err
	}

	return to, false, nil
}

// DeleteIfMissing deletes a secret in a cluster if cannot be found in a list of secrets.
// Returns true if the secret has been deleted and false otherwise
func DeleteIfMissing(
	client clientset.Interface,
	secret *v1.Secret,
	expectedList []v1.Secret,
) (bool, error) {
	if secret == nil {
		return false, fmt.Errorf("Config map is nil")
	}
	for _, expectedSecret := range expectedList {
		if (secret.GetNamespace() == expectedSecret.GetNamespace()) &&
			(secret.GetName() == expectedSecret.GetName()) {
			return false, nil
		}
	}
	if err := Delete(client, secret); err != nil {
		return false, err
	}
	return true, nil
}

// DeleteMissing deletes from currentList all Secrets not found in existingList.
// A Secretis in existingList if there is an element in that list with the same name
// and namespace.
func DeleteMissing(
	client clientset.Interface,
	currentList []*v1.Secret,
	expectedList []v1.Secret,
) (deleted []v1.Secret, errors []error) {
	deleted = make([]v1.Secret, 0, len(currentList))
	errors = make([]error, 0, len(currentList))
	for _, current := range currentList {
		if current == nil {
			continue
		}
		del, err := DeleteIfMissing(client, current, expectedList)
		if err != nil {
			errors = append(errors, err)
			continue
		}
		if del {
			deleted = append(deleted, *current)
		}
	}
	return deleted, errors
}
