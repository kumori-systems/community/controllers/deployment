/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package deployments

import (
	"context"
	"fmt"

	"k8s.io/apimachinery/pkg/api/errors"

	apiequality "k8s.io/apimachinery/pkg/api/equality"

	utils "kuku-controller/pkg/utils"

	log "github.com/sirupsen/logrus"
	v1 "k8s.io/api/apps/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	labels "k8s.io/apimachinery/pkg/labels"
	clientset "k8s.io/client-go/kubernetes"
	listers "k8s.io/client-go/listers/apps/v1"
)

// Create is used to create a new configmap in a Kubernetes cluster using a client-go
// instance.
func Create(
	client clientset.Interface,
	deployment *v1.Deployment,
) (*v1.Deployment, error) {
	if deployment == nil {
		return nil, fmt.Errorf("Deployment is nil")
	}
	log.Debugf("deployments.Create. Deployment: %s", deployment.Name)
	ctx := context.TODO()
	options := metav1.CreateOptions{
		FieldManager: "kumori-kucontroller",
	}

	return client.AppsV1().Deployments(deployment.GetNamespace()).Create(ctx, deployment, options)
}

// Update is used to update an existing configmap in a Kubernetes cluster using a client-go
// instance.
func Update(
	client clientset.Interface,
	deployment *v1.Deployment,
) (*v1.Deployment, error) {
	if deployment == nil {
		return nil, fmt.Errorf("Deployment is nil")
	}
	log.Debugf("deployments.Update. Deployment: %s", deployment.Name)
	ctx := context.TODO()
	options := metav1.UpdateOptions{
		FieldManager: "kumori-kucontroller",
	}
	return client.AppsV1().Deployments(deployment.GetNamespace()).Update(ctx, deployment, options)
}

// Delete is used to delete an existing configmap in a Kubernetes cluster using a client-go
// instance.
func Delete(
	client clientset.Interface,
	deployment *v1.Deployment,
) error {
	if deployment == nil {
		return fmt.Errorf("Deployment is nil")
	}
	log.Debugf("deployments.Delete. Deployment: %s", deployment.Name)
	ctx := context.TODO()
	policy := metav1.DeletePropagationForeground
	options := metav1.DeleteOptions{
		PropagationPolicy: &policy,
	}
	return client.AppsV1().Deployments(deployment.GetNamespace()).Delete(ctx, deployment.GetName(), options)
}

// Get recovers from the Kumori cluster the current version of a Deployment, if any
func Get(
	lister listers.DeploymentLister,
	deployment *v1.Deployment,
) (*v1.Deployment, error) {
	if deployment == nil {
		return nil, fmt.Errorf("Deployment is nil")
	}
	return lister.Deployments(deployment.GetNamespace()).Get(deployment.GetName())
}

// CreateOrUpdate checks if the given deployments exists in the cluster and updates it
// if exists or creates it otherwirse. Returns also a flag indicating if the object has
// been created (true) or updated (false).
func CreateOrUpdate(
	client clientset.Interface,
	lister listers.DeploymentLister,
	deployment *v1.Deployment,
) (*v1.Deployment, utils.Operation, error) {
	if deployment == nil {
		return nil, utils.NoneOperation, fmt.Errorf("Deployment is nil")
	}
	currentDeployment, err := Get(lister, deployment)
	if errors.IsNotFound(err) {
		finalDeployment, err := Create(client, deployment)
		return finalDeployment, utils.CreatedOperation, err
	} else if err != nil {
		return deployment, utils.NoneOperation, err
	}
	currentDeployment = currentDeployment.DeepCopy()
	finalDeployment, changed, err := Apply(client, deployment, currentDeployment)
	if changed {
		return finalDeployment, utils.UpdatedOperation, err
	}

	return finalDeployment, utils.NoneOperation, err
}

// CreateOrUpdateList creates or updates a list of deployments in the cluster. Returns
// the list of created deployments, the list of updated deployments and the list of errors
// found during the process.
func CreateOrUpdateList(
	client clientset.Interface,
	lister listers.DeploymentLister,
	deploymentList []v1.Deployment,
) (created []v1.Deployment, updated []v1.Deployment, errors []error) {
	listLength := len(deploymentList)
	errors = make([]error, 0, 3)
	created = make([]v1.Deployment, 0, listLength)
	updated = make([]v1.Deployment, 0, listLength)
	if listLength <= 0 {
		return
	}
	for _, deployment := range deploymentList {
		newDeployment, operation, err := CreateOrUpdate(client, lister, &deployment)
		if err != nil {
			errors = append(errors, err)
			continue
		}
		if operation == utils.NoneOperation {
			continue
		}
		if operation == utils.CreatedOperation {
			created = append(created, *newDeployment)
			continue
		}
		if operation == utils.UpdatedOperation {
			updated = append(updated, *newDeployment)
			continue
		}

		errors = append(errors, fmt.Errorf("Unexpected applied operation %s for deployment %s. Expected operations are %s or %s", operation, deployment.GetName(), utils.CreatedOperation, utils.UpdatedOperation))
	}
	return
}

// CreateUpdateOrDeleteList receives a list of deployments and creates all elements in that
// list not currently part of the cluster state, updates those which are part of the cluster
// state but are outdated and deletes those currently existing in the cluster state but not
// in the list
func CreateUpdateOrDeleteList(
	client clientset.Interface,
	lister listers.DeploymentLister,
	deploymentsList []v1.Deployment,
	selector labels.Selector,
	namespace string,
) (
	created []v1.Deployment,
	updated []v1.Deployment,
	deleted []v1.Deployment,
	errors []error,
) {
	created, updated, errors = CreateOrUpdateList(client, lister, deploymentsList)
	currentDeploymentsList, err := lister.Deployments(namespace).List(selector)
	if err != nil {
		errors = append(errors, err)
		deleted = []v1.Deployment{}
		return
	}
	if (currentDeploymentsList != nil) && (len(currentDeploymentsList) > 0) {
		var errs []error
		deleted, errs = DeleteMissing(client, currentDeploymentsList, deploymentsList)
		if (errs != nil) && (len(errs) > 0) {
			errors = append(errors, errs...)
		}
	} else {
		deleted = []v1.Deployment{}
	}
	return
}

// Apply updates "to" configmap with "from" values if any change is detected. The changes
// are also applied to a kubernetes cluster
func Apply(
	client clientset.Interface,
	from *v1.Deployment,
	to *v1.Deployment,
) (*v1.Deployment, bool, error) {

	if from == nil {
		return to, false, fmt.Errorf("From config map is null")
	}

	if to == nil {
		return to, false, fmt.Errorf("From config map is null")
	}

	changed := false

	if !apiequality.Semantic.DeepEqual(from.OwnerReferences, to.OwnerReferences) {
		log.Debugf("deployments.Apply. Owner References differ in %s", from.GetName())
		changed = true
		to.OwnerReferences = from.OwnerReferences
	}

	for key, newValue := range from.GetLabels() {
		if currentValue, ok := to.Labels[key]; !ok || (currentValue != newValue) {
			log.Debugf("deployments.Apply. Label %s differ in %s", key, from.GetName())
			changed = true
			to.Labels[key] = newValue
		}
	}

	for key, newValue := range from.GetAnnotations() {
		if currentValue, ok := to.Annotations[key]; !ok || (currentValue != newValue) {
			log.Debugf("deployments.Apply. Annotation %s differ in %s", key, from.GetName())
			changed = true
			to.Annotations[key] = newValue
		}
	}

	autoscaled := utils.IsAutoscaled(&from.ObjectMeta)
	if !autoscaled {
		if *from.Spec.Replicas != *to.Spec.Replicas {
			log.Debugf("deployments.Apply. Number of replicas differ in %s. From %d to %d", from.GetName(), *from.Spec.Replicas, *to.Spec.Replicas)
			changed = true
			to.Spec.Replicas = from.Spec.Replicas
		}
	}

	if !apiequality.Semantic.DeepEqual(from.Spec.Selector, to.Spec.Selector) {
		log.Debugf("deployments.Apply. Selector differ in %s", from.GetName())
		changed = true
		to.Spec.Selector = from.Spec.Selector
	}

	for key, newValue := range from.Spec.Template.GetLabels() {
		if currentValue, ok := to.Spec.Template.Labels[key]; !ok || (currentValue != newValue) {
			log.Debugf("deployments.Apply. Template label %s differ in %s", key, from.GetName())
			changed = true
			to.Spec.Template.Labels[key] = newValue
		}
	}

	for key, newValue := range from.Spec.Template.GetAnnotations() {
		if to.Spec.Template.Annotations == nil {
			to.Spec.Template.Annotations = map[string]string{}
		}
		if currentValue, ok := to.Spec.Template.Annotations[key]; !ok || (currentValue != newValue) {
			log.Debugf("deployments.Apply. Template annotation %s differ in %s", key, from.GetName())
			changed = true
			if to.Spec.Template.Annotations == nil {
				to.Spec.Template.Annotations = map[string]string{}
			}
			to.Spec.Template.Annotations[key] = newValue
		}
	}

	areEqual, err := utils.ContainersAreEqual(from.Spec.Template.Spec.Containers, to.Spec.Template.Spec.Containers)
	if err != nil {
		log.Errorf("deployments.Apply. Error comparing containers: %v", err)
	}
	if !areEqual {
		log.Debugf("deployments.Apply. Containers differ in %s", from.GetName())
		to.Spec.Template.Spec.Containers = from.Spec.Template.Spec.Containers
		changed = true
	}

	areEqual, err = utils.ContainersAreEqual(from.Spec.Template.Spec.InitContainers, to.Spec.Template.Spec.InitContainers)
	if err != nil {
		log.Errorf("deployments.Apply. Error comparing init containers: %v", err)
	}
	if !areEqual {
		log.Debugf("deployments.Apply. Init containers differ in %s", from.GetName())
		to.Spec.Template.Spec.InitContainers = from.Spec.Template.Spec.InitContainers
		changed = true
	}

	if !apiequality.Semantic.DeepEqual(from.Spec.Template.Spec.HostAliases, to.Spec.Template.Spec.HostAliases) {
		log.Debugf("deployments.Apply. Template HostAliases differs in %s", from.GetName())
		to.Spec.Template.Spec.HostAliases = from.Spec.Template.Spec.HostAliases
		changed = true
	}

	if !apiequality.Semantic.DeepEqual(from.Spec.Template.Spec.ImagePullSecrets, to.Spec.Template.Spec.ImagePullSecrets) {
		log.Debugf("deployments.Apply. Template ImagePullSecrets differs in %s", from.GetName())
		to.Spec.Template.Spec.ImagePullSecrets = from.Spec.Template.Spec.ImagePullSecrets
		changed = true
	}

	if !apiequality.Semantic.DeepEqual(from.Spec.Template.Spec.DNSConfig, to.Spec.Template.Spec.DNSConfig) {
		log.Debugf("deployments.Apply. Template DNSConfig differs in %s", from.GetName())
		to.Spec.Template.Spec.DNSConfig = from.Spec.Template.Spec.DNSConfig
		changed = true
	}

	if !apiequality.Semantic.DeepEqual(from.Spec.Template.Spec.TopologySpreadConstraints, to.Spec.Template.Spec.TopologySpreadConstraints) {
		log.Debugf("deployments.Apply. Template TopologySpreadConstraints differs in %s", from.GetName())
		to.Spec.Template.Spec.TopologySpreadConstraints = from.Spec.Template.Spec.TopologySpreadConstraints
		changed = true
	}

	if !apiequality.Semantic.DeepEqual(from.Spec.Template.Spec.Volumes, to.Spec.Template.Spec.Volumes) {
		log.Debugf("deployments.Apply. Template volumes differs in %s", from.GetName())
		// content, _ := json.MarshalIndent(from.Spec.Template.Spec.Volumes, "", "  ")
		// fmt.Printf("\n\n------->FROM VOLUMES: %s\n", string(content))
		// content, _ = json.MarshalIndent(to.Spec.Template.Spec.Volumes, "", "  ")
		// fmt.Printf("\n\n------->TO VOLUMES: %s\n", string(content))
		to.Spec.Template.Spec.Volumes = from.Spec.Template.Spec.Volumes
		changed = true
	}

	if !apiequality.Semantic.DeepEqual(from.Spec.Template.Spec.SecurityContext, to.Spec.Template.Spec.SecurityContext) {
		log.Debugf("deployments.Apply. Pod security context differs in %s", from.GetName())
		to.Spec.Template.Spec.SecurityContext = from.Spec.Template.Spec.SecurityContext
		changed = true
	}

	if from.Spec.Template.Spec.PriorityClassName != to.Spec.Template.Spec.PriorityClassName {
		log.Debugf("deployments.Apply. Priority class differ in %s. From %s to %s", from.GetName(), from.Spec.Template.Spec.PriorityClassName, to.Spec.Template.Spec.PriorityClassName)
		changed = true
		to.Spec.Template.Spec.PriorityClassName = from.Spec.Template.Spec.PriorityClassName
	}

	if changed {
		newDeployment, err := Update(client, to)
		return newDeployment, changed, err
	}

	return to, false, nil
}

// DeleteIfMissing deletes a deployment in a cluster if cannot be found in a list of deployments.
// Returns true if the deployment has been deleted and false otherwise
func DeleteIfMissing(
	client clientset.Interface,
	deployment *v1.Deployment,
	expectedList []v1.Deployment,
) (bool, error) {
	if deployment == nil {
		return false, fmt.Errorf("Deployment is nil")
	}
	for _, expectedDeployment := range expectedList {
		if (deployment.GetNamespace() == expectedDeployment.GetNamespace()) &&
			(deployment.GetName() == expectedDeployment.GetName()) {
			return false, nil
		}
	}
	if err := Delete(client, deployment); err != nil {
		return false, err
	}
	return true, nil
}

// DeleteMissing deletes from currentList all Deployments not found in existingList.
// A Deploymentis in existingList if there is an element in that list with the same name
// and namespace.
func DeleteMissing(
	client clientset.Interface,
	currentList []*v1.Deployment,
	expectedList []v1.Deployment,
) (deleted []v1.Deployment, errors []error) {
	deleted = make([]v1.Deployment, 0, len(currentList))
	errors = make([]error, 0, len(currentList))
	for _, current := range currentList {
		if current == nil {
			continue
		}
		del, err := DeleteIfMissing(client, current, expectedList)
		if err != nil {
			errors = append(errors, err)
			continue
		}
		if del {
			deleted = append(deleted, *current)
		}
	}
	return deleted, errors
}
