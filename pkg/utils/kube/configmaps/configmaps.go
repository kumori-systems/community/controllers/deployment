/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package configmaps

import (
	"context"
	"fmt"

	apiequality "k8s.io/apimachinery/pkg/api/equality"

	"k8s.io/apimachinery/pkg/api/errors"

	utils "kuku-controller/pkg/utils"

	log "github.com/sirupsen/logrus"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	labels "k8s.io/apimachinery/pkg/labels"
	clientset "k8s.io/client-go/kubernetes"
	listers "k8s.io/client-go/listers/core/v1"
)

// Create is used to create a new configmap in a Kubernetes cluster using a client-go
// instance.
func Create(
	client clientset.Interface,
	configMap *v1.ConfigMap,
) (*v1.ConfigMap, error) {
	if configMap == nil {
		return nil, fmt.Errorf("Config map is nil")
	}
	log.Debugf("configmaps.Create. ConfigMap: %s", configMap.Name)
	ctx := context.TODO()
	options := metav1.CreateOptions{
		FieldManager: "kumori-kucontroller",
	}
	return client.CoreV1().ConfigMaps(configMap.GetNamespace()).Create(ctx, configMap, options)
}

// Update is used to update an existing configmap in a Kubernetes cluster using a client-go
// instance.
func Update(
	client clientset.Interface,
	configMap *v1.ConfigMap,
) (*v1.ConfigMap, error) {
	if configMap == nil {
		return nil, fmt.Errorf("Config map is nil")
	}
	log.Debugf("configmaps.Update. ConfigMap: %s", configMap.Name)
	ctx := context.TODO()
	options := metav1.UpdateOptions{
		FieldManager: "kumori-kucontroller",
	}
	return client.CoreV1().ConfigMaps(configMap.GetNamespace()).Update(ctx, configMap, options)
}

// Delete is used to delete an existing configmap in a Kubernetes cluster using a client-go
// instance.
func Delete(
	client clientset.Interface,
	configMap *v1.ConfigMap,
) error {
	if configMap == nil {
		return fmt.Errorf("Config map is nil")
	}
	log.Debugf("configmaps.Delete. ConfigMap: %s", configMap.Name)
	ctx := context.TODO()
	policy := metav1.DeletePropagationForeground
	options := metav1.DeleteOptions{
		PropagationPolicy: &policy,
	}
	return client.CoreV1().ConfigMaps(configMap.GetNamespace()).Delete(ctx, configMap.GetName(), options)
}

// Get recovers from the Kumori cluster the current version of a ConfigMap, if any
func Get(
	lister listers.ConfigMapLister,
	configMap *v1.ConfigMap,
) (*v1.ConfigMap, error) {
	if configMap == nil {
		return nil, fmt.Errorf("Config map is nil")
	}
	return lister.ConfigMaps(configMap.GetNamespace()).Get(configMap.GetName())
}

// Get recovers from the Kumori cluster the configmap providing the namespace and name
func GetByName(
	lister listers.ConfigMapLister,
	namespace string,
	name string,
) (*v1.ConfigMap, error) {
	return lister.ConfigMaps(namespace).Get(name)
}

// CreateOrUpdate checks if the given config map exists in the cluster and updates it
// if exists or creates it otherwirse. Returns also a flag indicating if the object has
// been created (true) or updated (false).
func CreateOrUpdate(
	client clientset.Interface,
	lister listers.ConfigMapLister,
	configMap *v1.ConfigMap,
) (*v1.ConfigMap, utils.Operation, error) {
	if configMap == nil {
		return nil, utils.NoneOperation, fmt.Errorf("Config map is nil")
	}
	currentConfigMap, err := Get(lister, configMap)
	if errors.IsNotFound(err) {
		finalConfigMap, err := Create(client, configMap)
		return finalConfigMap, utils.CreatedOperation, err
	} else if err != nil {
		return configMap, utils.NoneOperation, err
	}
	currentConfigMap = currentConfigMap.DeepCopy()
	finalConfigMap, changed, err := Apply(client, configMap, currentConfigMap)
	if changed {
		return finalConfigMap, utils.UpdatedOperation, err
	}

	return finalConfigMap, utils.NoneOperation, err
}

// CreateOrUpdateList creates or updates a list of config maps in the cluster. Returns
// the list of created config maps, the list of updated config maps and the list of errors
// found during the process.
func CreateOrUpdateList(
	client clientset.Interface,
	lister listers.ConfigMapLister,
	configMapList []v1.ConfigMap,
) (created []v1.ConfigMap, updated []v1.ConfigMap, errors []error) {
	listLength := len(configMapList)
	errors = make([]error, 0, 3)
	created = make([]v1.ConfigMap, 0, listLength)
	updated = make([]v1.ConfigMap, 0, listLength)
	if listLength <= 0 {
		return
	}
	for _, configMap := range configMapList {
		newConfigMap, operation, err := CreateOrUpdate(client, lister, &configMap)
		if err != nil {
			errors = append(errors, err)
			continue
		}
		if operation == utils.NoneOperation {
			continue
		}
		if operation == utils.CreatedOperation {
			created = append(created, *newConfigMap)
			continue
		}
		if operation == utils.UpdatedOperation {
			updated = append(updated, *newConfigMap)
			continue
		}

		errors = append(errors, fmt.Errorf("Unexpected applied operation %s for configMap %s. Expected operations are %s or %s", operation, configMap.GetName(), utils.CreatedOperation, utils.UpdatedOperation))
	}
	return
}

// CreateUpdateOrDeleteList receives a list of configMaps and creates all elements in that
// list not currently part of the cluster state, updates those which are part of the cluster
// state but are outdated and deletes those currently existing in the cluster state but not
// in the list
func CreateUpdateOrDeleteList(
	client clientset.Interface,
	lister listers.ConfigMapLister,
	configMapsList []v1.ConfigMap,
	selector labels.Selector,
	namespace string,
) (
	created []v1.ConfigMap,
	updated []v1.ConfigMap,
	deleted []v1.ConfigMap,
	errors []error,
) {
	created, updated, errors = CreateOrUpdateList(client, lister, configMapsList)
	currentConfigMapsList, err := lister.ConfigMaps(namespace).List(selector)
	if err != nil {
		errors = append(errors, err)
		deleted = []v1.ConfigMap{}
		return
	}
	if (currentConfigMapsList != nil) && (len(currentConfigMapsList) > 0) {
		var errs []error
		deleted, errs = DeleteMissing(client, currentConfigMapsList, configMapsList)
		if (errs != nil) && (len(errs) > 0) {
			errors = append(errors, errs...)
		}
	} else {
		deleted = []v1.ConfigMap{}
	}
	return
}

// Apply updates "to" configmap with "from" values if any change is detected. The changes
// are also applied to a kubernetes cluster
func Apply(
	client clientset.Interface,
	from *v1.ConfigMap,
	to *v1.ConfigMap,
) (*v1.ConfigMap, bool, error) {

	if from == nil {
		return to, false, fmt.Errorf("From config map is null")
	}

	if to == nil {
		return to, false, fmt.Errorf("From config map is null")
	}

	changed := false

	if !apiequality.Semantic.DeepEqual(from.OwnerReferences, to.OwnerReferences) {
		log.Debugf("configmaps.Apply. Owner references differ for %s", from.GetName())
		changed = true
		to.OwnerReferences = from.OwnerReferences
	}

	for key, newValue := range (*from).GetLabels() {
		if currentValue, ok := to.Labels[key]; !ok || (currentValue != newValue) {
			log.Debugf("configmaps.Apply. Label %s differ in %s", key, from.GetName())
			changed = true
			to.Labels[key] = newValue
		}
	}

	for key, newValue := range (*from).GetAnnotations() {
		if currentValue, ok := to.Annotations[key]; !ok || (currentValue != newValue) {
			log.Debugf("configmaps.Apply. Annotation %s differ in %s", key, from.GetName())
			changed = true
			if to.Annotations == nil {
				to.Annotations = map[string]string{}
			}
			to.Annotations[key] = newValue
		}
	}

	if !apiequality.Semantic.DeepEqual(from.BinaryData, to.BinaryData) {
		log.Debugf("configmaps.Apply. Binary Data differ in %s", from.GetName())
		// content, _ := json.MarshalIndent(from.BinaryData, "", "  ")
		// fmt.Printf("\n\n------->FROM BINARY DATA: %s", string(content))
		// content, _ = json.MarshalIndent(to.BinaryData, "", "  ")
		// fmt.Printf("\n\n------->TO BINARY DATA: %s", string(content))
		changed = true
		to.BinaryData = from.BinaryData
	}

	if !apiequality.Semantic.DeepEqual(from.Data, to.Data) {
		log.Debugf("configmaps.Apply. Data differ in %s", from.GetName())
		changed = true
		to.Data = from.Data
	}

	if changed {
		newConfigMap, err := Update(client, to)
		return newConfigMap, changed, err
	}

	return to, false, nil
}

// DeleteIfMissing deletes a configMap in a cluster if cannot be found in a list of configMaps.
// Returns true if the configMap has been deleted and false otherwise
func DeleteIfMissing(
	client clientset.Interface,
	configMap *v1.ConfigMap,
	expectedList []v1.ConfigMap,
) (bool, error) {
	if configMap == nil {
		return false, fmt.Errorf("Config map is nil")
	}
	for _, expectedConfigMap := range expectedList {
		if (configMap.GetNamespace() == expectedConfigMap.GetNamespace()) &&
			(configMap.GetName() == expectedConfigMap.GetName()) {
			return false, nil
		}
	}
	if err := Delete(client, configMap); err != nil {
		return false, err
	}
	return true, nil
}

// DeleteMissing deletes from currentList all ConfigMaps not found in existingList.
// A ConfigMapis in existingList if there is an element in that list with the same name
// and namespace.
func DeleteMissing(
	client clientset.Interface,
	currentList []*v1.ConfigMap,
	expectedList []v1.ConfigMap,
) (deleted []v1.ConfigMap, errors []error) {
	deleted = make([]v1.ConfigMap, 0, len(currentList))
	errors = make([]error, 0, len(currentList))
	for _, current := range currentList {
		if current == nil {
			continue
		}
		del, err := DeleteIfMissing(client, current, expectedList)
		if err != nil {
			errors = append(errors, err)
			continue
		}
		if del {
			deleted = append(deleted, *current)
		}
	}
	return deleted, errors
}
