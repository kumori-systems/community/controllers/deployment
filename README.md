# Kmv3Controller

## Table of Contents

* [Introduction](#introduction)
* [Project Structure](#project-structure)
* [CRDs](#crds)
* [Main](#main)
  * [Configuration](#configuration)
* [V3DeploymentController](#v3deploymentcontroller)
* [Historic and Revisions Management](#historic-and-revisions-management)
* [License](#license)

## Introduction

The main goal of this controller is to convert Kumori's V3Deployments in Kubernetes objects processable by the average Kubernetes controllers. Those objects are:

* _Deployments_: one per stateless role declared in the manifest. Currently, a role is considered stateful if it at least one of its duplex or server channels is connected to a complete connector.
* _Stateful Sets_: one per stateful role declared in the manifest.
* _Services_:
  * One headless service per stateful role declared in the manifest. This service is required by Stateful Sets.
  * One headless service per connector declared in the manifest. It is used to manage the list of server channels connected to each connector.
  * One ClusterIP service per load balancer connector declared in the manifest. It is used to balance requests between the server channels linked to each connector.
* _ConfigMap_: one per role. Contains the data of the files exposed to PODs representing roles instances.
* _Network Policies_: enable communication paths between connected roles and services, including intra-service links and services linked to inbounds.
* _Secrets_:
  * One secret per KukuSecret exposed as environment variable or file in a V3Deployment.
  * One secret per KukuSecret used to access a private Docker hub in a V3Deployment.

Hence, _V3Deployments_ are the primary Kubernetes object type for this controller and Kubernetes _Deployments_, _Services_, _Config Maps_, _Network Policies_, _KukuLink_ and _KukuPersistentVolume_ are the secondary objects. So:

* Any event affecting a _V3Deployment_ will fire this controller.
* Any event affecting a _Deployment_, _StatefulSet_, _Service_, _Config Map_ or _Network Policy_ owned by this controller, will fire this controller.
* Any event affecting a _KukuLink_ linking a V3Deployment channel will fire the controller.
* Any event affecting a _KukuSecret_ referenced in a V3Deployment will fire the controller.
* Any event affecting a _KukuPersistentVolume_ assigned to a V3Deployment will fire the controller (not currenly available since persistent volumes are not supported yet).

Internally, each time an event wakes up the controller, the related _V3 Deployment_ is added to a pending queue. When a _V3Deployment_ is popped from that queue, the controller checks if its expected state for that _V3Deployment_ and the current cluster state match. If they don't, the following corrective actions are taken:

* Create all Kubernetes expected objects not found in the cluster state.
* Update all Kubernetes expected if any relevance difference is found between the expected and the current state.
* Remove all kubernetes objects found in the current state but not expected.

All this process is performed in the following three high abstraction steps:

* _Context_ calculation: the _context_ is a memory structure gathering all relevant information about a given _V3Deployment_. Currently it doesn't make much sense this context calculation since the _V3Deployment_ is already a memory structure containing all relevant data. However, the _context_ is still used for historical reasons. This step is performed in `pkg/controllers/deployments/kuku-context.go` file.
* _Expected state_ calculation: calculates the expected Kubernetes objects (deployments, services, stateful sets, config maps, network policies) from the information stored in the _V3Deployment_ context. The `pkg/controllers/deployments/*-generator.go` files are used to generate the expected state.
* _State update_: changes the cluster state comparing the expected and current states for this _V3Deployment_. The `pkg/controllers/deployments/controller.go` will do that itself using the tools in `pkg/utils`.

This controller is based on [base-controller](https://gitlab.com/kumori-systems/community/libraries/base-controller).

## Project Structure

```
.
├── artifacts              // Some controller samples
├── bin                    // The controller binary
├── hack                   // Some utility tools like, for example, a script to run Kubernetes code-generator.
├── cmd                    // The project apps. Currently `controller-manager` is the only one.
│   └── controller-manager // Manages the controller included in this project.
├── manifests              // Sample manifests to deploy this controller
│   └── sample.yaml        // A sample manifest including a Deployment and the ConfigMap to store the controller configuration.
├── go.mod                 // Dependencies
├── pkg
│   ├── controllers        // Contains one folder per controller.
│   │   ├──  deployment    // Listens for events related with V3Deployments
│   │   └──  history       // Utility library imported from kubernetes GitHub and used to manage V3Deployments versions history.
│   └── utils              // Utility functions and structures
│       ├── errors         // Utility library to manage errors in controllers.
│       ├── hash           // Utility library used to calculate hash from an object. Imported from Kubernetes GitHub
│       ├── kube           // Tools to manage kube objects.
│       ├── kukulink       // Tools to manage KukuLinks.
│       └── utils.go       // Generic Tools.
└── README.md              // This file
```

## CRDs

This projects includes a copy of the _V3Deployment_ CRD. The _official_ version is stored in [client-go](https://gitlab.com/kumori-systems/community/libraries/client-go).

We also include a V3Deployment sample in `artifacts/example` folder.

## Main

The `cmd` contains the main function of the KukuController's controller manager. In essence, it gets the cluster configuration and creates one instance of `pkg/controllers/deployment` controller.

The controller manager accepts four parameters:

* kubeconfig (optional): rute to the cluster configuration file. It must be used if
  the controller manager is not executed as a Kubernetes application and you don't want
  to use the default configuration file (if any).
* ns (mandatory): namespace of the objects managed by this controller. The controller
  execution will be aborted if this parameter is missing.
* label (optional): if set, only the objects containing the labels will be managed.
* resync (optional): time the informers will wait between resynchronizations with the cluster.

Example:

```
$ ./bin/manager --namespaces=kumori --kubeconfig=$PWD/config/kubeconfig --label="name=value"
```

### Configuration

The deployment controller configuration parameters are:

* `namespace`: namespace used in kube and kumori informers.
* `label`: labels used to configure kube and kumori informers.
* `kubeconfig`: where to find the kubeconfig.
* `resync`: the interval used to resync the _V3Deployment_ objects (default `0`).
* `defaultStartupPeriod`: how often to perform the deployments probes during startup  (default `10s`).
* `defaultStartupFailureThreshold`: minimum consecutive failures for probes to be considered in startup (default `3`).
* `defaultStartupInitialDelay`: initial delay before initiate the startup probe (default `0s`).
* `defaultStartupTimeout`: time after which the startup probe times out (default `1s`).
* `defaultLivenessPeriod`: how often to perform the deployments liveness probes (default `10s`).
* `defaultLivenessFailureThreshold`: minimum consecutive failures for liveness probes to be considered failed after having succeeded (default `3`).
* `defaultLivenessInitialDelay`: initial delay before initiate the liveness probe (default `0s`).
* `defaultLivenessTimeout`: time after which the liveness probe times out (default `1s`).
* `livenessHighFrequencyPeriod`: how often liveness probes are executed for high values (default `1s`).
* `livenessMediumFrequencyPeriod`: how often liveness probes are executed for medium values (default `10s`).
* `livenessLowFrequencyPeriod`: how often liveness probes are executed for low values (default `60s`).
* `defaultReadinessPeriod`: how often to perform the deployments readiness probes (default `10s`).
* `defaultReadinessFailureThreshold`: minimum consecutive failures for readiness probes to be considered failed after having succeeded (default `3`).
* `defaultReadinessInitialDelay`: initial delay before initiate the readiness probe (default `0s`).
* `defaultReadinessTimeout`: time after which the readiness probe times out (default `1s`).
* `readinessHighFrequencyPeriod`: how often readiness probes are executed for high values (default `1s`).
* `readinessMediumFrequencyPeriod`: how often readiness probes are executed for medium values (default `10s`).
* `readinessLowFrequencyPeriod`: how often readiness probes are executed for low values (default `6s`).
* `enableStartupProbe`: if set to `true`, the _V3Deployment_ objects `startupGraceWindow` will be used to set a startup probe. If set to `false`, the information in `startupGraceWindow` will be used to set the `initialDelaySeconds` attribute in the liveness probe (default `false`).
* `disablePersistentVolumes`: if true the persistent volumes declared in V3Deployments will be ignored (default `true`).
* `forceRebootOnUpdate`: if set to `true`, all mounted files will be processed with the `rebootOnUpdate` flag set to true, no matter the value set in the original manifests. Hence, any change on any file mounted in a role instance will force the instance reboot. This includes the `/kumori/config.json`. (default `false`).
* `logLevel`: minimum severity to show a log line: debug, info, warn, err (default `debug`).
* `usePvcForVolatileVolumes`: if true the volatile volumes will be translated to PVCs using the storageClass indicated in volatileVolumesStorageClass (default `false`).
* `volatileVolumesStorageClass`: the storageClass to be used. Only if usePvcForVolatileVolumes is true (default `""`).
* `topologySpreadConstraintsSoftLabelKeys`: the topology keys used to constraint the role instances spreading. Two nodes are considered as part of the same topology element if they have the same value in the labels indicated in this parameter. Role instances will be distributed among topology elements. The maximum skew defined in topologySpreadConstraintsMaxSkew will be used as a goal but will not forbid creating a new instance if the target skew is violated (default `kubernetes.io/hostname`).
* `topologySpreadConstraintsHardLabelKeys`: the topology keys used to constraint the role instances spreading. Two nodes are considered as part of the same topology element if they have the same value in the labels indicated in this parameter. Role instances will be distributed among topology elements. The maximum skew defined in topologySpreadConstraintsMaxSkew cannot be violated under any circunstances and instances will not be scheduled if there is not a selectable node which also keeps the skew under the maximum (default `kubernetes.io/hostname`).
* `topologySpreadConstraintsMaxSkew`: the maximum difference allowed between the number of instances of a given role running in each element of the topology  (default `0` = disable).
* `userPodsPriorityClass`:  The priority class assigned to pods representing role instances (default `kumori-user`).
* `revisionHistoryLimit`: the maximum number of versions stored per V3Deployment (excluding the live one) (default `10`).
* `defaultDiskRequestSize`: default disk request size for containers. Set to `0` or `-1` to disable disk limitation (default `1`).
* `defaultDiskRequestUnit`: default disk request unit for containers (M, Mi, G, Gi, etc.) (default `Gi`).
* `fsGroup`: fsGroup used in pods security context when a custom user or group is declared for a component container. Allowed values: `disabled` (default value), `fixed` (sets always the value configured in `defaultFsGroup`), `user` (sets the group declared in the user section).
*	`defaultFsGroup`: group ID used when the `fsGroup` configuration parameter is set to `fixed` or `user` and a container component sets a custom user. If `fsGroup` parameter is set to `user`, the default is used only if a custom group ID is not declared in the container user section. If it is set to `fixed`, the default value is always used
* `defaultMappingEnableTemplate`: process mapping elements values (environment variables and files) as go templates if the template flag is not set
* `enableNodeAffinities`: if set to `true`, deployments will include `NodeAffinity` sections to control what nodes the Pods are aligible to run in.
* `hpaEnabledLabelKey`: which key in role metadata declares a role as auto-scaled by the HPA controller. If a role contains that label set to `true`, this role replicas will set only when the underlying _Deployment_ or _StatefulSet_ is created but will never be updated because the number of that role instances is supposed to be automatically calibrated by HPA. The value represents a path. For example, `hpa.enabled` means that kucontroller will look for the key `enabled` inside the `hpa` object in the role metadata (`{"hpa":{"enabled": true}}`).

If HPA is enabled, the kucontroller will set the role replicas only at creation time but will never update it. If empty, the kucontroller will always set the number of replicas at creation and update times for all roles. The '.' in the label name determines the label path in the metadata JSON document. Default: hpa.enabled ('enabled' key in 'hpa' section)")


Those configuration parameters can be set in command line (for example: `./bin/manager --namespaces=kumori --kubeconfig=$PWD/config/kubeconfig --label="name=value`) or in a configuration file in `/kumori/config.yaml`. This file will have an entry per parameter to be set. For example (from `manifests/sample.yaml`):

```yaml
apiVersion: v1
data:
  config.yaml: |
    defaultStartupPeriod: 10s
    defaultStartupFailureThreshold: 3
    defaultLivenessPeriod: 10s
    defaultLivenessFailureThreshold: 3
kind: ConfigMap
metadata:
  name: kucontroller-config
  namespace: kumori
```

The precedence order is:

* Command line parameters.
* `/kumori/config.yaml` parameters.
* Default values.

When a liveness probe is defined for a component container in a _V3Deployment_ object, a `startupGraceWinwow` is provided to declare how much time the component needs to startup and avoid running the liveness probe before the container has actually started. This window can be declared either:

* providing the number of attempts. In this case, the kucontroller will use `defaultStarupPeriod` as the wait time beteween attempts.
* providing the maximmum amount of millisencods it requires to start. In that case, `defaultStartupPeriod` to calculate the number of attempts to be made.

## V3DeploymentController

The Controller Manager main file can be found in `cmd/controller-manager/main`. A controller manager can run several controllers, all of them sharing the same Kubernetes objects cache, among other things. In this case, a single controller is included to convert `kind: V3Deployment` custom resources defined by Kumori into a running service in the cluster.

As other controllers, the _V3Deployment_ controller has a primary object type, which is the `kind: V3Deployment`, and several secondary object types, which usually include the types of Kubernetes core objects created by the controller to run the service (through the kubernetes core controllers).

In this case, the _V3Controller_ is interested in:

* _V3Deployment_ create or update events. Delete events are not relevant since the delete cascade process will remove all related kubernetes objects.
* _Deployment_, _Stateful Set_, _Service_, _Config Map_, _Secret_ and _Network Policy_ create, update or delete events. Actually, the controller will be interested on those events only if the event related object is owned by a _V3Deployment_.
* _KukuLink_ create, update or delete events, only if it links a _V3Deployment_. Eventually, all links will point to _V3Deployments_ but currently this controller will still coexist with the deprecated _KukuDeployment_ controller.
* _KukuSecret_ create, update or delete events but only it is referenced from a _V3Deployment_. A Secret can be exposed as a file or environment variable or used to access a private Docker Hub.
* _KukuPersistentVolume_ create, update or delete events, only if their are assigned to a _V3Deployment_.

When one of those events occur, the controller retrieves and enqueues the associated _V3Deployment_ (see `HandleSecondary` funciton). For each element in that queue, the `SyncHandler` function is called. This funcion compares de _V3Deployment_ expected and current state and takes the necessary corrective actions, if any. This is done in several steps:

* Create a `KukuContext`. The `KukuContext` struct contains all relevant data for a given _V3Deployment_. In previous versions, the required information was scattered here and there and this step was made to easy the process of calculating the expected state. However, currently all information already comes in the _V3Deployment_ and, most probably, this step is not necessary. However, right now, the context construction is still a necessary step.
* Calculate the expected state. This state is composed by kubernetes deployments, stateful sets, services, config maps, secrets and network policies.
* Compare the expected state with the current state and take corrective action. A coarse grain comparison cannot be used here since other Kubernetes controllers add information to the `spec` sections of the kubernetes objects created by this controller. So, a fine grained comparison is required in some cases to avoid false positives. If any difference is found, the controller will add, update or delete the necessary objects to fit the expected and current state for the _V3Deployment_ being processed.

Thos steps are detailed in the following subsections

### KukuContext

The KukuContext is a _struct_ filled with all data required to process a _V3Deployment_. The KukuContext structure contains:

* `Deployment`: the original _V3Deployment manifest_.
* `ServiceInfo`: information about the service being deployed which includes:
  * `Name`: the service name.
  * `ChannelsInfo`: information about the service channels. The structure of this object is described below.
* `Owners`: an Owner Reference to the _V3Deployment_.
* `Namespace`: the namespace of the _V3Deployment_.
* `Name`: the _V3Deployment_ name.
* `ConnectorsInfo`: information about the service topology. Is an object storing a `ConnectorTagInfo` per connector and tag in the topology. It includes:
  * `Name`: the connector-tag pair name.
  * `ShortName`: a short name used in some places with strong restrictions in names length and format.
  * `ConnectorName`: the name of the connector.
  * `TagName`: the name of the tag
  * `Port`: the port used to access the connector. Currently, always the `80`.
  * `Type`: the connector type: `loadbalancer` or `full`
  * `Servers`: which channels can receive connections through this connector-tag pair.
  * `Clients`: which channels can start a connection through this connector-tag pairl
* `ReleaseName`: the name of this release. Currently, also the _V3Deployment_ name.
* `RolesInfo`: information about this service roles and their configuration. It includes:
  * `ChannelsInfo`: the channels declared in this role.
  * `RoleSize`: information about the role instances (minimum, maximum, current and resilience). The minimum should be at least the resilience plus one unless if resilience is `0`. In that case, the minimum should be at least `0`. If the current number of instances is `0` and the minimum and resilience are also `0`, no instance will be created.
  * `ComponentName`: the name of the component assigned to this role.
  * `Name`: the role name.
  * `Stateful`: if the role is stateful (`true`) or not (`false`).
  * `ContainersInfo`: information about the containers created for each of this role instances. For each container:
    * `Name`: the container name.
    * `Image`: the Docker image used to create the container.
    * `Secrets`: the `SecretsInfo` storing the secret needed to access an image if it os stored in a private Docker hub.
    * `SecretKey`: which secret in the `SecretsInfo` object stores the needed secret to access a private Docker Hub.
    * `Cmd`: command to be executed when the container is created. If not specified, the command declared in the image will be used.
    * `Args`: the command arguments. If not specified, the arguments declared in the image will be used.
    * `User`: the user and group id used when the container command is launched. Optional.
    * `Files`: the files to be mounted in the container, if any. Includes the file name, path, content and permissions.
    * `Envs`: the environment variables to be exposed, if any. Includes the name and value.
    * `Volumes`: the volumes to be mounted in the container. Includes the name, path and resourceto be mounted. Currently, only volatile volumes are allowed.
    * `Size`: the resources assigned to this container (CPU, RAM and so on).
* `Links`: the links affecting this _V3Deployment_. Not tested yet in this controller.
* `KukuLinkTools`: the tool to be used to access the links.
* `KumoriOwner`: a text representing the Kumori user owning this _V3Deployment_.

The `ChannelInfo` struct has the following attributes:

* `Name`: the channel name.
* `Type`: `client`, `server` or `duplex`.
* `Protocol`: `http` or `tcp`.
* `Port`: the port bound by this channel (only for `server` and `duplex` channels).
* `Address`: where this channels connects. Only for `client` and `duplex`. Probably not needed since, currently, the channel names can be used for that.
* `RoleInConnector`: the real role of this channel from the connector point of view. This is used for service channels. If a service channel is declared as `server`, its role is `client` from the connector point of view.

A context can be created calling the `CreateKukuContext` function. Internally, it initializes the context and fills it calling its `load` method, which loads all source manifests (the V3Deployment and KukuLinks, currently), calculates the aggregated information and generates the data stored in the context.

As a result, `CreateKukuContext` returns a filled `KukuContext` and an error if something goes wrong. If this error is a `KukuError`, it will include a `Severity` field, which is later used by the controller to decide if the _V3Deployment_ processing must be aborted (the severity is greather than `minor`) and retried (if the severity is not `fatal`).

### Updating the cluster state

The `pkg/controllers/deployment/controller.go` is in charge of getting the expected objects and updating the cluster state if any relevant deviation is found. This process is executed per kubernetes object type and in the following order:

* Kubernetes _Service_ objects related to connectors and tags. This is done first due to historical reason. Previously, some changes on the containers name resoultion mecanism whas added based on the information provided by those connectors. That is not needed anymore.
* Kubernetes _Network Policies_ objects used to enable the declared communication paths. For security reasons, PODs will not be created unless the communication restrictions are properly set up.
* Kubernetes _Config Maps_ objects for roles. Their content is used to mount the required filenames and that's why they must be added befor creating the PODs.
* Kubernetes _Secrets_ objects to expose secrets in role's instances as files or environment variables. They are also used when a given role container image is stored in a private Docker hub.
* Kubernetes _Stateful Sets_ and _Deployments_.
* Kubernetes _Service_ objects related to stateful roles. Those are required by the _Stateful Set_ objects.

The controller creates one headless _Service_ object per connector and tag. That will add an entry in the cluster DNS (`CONNECTOR_NAME>-<TAG_NAME>-<DEPLOYMENT_NAME>`) resolved to the SRV records representing the reachable Pods and channels connected to that connector as servers (server or duplex channels). If the connector is a load balancer, an extra Cluster IP _Service_ is added which will balance the requests betwen the available targets. For this connector, the DNS entry will be named as `CONNECTOR_NAME>-<TAG_NAME>-<DEPLOYMENT_NAME>-lb`.

The _Service_ objects representing connector-tags pairs are created as _services without selector_. So, they not include the `spec.selector` section and, hence, the underlying Kubernetes controller will not create the required _Endpoints_ objects to keep track of the reachable Pods and ports. We created another controller, the [Topology Controller](https://gitlab.com/kumori-systems/community/controllers/topology), which in charge of this task.

The controller includes methods to create and update each one of the Kubernetes types used in the previous list:

* Methods to create the expected objects from the context: `newConnectorServices`, `newStatefulSets`, `newNetworkPolicies`, `newDeployments`, `newSecrets`. Note that the _Config Maps_ and the _Services_ related to roles are created together in `newStatefulSets` and `newDeployments`. Those methods do not create the objects but theirselves but delegate that operation to the _generators_. A _generator_ is a struct with a function _Generate_ returning instances of Kubernetes objects (or an error). There is a go file for each Kubernetes object type:
  * `pkg/controllers/deployment/configmap-generator.go`.
  * `pkg/controllers/deployment/deployment-generator.go`. Internally also uses `pkg/controllers/deployment/pod-template-generator.go`.
  * `pkg/controllers/deployment/networkpolicy-generator.go`.
  * `pkg/controllers/deployment/secret-generator.go`.
  * `pkg/controllers/deployment/service-generator.go`.
  * `pkg/controllers/deployment/statefulset-generator.go`. Internally also uses `pkg/controllers/deployment/pod-template-generator.go`.
* Methods to update the cluster state if any difference is found with the expected state. Those methods are `updateServices`, `updateNetworkPolicies`, `updateConfigMaps`, `updateSecrets`, `updateStatefulSets` and `updateDeployments`. Internally, those methods use the tools stored in `pkg/utils/kube`. There is one tool per Kubernetes object type. Resuming, each tool contains utility methods for CRUD operations over a given Kuberentes object type.

### Environment variables exposed in role instances

For each role instance container the following environment variables are set:

* `KUMORI_INSTANCE_ID`: contains the name of that instance (POD).
* `KUMORI_DEPLOYMENT`: contains the deployment name generated by the platform (`kd-XXXXXX-XXXXXXXX`).
* `KUMORI_ROLE`: contains the name of the role (example: `worker000`). Recall that the role name is the name used in the original CUE plus a number representing the tag number.
* `KUMORI_FILES_HASH`: **for internal use only**. Is a has created using the content of the parametrs and secrets exposed as variables and flagged as `RebootOnUpdate` (that is, reboot the POD if the content of the file changes because the software reads the file content only when starts).
* `KUMORI_ENV_HASH`: **for internal use only**. This hash is calculated with the content of the secrets exposed as environment variables. In this case, if the secret changes the Kubernetes machinery will not reboot the affected PODs, which will keep the original value. To force a rolling update, we use this environment variabele.

### The `/kumori/config.json` file

In each POD container, a `/kumori/config.json` file is exposed containing information about the instance. Currently, only explains which tags are available per channel. For example:

```json
{
  "channels": {
    "restapiclient": {
      "0": [
        {
          "auto": {
            "compRef": {
              "domain": "kumori.systems.examples",
              "kind": "component",
              "name": "calccachecache",
              "version": [
                0,
                0,
                1
              ]
            },
            "roleName": "cache"
          },
          "user": {
            "usecache": true
          }
        }
      ],
      "1": [
        {
          "auto": {
            "compRef": {
              "domain": "kumori.systems.examples",
              "kind": "component",
              "name": "calccacheworker",
              "version": [
                0,
                0,
                1
              ]
            },
            "roleName": "worker"
          },
          "user": {
            "usecache": false
          }
        }
      ]
    }
  }
}
```

Per each channel and tag, the following information is included:

* Which component is assigned to that tag and the connected role.
* The labels assigned to that tags when the service was deployed.

### Managing File Updates

In a V3Deployment, files can be exposed in role containers. For example:

```yaml
apiVersion: kumori.systems/v1
kind: V3Deployment
metadata:
  name: kd-180701-f4089bdc
  namespace: kumori
  ...
spec:
  description:
    ...
    service:
      ...
      description:
        ...
        role:
          ...
          frontend000:
            ...
            component:
              ...
              description:
                ...
                code:
                  ...
                  frontend:
                    ...
                    name: frontend
                    mapping:
                      ...
                      filesystem:
                      - data:
                          param_one: myparam_one
                          param_two: 234
                        format: json
                        mode: 420
                        path: /config/config.json
                      - mode: 420
                        path: /config/secret.json
                        rebootOnUpdate: true
                        secret: jb.kumori.systems/secretjson
```

The file content can be explicitly provided (like in `/config/config.json`) or referenced from a _KukuSecret_ (like in `/config/secret.json`). By default, when that file content is updated (the `data` section changes in the _V3Deployment_ o r the referenced _kukuSecret_ content changes), the changes are propagated to the exposed files without rebooting the underlying PODs. However, sometimes the software being executed in the affected container is not warching for changes in those files and, hence, it has to be rebooted to load the updated information. If that's the case, the `rebootOnUpdate` flag must be added to that file declaration (see `/config/secret.json` file in the previous example).

When the `rebootOnUpdate` flag is added to at least one container file, under the hoods a new `KUMORI_FILES_HASH` environment variable is included for that container, which contains a hash calculated using the content of the files with that flag set to `true`. If one of those files content changes, the hash will change and so will do the `KUMORI_FILES_HASH` value, which will trigger the rolling update procedure for the instances of the role declaring that container.

### Liveness Probes

When a new service is deployed and a liveness probe is configured, the underlying kubernetes _Deployment_ or _StatefulSet_ will include:

* A Liveness probe with the required startup period, threshold and initial delay.
* A Readiness probe with exactly the same values used in the Liveness probe.

The readiness probe is used to ensure a given instance won't be available before the defined startup period.

For example, the following liveness probe in a component CUE manifest:

```
probe: liveness: httpd: {
  port: 80,
  attributes: {
    protocol: "http"
    attributes: path: "/"
    startupGraceWindow: {
      unit: "ms",
      duration: 60000
    }
  }
}
```

Will generate the following probes in the _Deployment_ related to this component:

```yaml
livenessProbe:
  failureThreshold: 3
  httpGet:
    path: /
    port: 80
    scheme: HTTP
  initialDelaySeconds: 60
  periodSeconds: 6
  successThreshold: 1
  timeoutSeconds: 5
readinessProbe:
  failureThreshold: 3
  httpGet:
    path: /
    port: 80
    scheme: HTTP
  initialDelaySeconds: 60
  periodSeconds: 6
  successThreshold: 1
  timeoutSeconds: 5
```

## Historic and Revisions Management

The controller keeps track of the different revisions for each V3Deployment. When a V3Deployment manifest is processed, the controller checks if changes the current revision or not. If that is the case, it might be a rollback to a previous version or a new one. If it is a new version, the controller will create a new _ControllerRevision_ object with the following content:

* `data`: contains the `spec` included in the V3Deployment manifest being processed.
* `metadata`:
  * `annotations`: will contain the annotations included in the V3Deployment manifest plus:
    * `kumori.systens.history`: contains a stringified JSON array. Each time this revision is set as the current revision, a new entry is added to this array. The entry is a JSON document containing:
      * `timestamp`: the moment (in seconds from 01-01-1970) the revision was set.
      * `comment`: the comment included when the revison was set.
      * `revision`: the revision number generated when this revision was set.
  * `labels`: will contain the labels included in the V3Deployment manifest.
  * `ownerReferences`: will contain a resource of the V3Deployment manifest.
* `revision`: the revision number, which is the last revision number plus one.

For example:

```yaml
apiVersion: apps/v1
data:
  spec:
    ref:
      domain: mydomain
      kind: deployment
      name: mydeployment2
      version: null
    description:
      ...
kind: ControllerRevision
metadata:
  annotations:
    kumori.systems/history: '[{"timestamp":1600682466,"comment":"Initial version","revision":1}]'
    comment: ...
    manifest: '...'
    sha: ...
    ...
  labels:
    app: ...
    domain: ...
    kumoriOwner: ...
    name: ...
    version: ...
    ...
  name: kd-091539-4cfb3eb5-74c745f55b
  namespace: kumori
  ownerReferences:
  - apiVersion: kumori.systems/v1
    blockOwnerDeletion: true
    controller: true
    kind: V3Deployment
    name: kd-091539-4cfb3eb5
    uid: 0dffafb6-dc0b-4ab5-94b8-6982a6f6f662
revision: 1
```

If it is a rollback, the related _ControllerRevision_ version is loaded and its `revision` updated to the last number plus one.

The V3Deployment status is also updated including:

* `observedGeneration`: the most recent `generation` observed for this V3Deployment.
* `currentRevision`: the revision created after this version of the manifest.
* `collisionCount`: used to avoid name collisions when a ControllerRevision is created. This number is increased when a _ControllerRevison_ is found with the same name. The name combines the V3Deployment name and a hash calculated using the _ControllerRevision_ data and collision count.

For example:

```yaml
apiVersion: kumori.systems/v1
kind: V3Deployment
metadata:
  ...
spec:
  ...
status:
  collisionCount: 0
  currentRevision: kd-132241-3484a0fe-5dd9855c
  observedGeneration: 3
```

### Enabling Status Subresource

The _V3Deployment_ status should be exposed as a subresource ([Status Subresource](https://book-v1.book.kubebuilder.io/basics/status_subresource.htm)):

```yaml
apiVersion: apiextensions.k8s.io/v1beta1
kind: CustomResourceDefinition
metadata:
  name: v3deployments.kumori.systems
spec:
  ...
  versions:
    - name: v1
      ...
      subresources:
        status: {}
```

We need this to enable status updates using a `PUT` request to `/status`. This will ignore changes to any other section of the _V3Dedployment_. Furthermore, an update operation over the whole custom resource will ignore changes in the `status` section also ([subresources](https://kubernetes.io/docs/tasks/extend-kubernetes/custom-resources/custom-resource-definitions/#subresources)).

To update a V3Deployment status the `UpdateStatus` function is used:

```go
kukuContext.Deployment.Status = status
_, err = c.kumoriClientset.KumoriV1().V3Deployments(namespace).UpdateStatus(kukuContext.Deployment)
if err != nil {
  return err
}
```

## Inter-service links

In KukuContext, for each connector:

* For each client channel:
** Calculates the endpoint: this switches service channels type
** The endpoint is added to the list of the connector client channels.
** If this endpoint is a service channels, gets the endpoints from other V3Deployments reachable through the links connected to this service channel. Those channels are added to the list of linked clients avoiding duplicates and removing an endpoint if it is in the connectors client list.
* For each server tag
** For each server channel in this tag
*** Calculates the endpoint: this switches channels type
*** Adds the endpoint to the list of server channels. If the endpoint is a duplex channel, it is also added to the list of client channels.
*** If this endpoint is a service channels, gets the endpoints from other V3Deployments reachable through the links connected to this service channel. Those channels are added to the list of linked servers avoiding duplicates and removing an endpoint if it is in the connectors client list. If any of those channels is a duplex channel, it is also added to the list of linked client channels.

How the linked channels are calculated. The process starts with a provided endpoint which is added to a list of endpoints pending of being processed. To "process" an endpoint means to find which other endpoints are reachable through links connecting this endpoints. The process is a loop consisting on poping endpoints from the pending list until the list is empty. For each popped endpoint:
* It is added to the list of processed endpoints.
* Gets the v3deployments linked to this endpoint. Each element in the list contains a V3Deployment, the channel linked and the kukulink linking both endpoints.
* For each of those linked deployments:
** An endpoint is calculated with this linked endpoint. Service endpoints type is switched.
** finds out which connector of the linked deployment is linked to this channel.
** If the linked channel is a client channel
*** For each connected server channel. If the linked endpoint is a duplex channel, then only the server channels of the same tag are considered.
**** Calculates the endpoint information, switching the type if it is a service channel. The endpoint includes the link name.
**** If the endpoint is not a service channel, adds it to the list of endpoints to be returned.
**** If the endpoint is a service channel and it is not in the processed or pending of being processed list, then it is added to the pending list.
** If the linked channel is a server channel or a duplex channel
*** For each client channel
**** Calculates the endpoint information, switching the type if it is a services channel.
**** If the linked channel is not a service channels, adds it to the lisf of endpoints to be returned. The endpoint includes the link name.
**** If it is a service channel and is not in the list of processed endpoints or in the list of pending endpoints, it is added to the list of pending endpoints.

When the connector _Service_ objects are calculated, for each link appearing in linked channels, a label is added using the link name as a key ane `link-in-use` as a value. The same happens with the network policies.

## License

Copyright 2022 Kumori Systems S.L.

Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
You may not use this work except in compliance with the Licence.
You may obtain a copy of the Licence at:

https://joinup.ec.europa.eu/software/page/eupl

Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the Licence for the specific language governing permissions and limitations under the Licence.

pkg/controllers/history: Apache License 2.0 © The Kubernetes Authors
pkg/utils/hash: Apache License 2.0 © The Kubernetes Authors
