/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package main

import (
	"flag"
	"fmt"
	"io"
	"os"
	"os/user"
	"path"
	"path/filepath"
	"time"

	kumoriclientset "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated/clientset/versioned"
	kumoriinformers "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated/informers/externalversions"

	deployment "kuku-controller/pkg/controllers/deployment"

	stopsignal "gitlab.com/kumori-systems/community/libraries/base-controller/pkg/stop-signal"

	log "github.com/sirupsen/logrus"
	pflag "github.com/spf13/pflag"
	viper "github.com/spf13/viper"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	kubeinformers "k8s.io/client-go/informers"
	kubeclientset "k8s.io/client-go/kubernetes"
	rest "k8s.io/client-go/rest"
	clientcmd "k8s.io/client-go/tools/clientcmd"
)

// How many workers will be launched
const threadiness int = 1

// Command-line args (see init function)
var ns string
var labelselector string
var kubeconfig string
var resyncInterval time.Duration

// init reads command-line args (init function is executed when package is imported)
func init() {
	meth := "main.init"
	var writers io.Writer
	dir, err := filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		fmt.Printf("%s. Error getting kukucontroller path: %s\n", meth, err.Error())
		writers = io.MultiWriter(os.Stdout)
	} else {
		logfilepath := path.Join(dir, "kukucontroller.log")
		file, err := os.OpenFile(logfilepath, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666)
		if err != nil {
			fmt.Printf("%s. Error creating log file: %s\n", meth, err.Error())
			writers = io.MultiWriter(os.Stdout)
		} else {
			writers = io.MultiWriter(os.Stdout, file)
		}
	}
	log.SetFormatter(&log.TextFormatter{
		ForceColors:   true,
		FullTimestamp: true,
	})
	log.SetLevel(log.InfoLevel)
	log.SetOutput(writers)
	log.Debug(meth)
	flag.String("namespace", "",
		"Namespace to take into acount. All, if not specified.")
	flag.String("label", "",
		"'LabelName=LabelValue' to take into acount. All, if not specified.")
	flag.String("kubeconfig", "",
		"Paths to a kubeconfig. Only required if out-of-cluster.")
	flag.Duration("resync", 0,
		"Resync interval (5s, 1m...). Never, if not specified or 0.")
	flag.String("defaultStartupPeriod", "10s", "How often to perform the deployments probes during startup.")
	flag.Int("defaultStartupFailureThreshold", 3, "Minimum consecutive failures for probes to be considered in startup.")
	flag.String("defaultStartupInitialDelay", "0s", "Initial delay before initiate the startup probe")
	flag.String("defaultStartupTimeout", "1s", "Time after which the startup probe times out (if enabled)")
	flag.String("defaultLivenessPeriod", "10s", "How often to perform the deployments liveness probes.")
	flag.Int("defaultLivenessFailureThreshold", 3, "Minimum consecutive failures for liveness probes to be considered failed after having succeeded.")
	flag.String("defaultLivenessInitialDelay", "0s", "Initial delay before initiate the liveness probe")
	flag.String("defaultLivenessTimeout", "1s", "Time after which the liveness probe times out")
	flag.String("livenessHighFrequencyPeriod", "1s", "How often liveness probes are executed for high values")
	flag.String("livenessMediumFrequencyPeriod", "10s", "How often liveness probes are executed for medium values")
	flag.String("livenessLowFrequencyPeriod", "60s", "How often liveness probes are executed for low values")
	flag.String("defaultReadinessPeriod", "10s", "How often to perform the deployments readiness probes.")
	flag.Int("defaultReadinessFailureThreshold", 3, "Minimum consecutive failures for readiness probes to be considered failed after having succeeded.")
	flag.String("defaultReadinessInitialDelay", "0s", "Initial delay before initiate the readiness probe")
	flag.String("defaultReadinessTimeout", "1s", "Time after which the readiness probe times out")
	flag.String("readinessHighFrequencyPeriod", "1s", "How often readiness probes are executed for high values")
	flag.String("readinessMediumFrequencyPeriod", "10s", "How often readiness probes are executed for medium values")
	flag.String("readinessLowFrequencyPeriod", "60s", "How often readiness probes are executed for low values")
	flag.Bool("enableStartupProbe", false, "Use Kubernetes startup probes to represent V3Deployment startupGateWindow. If it is set to false the initialDelaySeconds of the liveness probe will be used instead.")
	flag.Bool("disablePersistentVolumes", true, "If true the persistent volumes declared in V3Deployments will be ignored")
	flag.Bool("forceRebootOnUpdate", false, "If set to true, a change in any mounted file will force a pod reboot.")
	flag.String("logLevel", "debug", "Minimum severity to show a log line: debug, info (default), warn, err")
	flag.Bool("usePvcForVolatileVolumes", false, "If true the volatile volumes will be translated to PVCs using the volume type indicated in defaultVolatileVolumesType")
	flag.String("defaultVolatileVolumesType", "volatile", "The volume type used by default to create volatile volumes. Only if usePvcForVolatileVolumes is true")
	flag.String("topologySpreadConstraintsSoftLabelKeys", "kubernetes.io/hostname", "The topology keys used in soft constraint rules applied to spread role instances. Two nodes are considered as part of the same topology element if they have the same value in the label with the key indicated in this parameters. Role instances will be distributed among topology elements. The maximum skew defined in topologySpreadConstraintsMaxSkew will be used as a goal but will not forbid creating a new instance if the target skew is violated.")
	flag.String("topologySpreadConstraintsHardLabelKeys", "", "The topology keys used in hard constraint rules applied to spread role instances. Two nodes are considered as part of the same topology element if they have the same value in the label with the key indicated in this parameters. Role instances will be distributed among topology elements. The maximum skew defined in topologySpreadConstraintsMaxSkew cannot be violated under any circunstances and instances will not be scheduled if there is not a selectable node which also keeps the skew under the maximum.")
	flag.Int("topologySpreadConstraintsMaxSkew", 0, "The maximum difference allowed between the number of instances of a given role running in each element of the topology (0 to disable)")
	flag.String("userPodsPriorityClass", "kumori-user", "The priority class assigned to pods representing role instances")
	flag.Int("revisionHistoryLimit", 10, "The maximum number of versions stored per V3Deployment (excluding the live one)")
	flag.Int("defaultDiskRequestSize", 1, "Default disk request size for containers")
	flag.String("defaultDiskRequestUnit", "Gi", "Default disk request unit for containers (M, Mi, G, Gi, etc.)")
	flag.String("fsGroup", "disabled", "fsGroup used in pods security context when a custom user or group is declared for a component container. Allowed values: disabled (default value), fixed (sets always the value configured in defaultFsGroup), user (sets the group declared in the user section)")
	flag.Int64("defaultFsGroup", 6666, "The default group ID used when the fsGroup configuration parameter is set to fixed or user and a container component sets a custom user. If it is set to user, the defaultFsGroup will be used only if a custom group ID is not declared in the container user section. If it is set to fixed the default user is always used")
	flag.Bool("defaultMappingEnableTemplate", true, "Process mapping elements values (environment variables and files) as go templates if the template flag is not set")
	flag.Bool("enableNodeAffinities", true, "Include NodeAffinities in the deployments to control where pods are scheduled.")
	flag.String("hpaEnabledLabelKey", "hpa.enabled", "Which key in role metadata declares a role as auto-scaled by the HPA controller. If HPA is enabled, the kucontroller will set the role replicas only at creation time but will never update it. If empty, the kucontroller will always set the number of replicas at creation and update times for all roles. The '.' in the label name determines the label path in the metadata JSON document. Default: hpa.enabled ('enabled' key in 'hpa' section)")
	flag.String("clusterConfigMapName", "cluster-config", "Name of the configmap storing the cluster configuration")
	flag.String("clusterConfigMapNamespace", "kumori", "Namespace of the configmap storing the cluster configuration")
	flag.String("clusterConfigMapKey", "cluster-config.yaml", "Key storing the cluster configuration in the ConfigMap described in clusterConfigMapName property")

	pflag.CommandLine.AddGoFlagSet(flag.CommandLine)
	pflag.Parse()

	viper.SetConfigName("config")
	viper.SetConfigType("yaml")
	viper.AddConfigPath("/kumori")
	viper.BindPFlags(pflag.CommandLine)

	if err := viper.ReadInConfig(); err != nil {
		if _, ok := err.(viper.ConfigFileNotFoundError); ok {
			log.Warnf("%s. Config file '/kumori/config.json' not found", meth)
		} else {
			log.Warnf("%s. Error reading configuration file in '/kumori/config.json': %s", meth, err.Error())
		}
	}

	deployment.UpdateLogLevel()

	ns = viper.GetString("namespace")
	labelselector = viper.GetString("label")
	kubeconfig = viper.GetString("kubeconfig")
	resyncInterval = viper.GetDuration("resync")
}

// getConfig loads a REST Config, depending of the environment and flags:
// Config precedence:
// * --kubeconfig flag pointing at a file
// * KUBECONFIG environment variable pointing at a file
// * In-cluster config if running in cluster
// * $HOME/.kube/config if exists (default location in the user's home directory)
func getConfig() (config *rest.Config, err error) {
	log.Debug("main.getConfig")
	if len(kubeconfig) > 0 {
		log.Debug("main.getConfig. Kubeconfig flag found: ", kubeconfig)
		return clientcmd.BuildConfigFromFlags("", kubeconfig)
	}
	if len(os.Getenv("KUBECONFIG")) > 0 {
		log.Debug("main.getConfig. KUBECONFIG environment variable found: ", os.Getenv("KUBECONFIG"))
		return clientcmd.BuildConfigFromFlags("", os.Getenv("KUBECONFIG"))
	}
	if c, err := rest.InClusterConfig(); err == nil {
		fmt.Println("main.getConfig. In cluster config")
		return c, nil
	}
	if usr, err := user.Current(); err == nil {
		c, err := clientcmd.BuildConfigFromFlags("", filepath.Join(usr.HomeDir, ".kube", "config"))
		if err == nil {
			return c, nil
		}
	}
	return nil, fmt.Errorf("could not locate a kubeconfig")
}

// setQPSBurst applies saner defaults for QPS and burst based on the Kubernetes
// controller manager defaults (20 QPS, 30 burst)
// These 2 flags set normal and burst rate that controller can talk to
// kube-apiserver.
// Default values work pretty well (20 for QPS and 30 for burst).
// Increase these values for large, production clusters.
func setQPSBurst(config *rest.Config) {
	if config.QPS == 0.0 {
		config.QPS = 20.0
		config.Burst = 30.0
	}
}

// main is the entrypoint of the controller
func main() {
	log.Info("main.main() Running kuku-controller...")

	// Set up signals so we handle the shutdown signal gracefully
	stopCh := stopsignal.SetupStopSignal()
	errorCh := make(chan error)

	flag.Parse()
	if ns == "" {
		log.Fatal("main.main() Namespace command-line arg is mandatory")
	}

	config, err := getConfig()
	if err != nil {
		log.Fatal("main.main() Error getting Kubernetes config: ", err)
	}
	setQPSBurst(config)

	// Create a clientset and informer-factory for kubernetes standard objects
	// - Clientset is built as an abstraction above a RESTClient, and exposes
	//   versioned API resources and their serializers.
	//   Clientset contains the clients for groups, and each group has exactly
	//   one version included in a Clientset.
	// - SharedInformer provfmt.Printlnides eventually consistent linkage of its
	//   clients to the authfmt.Printlnoritative state of a given collection of
	//   objects.  An object is identified by its API group, kind/resource,
	//   namespace, and name.  One SharedInfomer provides linkage to objects
	//   of a particular API group and kind/resource.  The linked object
	//   collection of a SharedInformer may be further restricted to one
	//   namespace and/or by label selector and/or field selector.
	//   Informers will “sync” periodically`(resyncInterval parameter), they will
	//   deliver every matching object in the cluster to your Update method.
	//   This is good for cases where you may need to take additional action on
	//   the object, but sometimes you know there won't be more work to do.
	kubeClientset, err := kubeclientset.NewForConfig(config)
	if err != nil {
		log.Fatal("main.main() Error building kubernetes clientset: ", err)
	}
	kubeInformerFactory := kubeinformers.NewFilteredSharedInformerFactory(
		kubeClientset,
		resyncInterval,
		ns,
		func(opt *metav1.ListOptions) {
			if labelselector != "" {
				opt.LabelSelector = labelselector
			}
		},
	)

	// Create a clientset and informer-factory for KukuComponent objects
	kumoriClientset, err := kumoriclientset.NewForConfig(config)
	if err != nil {
		log.Fatal("main.main() Error creating the crd clientset: ", err)
	}
	kumoriInformerFactory := kumoriinformers.NewFilteredSharedInformerFactory(
		kumoriClientset,
		resyncInterval,
		ns,
		func(opt *metav1.ListOptions) {
			if labelselector != "" {
				opt.LabelSelector = labelselector
			}
		},
	)

	// Create the controller that manage KukuDeployment objects, but Deployment objects
	// too.
	// kubeClientset kubeclientset.Interface,
	// kumoriClientset kumoriclientset.Interface,
	// deploymentInformer kubeinformers.DeploymentInformer,
	// statefulSetInformer kubeinformers.StatefulSetInformer,
	// configMapInformer coreinformers.ConfigMapInformer,
	// serviceInformer coreinformers.ServiceInformer,
	// networkPolicyInformer netinformers.NetworkPolicyInformer,
	// v3DeploymentInformer kumoriinformers.V3DeploymentInformer,
	// kukuLinkInformer kumoriinformers.KukuLinkInformer,
	// kukuHTTPInboundInformer kumoriinformers.KukuHttpInboundInformer,
	deploymentController := deployment.NewController(
		kubeClientset,
		kumoriClientset,
		kubeInformerFactory.Apps().V1().Deployments(),
		kubeInformerFactory.Apps().V1().StatefulSets(),
		kubeInformerFactory.Core().V1().ConfigMaps(),
		kubeInformerFactory.Core().V1().Services(),
		kubeInformerFactory.Networking().V1().NetworkPolicies(),
		kubeInformerFactory.Apps().V1().ControllerRevisions(),
		kubeInformerFactory.Core().V1().Secrets(),
		kubeInformerFactory.Policy().V1().PodDisruptionBudgets(),
		kumoriInformerFactory.Kumori().V1().V3Deployments(),
		kumoriInformerFactory.Kumori().V1().KukuLinks(),
		kumoriInformerFactory.Kumori().V1().KukuSecrets(),
		kumoriInformerFactory.Kumori().V1().KukuVolumes(),
		kumoriInformerFactory.Kumori().V1().KukuCas(),
		kumoriInformerFactory.Kumori().V1().KukuCerts(),
		kumoriInformerFactory.Kumori().V1().KukuDomains(),
		kumoriInformerFactory.Kumori().V1().KukuPorts(),
	)

	// Start informers
	// Notice that there is no need to run Start methods in a separate goroutine.
	// Start method is non-blocking and runs all registered informers in a
	// dedicated goroutine.
	kubeInformerFactory.Start(stopCh)
	kumoriInformerFactory.Start(stopCh)

	// Start component controller
	go deploymentController.Run(threadiness, stopCh, errorCh)

	select {
	case err := <-errorCh:
		log.Fatal("main.main() Error running controller", err)
	case <-stopCh:
		log.Info("main.main() Closing kuku-controller")
	}

	// // Wait application closed
	// <-stopCh
	// log.Info("main.main() Closing kuku-controller")
}
