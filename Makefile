# Copyright 2022 Kumori Systems S.L.
#
# Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
# You may not use this work except in compliance with the Licence.
# You may obtain a copy of the Licence at:
#
# https://joinup.ec.europa.eu/software/page/eupl
#
# Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the Licence for the specific language governing permissions and limitations under the Licence.

# Image URL to use all building/pushing image targets
# WARNING: This version number is also used by CI to tag generated docker images

# Get the currently used golang install path (in GOPATH/bin, unless GOBIN is set)
ifeq (,$(shell go env GOBIN))
GOBIN=$(shell go env GOPATH)/bin
else
GOBIN=$(shell go env GOBIN)
endif

# Run go fmt against code
fmt:
	go fmt ./...

# Run go vet against code
vet:
	go vet ./...

# Build manager binary
manager: fmt vet
	go build -o bin/manager cmd/controller-manager/main.go

# Run against the configured Kubernetes cluster in ~/.kube/config
run: manager
	bin/manager --namespace kumori --resync 5m

# Run unit tests
test-deployment: fmt vet
	go test ./pkg/controllers/deployment/... -count=1 -v

# Run unit tests
test: test-deployment

############################
# DEVELOPMENT ONLY TARGETS #
############################

VERSION ?= 0.0.999
IMG ?= docker.io/kumoridev/kucontroller:${VERSION}

# Build the docker image
#
# IMPORTANT: check Dockerfile first (some actions required before)
#
docker-build:
	docker build . -t ${IMG}

# Push the docker image
docker-push:
	docker login
	docker push ${IMG}
	docker logout

# Push the docker image to a development cluster
docker-push-dev:
	kind load docker-image $IMG $IMG --name devtesting
